#ifndef util_celdas_h
#define util_celdas_h

struct Celda{
  int dato;
  Celda *sig;
};

void Add(Celda *&lista, const int &numero);
void Liberar (Celda *&lista);
void Listar (Celda *lista, std::ostream &os);
int Size(Celda *lista);
Celda *Buscar(Celda *&lista, int dato);
Celda **BuscarPuntero(Celda *& lista, int dato);
Celda *Descolgar(Celda **puntero);

#endif
