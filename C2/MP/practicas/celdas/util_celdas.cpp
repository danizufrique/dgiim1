#include <iostream>
#include <cstdlib> // rand, atoi
#include <ctime> // time
#include "util_celdas.h"
using namespace std;

// Genera un valor del intervalo [minimo,maximo]
/*int Uniforme(int minimo, int maximo)
{
  double u01= std::rand()/(RAND_MAX+1.0); // Uniforme01
  return minimo+(maximo-minimo+1)*u01;
}*/

void Add(Celda *&lista, const int &numero){
    Celda *aux = new Celda;
    aux -> dato = numero;
    aux -> sig = lista;
    lista = aux;
}

void Liberar (Celda *&lista){
   Celda *p;
   while (lista != 0){
     p = lista -> sig;
     delete lista;
     lista = p;  
   }
}

void Listar (Celda *lista, std::ostream &os){
   Celda *p = lista;
   os << "{";
   while (p != 0){	
	os << p -> dato;
	p= p -> sig;
	if (p != 0)
	  os << ",";
   }
   os << "}"<< endl;
}


int Size(Celda *lista){
   int i=0;
   if (lista !=0){
     Celda *p = lista;
     while (p != 0){	
	i++;
	p= p -> sig;
     }
   }
   return i;
}

Celda *Buscar(Celda *&lista, int dato){
  Celda *aux= lista;
  bool encontrado = false;
  while( (aux != 0) && (!encontrado) ){
    if (aux -> dato == dato)
	encontrado = true;
    else
	aux = aux -> sig;
  }
  if (!encontrado)
	aux = 0;
  return aux;
}
Celda **BuscarPuntero(Celda *& lista, int dato){
    Celda **aux = &lista;
    bool encontrado = false;
    while ( ((*aux)->sig != 0) && !encontrado) {
      if ( (*aux) -> dato == dato)
	encontrado = true;
      else
	aux = &((*aux)->sig);
    }
   return aux;
}

Celda *Descolgar(Celda **puntero){
   Celda *p = *puntero;
   *puntero = (*puntero)->sig;
   p -> sig =0;
   return p;
}
