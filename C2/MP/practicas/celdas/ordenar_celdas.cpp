#include <iostream>
#include <cstdlib> // rand, atoi
#include <ctime> // time
// FIXME 1: Crear módulo util_celdas y añadir la cabecera aquí
#include "util_celdas.h"
using namespace std;

// Genera un valor del intervalo [minimo,maximo]
int Uniforme(int minimo, int maximo){
  double u01= std::rand()/(RAND_MAX+1.0); // Uniforme01
  return minimo+(maximo-minimo+1)*u01;
}


// FIXME 2: Crear funciones BuscarMaximo y OrdenarSeleccion
Celda **BuscarMaximo(Celda *& lista){
   Celda **aux = &lista, **aux_return = &lista;
   int maximo = lista->dato;
   bool margen_correcto = true;
   while (margen_correcto){
     if ( (*aux)->sig == 0 )
	margen_correcto = false;
     if (margen_correcto){
        if ( (((*aux)->sig)->dato) >= maximo ){
	   maximo = ((*aux)->sig)->dato;
	   aux_return = &((*aux)->sig);
	}
	aux = &((*aux)->sig); 
     }
   }   
   return aux_return;
}

void OrdenaSeleccion(Celda *&lista){
  Celda **aux = &lista;
  Celda *p, *ordenado, *aux2;
  if (Size(lista) > 1){
     aux = BuscarMaximo (lista);
     ordenado = Descolgar(aux);
     ordenado->sig = 0;
     aux2 = ordenado;
     while (lista->sig != 0){
	aux = BuscarMaximo(lista);
	p = Descolgar(aux);
	p->sig = ordenado;	
	ordenado = p;	
     }
    lista = ordenado;
  }
}

// FIXME 3: Función Mezcla que mezcla dos listas ordenadas en una nueva, dejando las dos originales vacías

Celda *Mezclar(Celda *&lista1, Celda *&lista2){
    Celda *fin, **fin_eje, *p_aux, **pp_aux;
    bool queda1, queda2;

    if ( (lista1->dato) < (lista2->dato) )  pp_aux = &lista1;
    else  pp_aux = &lista2;

    fin = Descolgar(pp_aux);
    fin_eje = &(fin->sig);
    if (lista1 != 0)  
		queda1=true;
    else	
		queda1=false;

    if (lista2 != 0)  
	 	queda2=true;
    else
		queda2=false;

    fin_eje = &(fin->sig);   
    while( (queda1 || queda2)){
		cout << queda1 << ","<< queda2<<endl;
      if (!queda1)
		      pp_aux = &lista2;
      else if (!queda2)
			   pp_aux = &lista1;
		else{
			if (lista1->dato < lista2->dato)  
		      pp_aux = &lista1;
			else
				pp_aux = &lista2;
		}

      *fin_eje = Descolgar(pp_aux);
      fin_eje = &( (*fin_eje)->sig );   
      *fin_eje =0;   
      if (lista1 == 0)
			queda1=false; 
      if (lista2 == 0) 
			queda2=false; 
    }

   return fin;
}



void MergeSort(Celda *&lista1){
    Celda *lista2 = lista1, **aux;
    int elementos = Size(lista1);
    if (elementos > 1){
       for(int i=0; i< elementos/2; i++){
	      aux = &(lista2->sig);
	      lista2= lista2->sig;
       }	
	   lista2 = *aux;
	   *aux = 0; 
      MergeSort(lista1);
      MergeSort(lista2);
	   lista1 = Mezclar (lista1, lista2);
    }
}
    


void OrdenarEspecial(Celda *&l){
  Celda *vec[32]={0};
  Celda **tope=&(vec[0]);
  while (l) {
    Celda *acarreo= l;
    l= l->sig;
    acarreo->sig=0;
    Celda **aux=&(vec[0]);
      while(*aux!=0) {
	acarreo= Mezclar(acarreo,*aux);
	aux++;
//	aux = &((*aux)->sig);
      }
      *aux= acarreo;
      if (aux==tope)
        ++tope;
  }
    for (Celda **p=&(vec[0]);p!=tope;++p) {
      l= Mezclar(l,*p);
    }
}
int main(int argc, char *argv[])
{
  if (argc!=3) {
    cerr << "Uso: " << argv[0] << " <número de datos> <máximo dato>" <<endl;
    return 1;
  }
  
  srand(time(0)); // Inicializamos generador de números
  
  int n= atoi(argv[1]);
  if (n<5) {
    cerr << "Debería especificar al menos 5 elementos" << endl;
    return 2;
  }
  else {
    int max= atoi(argv[2]);
    if (max<5) {
      cerr << "Debería especificar un max de al menos 5" << endl;
      return 3;
    }
    
    Celda *lista1=0, *lista2=0; // Listas vacías

    for (int i=0;i<n;++i) {
      // Añadimos un elemento al principio de lista1 y otro a lista2
      Add(lista1,Uniforme(1,max));
      Add(lista2,Uniforme(1,max));
    }
    
    cout << "Lista1: ";
    Listar(lista1, cout);
    cout << "Lista2: ";
    Listar(lista2, cout);
    cout << "------------------" << endl;
    Celda **pp1, **pp2;
    Celda *p1, *p2;

    // FIXME 2: Llamar a ordenar las dos listas y listarlas ordenadas
    pp1 = BuscarMaximo(lista1);
    pp2 = BuscarMaximo(lista2);

    MergeSort(lista1);
//  OrdenaSeleccion(lista1);
    OrdenaSeleccion(lista2);

    cout << "Lista1 ordenada (mergesort): "<<endl;
    Listar(lista1, cout);
    cout << "Lista2 ordenada (selección): "<<endl;
    Listar(lista2, cout);
    // FIXME 4: Ordenar la primera lista con MergeSort

    // FIXME 3: Llamar a mezclar las dos listas y guardar el resultado en la lista1
    Celda *p= Mezclar(lista1, lista2);
    cout << "Lista mezcla: ";
    Listar(p, cout);
    cout << endl;

    Liberar(lista1);
    Liberar(lista2);
  }  
}
