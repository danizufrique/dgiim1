#include <iostream>
#include <cstdlib> // rand, atoi
#include <ctime> // time
using namespace std;

struct Celda {
  int dato;
  Celda *sig;
};

// Genera un valor del intervalo [minimo,maximo]
int Uniforme(int minimo, int maximo)
{
  double u01= std::rand()/(RAND_MAX+1.0); // Uniforme01
  return minimo+(maximo-minimo+1)*u01;
}


// FIXME 1: Añadir un elemento a una lista. Se añade como primer elemento
void Add(Celda *&lista, const int &numero){
    Celda *aux = new Celda;
    aux -> dato = numero;
    aux -> sig = lista;
    lista = aux;
}

// FIXME 2: Liberar todas las celdas de una lista y dejarla a cero
void Liberar (Celda *&lista){
   Celda *p;
   while (lista != 0){
     p = lista -> sig;
     delete lista;
     lista = p;  
   }

}

// FIXME 3: Listar los elementos de una lista entre llaves y separados por comas
void Listar (Celda *lista, std::ostream &os){
   Celda *p = lista;
   os << "{";
   while (p != 0){	
	os << p -> dato;
	p= p -> sig;
	if (p != 0)
	  os << ",";
   }
   os << "}"<< endl;

}
// FIXME 4: Crear función Size para calcular el número de elemento de la lista
int Size(Celda *lista){
   int i=0;
   if (lista !=0){
     Celda *p = lista;
     while (p != 0){	
	i++;
	p= p -> sig;
     }
   }
   return i;
}
// FIXME 5: Crear una función Buscar que devuelva la celda donde está un dato. Puntero nulo si no está.
Celda *Buscar(Celda *&lista, int dato){
  Celda *aux= lista;
  bool encontrado = false;
  while( (aux != 0) && (!encontrado) ){
    if (aux -> dato == dato)
	encontrado = true;
    else
	aux = aux -> sig;
  }
  if (!encontrado)
	aux = 0;
  return aux;
}

// FIXME 6: Crear una función BuscarPuntero que devuelve un puntero a puntero.
Celda **BuscarPuntero(Celda *& lista, int dato){
    Celda **aux = &lista;
    bool encontrado = false;
    while ( ((*aux)->sig != 0) && !encontrado) {
      if ( (*aux) -> dato == dato)
	encontrado = true;
      else
	aux = &((*aux)->sig);
    }
   return aux;
}

// FIXME 7: Recibe un puntero a celda, la desengancha, y la devuelve como resultado
Celda *Descolgar(Celda **&puntero){
   Celda *p = *puntero;
   if ( (*puntero)->sig != 0)
      *puntero = (*puntero)->sig;
   else
      *puntero = 0;
   return p;
}

// FIXME 8: Implementar Begin, Next, End
Celda** Begin(Celda *&lista){
   Celda **l= &lista;
   return l;
}

Celda** End(Celda *&lista){
   Celda **l= &lista;
   while ( ((*l)->sig) != 0)
      l =  &((*l)->sig);
   l =  &((*l)->sig);
   return l;
}

Celda** Next(Celda **&l){
   return &((*l)->sig);
}


// FIXME 9: Añada una función Imprimir que recibe un rango e imprime todos los elementos del rango
void Imprimir(Celda **&begin, Celda **&end, std::ostream& os){
     os << "{";
     os << ((*begin)->dato);
     while( ((*begin)->sig) != *end){
        begin = Next(begin);
        os << ","<<((*begin)->dato);
     }
	os << "}"<<endl;
   
}



int main(int argc, char *argv[])
{
  if (argc!=3) {
    cerr << "Uso: " << argv[0] << " <número de datos> <máximo dato>" <<endl;
    return 1;
  }
  
  srand(time(0)); // Inicializamos generador de números
  
  int n= atoi(argv[1]);
  if (n<1) {
    cerr << "Debería especificar al menos 5 elementos" << endl;
    return 2;
  }
  else {
    int max= atoi(argv[2]);
    if (max<5) {
      cerr << "Debería especificar un max de al menos 5" << endl;
      return 3;
    }
    
    Celda *lista1=0, *lista2=0; // Listas vacías

    for (int i=0;i<n;++i) 
      Add(lista1,Uniforme(1,max)); // Añadimos un elemento al principio de la lista
      
  
    // FIXME 3: Llamar a listar los elementos
      cout << "Lista: ";
      Listar (lista1, cout);

    // FIXME 4: Indicar el tamaño de la lista
    int tamanio = Size(lista1);
    cout << "La lista contiene " << tamanio << " elementos.\n";  
    // FIXME 5: Pregunte un dato a buscar e indique si está en la lista
    int dato;
    Celda *p;
    cout << "Introduzca un dato a buscar: ";
    cin >> dato;
    p = Buscar(lista1, dato);
    cout << "Buscar celda: ";
    if (p==0)
	cout << "El dato no está en la lista.\n";
    else{
       int contador = 1;
       Celda *aux= lista1;
       while(aux != p){
	   contador++;
     	   aux = aux -> sig;
       }
        cout << contador << endl;
    }
    // FIXME 6: Buscar el mismo dato con un puntero a puntero en cada lista
//    int dato;
//    Celda *p;

      Celda **pp = BuscarPuntero(lista1, dato);
    cout << "Buscar puntero: ";
    if ( (*pp) ==0 )
	cout << "El dato no está en la lista.\n";
    else{
       int contador = 1;
       Celda **aux= &lista1;
       while( (*aux)->dato != (*pp)->dato ){
	   contador++;
           aux = &((*aux)->sig);
       }
        cout << contador << endl;
    }
    // FIXME 7: Mientras que exista el dato, lo desengancha, lo destruye y busca el siguiente
     bool margen_correcto = true;
     pp = BuscarPuntero(lista1, dato);
       while( margen_correcto && (*pp)->dato == dato ){
	   p = Descolgar(pp);
	   delete p;
	   if (*pp == 0)	
		margen_correcto = false;
	   
	   if (margen_correcto)
                pp = BuscarPuntero( *pp, dato);
       }


    cout << "Sin ese dato: "; 
      Listar (lista1, cout);

    // FIXME 9: Listado repetido pero con Imprimir
        Celda **begin = Begin(lista1);
	Celda **end = End(lista1);

        cout << "Listado del rango total: ";
	Imprimir (begin, end, cout);

    
    

    
    // FIXME 2: Llamar a liberar los elementos de la lista
     Liberar(lista1);
  }  
}
