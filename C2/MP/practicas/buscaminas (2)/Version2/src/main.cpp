#include <iostream>
#include <fstream>
#include <cstdlib>
#include <time.h>
#include <cstring>
#include "buscaminas.h"
using namespace std;

int main(int argc, char* argv[]){
   CampoMinas mapa(0,0,0);

   srand(time(NULL));
   if (argc == 2){
      if ( !mapa.Leer(argv[1]) ){
         cout << "No se ha podido leer el archivo.\n";
         return 1;
      }
      if ( !Valido(mapa.Filas(), mapa.Columnas(), mapa.Minas()) ){
         cout << "Número de minas incorrecto.\n";
         return 1;
      }
   }
   else if (argc == 4){
      if ( Valido(atoi(argv[1]), atoi(argv[2]), atoi(argv[3])) )
         mapa = CampoMinas (atoi(argv[1]), atoi(argv[2]), atoi(argv[3])); // Constructor explícito.
      else
         return 1;
   }
   else{
      cout << "Uso:" << endl;
      cout << "   " << argv[0] << " <archivo_lectura>" << endl;
      cout << "   " << argv[0] << " <filas> <columnas> <minas>" << endl;
      return 1;
   }

   mapa.PrettyPrint();
   //mapa.RevelarTablero();

   mapa.Jugar();
   mapa.RevelarTablero();

	if (mapa.Ganado())
		cout << "¡¡Has ganado!! ( ͡ᵔ ͜ʖ ͡ᵔ )\n";
	else
		cout << "¡¡Has perdido!! ¯\\_(ツ)_/¯\n";
cout << endl;
}
