var classTablero =
[
    [ "Tablero", "classTablero.html#ab4912f28f1db392e1dd44ddc98bd4f59", null ],
    [ "Tablero", "classTablero.html#afa05c8303d648fa4632270e3ec2d7bee", null ],
    [ "~Tablero", "classTablero.html#a7d4a64967ce0bbe2cce49ec846834c84", null ],
    [ "Abrir", "classTablero.html#a0efe2ffae3e298b31310596914286380", null ],
    [ "CambiarMarca", "classTablero.html#a365028062f4a2a058dae338fb78043e2", null ],
    [ "Columnas", "classTablero.html#a8d33a67ac66029b26c1c290b58d284dc", null ],
    [ "Elemento", "classTablero.html#ad1c959991f75d5eea1de63e63dd47a18", null ],
    [ "Filas", "classTablero.html#a6e4bfc7d270e0ae36a1122d45a8a1350", null ],
    [ "Inicializar", "classTablero.html#a7fd62db4145c5e4f45f9628be529c3e0", null ],
    [ "InsertarMinas", "classTablero.html#a296a3351f0d293b27baf5247ef29d0a1", null ],
    [ "Minas", "classTablero.html#a39ecd9b8c0a7eaec87a722d68619c36f", null ],
    [ "Modificar", "classTablero.html#ad650ba8d5e54cda8001c26a4b0bd7634", null ],
    [ "operator()", "classTablero.html#a81dfa806c1e755b9873399ff900a5481", null ],
    [ "operator()", "classTablero.html#aa1da206ff2f13e345582e43851d60d0f", null ],
    [ "operator=", "classTablero.html#a53a18ac0f3e7f8f1705e15ae73959226", null ],
    [ "PosicionCorrecta", "classTablero.html#a315a7ce0cf8b1c1ad91579a7cba91daf", null ],
    [ "columnas", "classTablero.html#a3cc992b15708e90f0c12ec46184fe868", null ],
    [ "datos", "classTablero.html#a96114b8d4625d637836d97b70329d765", null ],
    [ "filas", "classTablero.html#a3d1469f371a89231a73c549a6044b06b", null ]
];