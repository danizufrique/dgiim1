var classCampoMinas =
[
    [ "CampoMinas", "classCampoMinas.html#a7f7e1b40b466c6fbc2c199263a56fac6", null ],
    [ "Abrir", "classCampoMinas.html#a6f19002160958b2b9139823dd81661f2", null ],
    [ "Columnas", "classCampoMinas.html#abf1476c3a36e9187437213decb3f16a5", null ],
    [ "Escribir", "classCampoMinas.html#a899af2c783c04cb2d69e90fc53bc050f", null ],
    [ "Explosionado", "classCampoMinas.html#a4bb432ade55186b8c2a8e180f9d5e673", null ],
    [ "Filas", "classCampoMinas.html#a04872b772ef455fe50c975946a45e02b", null ],
    [ "Ganado", "classCampoMinas.html#a0c351da5978c250a24b365b59aad9ef9", null ],
    [ "ImprimeCasilla", "classCampoMinas.html#a347fe4ec845de03010ae6902c872cefe", null ],
    [ "Jugar", "classCampoMinas.html#a8cbabd5621fe0fea16fab803dae38f24", null ],
    [ "Leer", "classCampoMinas.html#a7a6ff81f1b36d0df4fdcd8282307258a", null ],
    [ "Marcar", "classCampoMinas.html#a97719db3465f7a125c481aa41a73b077", null ],
    [ "Minas", "classCampoMinas.html#a3996598b2263d89e1ee3cca8ab1ab4f8", null ],
    [ "MinasProximas", "classCampoMinas.html#af73f5219e62e4e8a4fac651a8bd3b112", null ],
    [ "PrettyPrint", "classCampoMinas.html#a212e96c643e89dd04cd99925a3fba07f", null ],
    [ "PulsarBoton", "classCampoMinas.html#a76509607cd3ee1a3cf8882a5f68fe399", null ],
    [ "RevelarTablero", "classCampoMinas.html#a4b395d4da12f4030cac9131fe11b9bb6", null ],
    [ "explosion", "classCampoMinas.html#ada7b99854edb14ab295516965279a0f7", null ],
    [ "tab", "classCampoMinas.html#a70d2d2cff670e5c54a16f7e2bc7a8209", null ]
];