#include <iostream>
#include <cstdlib> // rand, atoi
#include <ctime> // time
#include "mostrar.cpp"

using namespace std;

// Genera un valor del intervalo [minimo,maximo]
int Uniforme(int minimo, int maximo)
{
  double u01= std::rand()/(RAND_MAX+1.0); // Uniforme01
  return minimo+(maximo-minimo+1)*u01;
}

// FIXME 1: Rellena el vector con n enteros del 1 a max y los mezcla (ver main)
void Generar(int* &v, int tamanio, int max){
   int par1, par2, aux;
   v = new int[tamanio];
   for (int i=0; i<tamanio; i++)
     v[i] = Uniforme(0, max);

   for (int i=0; i< 30; i++){
      par1 = Uniforme(0, tamanio-1);
      par2 = Uniforme(0, tamanio-1);
	aux = v[par1];
	v[par1] = v[par2]; 
	v[par2] = aux;
   }
}



// FIXME 4: Cambio de interfaz, devolviendo n si no está.
int Buscar(const int *v, int n, int dato)
{
  for (int i=0;i<n;++i)
    if (v[i]==dato)
      return i;
  return n;
}

// FIXME 5: Implementar búsqueda garantizada. Pre: el dato está
// FIXME 7: Cambiar la interfaz de la función
int* BuscarGarantizada(int *inicial, int dato)
{
  int i=0;
  while (true){
    if (*(inicial+i)==dato){
      int *p = inicial+i; 
      return p;    
    }
    i++;
  }
}


// FIXME 8: Incluir algoritmo OrdenarInsercion
void OrdenaInsercion(int v[], int n){
   int aux;
   bool seguir;
   int j=0;
   if (n != 1){
    for (int i=1; i<n; i++){
      aux = v[i];
       for(j=i;j>0 && aux < v[j-1]; j--){
         v[j] = v[j-1];	 
       } 
         v[j] = aux;
    }
   }
}
// FIXME 9: Incluir búsqueda binaria recursiva

// FIXME 10: Modificar función para implementar interpolación
int BusquedaBinariaRec(const int *vec, int n, int dato)
{
   int izq=0, der= n-1;
   
   while (izq<=der) {
     int centro= izq + (dato-vec[izq])*(der-izq)/(vec[der]-vec[izq]);
     if (vec[centro]>dato)
       der= centro-1;
     else if (vec[centro]<dato)
            izq= centro+1;
          else return centro;
   }
   return -1; 
}


int main(int argc, char *argv[])
{
  if (argc!=3) {
    cerr << "Uso: " << argv[0] << " <número de datos> <máximo dato>" <<endl;
    return 1;
  }
  
  srand(time(0)); // Inicializamos generador de números

  int n= atoi(argv[1]);

  if (n<5) {
    cerr << "Debería especificar al menos 5 elementos" << endl;
    return 2;
  }
  else {
    int *v;

    // FIXME 1: prepara el vector v
//    Generar( v, n, atoi(argv[2]) );

    // FIXME 6: reservar una posición más para garantizar la búsqueda
    int max = atoi(argv[2]);
    Generar(v,n+1,max);
    cout << "Generados: ";
    for (int i=0;i<n;++i)
      cout << v[i] << " "; 
    cout << endl;
    
    // FIXME 8: Ordenar el vector con OrdenarInsercion y listarlos
     OrdenaInsercion(v,n);
     cout << "Ordenados: ";
    for (int i=0;i<n;++i)
      cout << v[i] << " "; 
    cout << endl;
    
    // FIXME 2: Pregunta por dato a buscar y lo localiza
    int dato;
    cout << "Introduzca un dato a buscar: ";
    cin >> dato;  
//    cout << "Encontrado en: " << Buscar(v, atoi(argv[1]), dato) << endl;   

    // FIXME 3: Modificación para localizar todas las ocurrencias
    // El programa fallaba porque nunca cambia pos.
/*    int pos= Buscar(v,n,dato);
    int indice=pos;
    bool booleano = true;

    while(booleano){
      cout << " Encontrado en: " << indice << endl;	
      pos= Buscar(&(v[indice+1]), n-indice-1, dato);
      if (pos == n-indice-1)
	booleano = false;
      indice += pos+1;
    }    
*/
    // FIXME 6: Colocar el dato en la posición detrás de la última
	v[n] = dato;
    
    // FIXME 7: Cambiar a la nueva interfaz de la búsqueda
     int *pos= BuscarGarantizada(v,dato);
     int indice= pos-v;
     bool booleano = true;
	if (pos == (v+n))
	   booleano = false;
     while(booleano){
       cout << " Encontrado en: " << indice << endl;	
       pos= BuscarGarantizada((v+indice+1), dato);
       indice = pos - v;
	if (pos == (v+n) )
	  booleano = false;
     }
    
    // FIXME 9: Preguntar por búsqueda binaria y resolver la posición
    cout << "Introduzca un dato a buscar binario: ";
    cin >> dato;
    if (dato > v[n-1])
	cout << "Encontrado en: -1\n";
    else{ 
      indice = BusquedaBinariaRec(v,n,dato);
      cout << " Encontrado en: " << indice << endl;
    }
    // FIXME 1: deja de usarse v
    delete [] v;
  }
}
  
  
