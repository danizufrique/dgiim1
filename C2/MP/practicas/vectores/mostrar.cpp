#include <iostream>
#include <fstream> // ifstream
#include <string.h>
using namespace std;

// FIXME 5: añadir un nuevo campo
struct VecDin {
  int *datos;
  int usados;
  int reservados;
};

// FIXME 1: Redimensions v para que se quede con "nuevo" elementos
// FIXME 5: tener en cuenta un nuevo campo en v
void ReSize(VecDin& v,int nuevo)
{
   
    if (nuevo < v.usados){	
	v.usados = nuevo;
/*	Primera implementación.
       for (int i=0; i< nuevo; i++)
  	 p[i] = v.datos[i];*/
    }
    else if (nuevo > v.usados){
       if (nuevo <= v.reservados)
	  v.usados = nuevo;	
       else{
	       int *p = new int[nuevo];
	       for (int i=0; i< v.usados; i++)
	  	 p[i] = v.datos[i];
	       for (int i=v.usados; i< nuevo; i++)
	  	 p[i] = 0;	
	      delete [] v.datos;
	      v.reservados = nuevo;
	      v.datos = p;
       }
    }
    else
	cout << "ReSize con mismo número de elementos.\n";
}

// FIXME 2: Lee objetos int hasta final de flujo y devuelve VecDin con los datos (usa ReSize)
VecDin LeerVecDin(std::istream& flujo)
{
    int i=1;
    int nuevo;
    VecDin v={0,0,0}; // = new VecDin;
    while( flujo.peek() != EOF){
	flujo >> nuevo;
	ReSize(v, i);
	v.usados++;
	v.datos[i-1] = nuevo;

	i++;
    }
    ReSize(v,v.usados-1);
    return v;
}

// FIXME 3: Muestra en un flujo de salida los datos enteros de un VecDin (ver main)

void Mostrar(const VecDin& v, std::ostream& os){
   for (int i=0; i< v.usados; i++)
	os << v.datos[i] << " ";
   os << endl;
}

// FIXME 3: Libera la memoria reservada en un VecDin (ver main)

void Liberar(VecDin &v){
    delete [] v.datos;
}


// FIXME 4: Añade un dato al final del vector
// FIXME 6: Mejora de eficiencia con reservados
void Add(VecDin& v,int dato)
{
   if (v.usados < v.reservados)
      ReSize(v,v.usados+1);
   else if (v.usados == v.reservados){
	ReSize(v, v.reservados*2);
	v.usados = v.usados+1;
   }
      v.datos[v.usados -1] = dato;
}

// FIXME 7: Ordena v con "Selección" implementado recursivo
void SeleccionRecursivo(int *v, int n)
{  
    if (n !=1){
      int aux;
      int minimo= 0;
      for (int i=0; i<n; i++){
  	if(v[i] < v[minimo])
  	   minimo=i;	
      }
	aux = v[0];
	v[0] = v[minimo];
	v[minimo] = aux;

       SeleccionRecursivo(&(v[1]), n-1);
   
    }
}

// FIXME 7: Usa SeleccionRecursivo para ordenar v
void Ordenar(VecDin& v)
{
	SeleccionRecursivo(v.datos, v.usados);
}

/*
int main(int argc, char *argv[])
{
  VecDin v= {0,0,0};
  
  if (argc==1)
    v= LeerVecDin(cin);
  else if (argc==2){
    if ( strcmp(argv[1], "-s") == 0 ){
	v= LeerVecDin(cin);
	Ordenar(v);
    }
    else{
      ifstream f(argv[1]);
      if ( !f ) {
        cerr << "Error: Fichero " << argv[1] << " no válido." << endl;
        return 1;
      }
      v= LeerVecDin(f);
    }
  }
  else if (argc==3){
    ifstream f(argv[2]);
    if (!f) {
      cerr << "Error: Fichero " << argv[1] << " no válido." << endl;
      return 1;
    }
    v= LeerVecDin(f);
    if(strcmp(argv[1],"-s") == 0 ){
	Ordenar(v);
    }

  }

  Mostrar(v,cout);
  Liberar(v); // Libera la memoria reservada

}  */
