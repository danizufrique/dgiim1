#include <iostream>
#include <string.h>
#include "imagenES.h"
#include "codificar.h"
using namespace std;

int main(){
	const int MAXBUFFER= 1000000;
	const int MAXNOMBRE= 100;
	char nombre[MAXNOMBRE], otro_nombre[MAXNOMBRE]="Imagen_codificada";
	TipoImagen t;
	unsigned char imagen[MAXBUFFER];
	int filas, columnas;
	char mensaje_a_codificar[MAXBUFFER/8];

	cout << "Escriba el nombre de la imagen: \n";
	cin.getline (nombre, 100);

	cout << "Ahora introduzca el mensaje a codificar: \n";
	cin.getline (mensaje_a_codificar, 1000);
	

	t= LeerTipoImagen (nombre, filas, columnas);

	if (t==IMG_DESCONOCIDO) 		//Imagen desconocida.
		cout << "No se reconoce la imagen.\n";

	else if (t==IMG_PGM){ 			//Imagen PGM: grises.
		LeerImagenPGM (nombre, filas, columnas, imagen);
		InsertaCadena (mensaje_a_codificar, imagen);
		cout << "Codificando...\n";
		EscribirImagenPGM (otro_nombre, imagen, filas, columnas);	
	}

	else if (t==IMG_PPM){ 			//Imagen PPM: 
		LeerImagenPPM (nombre, filas, columnas, imagen);
		InsertaCadena (mensaje_a_codificar, imagen);
		cout << "Codificando...\n";
		EscribirImagenPPM (otro_nombre, imagen, filas, columnas);	
	}

cout << '\n';	
}
