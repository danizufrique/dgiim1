#include <iostream>
#include <string.h>
#include "imagenES.h"
#include "codificar.h"
using namespace std;

int main(){
	const int MAXBUFFER= 1000000, MAXNOMBRE= 100;
	char nombre[MAXNOMBRE];
	unsigned char imagen[MAXBUFFER], frase[MAXBUFFER/8];
	TipoImagen t;
	int filas, columnas;	
	int contador=0;
	
	cout << "Escriba el nombre de la imagen que quiere revelar: \n";
	cin.getline(nombre, 100);

	t= LeerTipoImagen (nombre, filas, columnas);

	if (t==IMG_DESCONOCIDO) 		//Imagen desconocida.
		cout << "No se reconoce la imagen.\n";

	else if (t==IMG_PGM){ 			//Imagen PGM: grises.
		LeerImagenPGM (nombre, filas, columnas, imagen);	
	}

	else if (t==IMG_PPM){ 			//Imagen PPM: 
		LeerImagenPPM (nombre, filas, columnas, imagen);
	}

	Revela (imagen, frase);	cout << '\n';	
	
	cout << "Revelando...\n";
	while(frase[contador] != '\0'){
		cout << frase[contador];
		contador++;
	}	
	cout << '\n';
}
