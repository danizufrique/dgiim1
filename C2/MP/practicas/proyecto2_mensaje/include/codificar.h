/**
  * @file codificar.h
  * @brief Fichero cabecera para el tratamiento de la codificación. 
  *	
  * Permite ocultar y revelar mensajes en una imagen.
  *
  */


#ifndef codificar_h
#define codificar_h

#include <iostream>
#include "imagenES.h"
using namespace std;

/**
  * @brief Consulta el bit n-ésimo de un carácter.
  *
  * @param caracter indica el carácter a consultar.
  * @param posicion referencia al bit que se desea conocer.
  * @return Devuelve 0 o 1, valor del bit que se desea conocer.
  * @pre posicion debe ser un valor entre 0 y 7.
  * @see char
  */
char BitN(char caracter, int posicion);


/**
  * @brief Inserta un carácter codificado en una imagen.
  *
  * @param caracter indica el carácter a codificar.
  * @param imagen buffer leído en el que se ocultará el carácter deseado.
  * @param posicion aquí se empezará a ocultar el carácter, necesitará 64 bits (8 bytes).
  * @return Función tipo void..
  * @pre posicion la posición debe ser menor que el número de caracteres (1 carácter = 8 bits = 1 	byte) de la imagen pasada restando 64 bits.
  * @see void
  */
void InsertaCaracter (char caracter, unsigned char imagen[], int posicion);


/**
  * @brief Inserta una cadena en una imagen.
  *
  * @param cadena frase que se ocultará.
  * @param imagen buffer leído en el que se ocultará el carácter deseado.
  * @return Devuelve 0 o 1, indicativo de éxito o error.
  * @pre el tamaño de la cadena no debe superar tamaño_imagen/8.
  * @retval true si se ha producido algún error en la escritura.
  * @retval false si ha tenido éxito en la escritura.
  * @see bool
  */
bool InsertaCadena (char cadena[], unsigned char imagen[]);


/**
  * @brief Revela un carácter codificado en una imagen.
  *
  * @param imagen buffer donde se oculta la información.
  * @param posicion referencia a la posición de la imagen donde se debe comenzar a buscar.
  * @return Devuelve un carácter oculto, si se realiza con éxito el carácter será imprimible.
  * @pre la imagen debe estar codificada con la función InsertaCadena.
  * @see char
  */
char RevelaCaracter(unsigned char imagen[], int posicion);


/**
  * @brief Decodifica una imagen hasta encontrar el carácter '\0'.
  *
  * @param imagen es el buffer donde se buscará la cadena escondida.
  * @param frase vector pasado por referencia que contendrá la información decodificada.
  * @return Devuelve 0 o 1, valor del bit que se desea conocer.
  * @retval true si se ha producido algún error en la escritura.
  * @retval false si ha tenido éxito en la escritura.
  * @pre la frase debe tener espacio suficiente para albergar el mensaje.
  * @see bool
  */
bool Revela(unsigned char imagen[], unsigned char frase[]);


#endif

/* Fin Fichero: codificar.h */
