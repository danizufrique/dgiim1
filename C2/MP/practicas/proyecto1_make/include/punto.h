#ifndef punto_h
#define punto_h

struct Punto {
    double x,y;
};

// Funciones para manejar puntos y circulos __________________________________
// Función obsoleta.
Punto LeerPunto(); 

void EscribirPunto (Punto p);

void InicializarPunto (Punto &p, double cx, double cy);

double GetX (Punto p);

double GetY (Punto p);

double Distancia (Punto p1, Punto p2);

Punto PuntoMedio (Punto p1, Punto p2);

// Actualización
void Avanzar(std::istream& is);

bool LeerPunto(std::istream& is, Punto& p);

bool EscribirPunto(std::ostream& os, const Punto& p);

#endif
