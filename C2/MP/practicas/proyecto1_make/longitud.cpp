#include <iostream>
#include <fstream>
#include "punto.h"
using namespace std;

int main(int argc, char* argv[]){
	double distancia=0;
	Punto punto1, punto2;
	bool bul;

  if (argc==1) { // Si no hemos dado parámetros en la línea de órdenes
	LeerPunto(cin, punto1); 
	while (cin.eof() == false){
		bul = LeerPunto(cin, punto2);
		if (bul){
			distancia += Distancia(punto1, punto2);
		}
		punto1 = punto2;
	}
  }
  else {
    ifstream fich(argv[1]); // Como parámetro, el nombre del archivo
    if (!fich) {
      cerr << "Error: no se abre " << argv[1] << endl;
      return 1;
    }
	LeerPunto(fich, punto1); 
	while (fich.eof() == false){
		bul = LeerPunto(fich, punto2);
		if (bul){
			distancia += Distancia(punto1, punto2);
		}
		punto1 = punto2;
	}
  }

	cout << '\n'<<"Hay " << distancia << " cm entre los puntos.\n";

cout << '\n';
}
