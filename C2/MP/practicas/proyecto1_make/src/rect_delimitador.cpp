#include <iostream>
#include <cmath>
#include <fstream>

#include "rectangulo.h"
#include "punto.h"
using namespace std;

int main(int argc, char* argv[]){
	Rectangulo rectangulo;
	Punto punto, otro_punto;
	cout << "Introduzca los puntos: \n";

  if (argc==1) { // Si no hemos dado parámetros en la línea de órdenes
	LeerPunto(cin, punto);
	otro_punto= punto;
	InicializarRectangulo(rectangulo, punto, otro_punto);

	while (! cin.eof()){
		LeerPunto(cin, punto);
		AmpliarRectangulo(rectangulo, punto);	
	}
  }
  else {
    ifstream fich(argv[1]); // Como parámetro, el nombre del archivo
    if (!fich) {
      cerr << "Error: no se abre " << argv[1] << endl;
      return 1;
    }
	LeerPunto(fich, punto);
	otro_punto= punto;
	InicializarRectangulo(rectangulo, punto, otro_punto);

	while (! fich.eof()){
		LeerPunto(fich, punto);
		AmpliarRectangulo(rectangulo, punto);	
	}

  }

cout << "El rectangulo delimitador es ";
EscribirRectangulo(cout, rectangulo);

cout << '\n';
}
