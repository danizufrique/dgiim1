#include <iostream>
#include <fstream>

#include "circulo.h"
using namespace std;

int main(int argc, char* argv[]){
	Circulo circulo;
	double area;

  if (argc==1) { // Si no hemos dado parámetros en la línea de órdenes
    LeerCirculo(cin, circulo);
  }
  else {
    ifstream fich(argv[1]); // Como parámetro, el nombre del archivo
    if (!fich) {
      cerr << "Error: no se abre " << argv[1] << endl;
      return 1;
    }
    LeerCirculo(fich, circulo);
  }

	area = Area(circulo);
	cout << "El área del círculo es: " << area << ".";
cout << '\n';
}
