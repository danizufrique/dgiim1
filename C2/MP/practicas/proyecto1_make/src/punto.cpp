#include <iostream>
#include <fstream>
#include <cmath> // sqrt, M_PI

#include "punto.h"
using namespace std;
// Funciones para manejar puntos y circulos __________________________________

Punto LeerPunto()
{
	while (cin.peek() == '#'){
		cin.ignore(4000, '\n');
	}	
	Punto punto;
	char c;
	cin >> c;
	cin >> punto.x;
	cin >> c;
	cin >> punto.y;
	cin >> c;
	while (cin.peek() == '#'){
		cin.ignore(4000, '\n');
	}
	return punto;
}

void EscribirPunto (Punto p)
{
	cout << "(" << p.x << "," << p.y << ")";
}

void InicializarPunto (Punto &p, double cx, double cy)
{
	p.x = cx;
	p.y = cy;
}

double GetX (Punto p)
{
	return p.x;
}


double GetY (Punto p)
{
	return p.y;
}

double Distancia (Punto p1, Punto p2)
{
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}


Punto PuntoMedio (Punto p1, Punto p2)
{
	Punto punto_medio;
	punto_medio.x = (p1.x + p2.x)/2;
	punto_medio.y = (p1.y + p2.y)/2;
	return punto_medio;
}

// Actualización 

void Avanzar(std::istream& is)
{
  while (isspace(is.peek()) || is.peek()=='#') {
    if (is.peek()=='#')
      is.ignore(1024,'\n'); // Suponemos una línea tiene menos de 1024
    else is.ignore();
  }
}


bool LeerPunto(std::istream& is, Punto& p){
	bool leer;
	char c;
	if (! ( is.fail() )){	
		leer = true;
		Avanzar(is);

		is >> c;
		is >> p.x;
		is >> c;
		is >> p.y;
		is >> c; 
	}
	if (is.fail()){
		leer = false;
	}
	return leer;
}


bool EscribirPunto(std::ostream& os, const Punto& p){
	bool escribir = true;
	os << "(" << p.x << "," << p.y << ")";
	return escribir;
}
