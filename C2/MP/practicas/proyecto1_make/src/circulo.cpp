#include <iostream>
#include <fstream>
#include <cmath> // sqrt, M_PI

#include "circulo.h"
using namespace std;

Circulo LeerCirculo()
{
	char delimitador;
	Circulo circulo;

	cin >> circulo.radio;
	cin.get(delimitador);
	circulo.centro = LeerPunto();

	return circulo;	
}

void EscribirCirculo(Circulo c)
{
	cout << c.radio << "-";
	EscribirPunto(c.centro);
}

void InicializarCirculo (Circulo& c, Punto centro, double radio)
{
	c.centro.x = centro.x;
	c.centro.y = centro.y;
	c.radio = radio; 
}

Punto Centro (Circulo c)
{
	Punto punto;
	punto.x = c.centro.x;
	punto.y = c.centro.y;
	return punto;
}

double Radio (Circulo c)
{
	return c.radio;
}

double Area (Circulo c)
{
	return M_PI * c.radio * c.radio; // pi*r^2
}

bool Interior (Punto p, Circulo c)
{
	bool contiene = ( Distancia(p, c.centro) <= c.radio );
	return contiene;
}

double Distancia (Circulo c1, Circulo c2)
{
	double distancia;	
	distancia= Distancia(c1.centro, c2.centro);
	distancia = distancia - c1.radio - c2.radio;
	if (distancia < 0)
		distancia = 0;
	return distancia;
}

// Actualizaciones
bool LeerCirculo (std::istream& is, Circulo& c){
	bool leer = true;
	char delimitador;

	is >> c.radio;
	is.get(delimitador);
	LeerPunto(is, c.centro);
	
	if (is.fail())
		leer = false;
	return leer;
}

bool EscribirCirculo(std::ostream& os, const Circulo& c){
	bool escribir = true;	

	os << c.radio << "-";
	EscribirPunto(os, c.centro);

	return escribir;
}


