//Ejemplo operador +
Polinomio Polinomio::operator+(const Polinomio &pol) const {
	int gmax = (this->getGrado() > pol.getGrado()) ?
		this->getGrado() : pol.getGrado();
	Polinomio resultado(gmax);
	for (int i = 0; i <= gmax; ++i) {
		resultado.setCoeficiente(i,
			this->getCoeficiente(i) + pol.getCoeficiente(i));
	}
	return resultado;
}



//Operador =
CLASE& CLASE::operator=(const CLASE &p)
{
	if (&p != this) { // Si no es el mismo objeto
	// Si *this tiene memoria din�mica -> liberarla
	// Copiar p en *this (reservar nueva memoria y copiar)
	}
	return *this; // Devolver referencia a *this
}

//Operador <<
ostream& operator<<(ostream& flujo, const Polinomio& p) {
	flujo << p.getCoeficiente(p.getGrado());//Mostrar t�rmino grado mayor
	if (p.getGrado() > 1)
		flujo << "x^" << p.getGrado();
	else if (p.getGrado() == 1)
		flujo << "x";
	for (int i = p.getGrado() - 1; i >= 0; --i) {//Recorrer resto de t�rminos
		if (p.getCoeficiente(i) != 0.0) { //Si el coeficiente no es 0.0
			if (p.getCoeficiente(i) > 0.0) //imprimir coeficiente positivo
				flujo << " + " << p.getCoeficiente(i);
			else //imprimir coeficiente negativo
				flujo << " - " << -p.getCoeficiente(i);
			if (i > 1)
				flujo << "x^" << i;
			else if (i == 1)
				flujo << "x";
		}
	}
	return flujo;
}

//Operador >>
istream& operator>>(std::istream &flujo, Polinomio &p) {
	int g;
	float v;
	do {
		flujo >> v >> g;//Introducir coeficientes en la forma "coeficiente grado"
		if (g >= 0) { // Se introduce grado<0 para terminar
			p.setCoeficiente(g, v);
		}
	} while (g >= 0);
	return flujo;
}

//Operador []
const Punto& VectorPuntos::operator[](int indice) const { // EXAMEN
	return puntos[indice];
}

Punto& VectorPuntos::operator[](int indice) { // EXAMEN
	return puntos[indice];
}

//Operador <
bool Polinomio::operator<(const Polinomio& pol) const {
	bool menor = this->getGrado() < pol.getGrado() ? true : false;
	if (!menor) {
		bool iguales = this->getGrado() == pol.getGrado() ? true : false;
		if (iguales) {
			menor = coeficientes[this->getGrado()] <
				pol.coeficientes[this->getGrado()] ? true : false;
		}
	}
	return menor;
}

//Operador ++obj
Polinomio& Polinomio::operator++() {
	*this = *this + 1;
	return *this;
}

//Operador obj++
Polinomio Polinomio::operator++(int valor) {
	Polinomio aux(*this);
	*this = *this + 1;
	return aux;
}

//Operador () [varias versiones]
double& operator() (int fila, int columna) {
	assert(fila >= 0 && fila < m_filas && columnas >= 0 && columna < m_columnas);
	return m_datos[fila*m_columnas + columna];
}

const double& operator() (int fila, int columna) const {
	assert(fila >= 0 && fila < m_filas && columnas >= 0 && columna < m_columnas);
	return m_datos[fila*m_columnas + columna];
}