#EJERCICIOS
##Tema 1
###Ejercicio1
~~~c++
void imprimirArray(const int array[], int util){
  cout << util << endl;
  for (int i = 0; i < util; i++) {
    cout << array[i] << " ";
  }
}

void generarArray(int array[], int util, int min, int max){
  for (int i = 0; i < util; i++) {
    array[i] = uniforme(min,max);
  }  
}
~~~

###???
~~~c++
void leerArray(int array[], int dim, int& util){
  cout << "Introduce número elementos.";
  cin >> util;

  if(dim < util){util = dim;}

  for(int i = 0; i < util; i++){
    cin >> array[i];
  }
}

int buscarMinimoArray(int array[], int util, int inicial, int final){
  int min = array[inicial];
  int posMin = inicial;

  //Creo que se ha liado con los índices del array
  for(int i = inicial+1; i <= final; i++){
    if(array[i] < min){
      min = array[i];
      posMin = i;
    }
  }

  return posMin;
}
~~~

###Ejercicio 5
~~~c++
void ordenarPorSeleccion(int array[], int util){
  int aux, int posMin;

  for(int i = 0; i < util - 1; i++){
    posMin = buscarMinimoArray(array, util, i, util-1);
    aux = array[posMin];
    array[posMin] = array[i];
    array[i] = aux;
  }
}
~~~
