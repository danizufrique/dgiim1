/*
 * EXAMEN MP - FINAL (ORDINARIO)
 * AÑO: 2018
 * SOLUCIÓN DE DANIEL PÉREZ RUIZ
 */

#ifndef INFOPARADA_H
#define INFOPARADA_H

class InfoParada{
private:
    bool activa;
    int indice_parada;
    
public:
    InfoParada();                                                                           //MÉTODO DADO EN EL EXAMEN
    InfoParada(bool p_activa, int index_parada);                                            //MÉTODO DADO EN EL EXAMEN
    InfoParada(const InfoParada &otro);                                                     //MÉTODO DADO EN EL EXAMEN
    InfoParada& operator=(const InfoParada &asig);                                          //MÉTODO DADO EN EL EXAMEN
    ~InfoParada();                                                                          //MÉTODO DADO EN EL EXAMEN
    
    void copiar(const InfoParada &orig);                                                    //MÉTODO DADO EN EL EXAMEN
    int GetIndice();                                                                        //MÉTODO DADO EN EL EXAMEN
    bool EstaActiva();                                                                      //MÉTODO DADO EN EL EXAMEN
    
    friend std::ostream & operator << (std::ostream & os, const InfoParada parada);         //MÉTODO AUXILIAR UTILIZADO
    friend std::istream & operator >> (std::istream & is, InfoParada parada);               //MÉTODO AUXILIAR UTILIZADO
};

std::ostream & operator << (std::ostream & os, const InfoParada & parada);
std::istream & operator >> (std::istream & is, InfoParada & parada);


#endif /* INFOPARADA_H */

