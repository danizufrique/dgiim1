/*
 * EXAMEN MP - FINAL (ORDINARIO)
 * AÑO: 2018
 * SOLUCIÓN DE DANIEL PÉREZ RUIZ
 */

#ifndef LINEA_H
#define LINEA_H

#include "InfoParada.h"

class Linea{
private:
    int num_paradas;
    InfoParada * paradas;
    
    void reservarMemoria(int nparadas);                                         //MÉTODO DADO EN EL EXAMEN
    void liberarMemoria();                                                      //MÉTODO DADO EN EL EXAMEN

public:
    Linea();                                                                    //MÉTODO DADO EN EL EXAMEN
    Linea(int nparadas);                                                        //MÉTODO DADO EN EL EXAMEN
    Linea(const Linea& otro);                                                   //MÉTODO DADO EN EL EXAMEN
    Linea& operator=(const Linea &asig);                                        //MÉTODO DADO EN EL EXAMEN
    ~Linea();                                                                   //MÉTODO DADO EN EL EXAMEN
    
    void copiar(const Linea &orig);                                             //MÉTODO DADO EN EL EXAMEN
    
    Linea& operator+=(const InfoParada & parada);                               //MÉTODO A IMPLEMENTAR (EJERCICIO 2)
    int GetNumParadas();                                                        //MÉTODO DADO EN EL EXAMEN
    InfoParada& operator[](int i);                                              //MÉTODO DADO EN EL EXAMEN
    const InfoParada& operator[](int i) const;                                  //MÉTODO DADO EN EL EXAMEN
    
    friend std::ostream & operator <<(std::ostream& os, const Linea &linea);    //MÉTODO AUXILIAR UTILIZADO
    friend std::istream & operator >>(std::istream& is, Linea &linea);          //MÉTODO AUXILIAR IMPLEMENTADO
    
    bool EstaTotalmenteOperativa();                                             //MÉTODO A IMPLEMENTAR (EJERCICIO 5)
};

std::ostream & operator <<(std::ostream& os, const Linea &linea);
std::istream & operator >>(std::istream& is, Linea &linea);


#endif /* LINEA_H */

