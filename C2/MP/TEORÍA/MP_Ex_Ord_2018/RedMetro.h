/*
 * EXAMEN MP - FINAL (ORDINARIO)
 * AÑO: 2018
 * SOLUCIÓN DE DANIEL PÉREZ RUIZ
 */

#ifndef REDMETRO_H
#define REDMETRO_H

#include "Linea.h"
#include "InfoParada.h"

class RedMetro{
private:
    int num_lineas;
    Linea * lineas;
    
    void reservarMemoria(int nlineas);                                          //MÉTODO AUXILIAR UTILIZADO
    void liberarMemoria();                                                      //MÉTODO AUXILIAR UTILIZADO
    
public:
    RedMetro();                                                                 //MÉTODO A IMPLEMENTAR (EJERCICIO 1)
    RedMetro(const char* fichero);                                              //MÉTODO A IMPLEMENTAR (EJERCICIO 3)
    RedMetro(const RedMetro &orig);                                             //MÉTODO A IMPLEMENTAR (EJERCICIO 1)
    RedMetro& operator=(const RedMetro &asig);                                  //MÉTODO A IMPLEMENTAR (EJERCICIO 1)
    ~RedMetro();                                                                //MÉTODO A IMPLEMENTAR (EJERCICIO 1)
    
    int GetNumLineas();                                                         //MÉTODO DADO EN EL EXAMEN
    int GetNumeroTotalParadas();                                                //MÉTODO DADO EN EL EXAMEN
    
    Linea& operator[](int i);                                                   //MÉTODO DADO EN EL EXAMEN
    const Linea& operator[](int i) const;                                       //MÉTODO DADO EN EL EXAMEN
    
    void copiar(const RedMetro &otro);                                          //MÉTODO AUXILIAR UTILIZADO
    
    friend std::ostream & operator<<(std::ostream& os, const RedMetro& red);    //MÉTODO A IMPLEMENTAR (EJERCICIO 2)
    friend std::istream & operator>>(std::istream& is, RedMetro& red);          //MÉTODO A IMPLEMENTAR (EJERCICIO 2)
    
    bool operator>(const RedMetro & red2) const;                                //MÉTODO A IMPLEMENTAR (EJERCICIO 4)
    bool operator!=(const RedMetro & red2) const;                               //MÉTODO A IMPLEMENTAR (EJERCICIO 4)
    bool operator==(const RedMetro & red2) const;                               //MÉTODO A IMPLEMENTAR (EJERCICIO 4)
    
    double indiceCalidad();                                                     //MÉTODO AUXILIAR UTILIZADO
    int getNumParadasActivas();                                                 //MÉTODO AUXILIAR UTILIZADO
    int MejorConectada();                                                       //MÉTODO A IMPLEMENTAR (EJERCICIO 5)
};

std::ostream & operator<<(std::ostream& os, const RedMetro& red);
std::istream & operator>>(std::istream& is, RedMetro& red);

#endif /* REDMETRO_H */