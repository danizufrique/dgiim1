/*
 * EXAMEN MP - FINAL (ORDINARIO)
 * AÑO: 2018
 * SOLUCIÓN DE DANIEL PÉREZ RUIZ
 */

#include <iostream>
#include <fstream>
#include "RedMetro.h"
#include "Linea.h"
#include "InfoParada.h"

using namespace std;

/*
 * EJERCICIO 4 : SOBRECARGA DE OPERADORES RELACIONALES
 */

/*
 * (0.75 puntos) Sobrecargue los operadores relacionales ==, != y > para la clase RedMetro en base a un valor de calidad. Este valor se 
 * calculará a partir de: 1) el número de lı́neas (30%) y 2) el número de paradas activas (70%).
 */

/*
 * MEDIDOR INDICE DE CALIDAD
 */
double RedMetro::indiceCalidad(){
    return 0.3*GetNumLineas() + 0.7*getNumParadasActivas();
}

int RedMetro::getNumParadasActivas(){
    int contador = 0;
    
    for(int i=0; i<num_lineas; i++){
        for(int j=1; j<=lineas[i].GetNumParadas(); j++){
            if(lineas[i][j].EstaActiva()){
                contador++;
            }
        }
    }
    
    return contador;
}

/*
 * SOBRECARGA OPERADOR MAYOR
 */
bool RedMetro::operator >(const RedMetro& red2) const{
    return this->indiceCalidad() > red2.indiceCalidad();
}

/*
 * SOBRECARGA OPERADOR !=
 */
bool RedMetro::operator !=(const RedMetro& red2) const{
    return this->indiceCalidad() != red2.indiceCalidad();
}

/*
 * SOBRECARGA OPERADOR ==
 */
bool RedMetro::operator ==(const RedMetro& red2) const{
    return this->indiceCalidad() == red2.indiceCalidad();
}
