/*
 * EXAMEN MP - FINAL (ORDINARIO)
 * AÑO: 2018
 * SOLUCIÓN DE DANIEL PÉREZ RUIZ
 */

#include <iostream>
#include "RedMetro.h"
#include "Linea.h"
#include "InfoParada.h"

using namespace std;

/*
 * EJERCICIO 2 : SOBRECARGA DE OPERADORES
 */

/*
 * 1. (0.5 puntos) Sobrecargue el operador += para la clase Linea. El objetivo es incorporar una nueva parada 
 * (InfoParada) a la lı́nea (Linea). El nuevo objeto InfoParada se añade al final del objeto Linea.
 */

/*
 * SOBRECARGA OPERADOR +=
 */
Linea& Linea::operator +=(const InfoParada& parada){
    InfoParada * nuevoParadas = new InfoParada[this->num_paradas+1];
    
    for(int i=0; i<this->num_paradas;i++){
        nuevoParadas[i] = this->paradas[i];
    }
    nuevoParadas[this->num_paradas] = parada;
    
    if(paradas != nullptr){
        delete [] paradas;
    }
    paradas = nuevoParadas;
    num_paradas++;
    
    return *this;
}

/*
 * 2. Implemente la siguiente funcionalidad para la clase RedMetro sobrecargando los operadores << y >>.
 */

/*
 * SOBRECARGA OPERADOR DE SALIDA PARA LA CLASE REDMETRO
 */
std::ostream & operator <<(std::ostream & os, const RedMetro & red){
    os << red.GetNumLineas() << endl;
    
    for(int i=1; i<=red.GetNumLineas(); i++){
        os << red[i];
    }
    
    return os;
}

/*
 * SOBRECARGA OPERADOR DE ENTRADA DE LA CLASE REDMETRO
 */
std::istream & operator >>(std::istream & is, RedMetro & red){
    int total_lineas = 0;
    Linea linea;
    
    is >> total_lineas;
    if(total_lineas > 0){
        red.reservarMemoria(total_lineas);
        for(int i=0; i<total_lineas; i++){
            //SE NECESITA SOBRECARGAR LA ENTRADA PARA LA CLASE LINEA
            is >> linea;
            //USO DE LA SOBRECARGA DE []
            red[i] = linea;
        }
    }
    
    return is;
}

/*
 * SOBRECARGA OPERADOR DE ENTRADA DE LA CLASE LINEA
 */
std::istream & operator >>(std::istream & is, Linea & linea){
    int total_paradas = 0;
    InfoParada parada;
    
    is >> total_paradas;
    if(total_paradas > 0){
        linea.reservarMemoria(total_paradas);
        for(int i=1; i<=total_paradas; i++){
            //SE NECESITA SOBRECARGAR LA ENTRADA DE LA CLASE INFOPARADA
            is >> parada;
            //USO DE LA SOBRECARGA DE []
            Linea[i] = parada;
        }
    }
    
    return is;
}

/*
 * SOBRECARGA OPERADOR DE ENTRADA DE LA CLASE INFOPARADA
 */
std::istream & operator >>(std::istream & is, InfoParada & parada){
    string activa;
    int indice = 0;
    
    is >> indice >> activa;
    parada.indice_parada = indice;
    if(activa == "S"){
        parada.activa = true;
    }
    else{
        parada.activa = false;
    }
    
    return is;
}