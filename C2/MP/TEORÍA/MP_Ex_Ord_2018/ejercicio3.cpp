/*
 * EXAMEN MP - FINAL (ORDINARIO)
 * AÑO: 2018
 * SOLUCIÓN DE DANIEL PÉREZ RUIZ
 */

#include <iostream>
#include <fstream>
#include "RedMetro.h"
#include "Linea.h"
#include "InfoParada.h"

using namespace std;

/*
 * EJERCICIO 3 : MÉTODOS Y FUNCIONES PARA E/S
 */

/*
 * 1. (0.75 puntos) Implemente un constructor de la clase RedMetro con argumento. Recibe el nombre de un fichero de texto con la 
 * información sobre una red. El fichero debe contener en la primera línea la cadena mágica "METRO" y un salto de línea. 
 * A continuación está el contenido de la red en formato de texto, tal como se indica en el ejercicio 2.
 */

/*
 * CONSTRUCTOR CON ARGUMENTOS [FICHERO]
 */
RedMetro::RedMetro(const char* fichero){
    reservarMemoria(0);
    
    ifstream fin;
    string MAGIC;
    
    fin.open(fichero);
    if(fin){
        fin >> MAGIC;
        if(MAGIC == "METRO"){
            fin >> *this;
            if(fin){
                cout << "SE HAN LEIDO CORRECTAMENTE LOS DATOS DE [" << fichero << "]" << endl;
            }
            else{
                cerr << "ERROR EN LA LECTURA DE DATOS DEL FICHERO [" << fichero << "]" << endl;
                exit(1);
            }
        }
        else{
            cerr << "ERROR. FORMATO DE ARCHIVO NO VALIDO. NO SE HA ENCONTRADO 'METRO'" << endl;
            exit(1);
        }
        fin.close();
    }
    else{
        cerr << "ERROR. EL ARCHIVO [" << fichero "] NO EXISTE O ESTA CORRUPTO" << endl;
        exit(1);
    }
}
