/*
 * EXAMEN MP - FINAL (ORDINARIO)
 * AÑO: 2018
 * SOLUCIÓN DE DANIEL PÉREZ RUIZ
 */

#include <iostream>
#include "RedMetro.h"

using namespace std;

/*
 * EJERCICIO 6 : APLICACIÓN
 */

/*
 * Escriba un programa completo que reciba desde la lı́nea de órdenes el nombre de dos ficheros de texto que contienen
 * la descripción de dos redes de metro (como se indica en el ejercicio 3) y muestre:
 * 
 * 1. Cuál es la mejor red.
 * 2. Para la mejor red informará sobre la operatividad de cada una de sus lı́neas.
 * 3. Para la mejor red indicará qué parada es la mejor conectada.
 */

int main(int narg, char * args[]){
    RedMetro red1, red2, mejor;
    char * mejor_red = nullptr;
    
    if(narg < 3){
        cerr << "ERROR. USO INCORRECTO DE APLICACION" << endl;
        cerr << "    USO: redmetro <fichero1> <fichero2>" << endl;
        exit(1);
    }
    else{
        red1(args[1]);
        red2(args[2]);
        
        cout << endl << "COMPROBANDO CUAL ES LA MEJOR RED..." << endl;
        if(red1 > red2){
            mejor = red1;
            mejor_red = args[1]; 
        }
        else{
            mejor = red2;
            mejor_red = args[2];
        }
        
        cout << "LA MEJOR LINEA ES [" << mejor_red << "]" << endl << endl;
        cout << "COMPROBANDO DISPONIBILIDAD DE LAS LINEAS..." << endl;
        for(int j=1; j<=mejor.GetNumLineas();j++){
            cout << "-> LINEA [" << j << "]" << " >> " << mejor[j].EstaTotalmenteOperativa() << endl;
        }
        cout << endl << endl << "COMPROBANDO CUAL ES LA PARADA MEJOR CONECTADA" << endl;
        cout << "LA PARADA MEJOR CONECTADA ES: " << mejor.MejorConectada() << endl;
    }
    
    return 0;
}