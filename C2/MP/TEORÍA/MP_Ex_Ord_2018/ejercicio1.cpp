/*
 * EXAMEN MP - FINAL (ORDINARIO)
 * AÑO: 2018
 * SOLUCIÓN DE DANIEL PÉREZ RUIZ
 */

#include <iostream>
#include "RedMetro.h"
#include "Linea.h"
#include "InfoParada.h"

using namespace std;

/*
 * EJERCICIO 1 : MÉTODOS BÁSICOS DE LA CLASE
 */

/*
 * 1. (0.5 puntos) Constructor sin argumentos (crea una red vacı́ía) y destructor. 
 * Escriba también el método EstaVacia que indique si una red está vacía.
 */

/*
 * FUNCIÓN PARA RESERVAR MEMORIA
 */
void RedMetro::reservarMemoria(int nlineas){
    if(lineas != nullptr){
        liberarMemoria();
    }
    
    if(nlineas > 0){
        lineas = new Linea[nlineas];
        num_lineas = nlineas;
    }
    else{
        lineas = nullptr;
        num_lineas = 0;
    }
}

/*
 * CONSTRUCTOR DE LA CLASE REDMETRO
 */
RedMetro::RedMetro(){
    reservarMemoria(0);
}

/*
 * FUNCIÓN PARA LIBERAR LA MEMORIA
 */
void RedMetro::liberarMemoria(){
    if(lineas != nullptr){
        for(int i=0; i<num_lineas; i++){
            delete [] lineas[i];
        }
        delete [] lineas;
        lineas = nullptr;
        
        num_lineas = 0;
    }
}

/*
 * DESTRUCTOR DE LA CLASE REDMETRO
 */
RedMetro::~RedMetro(){
    liberarMemoria();
}

/*
 * 2. (0.5 puntos) Constructor de copia y operador de asignación.
 */

/*
 * FUNCIÓN PARA COPIAR EL CONTENIDO DE UN OBJETO REDMETRO
 */
void RedMetro::copiar(const RedMetro& otro){
    num_lineas = otro.GetNumLineas();
    
    for(int i=0; i<otro.GetNumLineas(); i++){
        lineas[i] = otro.lineas[i];
    }
}

/*
 * CONSTRUCTOR DE COPIA
 */
RedMetro::RedMetro(const RedMetro& orig){
    reservarMemoria(orig.GetNumLineas());
    copiar(orig);
}

/*
 * OPERADOR DE ASIGNACIÓN
 */
RedMetro& RedMetro::operator =(const RedMetro& asig){
    if(&asig != this){
        liberarMemoria();
        reservarMemoria(asig.GetNumLineas());
        copiar(asig);
    }
    
    return *this;
}