/*
 * EXAMEN MP - FINAL (ORDINARIO)
 * AÑO: 2018
 * SOLUCIÓN DE DANIEL PÉREZ RUIZ
 */

#include <iostream>
#include "RedMetro.h"
#include "Linea.h"
#include "InfoParada.h"

using namespace std;

/*
 * EJERCICIO 5 : METODOS DE CALCULO
 */

/*
 * 1.(0.75 puntos) Implemente el método MejorConectada que permita obtener la parada (su ı́ndice) en la que
 * confluyen el mayor número de líneas, independientemente de si las paradas están activas o no.
 */

int RedMetro::MejorConectada(){
    int indice = 0, num_paradas = GetNumeroTotalParadas();
    int * arrayContador = new int [num_paradas];
    int ind_maximo = 0;
    
    for(int i=0; i<num_paradas; i++){
        arrayContador[i] = 0;
    }
    
    for(int i=0; i<GetNumLineas(); i++){
        for(int j=1; j<=lineas[i].GetNumParadas();j++){
            indice = lineas[i][j].GetIndice();
            arrayContador[indice-1]++;
        }
    }
    
    ind_maximo = arrayContador[0];
    for(int k=0; k<num_paradas; k++){
        if(ind_maximo < arrayContador[i])
            ind_maximo = arrayContador[i]
    }
    
    indice = ind_maximo + 1;
    delete[] arrayContador;
    
    return indice;
}

/*
 * 2.(0.25 puntos) Implemente el método EstaTotalmenteOperativa que indique si una lı́nea está totalmente 
 * operativa, o sea, si todas sus paradas están activas.
 */

bool Linea::EstaTotalmenteOperativa(){
    bool activa = true;
    
    for(int i=0; i<GetNumParadas() && activa; i++){
        if(!paradas[i].EstaActiva())
            activa = false;
    } 
    
    return activa;
}