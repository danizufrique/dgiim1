#include <iostream>
using namespace std;

class InfoParada{
	private:
		bool activa;
		int indice_parada;
	public:
		int GetIndice(){return indice_parada;}
		bool EstaActiva(){return activa;}
};

class Linea{
	private:
		int num_paradas;
		InfoParada * paradas;
	public:
		int GetNumParadas(){return num_paradas;}
		InfoParada GetParada(int posicion);
		void reservarMemoria(int n); //Opcional
		void liberarMemoria(); //Opcional
		Linea& operator =(Linea& otro);
		InfoParada const operator[](int indice) const{
			return paradas[indice];
		}
		InfoParada operator[](int indice){
			return paradas[indice];
		}
		Linea& operator +=(InfoParada& parada);
};

class RedMetro{
	private:
		int num_lineas;
		Linea * lineas;
		
	public:
		RedMetro();
		RedMetro(RedMetro & otro);
		~RedMetro();
		bool EstaVacia();
		void liberarMemoria(); //Opcional
		void reservarMemoria(int n); //Opcional
		Linea GetLinea(int posicion); //Opcional
		RedMetro& operator =(const RedMetro& otro);
		const int GetNumLineas(){return num_lineas;}
		int GetNumeroTotalParadas(){
			int contador = 0;
			
			for(int i=0; i<num_lineas; i++)
				for(int j=0; j<lineas[i].GetNumParadas(); j++)
					contador++;
		return contador;			
		}
		const Linea operator[](int indice) const{
			return lineas[indice];
		}
		Linea operator[](int indice){
			return lineas[indice];
		}	
};

int main(){
	
}

//EJERCICIO 1

//Apartado 1
RedMetro::RedMetro(){
	num_lineas = 0;
	lineas = nullptr;
}

void RedMetro::liberarMemoria(){
	if(lineas != nullptr)
		delete [] lineas;
	
	lineas = nullptr;
}

void RedMetro::reservarMemoria(int n){
	lineas = new Linea[n];
}

RedMetro::~RedMetro(){
	liberarMemoria();
}

bool RedMetro::EstaVacia(){
	if(num_lineas == 0)
		return true;
	else 
		return false;
}

//Apartado 2
RedMetro::RedMetro(RedMetro& otro){
	this->num_lineas = otro.GetNumLineas();
	liberarMemoria();
	reservarMemoria(num_lineas);
}

Linea RedMetro::GetLinea(int posicion){
	return lineas[posicion];	
}

RedMetro& RedMetro::operator =(const RedMetro& otro){
	num_lineas = otro.GetNumLineas();
	liberarMemoria();
	reservarMemoria(num_lineas);
	
	for(int i=0; i<num_lineas; i++)
		lineas[i] = otro.GetLinea(i);
	
	return *this;
}

//EJERCICIO 2
void Linea::liberarMemoria(){
	if(paradas != nullptr){
		delete [] paradas;
		paradas = nullptr;
	}
}

void Linea::reservarMemoria(int n){
	paradas = new InfoParada[n];
}

InfoParada Linea::GetParada(int posicion){
	return paradas[posicion];
}

Linea& Linea::operator =(Linea& otro){
	num_paradas = otro.GetNumParadas();
	liberarMemoria();
	reservarMemoria(num_paradas);
	
	for(int i=0; i<num_paradas; i++)
		paradas[i] = otro.GetParada(i);
		
	return *this;
}

Linea& Linea::operator +=(const InfoParada& parada){
	Linea aux = *this;
	int nuevasParadas = aux.GetNumParadas();
	
	liberarMemoria();
	reservarMemoria(nuevasParadas);
	
	for(int i=0; i<nuevasParadas-1; i++)
		paradas[i] = aux.GetParada(i);
		
	paradas[nuevasParadas-1] = parada;
	
	return *this;
	
}
