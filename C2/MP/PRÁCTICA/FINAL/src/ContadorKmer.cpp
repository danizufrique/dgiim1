/** 
 * @file ContadorKmer.cpp
 * @author Daniel Zufrí Quesada
*/

#include <iostream>
#include <fstream>
#include "Kmer.h"
#include "SecuenciasKmer.h"
#include "ContadorKmer.h"
#include <cmath>
using namespace std;

ContadorKmer::ContadorKmer(){
    _contador = nullptr;
    _nperfiles = 0;
    _VALIDOS = INI_VALIDOS;
    _K = INI_K;
    _NKMER = getNumKmer();
}

ContadorKmer::ContadorKmer(const std::string& validos, int k){
    _contador = nullptr;
    _nperfiles = 0;
    _VALIDOS = validos;
    _K = k;
    _NKMER = getNumKmer();
}

ContadorKmer::ContadorKmer(const ContadorKmer& orig){
    _contador = nullptr;
    *this = orig;    
}

ContadorKmer::~ContadorKmer(){
    liberarMemoria();
}

int ContadorKmer::getNumPerfiles() const{
    return _nperfiles;
}

long ContadorKmer::getNumKmer() const{
    return pow(_VALIDOS.size(),_K);
}

void ContadorKmer::addPerfil(){
    ContadorKmer copia = *this;
    int perfiles = _nperfiles+1;
    
    liberarMemoria();
    reservarMemoria(perfiles);
    
    _nperfiles = perfiles;
    _VALIDOS = copia._VALIDOS;
    _K = copia._K;
    _NKMER = getNumKmer();
    
    for(int i=0; i<_nperfiles-1; i++)
        for(int j=0; j<getNumKmer(); j++)
            _contador[i][j] = copia._contador[i][j];
          
    for(int i=0; i<getNumKmer(); i++)
        _contador[_nperfiles-1][i] = 0;
}

bool ContadorKmer::addKmer(int perfil, const Kmer& tg){
    int columna = getIndiceKmer(tg);
    int frec = tg.getFrecuencia();
    bool success = true;
    
    if(perfil < _nperfiles && perfil >= 0){
        if(frec >= 0){
            if(frec == 0)
                _contador[perfil][columna] += 1;
            else
                _contador[perfil][columna] += frec;
        }        
    }
    else{
        cerr << "Número de perfil no válido (debe estar entre 0 y " << _nperfiles-1 << ").";
        success = false;
    }
    
    return success;
}

void ContadorKmer::addPerfil(SecuenciasKmer perfil){
    ContadorKmer copia = *this;
    Kmer km;
    int perfiles = _nperfiles+1;
    
    reservarMemoria(perfiles);
    
    _nperfiles = perfiles;
    _VALIDOS = copia._VALIDOS;
    _K = copia._K;
    _NKMER = getNumKmer();
    
    for(int i=0; i<_nperfiles-1; i++)
        for(int j=0; j<getNumKmer(); j++)
            _contador[i][j] = copia._contador[i][j];
          
    for(int i=0; i<perfil.getSize(); i++){
        km = perfil.getPosicion(i);
        _contador[_nperfiles-1][getIndiceKmer(km)] = km.getFrecuencia();
    }   
}

SecuenciasKmer ContadorKmer::getSecuenciasKmer(int perfil, int frecmin) const{
    int posiciones[getNumKmer()];
    int posiciones_util=0;
    
    //Compruebo cuantas celdas no están vacias y su posicion
    for(int i=0; i<getNumKmer(); i++){
        if(_contador[perfil][i] > frecmin){
            posiciones[posiciones_util] = i;
            posiciones_util++;
        }
    }
    
    //Genero la SecuenciaKmer del tamaño justo y copio
    SecuenciasKmer seq(posiciones_util);
    Kmer km;
    int frec;
    
    for(int i=0; i<posiciones_util; i++){
        frec = _contador[perfil][posiciones[i]];
        string cadena = getIndiceInversoKmer(posiciones[i]).getCadena();
        
        km.setCadena(cadena);
        km.setFrecuencia(frec);
        seq.setPosicion(i,km);
    }
    
    return seq;
}

bool ContadorKmer::calcularFrecuenciasKmer(int perfil, const char* nfichero){
    bool success = true;
    ifstream fin;
    string adn;
    string cadena;
    int contador = 0;
    
    Kmer km, norm;

    fin.open(nfichero);  
    if(fin){
        fin >> adn;
        if(fin){
            for(int i=0; i<adn.length()-_K+1; i++){
                cadena = "";
                contador++;
                
                for(int j=0; j<_K; j++)
                    cadena += adn[i+j];
                
                km.setCadena(cadena);
                //norm = normalizaKmer(km);
                addKmer(perfil, km);                     
            }
            
            cout << "Se han leido correctamente " << contador << " elementos de " << nfichero << endl;
        }
        else{
            cerr << "ERROR leyendo la cadena de ADN." << endl;
            success = false;
        }
        fin.close();
    }
    else{
        cerr << "ERROR abriendo fichero " << nfichero << "." << endl;
        success=false;
    }
     
    return success;
}

void ContadorKmer::reservarMemoria(int _nperfiles){
    if(_contador != nullptr)
        liberarMemoria();
     
    _contador = new int* [_nperfiles];
    
    for(int i=0; i<_nperfiles; i++)
        _contador[i] = new int [_NKMER];
    
}

void ContadorKmer::liberarMemoria(){
    if(_contador != nullptr){
        for(int i=0; i<_nperfiles; i++){
            delete [] _contador[i];
            _contador[i] = nullptr;
        }
        delete [] _contador;
        _contador = nullptr;
    }   
}

ContadorKmer& ContadorKmer::operator =(const ContadorKmer& orig){  
    
    liberarMemoria();
    
    _nperfiles = orig._nperfiles;
    _VALIDOS = orig._VALIDOS;
    _K = orig._K;
    _NKMER = orig._NKMER;
    
    reservarMemoria(_nperfiles);
       
    for(int i=0; i<_nperfiles; i++){
        for(int j=0; j<orig._NKMER; j++){
            _contador[i][j] = orig._contador[i][j];
        }
    }
    
    return *this;
}

void ContadorKmer::copiar(const ContadorKmer& otro){
    *this = otro;
}

Kmer ContadorKmer::normalizaKmer(const Kmer& ng) const{
    Kmer resultado;  
    
    string cadena = ng.getCadena();
    string cadenaNor;
    int frecuencia = ng.getFrecuencia();
    
    //Pone todo a minúscula
    for (int i = 0; i < cadena.length(); i++){
        if (cadena[i] >= 'A' && cadena[i] <= 'Z'){
            cadena[i] = cadena[i] + 32;
        }
    }
    
    //Elimina caracteres sobrantes
    if(cadena.size() > _K){
        cadenaNor = cadena.substr(0,_K);
        cadena = cadenaNor;
    }

    //Añade '_' para cadenas con longitud menor de K
    else if(cadena.size() < _K){
        for(int i=cadena.size(); i< _K; i++){
            cadena += "_";
        }
    }
    
    //Comprueba que los caracteres sean válidos, y si no los sustituye por '_'
    for(int i=0; i<= _K; i++){
        bool valido = false;
        
        for(int j=1; j< _VALIDOS.size() && valido == false; j++){         
            if(cadena[i] == _VALIDOS[j]){
                valido = true;          
            }        
        }
        if(!valido)
            cadena[i] = '_';
        
    }
    
    //Comprueba que la frecuencia sea válida
    if(frecuencia < 0){
        frecuencia = 0;
    }
    
    //Asigna datos
    resultado.setCadena(cadena);
    resultado.setFrecuencia(frecuencia);
    
    return resultado;
}

Kmer ContadorKmer::getIndiceInversoKmer(long i) const{
    Kmer km;
    int resultado = i;
    int div;
    string cadena;
    char caracter;

    if(i<= pow(_VALIDOS.size(), _K)-1){
        for(int j=_K; j>0; j--){
            div = resultado/pow(5,j-1);   
            
            resultado -= pow(_K,j-1)*div;
            caracter = _VALIDOS[div];     
            cadena = caracter + cadena;
        }
        
        km.setCadena(cadena);
    }
    else
        cerr << "Ìndice no válido." << endl;
    
    return km;
}

long ContadorKmer::getIndiceKmer(const Kmer& t) const{
    Kmer norm = normalizaKmer(t);
    string cadena = norm.getCadena();
    int indice = 0;
        
    for(int i=0; i<_K; i++)
        indice += _VALIDOS.find(cadena[_K-i-1]) * pow(5,i);
    
    return indice;
    
}