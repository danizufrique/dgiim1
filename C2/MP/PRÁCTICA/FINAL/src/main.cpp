/**
 * @file main.cpp
 * @author Daniel Zufrí Quesada
 */
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include "Kmer.h"
#include "SecuenciasKmer.h"
#include "ContadorKmer.h"

using namespace std;

void mensajeError() {
    cerr << "ERROR en la llamada" << endl;
    cerr << "MODO LEARN: −l <fichero.kmer> <fichero1.dna> [<fichero2.dna>...<ficheron.dna>] " << endl <<
            "MODO CLASSIFY: -c <fichero.dna> <fichero1.kmer>[<fichero2.kmer>...<ficheron.kmer>]" << endl;
    exit(1);
}

const string ContadorKmer::INI_VALIDOS = "_acgt";
const int ContadorKmer::INI_K=5;
    
int main(int narg, char *args[]) {
    ifstream fin;
    ofstream fout;
    
    ContadorKmer cont;
    SecuenciasKmer seq;
     
    bool success = true;
    
    if(narg >= 4){
        if(!strcmp(args[1],"-l")){
            int nperfiles;
            int fallos=0;
            
            cout << "\t" << "__MODO LEARN__" << endl;
            cont.addPerfil();
            nperfiles = cont.getNumPerfiles();
            
            for(int i=3; i<narg; i++){
                success = cont.calcularFrecuenciasKmer(nperfiles-1, args[i]);
                if(!success){
                    fallos++;
                    cerr << "ERROR leyendo archivos de ADN del fichero " << args[i] << "." << endl;
                }
                    
            }         
                        
            cout << "Se han leído " << narg-3-fallos << " archivos de ADN correctamente." << endl;
            seq = cont.getSecuenciasKmer(nperfiles-1,0);  
            seq.ordenar();            
            seq.saveFichero(args[2]);                        
        }
        else if(!strcmp(args[1],"-c")){
            cout << "\t" << "__MODO CLASSIFY__" << endl;
            
            SecuenciasKmer seqLeido;
            
            float menorDistancia = 1;
            float distancia = 1;
            int elegido = 0; 
                                  
            cont.addPerfil();
            
            if(cont.calcularFrecuenciasKmer(0,args[2]))
                seq = cont.getSecuenciasKmer(0,0);           
            else
                cerr << "ERROR abriendo el archivo " << args[2] << "." << endl;
                      
            seq.ordenar();
            
            for(int i=3; i<narg; i++){
                success = seqLeido.loadFichero(args[i]);         
                distancia = seq.distancia(seqLeido);
                

                if(success){
                    if(i==3){
                        menorDistancia = distancia;
                        elegido = i;
                    }

                    else if(distancia < menorDistancia){                      
                        menorDistancia = distancia;
                        elegido = i;
                    }  
                    cout << "\t" << args[i] << ": " << distancia << endl;
                }
                else
                    cerr << "Ha ocurrido un ERROR durante la lectura del fichero " << args[i] << "." << endl;
                
                
            }
            
            if(elegido != 0)
                cout << endl << "RESULTADO: El perfil que más se asemeja a " << args[2] << " es " << args[elegido] << ", con una distancia de " << menorDistancia << "." << endl;
            else
                cerr << "No es posible dar ninguna sugerencia. Compruebe que los ficheros existen o no estén vacíos." << endl; 
                        
        }
        else
            mensajeError();
        
    }
    else
        mensajeError();
    
    
    
    return 0;
}

