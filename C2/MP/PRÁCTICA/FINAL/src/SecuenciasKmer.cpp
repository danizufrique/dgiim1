/** 
 * @file SecuenciasKmer.cpp
 * @author Daniel Zufrí Quesada
*/

#include <iostream>
#include <fstream>
#include <cmath>

#include "Kmer.h"
#include "SecuenciasKmer.h"

using namespace std;



double SecuenciasKmer::distancia(const SecuenciasKmer & d) const {
    long posB;
    long posA = 0;
    double dist = 0.0;

    for (long i = 0; i < this->getSize(); ++i) {
        posB = d.findKmer (getPosicion(i).getCadena());
        if (posB < 0) {
            posB = getSize();
        }

        dist += abs(posA - posB);
        posA++;
    }
    return dist / (getSize() * getSize()); 
}

SecuenciasKmer::SecuenciasKmer(){
    _nKmer = 0;
    _conjunto = nullptr;
}

SecuenciasKmer::SecuenciasKmer(long nkm){
    _nKmer = nkm;
    _conjunto = new Kmer[_nKmer];
}

SecuenciasKmer::SecuenciasKmer(const SecuenciasKmer& orig){
    _nKmer = orig.getSize();
    _conjunto = new Kmer[_nKmer];
        
    for(int i=0; i<_nKmer; i++)
        _conjunto[i] = orig.getPosicion(i);    
}

SecuenciasKmer::~SecuenciasKmer(){
    delete [] _conjunto;
    _conjunto = {NULL};
}

SecuenciasKmer& SecuenciasKmer::operator =(const SecuenciasKmer& orig){
    if(&orig != this){
        delete [] _conjunto;
        _nKmer = orig.getSize();
        _conjunto = new Kmer[_nKmer];
        
        for(int i=0; i<_nKmer; i++)
            _conjunto[i] = orig.getPosicion(i);        
    }
    
    return *this;
}

Kmer SecuenciasKmer::getPosicion(long p) const{
    Kmer copy;
    
    if (p<getSize() && p>= 0)
        copy = _conjunto[p];   
    else
        cerr << "ERROR: posición no válida.";
    
    return copy;
}

void SecuenciasKmer::setPosicion(long p, const Kmer& km){
    if (p<getSize())
        _conjunto[p] = km;  
    else
        cerr << "ERROR: Posición no válida.";
}

long SecuenciasKmer::findKmer(const std::string& km) const{
    bool encontrado = false;
    long posicion = -1;
    
    for(int i=0; i<getSize() && encontrado == false; i++){
        if(_conjunto[i].getCadena() == km){
            encontrado = true;
            posicion = i;
        }
    }
    
    return posicion;
}

void SecuenciasKmer::ordenar(){
    Kmer burbuja;
    
    for(int j=0; j<getSize()-1; j++){
        for(int i=j+1; i<getSize(); i++){
            if(_conjunto[j].getFrecuencia() < _conjunto[i].getFrecuencia()){
                burbuja = _conjunto[i];
                _conjunto[i] = _conjunto[j];
                _conjunto[j] = burbuja;
            }
        }  
    }
}

bool SecuenciasKmer::saveFichero(const char* fichero) const{
    ofstream fout;
    string cadena;
    int frecuencia;
    bool success = true;
    
    fout.open(fichero);
    if(fout){
        fout << MAGIC << endl;
        
        fout << *this;
             
        if(fout){
            cerr << "\nSe han guardado " << _nKmer << " kmers correctamente en " << fichero << "." << endl;
        }           
        else{
            success = false;
            cerr << "ERROR en la escritura de datos" << endl;
        }           
    }
    
    else{
        cerr << "ERROR en la apertura del fichero de salida" << endl;
        success = false;
    }          
    fout.close();   
    
    return success;
}

bool SecuenciasKmer::loadFichero(const char* fichero){
    bool success = true;
    ifstream fin;
    
    Kmer km;
    
    string magic;  
    string cadena;
    int frecuencia;
    
    
    fin.open(fichero);
    
    if(fin){
        fin >> magic;

        if(fin){
            if(magic == MAGIC){
                fin >> *this;
                if(!fin){
                    cerr << "ERROR añadiendo los Kmers del fichero " << fichero << "." << endl;
                    success = false;
                }
                else
                    cout << endl << "Datos de " << fichero << " leídos correctamente." << endl;
                    
            }
            else{
                cerr << "La palabra mágica del fichero " << fichero << " no es correcta." << endl;
                success = false;
            }                 
        }
        else{
            cerr << "ERROR en la lectura de la palabra mágica del fichero " << fichero << endl;
            success = false;
        }
        fin.close();
    }
    else{
        cerr << "ERROR en la apertura del archivo " << fichero << endl;
        success = false;
    }
        
    return success;   
}


void SecuenciasKmer::deletePosicion(long p){
    if(0<=p && p<=_nKmer){
        int contador = 0;
        Kmer *resize = new Kmer[_nKmer-1];
        
        for(int i=0; i<_nKmer; i++){
            if(i != p){
                resize[contador] = _conjunto[i];
                contador++;
            }     
        }
        
        delete [] _conjunto;
        _conjunto = resize;

        _nKmer--;
    }
    else
        cerr << "Posición incorrecta.";
        
}

void SecuenciasKmer::reservarMemoria(long n){
    if(_conjunto == NULL){
        _conjunto = new Kmer[n];
    }
    else{
        liberarMemoria();
        _conjunto = new Kmer[n];
    }
}

void SecuenciasKmer::liberarMemoria(){
    if(_conjunto != NULL){
        delete [] _conjunto;
        _conjunto = {NULL};
    }    
}

void SecuenciasKmer::copiar(const SecuenciasKmer& otro){
    int cantidad;
    
    cantidad >> otro.getSize();
    
    if(cantidad > 0){
        delete [] _conjunto;
        
        _nKmer = cantidad;
        _conjunto = new Kmer[cantidad];
        
        for(int i=0; i<cantidad; i++)
            _conjunto[i] = otro.getPosicion(i);       
    }
    else
        cerr << "Secuencia de Kmers no válida.";
}

std::istream & operator>>(std::istream & is, SecuenciasKmer & i){
    int cantidad;
    int frec;
    std::string cadena;
    Kmer km;
    
    is >> cantidad;
    
    if(cantidad > 0){
        i._nKmer = cantidad;
        i.reservarMemoria(cantidad);
        
        for(int j=0; j<cantidad; j++){
            is >> cadena >> frec;
            km.setCadena(cadena);
            km.setFrecuencia(frec);
            i._conjunto[j]=km;;
        }
    }
    
    return is;
}

std::ostream & operator<<(std::ostream & os, const SecuenciasKmer & i){
    int cantidad;
    int frec;
    std::string cadena;
    cantidad = i._nKmer;
    
    if(cantidad > 0){
        os << i._nKmer << std::endl;
        
        for(int j=0; j<cantidad; j++){
            cadena = i._conjunto[j].getCadena();
            frec = i._conjunto[j].getFrecuencia();
            os << cadena << " " << frec << std::endl;
        }
            
    }
    
    return os;
    
}