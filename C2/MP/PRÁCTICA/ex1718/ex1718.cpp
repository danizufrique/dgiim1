#include <iostream>
#include <string>
#include <fstream>
using namespace std;

void leerBigramas(char* fichero, string* &bg1, string* &bg2, int &cantidad1, int &cantidad2){
	ifstream fin;
	int cantidad;
	string bg;
	
	fin.open(fichero);
	if(fin){
		fin >> cantidad;
		if(fin){
			cantidad1=cantidad;
			bg1 = new string[cantidad];
			for(int i=0; i<cantidad; i++){
				fin >> bg;
				bg1[i] = bg;
			}
		}
		fin >> cantidad;
		if(fin){
			cantidad2=cantidad;
			bg2 = new string[cantidad];
			for(int i=0; i<cantidad; i++){
				fin >> bg;
				bg2[i] = bg;
			}
		}
	}
}

void mostrarBigrama(string* &bg, int n){
	for(int i=0; i<n; i++){
		cout << bg[i] << endl;
	}	
}

string * Union(string *bg1, int cantidad1, string *bg2, int cantidad2){
	int cantidad = cantidad1 + cantidad2;
	string *resultado = new string[cantidad];
	int contador = 0;
	
	for(int i=0; i<cantidad1; i++){
		resultado[contador] = bg1[i];
		contador++;
	}
	for(int i=0; i<cantidad2; i++){
		resultado[contador] = bg2[i];
		contador++;
	}
	
	return resultado;
}


string * Interseccion(string *bg1, int cantidad1, string *bg2, int cantidad2, int &cantidad){
	// Asumimos que no hay elementos repetidos. En otro caso habria que que eliminarlos con otra funcion.
	
	cantidad = 0;
	string bgActual;
	string * resultado = nullptr;
	string * aux = nullptr;
	bool encontrado = false;
	
	for(int i=0; i<cantidad1; i++){
		bgActual = bg1[i];
		encontrado = false;
		for(int j=0; j<cantidad2 && !encontrado; j++){
			
			if(bgActual == bg2[j]){
				aux = resultado;
				
				if(resultado != nullptr)
					delete [] resultado;
				
				cantidad++;
				resultado = new string[cantidad];
				
				for(int k=0; k<cantidad-1; k++){
					resultado[k] = aux[k];
				}
				resultado[cantidad-1] = bgActual;
				
				encontrado = true;
			}
		}
	}
	
	return resultado;
}

int main(){
	
	string * bg1 = nullptr;
	string * bg2 = nullptr;
	string * _union = nullptr;
	string * _interseccion = nullptr;
	
	int cantidad1, cantidad2, cantidadInter;
	char fichero[] = "bigramas";
	
	
	// ENTRADA
	leerBigramas(fichero, bg1, bg2, cantidad1, cantidad2);
	
	// MOSTRAR DATOS
	cout << "BIGRAMA 1: " << endl;
	mostrarBigrama(bg1, cantidad1);
	cout << "BIGRAMA 2: " << endl;
	mostrarBigrama(bg2, cantidad2);
	
	// UNIÓN
	cout << "UNIÓN: " << endl;
	_union = Union(bg1, cantidad1, bg2, cantidad2);
	mostrarBigrama(_union, cantidad1+cantidad2);
	
	// INTERSECCIÓN
	_interseccion = Interseccion(bg1, cantidad1, bg2, cantidad2, cantidadInter);
	mostrarBigrama(_interseccion, cantidadInter);
	
}
