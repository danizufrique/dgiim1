
/**
 * @file examen1718.cpp
 * @author Daniel Zufrí Quesada
*/

#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;

struct Punto{
	private:
		int x;
		int y;
		
	public:
		Punto(){
			x=0;
			y=0;
		}
		
		Punto(int a, int b){
			x=a;
			y=b;
		}
		
		void setPunto(int a, int b){
			x=a;
			y=b;		
		}
		
		void mostrarPunto(){
			cout << "(" << x << ", " << y << ")" << endl;
		}
		
		int getX(){return x;}
		int getY(){return y;}
};

void anadirPunto(Punto* &puntos, Punto punto, int &cantidad){
	Punto * aux = new Punto[cantidad];
	
	// Copio en aux
	for(int i=0; i<cantidad; i++){
		aux[i] = puntos[i];
	}
	
	// Libero y reservo memoria
	delete [] puntos;
	cantidad++;
	puntos = new Punto[cantidad];
	
	// Añado el punto
	for(int i=0; i<cantidad-1; i++){
		puntos[i] = aux[i];
	}
	puntos[cantidad-1] = punto;
	
}

void leerPuntos(char* fichero, Punto* &puntos, int &cantidad){
	ifstream fin;
	Punto punto;
	int x,y;
	fin.open(fichero);
	
	if(fin){
		fin >> cantidad;
		if(fin){
			puntos = new Punto[cantidad];
			for(int i=0; i<cantidad; i++){
				fin >> x >> y;
				punto.setPunto(x,y);
				puntos[i] = punto;
			}
		}
		else
			cerr << "Error en la lectura del número de puntos";		
	}
	else
		cerr << "Error en la apertura del archivo";
	
	fin.close();
}

void separarPuntos(Punto* &dentro, int &cantidadDentro, Punto* &fuera, int &cantidadFuera, Punto* puntos, int cantidadTotal, float radio, Punto origen){
	float modulo;
	float diferenciaX, diferenciaY;
	
	for(int i=0; i<cantidadTotal; i++){
		diferenciaX = puntos[i].getX() - origen.getX();
		diferenciaY = puntos[i].getY() - origen.getY();
		
		modulo = sqrt(diferenciaX*diferenciaX + diferenciaY*diferenciaY);
		
		if(modulo <= radio){
			anadirPunto(dentro, puntos[i], cantidadDentro);
		}
		else{
			anadirPunto(fuera, puntos[i], cantidadFuera);
		}
		
		
	}
	
	
}

void guardarPuntos(Punto* dentro, int cantidadDentro, Punto* fuera, int cantidadFuera, char* fichero){
	
	ofstream fout;
	
	fout.open(fichero);
	
	if(fout){
		fout << "DENTRO: " << cantidadDentro << " puntos." << endl;
		for(int i=0; i<cantidadDentro; i++){
			fout << "(" << dentro[i].getX() << ", " << dentro[i].getY() << ")" << endl;
		}
		fout << "FUERA: " << cantidadFuera << " puntos." << endl;
		for(int i=0; i<cantidadFuera; i++){
			fout << "(" << fuera[i].getX() << ", " << fuera[i].getY() << ")" << endl;
		}
	}
	else
		cerr << "Error en la escritura de los archivos";
		
	fout.close();	
}



int main(){
	char fichero[] = "data";
	char salida[] = "salida";
	Punto *puntos = nullptr;
	int cantidad = 0;
	
	float radio = 5;
	Punto *dentro = nullptr;
	Punto *fuera = nullptr;
	Punto origen(0,0);
	
	int cantidadDentro = 0, cantidadFuera = 0;
	
	// Leo datos del archivo;
	leerPuntos(fichero, puntos, cantidad);
	
	// Imprimo puntos
	cout << "__DATOS__" << endl;
	for(int i=0; i<cantidad; i++){
		puntos[i].mostrarPunto();
	}
	
	// Separo puntos
	separarPuntos(dentro, cantidadDentro, fuera, cantidadFuera, puntos, cantidad, radio, origen);
	
	// Imprimo puntos separados
	cout << "__DENTRO__" << endl;
	for(int i=0; i<cantidadDentro; i++){
		dentro[i].mostrarPunto();
	}
	cout << "__FUERA__" << endl;
	for(int i=0; i<cantidadFuera; i++){
		fuera[i].mostrarPunto();
	}
	
	// Guardo resultados en archivo de salida
	guardarPuntos(dentro, cantidadDentro, fuera, cantidadFuera, salida);
	
	
}




