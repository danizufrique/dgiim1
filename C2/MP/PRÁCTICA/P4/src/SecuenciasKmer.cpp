/** 
 * @file SecuenciasKmer.cpp
 * @author Daniel Zufrí Quesada
*/

#include <iostream>
#include <fstream>
#include <cmath>
#include <string>

#include "Kmer.h"
#include "SecuenciasKmer.h"

using namespace std;

const int K=5; /// Número de genes en el Kmer
const string VALIDOS="_atgc"; /// Caracteres vaĺidos

/**
 * @brief Calcula el número de combinaciones posibles de trigramas
 * @return Un entero largo
 */
long maxKmer()  {
    return pow(VALIDOS.size(),K);
}

/**
 * @brief Normaliza un Kmer. La longitud debe ser exactamente la fijada por @p K y cualquier carácter
 * ausente o nó @p VALIDO debe ser sustituido por el carácter comodín @p VALIDOS[0].
 * Igualmente, cualquier carácter @p VALIDO debe pasarse a minúscula. Finalmente, la frecuencia
 * debe ser >= 0
 * @param km El Kmer a normalizar
 * @return Una copia normalizada de @p km
 */
Kmer normalizaKmer(const Kmer & km)  {
    Kmer resultado;  
    
    string cadena = km.getCadena();
    string cadenaNor;
    int frecuencia = km.getFrecuencia();
    
    //Pone todo a minúscula
    for (int i = 0; i < cadena.length(); i++){
        if (cadena[i] >= 'A' && cadena[i] <= 'Z'){
            cadena[i] = cadena[i] + 32;
        }
    }
    
    //Elimina caracteres sobrantes
    if(cadena.size() > K){
        cadenaNor = cadena.substr(0,K);
        cadena = cadenaNor;
    }

    //Añade '_' para cadenas con longitud menor de K
    else if(cadena.size() < K){
        for(int i=cadena.size(); i<K; i++){
            cadena += "_";
        }
    }
    
    //Comprueba que los caracteres sean válidos, y si no los sustituye por '_'
    for(int i=0; i<=K; i++){
        bool valido = false;
        
        for(int j=1; j<VALIDOS.size() && valido == false; j++){         
            if(cadena[i] == VALIDOS[j]){
                valido = true;          
            }        
        }
        if(!valido){
            cadena[i] = '_';
        }
    }
    
    //Comprueba que la frecuencia sea válida
    if(frecuencia < 0){
        frecuencia = 0;
    }
    
    //Asigna datos
    resultado.setCadena(cadena);
    resultado.setFrecuencia(frecuencia);
    
    return resultado;
}

SecuenciasKmer::SecuenciasKmer(){
    _conjunto = nullptr;
    _nKmer = 0;
}

SecuenciasKmer::SecuenciasKmer(long nkm){
    _conjunto = nullptr;
    _nKmer = nkm;
    reservarMemoria(nkm);
}

SecuenciasKmer::~SecuenciasKmer(){
    delete [] _conjunto;
    _conjunto = nullptr;
}

Kmer SecuenciasKmer::getPosicion(long p) const{
    Kmer copy;
    
    if (p<getSize()){
        copy = _conjunto[p];
    }
    
    else{std::cout << "ERROR: posición no válida.";}
    
    return copy;
}

void SecuenciasKmer::setPosicion(long p, const Kmer& km){
    if (p<getSize()){
        _conjunto[p] = km;
    }
    else{std::cout << "ERROR: Posición no válida.";}
}

void SecuenciasKmer::deletePosicion(long p){   
    if(0<=p && p<=_nKmer){
        int contador = 0;
        Kmer *resize = new Kmer[_nKmer-1];
        
        for(int i=0; i<_nKmer; i++){
            if(i != p){
                resize[contador] = _conjunto[i];
                contador++;
            }     
        }
        
        liberarMemoria();
        _conjunto = resize;

        _nKmer--;
    }    
}

long SecuenciasKmer::findKmer(const std::string& km, long inicial) const{
    bool encontrado = false;
    long posicion = -1;
    
    for(int i=inicial; i<getSize() && encontrado == false; i++){
        if(_conjunto[i].getCadena() == km){
            encontrado = true;
            posicion = i;
        }
    }
    
    return posicion;
}

void SecuenciasKmer::ordenar(){
    Kmer burbuja;
    
    for(int j=0; j<getSize()-1; j++){
        for(int i=j+1; i<getSize(); i++){
            if(_conjunto[j].getFrecuencia() < _conjunto[i].getFrecuencia()){
                burbuja = _conjunto[i];
                _conjunto[i] = _conjunto[j];
                _conjunto[j] = burbuja;
            }
        }  
    }
}

void SecuenciasKmer::zipSecuenciasKmer(){  
    int posicion;
    int i=0;
    
    while(i < _nKmer){
        if(_conjunto[i].getFrecuencia() == 0){
            deletePosicion(i);
        }
        else{
            posicion = findKmer(_conjunto[i].getCadena(), i+1);
        
            while(posicion != -1){
                _conjunto[i].setFrecuencia(_conjunto[i].getFrecuencia() + _conjunto[posicion].getFrecuencia());
                deletePosicion(posicion);
                posicion = findKmer(_conjunto[i].getCadena(), i+1);
            } 
            i++;
        }        
    }
}

void SecuenciasKmer::readSecuenciasKmer(){
    int cantidad;
    string cadena;
    int frecuencia;
    Kmer km;
    Kmer resultado;
    
    cin >> cantidad;
    
    reservarMemoria(cantidad);
    
    cout << "\nLeyendo " << cantidad << " Kmers...";
    
    for(int i=0; i<cantidad; i++){
        cin >> cadena >> frecuencia;
        km.setCadena(cadena);
        km.setFrecuencia(frecuencia);
        resultado = normalizaKmer(km);
        _conjunto[_nKmer] = resultado;
        _nKmer++;
    }
}

void SecuenciasKmer::writeSecuenciasKmer(int frecmin) const{
    cout << "\nImprimiendo " << _nKmer << " kmers:" << endl;
    for(int i=0; i<_nKmer; i++){
        if(_conjunto[i].getFrecuencia() >= frecmin){
            _conjunto[i].writeKmer();
            std::cout << "\n";
        }
    }
}

bool SecuenciasKmer::saveFichero(const char* fichero) const{
    ofstream fout;
    string cadena;
    int frecuencia;
    bool success = true;
    Kmer normalizado;
    
    fout.open(fichero);
    if(fout){
        fout << MAGIC << endl;
        fout << _nKmer << endl;
        
        for(int i=0; i<_nKmer; i++){
            normalizado = normalizaKmer(_conjunto[i]);
            cadena = normalizado.getCadena();
            frecuencia = normalizado.getFrecuencia();
            fout << cadena << "\t" << frecuencia << endl;
        }
        
        
        if(fout){
            cout << "\nArchivos guardados correctamente en " << fichero << endl;
        }           
        else{
            success = false;
            cout << "Error en la escritura de datos" << endl;
        }           
    }
    
    else{
        cout << "Error en la apertura del fichero de salida" << endl;
        success = false;
    }
        
      
    fout.close();   
    
    return success;
}

bool SecuenciasKmer::loadFichero(const char* fichero){
    bool success = true;
    ifstream fin;
    
    Kmer km;
    Kmer resultado;
    
    string magic;  
    int cantidad;
    string cadena;
    int frecuencia;
    
    
    fin.open(fichero);
    
    if(fin){
        fin >> magic;

        if(fin){
            if(magic == "MP-KMER"){
                fin >> cantidad;
                if(fin){
                    cout << "Leyendo " << cantidad << " datos de " << fichero << "..." << endl;
                    reservarMemoria(cantidad);
                    
                    for(int i=0; i<cantidad && success; i++){
                        fin >> cadena >> frecuencia;

                        if(fin){
                            km.setCadena(cadena);
                            km.setFrecuencia(frecuencia);
                            resultado = normalizaKmer(km);
                            _conjunto[_nKmer] = resultado;
                            _nKmer++;
                        }
                        else{
                            cout << "Error en la lectura del Kmer " << i << "." << endl;
                            success = false;
                        }                            
                    }                             
                }  
                else{
                    cout << "Error en la lectura del número de Kmers." << endl;
                    success = false;
                }           
            }
            else{
                cout << "La palabra mágica no es correcta." << endl;
                success = false;
            }                 
        }
        else{
            cout << "Error en la lectura de la palabra mágica." << endl;
            success = false;
        }
        fin.close();
    }
    else{
        cout << "Error en la apertura del archivo." << endl;
        success = false;
    }
        
    return success;
}

void SecuenciasKmer::liberarMemoria(){   
    if(_conjunto != nullptr){
        delete [] _conjunto;
        _conjunto = nullptr;
    }   
}

void SecuenciasKmer::reservarMemoria(long n){
    if(_conjunto == nullptr){
        _conjunto = new Kmer[n];
    }
    else{
        liberarMemoria();
        _conjunto = new Kmer[n];
    }
}