/**
 * @file Kmer.cpp
 * @author Daniel Zufrí Quesada
 */
#include <string>
#include <iostream>
#include "Kmer.h"

using namespace std;

Kmer::Kmer(){
    _kmer = "";
    _frecuencia = 0;
}

Kmer::Kmer(const std::string& c, int f){
    _kmer = c;
    _frecuencia = f;
}

string Kmer::getCadena() const{
    return _kmer;
}

int Kmer::getFrecuencia() const{
    return _frecuencia;
}

void Kmer::setCadena(const std::string& c){
    _kmer = c;
}

void Kmer::setFrecuencia(int f){
    _frecuencia = f;
}       

void Kmer::readKmer(){   
    cin >> _kmer;
    cin >> _frecuencia;
}

void Kmer::writeKmer() const{
    cout << _kmer << "  " << _frecuencia;
}