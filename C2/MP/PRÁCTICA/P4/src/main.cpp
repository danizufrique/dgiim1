/**
 * @file main.cpp
 * @author Daniel Zufrí Quesada
 */
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include "Kmer.h"
#include "SecuenciasKmer.h"

using namespace std;

void mensajeError() {
    cerr << "ERROR en la llamada" << endl;
    cerr << "   kmer [-i <fichero-entrada>] [-o <fichero-salida>]" << endl;
    exit(1);
}
 
int main(int narg, char *args[]) {
    int cantidad;
    bool continua;
    string out="", in="";
    
    SecuenciasKmer seq;
    Kmer km;
    
    if(narg == 3){
        if(strcmp(args[1], "-i") == 0)
            in = args[2];
        else if(strcmp(args[1], "-o") == 0)
            out = args[2];
        else mensajeError();
        
    }
    else if(narg == 5){
        if(strcmp(args[1], "-i") == 0 && strcmp(args[3], "-o") == 0){
            in = args[2];
            out = args[4];
        } 
        else if(strcmp(args[1], "-o") == 0 && strcmp(args[3], "-i") == 0){
            in = args[4];
            out = args[2];
        }
        else
            mensajeError();
    }
    else if(narg != 1)
        mensajeError();
    
    if(in == "")
        seq.readSecuenciasKmer();
    else if(!seq.loadFichero(in.c_str()))
        return 1;
    

    seq.zipSecuenciasKmer();
    seq.ordenar();

    if(out == "")
        seq.writeSecuenciasKmer();
    else if(!seq.saveFichero(out.c_str()))
        return 1;
    
    
    return 0;
}

