/**
 * @file main.cpp
 * @author MP
 */
#include <iostream>
#include <string>
#include <cmath>
#include "Kmer.h"

using namespace std;

const int K=3; /// Número de genes en el Kmer
const string VALIDOS="_atgc"; /// Caracteres vaĺidos

long maxKmer();
Kmer normalizaKmer(const Kmer & km);
 
int main() {

    return 0;
}

/**
 * @brief Calcula el número de combinaciones posibles de trigramas
 * @return Un entero largo
 */
long maxKmer()  {
}

/**
 * @brief Normaliza un Kmer. La longitud debe ser exactamente la fijada por @p K y cualquier carácter
 * ausente o nó @p VALIDO debe ser sustituido por el carácter comodín @p VALIDOS[0].
 * Igualmente, cualquier carácter @p VALIDO debe pasarse a minúscula. Finalmente, la frecuencia
 * debe ser >= 0
 * @param km El Kmer a normalizar
 * @return Una copia normalizada de @p km
 */
Kmer normalizaKmer(const Kmer & km)  {
}