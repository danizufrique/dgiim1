#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;


bool loadFichero(char* fichero, string* &frutas){
	ifstream fin;
	int cantidad;
	string fruta;
	
	bool success = false;
	
	fin.open(fichero);
	
	if(fin){		
		fin >> cantidad;
		frutas = new string[cantidad];
		
		if(fin){
			for(int i=0; i<cantidad; i++){
				fin >> fruta;
				if(fin)
					frutas[i] = fruta;	
				if(i==cantidad)			
					success = true;
			}
		}
	}
	
	
	fin.close();
	
	return success;	
}

int getCantidad(char* fichero){
	ifstream fin;
	int cantidad;
	
	fin.open(fichero);
	
	if(fin)
		fin >> cantidad;
		
	fin.close();
	
	return cantidad;
}

void zip(string* &frutasUnion, int &cantidad){
	string frutaActual;
	string * resultado = nullptr;
	int contador;
	
	for(int i=0; i<cantidad-1; i++){
		frutaActual = frutasUnion[i];
		
		for(int j=i+1; j<cantidad; j++){
			
			if(frutaActual == frutasUnion[j]){
				contador = 0;
				
				if(resultado != nullptr)
					delete [] resultado;
					
				resultado = new string[cantidad-1];
				
				for(int k=0; k<cantidad; k++){
					if(k!=j){
						resultado[contador] = frutasUnion[k];
						contador++;
					}
				}
				cantidad--;
				
				delete [] frutasUnion;
				frutasUnion = resultado;
			}					
		}
	}
	
		
	
}

int main(){
	string * frutasA = nullptr;
	string * frutasB = nullptr;
	string * frutasUnion = nullptr;
	
	char ficheroA[] = "ficheroA";
	char ficheroB[] = "ficheroB";
	
	int cantidadA, cantidadB, cantidad;
	
	cantidadA=getCantidad(ficheroA);
	cantidadB=getCantidad(ficheroB);
	cantidad = cantidadA + cantidadB;
	
	loadFichero(ficheroA, frutasA);
	loadFichero(ficheroB, frutasB);
	
	
	// Unión
	cantidad = cantidadA+cantidadB;
	frutasUnion = new string[cantidad];
	
	for(int i=0; i<cantidadA; i++){
		frutasUnion[i] = frutasA[i];
	}
	for(int i=cantidad-cantidadB; i<cantidad; i++){
		frutasUnion[i] = frutasB[i-cantidadA];
	}
	
	
	// Muestra listas originales
	cout << "FICHERO A" << endl;
	for (int i = 0; i < cantidadA; i++)
	{
		cout << frutasA[i] << endl;
	}
	cout << endl << "FICHERO B" << endl;
	for (int i = 0; i < cantidadB; i++)
	{
		cout << frutasB[i] << endl;
	}
	
	
	// Zip
	zip(frutasUnion, cantidad);
	
	// Resultado
	
	cout << endl << "__RESULTADO__" << endl << "Hay " << cantidad << " elementos:" << endl;
	for(int i=0; i<cantidad; i++){
		cout << frutasUnion[i] << endl;
	}
		
}
