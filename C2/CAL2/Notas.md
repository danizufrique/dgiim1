# Notas

## Taylor

- Un número a es una raíz de un polinomio p con multiplicidad $k$ si, y
  sólo si, $p(a) = p'(a) = \cdots = p^{(k-1)}(a) = 0$ y $p^{(k)}(a) \neq 0$.
- $P'_n (f, x) = P_{n-1}(f', x)$.
- **Fórmula infinitesimal del resto:** $\lim_{x \rightarrow a}\frac{f(x)-P_n(x)}{(x-a)^n}=0$.
- $\lim_{x \rightarrow a}\frac{f(x)-P_{n-1}(x)}{(x-a)^n}=\frac{f^{(n)}}{n!}$.
- El polinomio de *Maclaurin* de una función par (resp. impar) sólo tiene términos con potencias pares (resp. impares).
- **Resto de Taylor:** $f(x)-P_n(x)=\frac{f^{(n+1)}(c)}{(n+1)!}(x-a)^{n+1}$.
- **Resto de Cauchy:** $f(x)-P_n(x)=\frac{f^{(n+1)}(c)}{n!}(x-d)^{n}(x-a)$.
- $f\in C^{\infty} \subset I, \vert f^{(n)}(x)\vert \le M \Rightarrow$ Taylor representa a $f$.

## Convexas

- $f$ función convexa $\Rightarrow f$ está acotada.
- $f_a(x)=\frac{f(x)-f(a)}{x-a}$ es creciente.
- $f'(a)\le f_a(x) = f_x(a) \le f_x(b) = f_b(x) \le f'(b)$.
- El conjunto de puntos donde $f$ no es derivable es numerable.
- $f'$ es continua.
- $f(x) \ge f(a)+f'(a)(x-a)$.

## Continuidad uniforme

- **Teorema de Heine:** $f$ función continua $\Rightarrow f$ es uniformemente continua.
- $f$ es lips. si $\exists K$ tal que $\vert f(x)-f(y)\vert \le K \vert x-y\vert$.
- $f \in C^1 \Rightarrow f$ es lips.
- $f'$ acotada $\Rightarrow f$ lips.

 ## Riemann

- $f$ acotada. $f$ integrable $\iff \forall \varepsilon ,\exist P: I-\varepsilon<I(f,P)\le I \le S(f,P) < I+\varepsilon$.

- $f$ acotada. $f$ integrable $\iff \lim_{n\rightarrow \infty} (S(f,P_n)-I(f,P_n))=0$.

- Las funciones continuas son integrables.

- Las funciones monótonas son integrables.

- $\sum(f,P)=\sum_{i=1}^n f(t_i)(x_i-x_{i-1})$.

- $I(f,P)\le \sum(f,P) \le S(f,P)$.

- $f$ acotada. $f$ integrable $\Rightarrow \vert \sum(f,P)-I\vert < \varepsilon, \Vert P \Vert <\delta$.

  

  ## Teorema Fundamental del Cálculo

- $f:[a,b] \rightarrow \R$ integrable. $F(x)=\int_a^xf(t)dt$.

  - $F$ continua
  - Si $f$ continua en $c \in [a,b]$, $F$ derivable y $F'(c) = f(c)$

- $f: J \rightarrow \R$ continua. $g,h: I \rightarrow \R$ derivables con $g(I), h(I) \subset J$. Entonces

  $F: I \rightarrow \R$, $F(x)=\int_{g(x)}^{h(x)}f(t)dt$

- $F$ derivable y $F'(x)=f(h(x))·h'(x)-f(g(x))·g'(x)$

- Toda función continua tiene una primitiva.

- **Regla de Barrow:** $f: [a,b] \rightarrow \R$ integrable y $F$ una primitiva: $\int_a^bf(x)dx=F(b)-F(a)$

- $f: [a,b] \rightarrow \R$ integrable y $F$ una primitiva. Entonces $G(x)$  cte: $G(x)=F(x)-\int_a^xf(t)dt$.

* **Integración por partes:** $F,G: [a,b] \rightarrow \R$ derivables. $FG'$ integrable si y solo si lo es $F'G$. Entonces: $\int_a^bF(x)G'(x)dx=F(b)G(b)-F(a)G(a)-\int_a^bF'(x)G(x)dx$.

* **Cambio de variable:** $g:[a,b] \rightarrow \R$ derivable en el intervalo, con $g'$ integrable. $I$ intervalor tal que $g([a,b]) \subseteq I$ y $f: I \rightarrow \R$ continua, $(f \circ g)g'$ integrable en $[a,b]$ y: $\int_a^b(f\circ g)g'=\int_{g(a)}^{g(b)}f=F(g(b))-F(g(a))$.



* **Teorema del valor medio** (1er teorema): Sean:
  * $f,g: [a,b] \rightarrow \R$ integrables.
  * Supongamos $g(x) \ge 0$ $\forall x\in[a,b]$.
    * $\exist \mu\in [\inf f([a,b]), \sup f([a,b])]$ tal que:

$$
\int_a^bf(x)g(x)dx= \mu\int_a^bg(x)dx
$$

* **Consecuencia del T.V.M** (1er Teorema):

  * $f:[a,b] \rightarrow \R$ continua.
  * Existe $c \in [a,b]​$ tal que:

  $$
  \int_a^bf(x)dx=f(c)(b-a)
  $$

* **Teorema del valor medio** (2o teorema): Sean:

  * $f,g: [a,b] \rightarrow \R$ integrables.
  * Supongamos $g$ monótona.
    * $\exist c \in [a,b]$ tal que:

  $$
  \int_b^af(x)g(x)dx=g(a)\int_a^cf(x)dx+g(b)\int_c^bf(x)dx
  $$

* **Consecuencia del T.V.M** (2o Teorema):

  * $f,g: [a,b] \rightarrow \R$ integrables.

    

  * Caso 1:

    * $g$ decreciente y $g(x) \ge 0$ para cualquier $x$, existe $c \in [a,b]$ tal que

    $$
    \int_a^bf(x)g(x)dx=g(a)\int_a^cf(x)dx
    $$

  * Caso 2:

    * $g$ creciente y $g(x) \ge 0$ para cualquier $x$, existe $c \in [a,b]$ tal que

    $$
    \int_a^bf(x)g(x)dx=g(b)\int_c^bf(x)dx
    $$

    ## Derivadas

    

| Función      | Derivada                             |
| ------------ | ------------------------------------ |
| $senh(x)$    | $cosh(x)$                            |
| $cosh(x)$    | $senh(x)$                            |
| $tgh(x)$     | $sech^2(x)$                          |
| $cotgh(x)$   | $-cosech^2(x)$                       |
| $arcsenh(x)$ | $\frac{1}{\sqrt{x^2+1}}$             |
| $arccosh(x)$ | $\frac{1}{\sqrt{x^2-1}}, \quad x >1$ |
| $tg(x)$      | $\frac{1}{cos^2(x)}=sec^2(x)$        |
| $arctg(x)$   | $\frac 1 {1+x^2}$                    |
| $arcsen(x)$  | $\frac{1}{\sqrt{1-x^2}}$             |
| $arccos(x)$  | $\frac{-1}{\sqrt{1-x^2}}$            |
|              |                                      |

## Identidades

- $sec(x)=\frac 1 {cos(x)}​$
- $sin^2 (a) + cos^2 (a) = 1$
- $cosh^2(x)-sinh^2(x)=1$
- $cos^2(x)=\frac 1 2+\frac 1 2cos(2x)$
- $sin^2(x)=\frac 1 2-\frac 1 2cos(2x)​$
- $sec^2 (\alpha ) = 1+tg^2 (\alpha )$
- $cosec^2 (\alpha ) =1 + cotg^2 (\alpha )​$
- $sin (-\alpha ) = - sin(\alpha )$
- $cos (- \alpha ) = cos (\alpha )$
- $sin(a\pm b)=sin(a)cos(b)\pm cos(a)sin(b)$
- $cos(a\pm b)=cos(a)cos(b)\mp sin(a)sin(b)$
- $sin(2a)=2sin(a)cos(a)$
- $cos(2a)=cos^2(a)-sin^2(a)$
- $sin(a)cos(b)=\frac 1 2 (sin(a+b)+sin(a-b))$
- $cos(a)cos(b)=\frac 1 2 (cos(a+b)+cos(a-b))$
- $sin(a)sin(b)=\frac 1 2 (cos(a+b)-cos(a-b))$