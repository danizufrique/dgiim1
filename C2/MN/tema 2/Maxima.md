# MÁXIMA
## Tema 1
### Comandos Básicos

~~~~mathematica
➔ %on (*Hace referencia al resultado n*)
➔ print
~~~~

### Operaciones Algebráicas Básicas

#### Redondeo Decimal

Podemos trabajar con la expresión decimal, bien como acabamos de hacer introduciendo alguno de los números en forma decimal, bien mediante la sentencia ***float***, que genera redondeos decimales de números reales. Con ***bfloat*** podemos modificar el número de cifras significativas (b denota 10^).

~~~mathematica
➔ float(sqrt(2));
➔ float(%e);

➔ fpprec : 20;
➔ bfloat(%pi);
➔ bfloat(%e);
~~~

#### Asignación de valores

- Para asignar el valor a una variable se utilizan los ***:*** .
- Para eliminar el valor asignado a una variable o función se usa la orden ***kill***.
- El argumento ***all*** en esta sentencia elimina los valores que tengan asignados todas las variables que se hayan utilizado previamente.

~~~~mathematica
➔ x:3;
➔ kill(x);
~~~~

### Funciones Matemáticas

#### Elementales

~~~~mathematica
➔ sin(%pi);
➔ cos(1.0);
➔ tan(%pi/4);
➔ asin(1);
➔ acos(−1);
➔ atan(sqrt(3));
➔ sqrt(2.0);
➔ log(%e);
➔ exp(0.98);
➔ abs(−3.4);
➔ abs(3−%i);
~~~~

#### Definición y comandos básicos

~~~~mathematica
➔ f(x):=−x^2+%e^x+3; (*Definicion de funcion*)
➔ f(1) (*Evaluar funcion*)
➔ diff(f(x),x,2); (*Derivar dos veces*)
➔ integrate(f(x),x,0,2); (*Integral definida entre 0 y 2*)
➔ integrate(f(x),x); (*Integral indefinida*)
➔ wxplot2d([f(x)],[x,0,2]); (*Grafica en editor*)
➔ plot2d([f(x)],[x,0,2],[plot_format,gnuplot]); (*Grafica en ventana indipendiente*)

~~~~

Para definir funciones a trozos:

~~~~mathematica
➔ g(x):=if x<0 then x^2+2·x else sin(x+1);
➔ h(x):=if x<1 then 2·x else
	if x<=2 then x^3−5 else log(x) ;
~~~~

Aunque la derivación e integración hay que realizarlas por trozos, se puede evaluar en un punto cualquiera.

### Vectores



~~~~mathematica
➔ a:[3,7,−5/9]; (*Definicion*)
➔ length(a); (*Obtener su longitud*)
➔ a[2]; (*Acceder a elemento*)

➔ b:makelist(i^2,i,1,10); 
[1,4,9,16,25,36,49,64,81,100]

➔ [1,2,1].[3,2,1]; (*Producto escalar*)

➔ apply("+",b); (*Sumar vectores*)
➔ apply("·",b); (*Multiplicar vectores*)
➔ apply(max,b); (*Obtener maximo del vector*)
~~~~

### Matrices



~~~~mathematica
(*Definicion*)
➔ c:matrix([1,2,3],[4,5,6]); 

➔ ident(6); //Identidad de orden 6

➔ matrix_size(c);
➔ c[1,2];
➔ c[1];
➔ d: genmatrix(lambda([i,j], i−j/4), 3, 4);
~~~~

Para ciertos tipos de matrices, como las simétricas, es mejor introducirlas desde el menú del Álgebra.

#### Operaciones con Matrices

~~~~mathematica
➔ m+n; 
➔ 3·n; (*Matriz por escalar*)
➔ n.o; (*Producto matrices*)
➔ transpose(o);
➔ transpose((4·m−6·n).o);
➔ m^^2;
➔ c.a; (*Matriz por vector*)

➔ rank(n);
➔ determinant(n);
➔ invert(n); (*Matriz inversa*)
➔ n^^(−1); (*Matriz inversa*)
➔ n^(-1); (*Inverso de los elementos de la matriz*)
~~~~

Además, existen otros comandos útiles, tales como:

~~~~mathematica
➔ c:entermatrix(2,3);
Row 1 Column 1: 1;
Row 1 Column 2: 2;
Row 1 Column 3: 4;
Row 2 Column 1: -1;
Row 2 Column 2: 0;
Row 2 Column 3: 2;

➔ diagmatrix(4,2); //Crea una matriz con 2 en la diagonal
➔ nullspace(matrix([1,2,4],[-1,0,2])); (*Da una base del núcleo de la matriz*)
➔ triangularize(m); (*Aplica Gauss-Jordan a la matriz*)
➔ minor(m,2,1); (*Menor que se obtiene al eliminar la segunda fila y la primera columna de la matriz m*)
➔ submatrix(2,m,1,4); (*Elimina la fila 2 y las columnas 1 y 4 de la matriz m*)
➔ col(m,2); (*Extraer columna*)
➔ row(m,1); (*Extraer fila*)
➔ matrixp(m); (*Devuelve true si m es una matriz*)
~~~~

#### Valores y Vectores propios

~~~~mathematica
➔ eigenvalues(e); (*Valores propios de la matriz cuadrada*)
[[1-sqrt(2),sqrt(2)+1,1/2],[1,1,1]] (*[Valores][Multiplicidad]*)

➔ eigenvectors(e);
[[[1-sqrt(2),sqrt(2)+1,1/2],[1,1,1]],[[[1,-(69*2^(3/2)+120)/47,(15*2^(5/2)+138)/47]],[[1,(69*2^(3/2)-120)/47,-(15*2^(5/2)-138)/47]],[[1,0,0]]]] 
(*Al final se añade la base de vectores propios*)
~~~~

### Rudimentos de Programación



~~~~mathematica
➔ if determinant(f)=0 then print("singular") else print("regular");
~~~~

También pueden usarse bucles:

~~~~mathematica
➔ for i:1 step 1 thru 6 do print("el cubo de",i,"es ",i^3);
1,8,27,64,125,216
(*Terminar tras 6 iteraciones*)

➔ for i:1 thru 3 do (print((i^3)/4.0),print(i));
(*Si el paso es 1*)

➔ for i:2 while i<16 do display(i^2);
(*Terminar si se cumple condición*)
~~~~



**<u>Ejemplo:</u>** Calcular la suma de los primeros 100 números naturales.

~~~~mathematica
➔ s:0; 
(*No es necesario declarar la variable*)

➔ for i:1 thru 100 do s:s+i;
➔ s;
5050

(*Otra forma*)
➔ y:makelist(i,i,1,100);
➔ s:apply("+",y);
➔ s;
5050
~~~~

Otros operadores interesantes son:

~~~~mathematica
➔ sum(2*i,i,1,3);
12
(* 2·1 + 2·2 + 2·3 *)

➔ product(i,i,1,8);
40320
~~~~



**<u>Ejemplo:</u>** Calcular la norma del máximo de una matriz cuadrada cualquiera.

~~~~mathematica
➔ a: 1/10.0·genmatrix(lambda([i,j], (−1)^(i+j)·i/j), 5, 5);
➔ n:matrix_size(a)[1];
➔ v:makelist(apply("+",abs(a[i])),i,1,n);
➔ apply(max,v);
~~~~

​	Haciéndolo en forma de módulo:

~~~~mathematica
➔ normmax(b):=block(apply(max,makelist(apply("+",abs(b[i])),i,1,
	matrix_size(b)[1])));
~~~~

​	Para comprobar la solución:

~~~~mathematica
➔ normmax(a);
~~~~

### Ejercicios

> **1.-** Define la función real g en el intervalo [0,1], calcula su integral y dibuja su gráfica como:
> 		2x-log(x),	 si 	0.3≤x≤0.5
> g(x)=	2/x+|x-0.6|, 	si 	0.5≤x≤0.8
> 		0, 				en otro caso.



> **2.-** Halla el radio espectral de la matriz de orden 4x4 cuyo coeficiente (i,j) es |2i-4j|.

> **3.-** Calcula la suma de los cubos de los 23 primeros números naturales mediante un adecuado bucle. Comprueba que el resultado obtenido es correcto mediante el comando *apply*.

> **4.-** Halla el producto de los inversos de los números naturales comprendidos entre 6 y 19 haciendo uso de un bucle, y comprueba la validez de tu respuesta con la orden *apply*.

> **5.-** Determina el término 43º de la sucesión de Fibonacci de forma recurrente y a partir de la expresión explícita de dicho término.

> **6.-** Calcula, aplicando el comando *float*, el valor de √(5+10^-n)-√5, para n=1,...,20. Repite los cálculos, reescribiendo la expresión √(5+10^-n)-√5 como 10^(-n)/(√(5+10^-n)+√5). Interpreta los resultados.

> **7.-** Programa (no necesariamente módulo) el cálculo de la norma 1 de una matriz cuadrada cualquiera.

> **8.-** Diseña un programa que, a partir de una matriz cuadrada, genere como salida su condicionamiento (norma infinito) si es regular, o el mensaje "la matriz no es regular" en caso contrario.

> **9.-** Calcula la norma euclídea de la matriz 2x4 cuyo coeficiente (i,j) sea i/(i+j+1).

## Tema 2

### Métodos Directos

Para resolver un sistema de ecuaciones lineales podemos usar ***linsolve***:

~~~~mathematica
➔ linsolve([2·x+y−3·z=0, x+y+z=1,x−y+9·z=−2], [x,y,z]);
➔ linsolve([2·x+y−3·z=0, x+y+z=1,x+y+z=11], [x,y,z]);
➔ linsolve([2·x+y−3·z−7·w−t=1, x+y+z+w+t=1,2.5·x+1.5·y−2.5·z−7.5·w=0.5], [x,y,z,w,t]);
~~~~

Maxima cuenta con el comando ***lu_factor***, que aplicado a una matriz regular a da como salida su factorización LU tipo Doolittle. 

- El <u>**primer argumento**</u> de la salida da una matriz *s* que, de forma compacta, no es más que la factorización LU de la matriz *a*: 
  - *L* es la <u>matriz triangular inferior</u> con 1's en la diagonal principal y con la parte inferior de *s*.
  - *U* es la <u>matriz triangular superior</u> con la diagonal principal de *s* y la parte superior de *s*. 
- El <u>**segundo argumento**</u> indica la eventual permutación de filas.

~~~~mathematica
➔ a:matrix([2,2,1,2],[4,6,1,3],[6,12,1,3],[2,4,−1,−1]);
matrix(
		[2,	2,	1,	2],
		[4,	6,	1,	3],
		[6,	12,	1,	3],
		[2,	4,	-1,	-1]
	)
➔ lu_factor(a);
[matrix(
		[2,	2,	1,	2],
		[2,	2,	-1,	-1],
		[3,	3,	1,	0],
		[1,	1,	-1,	-2]
	),[1,2,3,4],generalring]
~~~~

Es decir, la matriz a admite la factorización tipo Doolittle:

~~~~mathematica
➔ l:matrix([1, 0, 0, 0],[2, 1,0,0],[3,3,1,0],[1,1,−1,1]);
matrix(
		[1,	0,	0,	0],
		[2,	1,	0,	0],
		[3,	3,	1,	0],
		[1,	1,	-1,	1]
	)
➔ u:matrix([2, 2, 1, 2],[0, 2, −1,−1],[0,0,1,0],[0,0,0,−2]);
matrix(
		[2,	2,	1,	2],
		[0,	2,	-1,	-1],
		[0,	0,	1,	0],
		[0,	0,	0,	-2]
	)
~~~~

Por su parte, la sentencia ***lu_backsub*** (lu_factor) devuelve la solución del sistema $ax=b​$ a partir de la factorización LU de a que acabamos de obtener:

~~~~mathematica
➔ b:transpose([1,−2,3,−4]);
matrix(
		[1],
		[-2],
		[3],
		[-4]
	)
	
➔ lu_backsub(lu_factor(a),b);
([matrix(
		[2,	2,	1,	2],
		[2,	2,	-1,	-1],
		[3,	3,	1,	0],
		[1,	1,	-1,	-2]
	),[1,2,3,4],generalring],matrix(
		[1],
		[-2],
		[3],
		[-4]
	))
~~~~

### Ejercicios

>1.- Programa la resolución de un sistema triangular superior compatible determinado. Aplícalo al sistema de matriz de coeficientes
~~~~mathematica
➔ matrix([0.34,−1.99,2/7,0],[0,1.1,2.3,−3.57],[0,0,3.2,33],
[0,0,0,66.72]);
~~~~
>y vector de términos independientes
~~~~mathematica
➔ [1,34,78,−9.42].;
~~~~


>2.- Programa el método de Gauss y úsalo para resolver el sistema con matriz de coeficientes

~~~~mathematica
➔ matrix([0.24,1.1,3/2,3.45],[−1.2,1,3.5,6.7],[33.1,1,2,−3/8],[4,17,71,−4/81]);
~~~~
>y vector de términos independientes
~~~~mathematica
➔ [1,2,4,−21/785].;
~~~~


>3.- Programa el método de Crout y aplícalo para encontrar la solución del sistema con matriz de coeficientes y vector de términos independientes, respectivamente
~~~~mathematica
➔ matrix([3,6,9],[1,4,11],[0,4,19]);
~~~~
>y
~~~~mathematica
➔ [1/2,−2/3,−3/4].;
~~~~


>4.- Programa los métodos de Jacobi y Gauss-Seidel y aplícalos, partiendo de la iteración inicial
~~~~mathematica
➔ [1,−1.34,1.456];
~~~~

>y realizando 15 iteraciones, para obtener una aproximación de la solución del sistema con matriz de coeficientes
~~~~mathematica
➔ matrix([3,−2,0.25],[2,9,−5],[2,3,−6]);
~~~~
>y vector de términos independientes
~~~~mathematica
➔ [1.1,2.2,3.3].;
~~~~

