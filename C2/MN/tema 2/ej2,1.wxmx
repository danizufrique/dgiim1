PK     ݣ�N�B�H         mimetypetext/x-wxmathmlPK     ݣ�N�T�D    
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/andrejv/wxmaxima.
It also is part of the windows installer for maxima
(http://maxima.sourceforge.net).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using an text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     ݣ�Na���h  h     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created by wxMaxima 18.02.0   -->
<!--https://andrejv.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="110" activecell="11">

<cell type="code">
<input>
<editor type="input">
<line>A:matrix([4,4,2],[4,17/2,3],[2,3,2436/25]);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="A">(%o10) </lbl><tb><mtr><mtd><n>4</n></mtd><mtd><n>4</n></mtd><mtd><n>2</n></mtd></mtr><mtr><mtd><n>4</n></mtd><mtd><f><r><n>17</n></r><r><n>2</n></r></f></mtd><mtd><n>3</n></mtd></mtr><mtr><mtd><n>2</n></mtd><mtd><n>3</n></mtd><mtd><f><r><n>2436</n></r><r><n>25</n></r></f></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>N:matrix_size(A)[1];</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="N">(%o2) </lbl><n>3</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>U:ident(N);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="U">(%o4) </lbl><tb><mtr><mtd><n>1</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>1</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>1</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>for j:1 thru N do(</line>
<line>    for i:1 thru j-1 do(</line>
<line>        U[i,j]:(1/U[i,i])*(A[i,j] - sum(U[k,i]*U[k,j],k,1,i-1))</line>
<line>    ),</line>
<line>    U[j,j]:sqrt(A[j,j] - sum(U[k,j]²,k,1,j-1))</line>
<line></line>
<line>);</line>
</editor>
</input>
<output>
<mth><lbl>(%o11) </lbl><v>done</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>U;</line>
</editor>
</input>
<output>
<mth><lbl>(%o12) </lbl><tb><mtr><mtd><n>2</n></mtd><mtd><n>2</n></mtd><mtd><n>1</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><f><r><n>3</n></r><r><q><n>2</n></q></r></f></mtd><mtd><f><r><q><n>2</n></q></r><r><n>3</n></r></f></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><f><r><q><n>21649</n></q></r><r><n>15</n></r></f></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>transpose(U).U;</line>
</editor>
</input>
<output>
<mth><lbl>(%o13) </lbl><tb><mtr><mtd><n>4</n></mtd><mtd><n>4</n></mtd><mtd><n>2</n></mtd></mtr><mtr><mtd><n>4</n></mtd><mtd><f><r><n>17</n></r><r><n>2</n></r></f></mtd><mtd><n>3</n></mtd></mtr><mtr><mtd><n>2</n></mtd><mtd><n>3</n></mtd><mtd><f><r><n>2436</n></r><r><n>25</n></r></f></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>transpose(U);</line>
</editor>
</input>
<output>
<mth><lbl>(%o14) </lbl><tb><mtr><mtd><n>2</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>2</n></mtd><mtd><f><r><n>3</n></r><r><q><n>2</n></q></r></f></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>1</n></mtd><mtd><f><r><q><n>2</n></q></r><r><n>3</n></r></f></mtd><mtd><f><r><q><n>21649</n></q></r><r><n>15</n></r></f></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>B:matrix([-0.5,0,0],[0,-0.06,-0.23],[0,-0.23,0.2]);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="B">(%o15) </lbl><tb><mtr><mtd><v>−</v><n>0.5</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><v>−</v><n>0.06</n></mtd><mtd><v>−</v><n>0.23</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><v>−</v><n>0.23</n></mtd><mtd><n>0.2</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>x:eigenvalues(B)[1];</line>
</editor>
</input>
<output>
<mth><t breakline="true" tooltip="Comúnmente las máquinas emplean números de punto flotante que pueden ser manipulados incrementalmente rápidas mientras">rat: replaced -0.0529 by -529/10000 = -0.0529</t><t breakline="true" tooltip="Comúnmente las máquinas emplean números de punto flotante que pueden ser manipulados incrementalmente rápidas mientras">rat: replaced -0.06 by -3/50 = -0.06</t><t breakline="true" tooltip="Comúnmente las máquinas emplean números de punto flotante que pueden ser manipulados incrementalmente rápidas mientras">rat: replaced 0.2 by 1/5 = 0.2</t><t breakline="true" tooltip="Comúnmente las máquinas emplean números de punto flotante que pueden ser manipulados incrementalmente rápidas mientras">rat: replaced -0.5 by -1/2 = -0.5</t><lbl userdefined="yes" userdefinedlabel="x">(%o22) </lbl><t>[</t><v>−</v><f><r><q><n>698</n></q><v>−</v><n>7</n></r><r><n>100</n></r></f><t>,</t><f><r><q><n>698</n></q><v>+</v><n>7</n></r><r><n>100</n></r></f><t>,</t><v>−</v><f><r><n>1</n></r><r><n>2</n></r></f><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float((sqrt(698)+7)/100);</line>
</editor>
</input>
<output>
<mth><lbl>(%o18) </lbl><n>0.3341968962724582</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>radio_espectral:apply(max,abs(x));</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="radio_espectral">(%o23) </lbl><f><r><n>1</n></r><r><n>2</n></r></f>
</mth></output>
</cell>

</wxMaximaDocument>PK      ݣ�N�B�H                       mimetypePK      ݣ�N�T�D    
             5   format.txtPK      ݣ�Na���h  h               t  content.xmlPK      �       