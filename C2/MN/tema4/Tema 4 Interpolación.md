# Tema 3: Interpolación

## Polinomio de interpolación de Lagrange

Dados $[x_i, f(x_i)]$, la forma de Lagrange que mejor aproxima la función dada, $p$, viene dada por:
$$
p(x)=\sum_{i=0}^N f(x_i)\cdot l_i(x); \quad \quad l_i(x)=\prod_{j=0, j\neq i}^{N} \frac{x-x_j}{x_i-x_j}
$$


## Polinomio de interpolación de Newton

Dados $[x_i, f(x_i)]$, la forma de Newton que mejor aproxima la función dada, $p$, viene dada por los elementos de la diagonal de la matriz:

$\left. \begin{array}{c}
x_0 \\
x_1 \\
x_2 \\
\vdots \\
x_N
\end{array} \right| 
\begin{array}{ccccc}
f \left[ x_0 \right] & & & & \\
f \left[ x_1 \right] & f \left[ x_0,x_1 \right] & & & \\
f \left[ x_2 \right] & f \left[ x_1;x_2 \right] & f \left[ x_0,x_1,x_2 \right] & & \\
\vdots & \vdots & \vdots & \ddots & \\
f \left[ x_N \right] & f \left[ x_{N-1},x_n \right] & f \left[ x_{N-2},x_{N-1},x_n \right] & \cdots & f \left[ x_0,...,x_N \right] 
\end{array}$



Siendo $f[x_0] = f(x_0)$ y $w_0(x) =1$ (la primera columna coincide con el valor de la función).

- El algoritmo para obtener dicha matriz es el siguiente:

  - Primera columna: 

  ~~~matlab
  for i:1 thru N+1 do difs[i,1]:y[i];
  ~~~

  - Resto de la matriz:

  ~~~matlab
  for i:2 thru N+1 do (
  	for j:2 thru i do (
  		difs[i,j]:float((difs[i,j−1]−difs[i−1,j−1])/(x[i]−x[i−j+1]))
  )
  );
  ~~~

  

- Para obtener los valores de forma analítica:

$f[x_0, \cdots , x_i]=\frac{f[x_1, \cdots , x_i] - f[x_0, \cdots , x_{i-1}]}{x_i-x_0}, \quad 1\le i \le N$

- Finalmente, para obtener el polinomio:

$$
p(x)=\sum_{i=0}^N f[x_0, \cdots, x_i]\cdot w_i(x), \quad w_i(x)=\prod_{j=0}^{i-1}(x-x_j)
$$



## Lebesgue

- Se define la **función de Lebesgue** como: $\lambda_N(x)=\sum_{i=o}^{N} \vert l_i(x)\vert$.
- Se define **base de los polinomios de Lagrange** a $l_0, \cdots, l_N$.
- Se define la **constante de Lebesgue** como $\Lambda_N=\Vert\lambda_N \Vert_\infty$.

- Sea $I_N$ un polinomio de interpolación: $\Vert I_N \Vert_\infty = \Lambda_N$.



## Chebysev

- Los **nodos de Chebysev** se determinan por: $x_i^{N} =\cos (\frac{2i+1}{2N}\cdot \pi)$

  - Tomando los nodos de Chevysev en lugar de los puntos $x_i$ para calcular la base de Lagrange e interpolando con dicha base se obtiene una aproximación mejor. 



## Hermite

La idea consiste en, dados los nodos $x_0, \cdots , x_N \in [a,b]​$, y una función $f​$, encontrar un polinomio $p​$ de grado mínimo tal que $p^{j)}(x_i) = f^{j}(x_i)​$, es decir, un polinomio cuya valor de la derivada en los nodos coincida con el valor de la derivada de la función original.

Para hallarlo:

- Como datos tenemos:

  - Los nodos: $x_0 , \cdots, x_N$.
  - La función $f$ . [$f(x_i) = y_i$]
  - Su derivada $f'$. [$f'(x_i) = d_i$]

- Hallamos la base de Lagrange de $f​$, y su derivada.

  ==Nota:== en Máxima, para derivar, una vez obtenida la base $l(i,t)$ para derivar basta con:

  ~~~mathematica
  ld(i,t) := subst([u=t], diff(l(i,u),u))
  ~~~

- Calculamos el polinomio de Hermite:
  $$
  p(x)=\sum_{i=0}^N (y_i\cdot h_{2i}(x) + d_i \cdot h_{2i+1}(x))
  $$
  siendo 
  $$
  h_{2i}(x)=(1 – 2(x – x_i )ld_i (x_i))\cdot l^2_i(x), \quad h_{2i+1}(x)= (x – x_i )\cdot l_i(x)^2
  $$
  

## Taylor

Idea similar a Hermite, pero esta vez la derivada j-ésima del polinomio a calcular debe coincidir con el valor de la función (que irá en función de $j$).

Ahora contamos con diferentes datos:

- Un único nodo $x_0$ donde la derivada de coincidir.
- Una serie de puntos $ d_0, \cdots, d_N$. 
- La función $f$.

Para hallar el polinomio:

- Calculamos $f(d_i) =  y_i$.

- Obtenemos el polinomio de Taylor correspondiente: $t_i(x)=\frac{(x-x_0)^i}{i!}$

- Finalmente, calculamos el polinomio buscado:
  $$
  p(x)=\sum_{i=0}^N y_{i}\cdot t_i(x)
  $$
  

## Splines Lineales

Para mejorar la precisión de la aproximación se toma una partición  $P=[a=x_0<x_1<\cdots < x_N = b]$ y un polinomio de grado bajo.

Como datos tendremos:

- La función $f$ a la que debemos aproximarnos.
- Los puntos de la partición: $x_1, \cdots, x_N​$.

Para obtener el polinomio necesitamos obtener la base usual, que viene dada por la función:
$$
B_i(x)= \left\{ \begin{array}{lcc}
             \frac{x-x_{i-1}}{x_i-x_{i-1}} &   si  & x \in [x_{i-1},x_i] \\
             \\ \frac{x_{i+1}-x}{x_{i+1}-x_{i}} &   si  & x \in [x_{i},x_{i+1}] \\
             \\ 0 &  si  & fuera 
             \end{array}
   \right.
$$


En *Máxima* podemos definir dicha función de la forma:

~~~~matlab
B(i, t, x) :=
    if i=1 then(
        if t < x[i] then 0
        else if t <= x[i+1] then (x[i+1]−t)/(x[i+1]−x[i])
        else 0
    )
    else if i=length(x) then(
        if t < x[i−1] then 0
        else if t <= x[i] then (t−x[i−1])/(x[i]−x[i−1])
        else 0
    )
    else if i<length(x) then(
        if t < x[i−1] then 0
        else if t < x[i] then (t−x[i−1])/(x[i]−x[i−1])
        else if t <= x[i+1] then (x[i+1]−t)/(x[i+1]−x[i])
        else 0
    )
    else
        0;
~~~~

Con este método podremos encontrar el **único** elemento de la base, $s$, tal que $s(x_j) = \alpha_j$

- El **spline** viene dado por:
  $$
  s(x)=\sum_{i=0}^N f(x_i)\cdot B_i(x)
  $$
  