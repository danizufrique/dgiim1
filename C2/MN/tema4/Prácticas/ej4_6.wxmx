PK     vZ�N�B�H         mimetypetext/x-wxmathmlPK     vZ�N�T�D    
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/andrejv/wxmaxima.
It also is part of the windows installer for maxima
(http://maxima.sourceforge.net).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using an text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     vZ�N�?yL�$  �$     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created by wxMaxima 18.02.0   -->
<!--https://andrejv.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="120" activecell="16">

<cell type="text">
<editor type="text">
<line>Considera un intervalo real cualquiera [a,b], con a&lt;b, y una partición suya P={x0,x1,...,xN}.</line>
<line>  a) Halla una base del espacio E de funciones splines continuas y afines a trozos.</line>
<line>  b) Utiliza la base anterior para encontrar el único elemento s de dicho espacio E de forma que s(xj)=αj, (j=0,1,...,N), siendo αj&apos;s escalares dados.</line>
<line>  c) Aplica lo anterior a la partición P={x0=0.4, x1=0.5, x2=2.34, x3=3.45, x4=4.567, x5=5.081,x6=5.26} del intervalo [0.4,5.26] para encontrar el único elemento s de E de forma que s(xj)=1-xj^2/20.78, (j=0,1,...,6). Dibuja conjuntamente las gráficas de s y de f(x)=1-x^2/20.78.</line>
</editor>

</cell>

<cell type="section" sectioning_level="2">
<editor type="section" sectioning_level="2">
<line>Halla una base del espacio E de funciones splines continuas y afines a trozos.</line>
</editor>

</cell>

<cell type="text">
<editor type="text">
<line>Haremos una base genérica, una función B, con los siguientes parámetros:</line>
<line>    i : el elemento de la base.</line>
<line>    t : la incógnita con respecto a la que se expresa B.</line>
<line>    x : vector de coordenadas x de los nodos.</line>
<line>    y : vector de coordenadas y de los nodos.</line>
<line></line>
<line>Obviamente, debe ser length(x) = length(y)</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>B(i, t, x) :=</line>
<line>    if i=1 then(</line>
<line>        if t &lt; x[i] then 0</line>
<line>        else if t &lt;= x[i+1] then (x[i+1]-t)/(x[i+1]-x[i])</line>
<line>        else 0</line>
<line>    )</line>
<line>    else if i=length(x) then(</line>
<line>        if t &lt; x[i-1] then 0</line>
<line>        else if t &lt;= x[i] then (t-x[i-1])/(x[i]-x[i-1])</line>
<line>        else 0</line>
<line>    )</line>
<line>    else if i&lt;length(x) then(</line>
<line>        if t &lt; x[i-1] then 0</line>
<line>        else if t &lt; x[i] then (t-x[i-1])/(x[i]-x[i-1])</line>
<line>        else if t &lt;= x[i+1] then (x[i+1]-t)/(x[i+1]-x[i])</line>
<line>        else 0</line>
<line>    )</line>
<line>    else</line>
<line>        0;</line>
</editor>
</input>
<output>
<mth><lbl>(%o1) </lbl><fn><r><fnm>B</fnm></r><r><p><v>i</v><t>,</t><v>t</v><t>,</t><v>x</v></p></r></fn><t>:=</t><fnm>if</fnm><v> </v><v>i</v><v>=</v><n>1</n><v> </v><fnm>then</fnm><v> </v><fnm>if</fnm><v> </v><v>t</v><t>&lt;</t><i><r><v>x</v></r><r><v>i</v></r></i><v> </v><fnm>then</fnm><v> </v><n>0</n><v> </v><fnm>else</fnm><v> </v><fnm>if</fnm><v> </v><v>t</v><t>&lt;=</t><i><r><v>x</v></r><r><v>i</v><v>+</v><n>1</n></r></i><v> </v><fnm>then</fnm><v> </v><f><r><i><r><v>x</v></r><r><v>i</v><v>+</v><n>1</n></r></i><v>−</v><v>t</v></r><r><i><r><v>x</v></r><r><v>i</v><v>+</v><n>1</n></r></i><v>−</v><i><r><v>x</v></r><r><v>i</v></r></i></r></f><v> </v><fnm>else</fnm><v> </v><n>0</n><v> </v><fnm>else</fnm><v> </v><fnm>if</fnm><v> </v><v>i</v><v>=</v><fn><r><fnm>#{Lisp function}</fnm></r><r><p><v>x</v></p></r></fn><v> </v><fnm>then</fnm><v> </v><fnm>if</fnm><v> </v><v>t</v><t>&lt;</t><i><r><v>x</v></r><r><v>i</v><v>−</v><n>1</n></r></i><v> </v><fnm>then</fnm><v> </v><n>0</n><v> </v><fnm>else</fnm><v> </v><fnm>if</fnm><v> </v><v>t</v><t>&lt;=</t><i><r><v>x</v></r><r><v>i</v></r></i><v> </v><fnm>then</fnm><v> </v><f><r><v>t</v><v>−</v><i><r><v>x</v></r><r><v>i</v><v>−</v><n>1</n></r></i></r><r><i><r><v>x</v></r><r><v>i</v></r></i><v>−</v><i><r><v>x</v></r><r><v>i</v><v>−</v><n>1</n></r></i></r></f><v> </v><fnm>else</fnm><v> </v><n>0</n><v> </v><fnm>else</fnm><v> </v><fnm>if</fnm><v> </v><v>i</v><t>&lt;</t><fn><r><fnm>#{Lisp function}</fnm></r><r><p><v>x</v></p></r></fn><v> </v><fnm>then</fnm><v> </v><fnm>if</fnm><v> </v><v>t</v><t>&lt;</t><i><r><v>x</v></r><r><v>i</v><v>−</v><n>1</n></r></i><v> </v><fnm>then</fnm><v> </v><n>0</n><v> </v><fnm>else</fnm><v> </v><fnm>if</fnm><v> </v><v>t</v><t>&lt;</t><i><r><v>x</v></r><r><v>i</v></r></i><v> </v><fnm>then</fnm><v> </v><f><r><v>t</v><v>−</v><i><r><v>x</v></r><r><v>i</v><v>−</v><n>1</n></r></i></r><r><i><r><v>x</v></r><r><v>i</v></r></i><v>−</v><i><r><v>x</v></r><r><v>i</v><v>−</v><n>1</n></r></i></r></f><v> </v><fnm>else</fnm><v> </v><fnm>if</fnm><v> </v><v>t</v><t>&lt;=</t><i><r><v>x</v></r><r><v>i</v><v>+</v><n>1</n></r></i><v> </v><fnm>then</fnm><v> </v><f><r><i><r><v>x</v></r><r><v>i</v><v>+</v><n>1</n></r></i><v>−</v><v>t</v></r><r><i><r><v>x</v></r><r><v>i</v><v>+</v><n>1</n></r></i><v>−</v><i><r><v>x</v></r><r><v>i</v></r></i></r></f><v> </v><fnm>else</fnm><v> </v><n>0</n><v> </v><fnm>else</fnm><v> </v><n>0</n>
</mth></output>
</cell>

<cell type="section" sectioning_level="2">
<editor type="section" sectioning_level="2">
<line>Utiliza la base anterior para encontrar el único elemento s de dicho espacio E de forma que s(xj)=αj, (j=0,1,...,N), siendo αj&apos;s escalares dados.</line>
</editor>

</cell>

<cell type="text">
<editor type="text">
<line>Se comprobará en el apartado siguiente.</line>
</editor>

</cell>

<cell type="section" sectioning_level="2">
<editor type="section" sectioning_level="2">
<line>Aplica lo anterior a la partición P={x0=0.4, x1=0.5, x2=2.34, x3=3.45, x4=4.567, x5=5.081,x6=5.26} del intervalo [0.4,5.26] para encontrar el único elemento s de E de forma que s(xj)=1-xj^2/20.78, (j=0,1,...,6). Dibuja conjuntamente las gráficas de s y de f(x)=1-x^2/20.78.</line>
</editor>

</cell>

<cell type="text">
<editor type="text">
<line>Definimos la partición:</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>x : [0.4, 0.5, 2.34, 3.45, 4.657, 5.081, 5.26];</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="x">(%o2) </lbl><t>[</t><n>0.4</n><t>,</t><n>0.5</n><t>,</t><n>2.34</n><t>,</t><n>3.45</n><t>,</t><n>4.657</n><t>,</t><n>5.081</n><t>,</t><n>5.26</n><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>f(t) := 1-t^2/20.78;</line>
</editor>
</input>
<output>
<mth><lbl>(%o3) </lbl><fn><r><fnm>f</fnm></r><r><p><v>t</v></p></r></fn><t>:=</t><n>1</n><v>−</v><f><r><e><r><v>t</v></r><r><n>2</n></r></e></r><r><n>20.78</n></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>n : length(x);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="n">(%o4) </lbl><n>7</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>y : makelist(f(x[i]), i, 1, n);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="y">(%o5) </lbl><t>[</t><n>0.9923002887391723</n><t>,</t><n>0.9879692011549567</n><t>,</t><n>0.7364966313763235</n><t>,</t><n>0.4272136669874879</n><t>,</t><v>−</v><n>0.04367897016361888</n><t>,</t><v>−</v><n>0.2423754090471608</n><t>,</t><v>−</v><n>0.331453320500481</n><t>]</t>
</mth></output>
</cell>

<cell type="text">
<editor type="text">
<line>Es fácil comprobar en la siguiente matriz que se verifica lo especificado en el apartado b):</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>genmatrix(lambda([i,j], B(i, x[j], x)), n, n);</line>
</editor>
</input>
<output>
<mth><lbl>(%o6) </lbl><tb><mtr><mtd><n>1.0</n></mtd><mtd><n>0.0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0.0</n></mtd><mtd><n>1.0</n></mtd><mtd><n>0.0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0.0</n></mtd><mtd><n>1.0</n></mtd><mtd><n>0.0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0.0</n></mtd><mtd><n>1.0</n></mtd><mtd><n>0.0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0.0</n></mtd><mtd><n>1.0</n></mtd><mtd><n>0.0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0.0</n></mtd><mtd><n>1.0</n></mtd><mtd><n>0.0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0.0</n></mtd><mtd><n>1.0</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="text">
<editor type="text">
<line>Ahora encontramos la función spline lineal, spline:</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>spline(t) := sum(y[i]*B(i, t, x), i, 1, n);</line>
</editor>
</input>
<output>
<mth><lbl>(%o7) </lbl><fn><r><fnm>spline</fnm></r><r><p><v>t</v></p></r></fn><t>:=</t><sm type="sum"><r><v>i</v><v>=</v><n>1</n></r><r><v>n</v></r><r><i><r><v>y</v></r><r><v>i</v></r></i><h>*</h><fn><r><fnm>B</fnm></r><r><p><v>i</v><t>,</t><v>t</v><t>,</t><v>x</v></p></r></fn></r></sm>
</mth></output>
</cell>

<cell type="text">
<editor type="text">
<line>Hacemos el correspondiente plot:</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>wxplot2d([spline(t), f(t)], [t,0.4,5.26]);</line>
</editor>
</input>
<output>
<mth><lbl>(%t9) </lbl><img>image1.png</img><lbl>(%o9) </lbl><v></v>
</mth></output>
</cell>

</wxMaximaDocument>PK     vZ�N"���VE  VE  
   image1.png�PNG

   IHDR  X  �   �W��   bKGD � � �����    IDATx���}\����שCI*�v���F�f���,J�r/l��l�ܳ�07�J�f��&D��f���rq�9�?��5��U�9��z>����\��s}���u]眏B�V���HW�����HN,�DD��X��H���Nc!$""��BHDD:�����t!�4B""�i,�DD��X��H���NӭBnnn>m�4����P���L�<��ի:t�;i:#2d����-,,�BDDD��;u���6ժU{��I%�!"� ���P!,�'O�d^�0k\���
���7�hf0��� �TX�B����M�d��ޮ��Q�,�Fy�Y7Է����y���X�e����n�����i7r�]˽rM��iØ� rj�ϭZ%��u��6h����ԭ+wj""*'�YT�vi�f��[���嬭�2�oL�J�����4��
����Oac�-���h�-[�ET�*S|""*��Z]9���ssssrr
�R��ҥ˾}��i���Eu�6mBD�ԁ�|��{��e\���4\���4���Q#�j�V�Ц��Ѥ	$���!��1I4$�DSc�I*,��t���aVV֫<}֬Y�?w�N�0w.���6s�Z��8|xG�Oaj
 ����KHMEJ
BB0u*�ޅ�ڴA۶h�m����Փ�KC�hH0I1LR����"s	��ՍJ����".��1..P>���=$'�������Q#���?��Q��T� "�b���	��ff""!!�r�����s.���"%'O��	�<�˗Ѻ�?g�hڴ���*�����tl؀��!|}��֭_��Ǐq�4N�@b"��J�Ν�� t���_=��X$��&'c�zl؀�51l���U)���HL�ѣHLDr2Z�D�nx�mt�
ss	��
1
�}ר,ڴ�w��߰t)._F����+V���>��b�B=��w�t)j����h�����clߎ��J���ܹsMLLƌS�'ry��P�%TѯnT*�ك�p�Ġ{www�����8u
��8xG��qc�쉞=ѽ{�߃JDҲ���4i�رc�����q=zԡC�9s�TP6��ʬjU�������˗�A���x����J%�|��ص��e�`n��Q�>�vŗ_��1��Vx7��9x�/����޻|��B�(�������_}��-�B�iӦ^�z�i������Ç��8�b!�G�9qq8s͚ᣏ`m�� �=[�M(�pp���ط�oc�,dea�Ԯoo�]��7+�DT�-[����̛7oŊ%6���p�̙�{�&%%ݼy3,,�[@` �
�(��,�2��ħ�����}}xz�U+|��^-�V�U����Ù3�p�A�V��3f��1��UT���F�@__�M�6���[�������<Z�BP�^ŪU�y��pp������2n�n]���ܺ�������Gݺ��ED<���DT&&&?�����u��X5�BGG|�=��3g��	�h�w�����.㶔Jt놠 $%!9=z 4VVx��Z�[�*�D�������%y������PC)�psCh(��C�",`�h��W���<��0n����9��]�`�\������W�^�*UΝ;��ŋ'O��;�L��t[KU���w�ܺ��71d�E��e�\�4�A����ؾݻ�N��ak+}�tU�j��ϟ?r�Ȇ6mڴ����Sw��q/^\��8TAD�����31iiGx8���ؼ���󑐀-[��ϨY�2ټ�dq�H\Z1g��}���Z��Ǳa6mBÆ������_�նx�(6m-�S�c�`4n,Yb"�͙/ B$����������(�k__�ﲈ哟�#G��[am���1dȫ�X"�6Ιŉ�	i��>y�]��~=�+�G�^ŖE,���q#�o�o`�0��ϥ0�*ȗ_~�iӦ���!C�h�¹Z=g�cP�E�ի<>>x��W�bv6������a�г'����KD@||��i�:���׺u����-[���Ę3E胄��B�/#<6 '����y巅޾�������/F��M�$��ɓǏ����ڵ�%K:t� w��c���Efc��3���M���\\С.�_�w�u���q��⠯gg8:b�*~a�+244,�����*����^�D:��P'�o����7))����֬Affy�ز%������?GL��1f��24�݋�;{�����������E���@�R�m���ٹ���ÇZ�l٢E��3g����إKss�������8p�@�N�j֬iee��_���			�;w������^�vm�_ZXX�kǎ��f�9���g��c��"�T���������v�کT����n�z�����߿ҤI���P��T����l��?�T���=<��Ǐ_m��n���W�ڪmm���ܑ&(��'�|������P��O�>����3gNNNNZZZ�z����K����������I�V?z����422R�V߻w���f���%x�𡙙ٲe��������Y���͛իW߶m�Z��|�r�z����K|���]�f�իW���;v�z�ꉉ�������W�\)��m���y�|��S��...�֭+qG%v��C7��+Vt��q˖-�'OV�խ[�NJJ*h�w�޶m�^�~�t�Ę3E胄��һ_�vqQ׬�5J���{�-9�9R]��z�`u\�:?_��T
ǏW�Ճ.q*����[�n��#2cƌA�����#GZ�j�|������� �>}Z�[��>��� w��)��|}}�����믍7��
���;���
[�9rҤI��� �޽[4|��,1���⣷nݺ������Y�KOO/Xtw	gΜ<x0��M����w��i޼��7��]��1g�ҨN31�ȑػ��h�h�HI)��tAp0��ѭ�NE�f���A�������lmm
E�C[[�����4������644,����M�&M
.T޸q��ѣ���%�U����_�ϙ�����...���/( yyy�:r䈇�G�OW(E�1֨Q����>hذa���sssK�f�9������,�%K� (qG%v��Cװa�������S�N���lܸQ__˖-�Ǐ�}����s�֭[�n�iӦ��pA�
�BH P�>>����={���w�A۶�7�Q�͙��������p\���-1x0$Me���U8022*\9��&O����ӱc��_U�Reݺu�}�Y�ڵ������
�ddd�����ۻ�V��ݻ���3s�L����������1??��ɓ۷o��ήR��رc�����}�����y�數�s��8��?����jժ�o���n?t���cǎ:t�_~Yx���'�|�����A����V B���_GP�ӱt)~��ڡG�Z���rm�S'�Y��t��ƌAǎ���S�Y�n]�Z�jժ���`ll|����_eff֨Q�h��ڵ�ܹsӧO/�����߿DDĝ;wnݺu�ܹ�;55�����ݽ�;S��ܞ>}z���իW\-,�f͚���!!!���s��0`�������U�������_���>�h���%n�Ĝ/=����_�x���_�/K�L7K<tU�V������Z�f��*!�@Oݻc�J\���>B\�4A�~ذ��}s���4	ii��+������(�N9� �*8�رc���݅
W?HNN���+ڸ����׮]�������:ujBB���ulll�ڵ��YXXxyy�ٳ�y���\]],XP�7�ϟ߼y3 }}�V�Z><22�yOwttLHH���߷n�z�ڵ:ܾ}�ĉ�T*���a��ܿ�/=�HLLT�՝;w~q���,��q^%�MJ���<�CB�nnj33�С��h�JU�m�?��0Amn���U?.eJz��=rrrlllfϞ�R�N�:eaa�V�###޵���֮][���S�N�<yR�V?~��W�^��^�����6m�e˖��9~�����={�j��[�:w�<q�����ѣF�����������&&&׮];y򤁁All�Z�>�|Æ�����Yb�w��ћ3g����×��]qժU۷o���Ę3E胄��
u�����]����Q������n(3S�`��iSu���^���?�W�n``������o``гg�g�?�K�.FFFVVV?��c�_��曳g�~A�BEg�;;��M�6i��������%ضm �"����juXXX�֭����ի7v��</��͛mllLMM���
FXX������i�&M�̙S���Yb�����7iҤQ�F=�w��n�x�1v�Xcc��?�����Ę3E�v	��uA�#=��Eժ6>>��.�V��c�.ťK�8��8!�b̙"�ABbj%;z��c�f4k__�ڵ˾��d,X��h��?.WQ%��&Ɯ)B$$Ơ�"'��ذ��pt��/<=˾d���X�k���S�B�t�H��1g��	�1��z���GB��z�Bժe��ÇX�
K��iSL�
77���g"�(b̙"�ABb����olތ���0|8�R�rr�i�������=~ڇH��1g��	�1����߱q#BB��	F����hܸ�OV��s'�����1d&�b̙"�ABb��:u
!!ظM�����n�R?96�|����#�x���*�s�}�����rs�y[͎��	C����[��HH�7� %S�b�8T�V�Y��Ę3Ź���EG���(U2�nn	�_a�l�
++���R|i�.ص۷#>M�`�<�����^��>�_�^����}�0D�ܽ�^�Bݭ���B=n�z���-�x���G]��z�l������c���СC*����_�T�l�rܸq!!!E\�v�����Ř������Ν�),=��ﾋ�x$%�ysL��F�0m��^^1�[#<���%��`��Q9R_����%�be��Sq�4��B77t者��_�N��	A|<��ac����I�%&"�'H!|�b��_JTQL���Q�	^�~��~üyHI�����v-�,$�_-["<��!!͛c�*���I%00��Q�D���t���/%Z��1�\��г'~�	ׯc�DG�Q#xya˖���j��[�u+6m��c���Wvh"q>3=ʝH�.�Y��2�r14Āغ׮�o_�\��^��ш�C	���bc�b�.��="#!�W"��®]�ZXX�k����!!!~~~ ���Μ9�M�6���������]�v�k�N��Tv��={�"5m���`e�)Sp�L��={��1a�,t�}�d�KD�@��BHKK7n\RR����_|1~�x �;wvww��/ ���Λ7����j��G�.411yf#b|8T�\�������1|8|}ai����ؼ3g�Q#�cGy��H�9S�>HH�A�Aj5��[ag��C1`������O?a�l88 (66�e%�s�}����˞>���X�������pw/�El��X���3g��BάD�O�9S�{�D��͛q�<<�z54�ȑ��An.P��MCZ��ak�y���C"��KH�W7Tԭ[ش	6��x{��P(�K������̞��C��!Q9�1g��	�1�T��t��#<*��wY�c���x�s���E�DZF�9S�>HH�A�+\�ys�����o��?^�}4D�'Ɯ)B$$ƠRi��`���c�nt邡�*��,��1c���^ �H��1g��	�1�T&�!27��!�vsF��V��૯0z4o��s�}���J�s�6oFHϟ^Q����~\�����E��Ę3E胄�TzEW� 4�Wo�|+�~����w��;�&c��cPI*�N�.OY���[^Z�NC�����̙"�ABb*I+e絿�Ln�w�t��^�r�!� b̙"�ABb*I.+������C�k�I�i1�L�)��匍1m������t|��M";[�PD$����xuC��|������;W9����x���I�9S�>HH�A�
�R��p;l�J����Œ%h�H�PD�c��Q���Z`��^��w�~;b�<��ȝ���I�b.!1^�P�u�F����P������+��(w(�J%Ɯ�3B�r�[�v������]�=?��7&L��{r�"��a!$*?�S�`�n��=佷Rsrh�
7ʝ��ʀ���U�o�S��m`�6aٯs���o�ꊫW��ED��BH$cc�[���Bש���8���7���˓;���9%$ƍ_�ѵk6ժ!lvz�/��￱fڷ�;Q�c��!��5��������LދI���Y�����ш�d,�D���̙ض')>J��9����ag��x��Q	D8������!����Ѹ}��hrj3>���o`d$w4"i�1g򌐨�XX 2��pp@�o�;��wao�#G�FD�O�b.!1^ݐ�9��h�+W�f�n��.�ܰp!�W�;�+c��!Q�{�u;�z�о=�����de�}{$$���xF�_b��!��cƏ�G��z��0i��Vc��!Q���'Ob�n���F��8ww�m[>,w4"��BHT�,-�?�tAǎ�s�aaX�>>�g�d!�Y���8�'�p����-����������шJK�9�g�D�pp�ɓ�wH�n��pL��^��p!���NG�CD(���i��`����1~<���Q�����kam-w4��c��!��F�#X�^^�4k�����\)w4"��BH$���q�(^{;��IƏGl,�/��'nݒ;��X�4��!��?̟�~������5N���ڷ��?˝�Hd"\ޕ�׻I�ݺ�Q�p�>֯G��@b"F����.������C�9S�3³g�:::���XZZ.Z��x����Q�F���[XX���{*���C�Tݺعpp��-@��8sի�}{$&ʝ�H@�B�Jշo_��������s��ܹ�6'N|����q��ŋ/o@�!����h|�Ə�ca�2,\OO"/O�DB�@\\ܰa�nܸ�P( ̜9��ŋ��ݻW�n�˗/[YY�`;b��0>�ԩ8t6���~Ç@h(4�;� s� g�iii���U���mjjj�gϞ533۲e������ʹi�rrrJܔ�������OT�5�㏘>�z��ggt�;�NG:'00��Q�D��fee>422���*� 333##C�R����߿?22r�%nJ]!�k�0=��`xy!�>�QQ����Ǐ�NG:$00��Q�D��߿��afff�5�6077W��S�N���oذ�	v��U�1����ǎ�M�k�#G�7���Sx����pA�tD�M�Bhggw���܂����vvvE������gdd<T��J���S��������퍯�F��)""0q"�uúur�#�b�®]�ZXX̙3'''����!!!~~~ ���Μ9��^sww�6m�J��q�Ə?�د_?�S���;Μ�ѣ��W�����!,\oo�(BD�'H!T*�QQQ111fff���AAANNN ������ڬ\�����S�N�N�<=='M�$kd�r�[��0 �� Z�ıc03C�N8sF�tD�G�w�JH����8}����K�����gL��i�����|��Ę39#$�A��#)	ի�C$'^^8|��8��ɝ�Hk�i�jհd	>���X���AB,-ѡN��;�v�VBb���x��h��V����Ļ�����GrG#��1g�H-Z��q�i;;��xx��a�]��C��/� �g�	������?ȱn�c�`h�N�p����4!�P��p�����[��V5�Y��>C�n��'��i(B"�XX 2>>��	7Çc�~̙���e8���>��ĸ�KT��)��=�h�r`���'6o����    IDAT##*=1�L�	�C�>���M�eƌ��ob���i����xuC�|�!>��'C��!��b�|�9���^�s�}���JTܵk>ժ!$u�o`� ��!$5k����s&/��F�p� `o����q� Z�B�v8yR�hD2��KH�W7D/�?F���Q�rk>� ��c��s�Vc��cP�^��m����_�&٩��+, W��2c��Q"�S���1z4�2�~�����=q��шd B1���n�J)%��h�
+��Ο��pl�{{�s��c��!��j�'N���:�s�s�wol�$w.�J%B1���n��**
����0�3Yo@x{#(z|�L/!Ɯ)B$$Ơ����6
�/�S�Co��",5jȝ�4�s&_� 4h��889�}���ŢAt�_�;Q���KH�W7D���	�������V�|*V����ܡHC�1g򌐈��S'�:��t\2<}�vL��iӐ�/w.��"B1���n�$�)S����= ��X���r�"�"Ɯ)B$$ƠI��E������uU�U���oG�zr�""Ɯ�K�D�\-Z��1�mh�"q]�}t���
�D��KH�W7D���Ř1���&�c`�j��+w"�b̙"�ABb*QE�y#F���3+o{*ǎB`�܉H~b̙�4JD�R�bb�r�}��G3�/���E$����xuCT�~�~>�W��P�/��m�][�D$1�L�Q�t�ç����򊓪����ND�JX���LL��>��I���~�m���r'"*?�j%$�i>Q���7��{��_�}���'c�C�M�9S�>HH�A%�L99X<�ׁk����h}
�Q�c��cP�*_����>L[�8�41�;U1�L�#$"	t�o��ꞿ�W�d������CT,�D$�Z�Uu�38��kV��I�CTZ,�D$���ω�=�g=y�{���r�!*q
�ٳgMLL,---Z��f*��m۶��Ε��Hw(�D���'�U�r�!z9A
�J��۷���GFFF\\�ܹsw��Yb�3f<~�������pZ�+����9����%)��R�T���J��e˖�ƍ		)�,!!a�ΝS�L���D:EO���f�>�ח��>��ܓ&�������*�� ���mjj�3m�����������6Q����­��q?vz��<����T�¬�������(++�6�'O����ر�7�(&������>�����K���&�o�ĝ;r'�W���(w"iR����߿_�033�F�E�ڵ�ܹsӧO��Ű���>քV]���~o�c\�*w"*����g�G�IC)w i���͚5+77W�THNN���+� <<�ڵk666 ���=zdmm}��}}}y��a��}y����uCd$:t�;����q �����ڎ92  �ܹs�z����prr���jذ���}�����aaaqqqŷ#��i �
^^��`ǧ��n���NDc��ҨR�������133���

rrr-w:"Bժزq��~�n;ƌ���r'"���\Bb��!�X������A��snx�}��˝�^�s�}���J��
j���_K.�����h��4��Ę3E胄�T"��ww�^/��tw4o�ի��{�F�9S�>HH�A%�|��O�j���;� `�&qC�#Ɯ�+D$##DE���)6��_=z���E:�����ab�ݻq(A��J���[7��ܡH��l����G���رx�-\䊾T�X�HN��ػǎ�?&��ѳ'���E�����dfj��$$`��aX�}����C�a!$"���a�^:��#}��{�ܡHW��F(�_�ofvFd$F��ƍr�"�����03�����U�t�����7��'r�"����]��x�m�~z�0z�­[�3G�\$2B"�,��a�>��}�F����>�e=t�4�GHD�A�ߏe�0w]]8�'���ϗ;�����4��%�ʕ�_�)��Ez:�Cn�ܹH@,�D�����o,���ꈊBf&||�Rɝ�D�BHD��Q#�ߏo��O�a�v������r�"���Fk���!0���ej׆�+<�;�����4����������O?��NN�{W�\$B"�͛c�nL��-[X�o�''ܺ%w.!i�֭�{7&MB��̓��~ׯ˝��?PODZ�m[�ك޽�w�L�kW�šiS���c!$"m�Z8u*�����X4k&w4�V,�D�e
j��+���{�R={"6-[���!i��m77�����ի��	{��uk����a!$"�Զ-v����2zz��11���;iB"�V��ص}�@�@�A��P�S۴�;i~|���X�v���رػ���ҥ���O˝��	!i�N���ñ?0p V�F�>HL�;iB"�z�;���ッ�w�Ap0�����r�"��BHD"����<�����?����EZ������u��鉓'DD`� >,w.�t,�D$W�.��<	��6�l���</�c!$"���b�Z��_~�z��c�@:$w.�\,�D$77�X��}q��쌰0x{��q�s��b!$"yz�ЧΝz�Fp0��c-�����������))��BB����Rq,�D$������T�w��T�ZH�%N!<{�����������E��7HLL�ҥ���y�������+?$U��1w.z��ŋ@����'xx��	�s���T��}�zxxddd���͝;w�ΝEdee���6��ݻG��),U��Cgg\��郵kѯ���
	R:�R�����Je˖-ǍR���'O�ϟ?a��BѨQ��ݻ_�xQ��DT�F������.�[��a-��´�4[[[�BQ����655�h�Z�j����������R��Vdv"��F��ѻ7�\�ܰl���߉�^,00��Q�D�d=¬�,CC�FFFYYY%����������vvv.��Z����D$�ѣ��''<k//<y�޽�?�7�;�v|��@�Z(H!466��~�����5jo������5f̘���JLGD�b�x���S�|}��gg8��M�F�����͚5+77W�THNN���{�MRRR�~��/_���.GF"������#����=q� 7�;�C�{�]�v����3gNNN��ӧCBB
�FEE�9s��'O���/^�*HD~�	Уn� ƌ����׮ɝ��!H!T*�QQQ111fff���AAANNN ���������\�re�С���ӧ�ܩ�H6�|�a�Ы��ƎŇ�W/ܼ)w.����)J��!�!3g"2����X�+W">u�ʝKk�1g
r������
j5���o,&OFf�?�#57�;U����xuCDe2}:v�¾}07��X���sz�s�}���JDe5u*�޽03Uc�D��b�nɝKӉ1g��	�1�DT�|�cǰw/Lj�1n~�;v��@�\M�9S�>HH�A%�rP���G������z��ӧؼJ���Ę3���+R(�d	ڷ��>�Gh(rs1z4�d��D(�����Z���Åص�����;h�˗C�/Ք�s&�����B�+���puEV^5DG#5,w.�@,�DD��P`�2�j�>}�Hm��h9�/��;U�j%$�i>���|����BT�e���oc�H��˝K��1g򌐈�zz��'ԭ����Fm�ك+�j�ܹHz"s	��ꆈ����Q�p�6"#ax�
�wǢE���;��c��cP�HByy1ؾ����k���M�\A�9��F��^D_!!03C��xڬ5"#1r$��;IF�b.!1^���rr0h��e\F�B\Z��;��Ę3yFHD�rU�`�&�����ήX�nn�zU�\$B"�R�R[� ;C�"�{�OG�^�qC�\��X��J�jUlތ��ƻ�B��{3�z!#C�\�JD��+!1�wQ�z�nn��ŊP|:ǎ!6V7/c��cP���=~ww4i��?������-lߎ*U��U�Ę3E胄�T"���qc�Z���3��n�oc�ԭ1#"�������xo��:4��?�;�!Q9c�.\���SՑQ8rAAr��2c!$"*?cc�ލ�|<�11X�k����F�˻�z7U����₮]�p�t�ￇ��ܡ*�s�}���JD�/#NNpw�l�d��M���[r��pb̙"�ABb*����쉁1����{Ѷ�ܡ*�s�}���JDr)���a��L���XZ���1g��	�1�D$�۷ѳ'||0��2���A͚r��(b̙"�ABb*�����??|zk*N�Ğ=04�;T�c��cP�Hvׯ���1i����Q�w?�}}�CIO�9��#$"�^�8p K�W��
����ND��BHDT!,-q� �.��S�m8z�~+w"*�R� DD�j�{��G���b-邺u1f�ܡ�Y,�DD����pv�]����gvG��ps�;���9%$ƍ_"�4�.���G��~���oȝHb̙"�ABb*i��Z�v@�Ӧwq�7�;�Ę3u��2gϞutt411���\�h��q�H�4o��X�����w�AF�܉��RU*U߾}=<<222����Ν�s�N�C�ni���3;<=���܉НK�qqqÆ�q�B� 0s�̋/FDD<�L��|"�d.��)�h#+k}�_�B�D�'Ɯ�+g�iii��������ڦ�����tS˖�ޥ���ug���ir�!�)�YYY�E�����(++�Ė�b>��͌3���
�Uџ���f̘Q⯸n�[��-�m����j�۸G��d���"00��Bᬶ4����6=z��app��Ν;�L31N�H+$&��w�L��Rm�B w��c�ԕ3B;;�.���<LNN����7�Ν�"��-/Z5~��;��ҕBصkW�9s����>}:$$���O�PD����nD~����.�GG�pV[Jiii�ƍKJJ�����/Ə_����D�]�㱭�O�S��1ԩ#w�2c��cP�H�<�3�L�(����02�;Ni�1g��	�1�D��b���y�tr�6����vܷc�ԎcMD$<�^
��U���k�grg�-,�DD��w?���m���O_-w����4��`���v����F�M�;�G'����H��=��ů"L'�^ޞ"w� �}N	�q㗈�~D�՟�$�5��T�1g��	�1�D$�=�g5����}��_�Zb̙"�ABb*�!7G}��:�OۤEh�jMb̙�GHD���U��d�~'�s��YD�BHD����U��o��Έ++��",B""�������;L�ϼ���Y��BHD�麍kq����|��]�;��D��)!1n����v[����׮S�4�;�?Ę3E胄�T"ғ'�n�qg����wA��&Ɯ�K�DD����Yp�O����ȝE(,�DDZ����鎰��q�\!wq�i��N��T~;���qrg��w%$��n"ފaGm��q:�J��2�c��cP�Hx��X�a����,.G�Zr�c��cP�H<|��M>u��� y7�T�%�s&�i�5���w�ӫ����Y�!��j�BO�)�~�ỳ�ɝE��i��j	ء�������΢�D��+!1�w��������A�g)�7����1g��	�1�D�k�>��c}_��Q��V�~Ř3E胄�T"�A7n`o�I�-�����~��T�9����DP�>�X|��ӿ��΢eX��a�Q��rsVx�Å��΢MX����odͭ�v�M��/w�!��]	�q���tY~>�LIcz�6��}�1g��	�1�D��<������ԺpիW܎Ę3E胄�T"��tk=ΥCF��-P(*h/b̙�GHD$�ƍ�0z���w2�|#wM�BHD$��=��F������(��h4�j%$�i>Q���|/ڽƩ�z�l%߸s�}���JDT(7�چ��9�֕�03�v�b̙"�ABb*QQw�`�͇�_�wj��߾&Ɯ�{�DD��]�,�����/�΢��)�gϞutt411���\�hQ����]�t177oР���~~~�$"�E�����k���.w�#H!T�T}�������Ȉ���;w�Ν;�6���rss6l�ݻw�=,SX""�3��.������=/w�"H!<t�J����W*�-[�7n\HHH�O�<�?��	
E�F��w�~��E����oy��������ɝE�R���lmm�~{���mjjj��j����+�9333>>��ť�M)�	���DD�D����aqU��p����>=00��"BV>A
aVV���a�C##����[fdd��������ٹ��bX�H���8���9���U��>3=VD�ʧŅpݺu�jժU��������������Y�F��OIMMupppww/��4DD���F����q��~�;�F�⏀dgg?�R������u��m�R	`���7o�/�>))�_�~˗/www�6��L�K�<�l�oz$�7~�U�7"Ɯ)B �����ڎ92  �ܹs�z����prr���jذ�����'OZ�n��w�0��cP��J#��z�_����̴|[c���K�E)�ʨ����333OOϠ� ''' AAA��� bbb�\�2t�P����G��DDr��h���#�۟���KH�W7DD�t�z�o6�����E|Q���1g
rFHDD�P�A�[~�u�n��Ȇ���H��޳��o7ל<���W��"�j%$�i>QY�~������AM��?K�9S�>HH�A%"*+��6�S9\]_�g�1g��(A�@����n^8:l��Y*!@5�j�vlj�᫔Չrg�T,�DD�k���_�1�0��;rg�<"\ޕ�׻��^��.3���n��W�qK1�L��t?�e���Q�r�$,�DD��U���6>��t`��Y*!=��m�;?lj8{܍×��R�D��+!1�wI"n���1˭�'V5+�S�b̙"�ABb*�$
>e_�@��bɟ�c��Q""*�B��'��~��(�?e�BHDD�U������&�_^�p\�,�����^��;6g'��6rУk˝�B�pyWBb\�&"���Ӭn���������Ř3yFHDD/���7�+N��-w���V�op �j��c��"1B""*�u�|V�tf�rg���w%$��n"�����w��#[܌�3�"Ɯ)B$$ƠU��\ub�z�9$.c�T�������R���Oٯw8��E�,���KH�W7DD-9�l�Q.���0g��2DDTfm��=7 �*K%w�W����!Q�1g򌐈�t!�4B""�i,�DD��X��H���Nc!$""��BHDD:�����tai��L��&)FCb�I�ѐB�oǑ��.Hs�I�I43�DSc�I*,��t�񌐈�t�8���ٳ���&&&����-z^3�Jնm[gg���FDDK�B�R������ᑑ�7w�ܝ;w��rƌ�?��xDD��)��R�T���J��e˖�ƍ		)�,!!a�ΝS�L���DD���r�FZZ����B�(xhkk���??�&++���/,,,%%��*�H�~Uɘ�LR��� ��!14� �0++��а𡑑QVV�3m&O����ӱc�B��DDDe�ŗF׭[W�V�Z�j988߿��W���5j�(�x׮]�Ν�>}z��$""������.(~J�2%%�������J���ɓo޼^�xذa���Z�*����G�խ[�ʕ+���r�'""M�Ņ����\[[ۑ#G�;w�W�^NNNQQQ6���/�8888,,,..N��DD�9���hQJ�2***&&������3((���	@PPPtt��鈈Hs	rFHDDT>���!�4B""�i,�DD��X��H��>�K�u���0000�׎;*-[xx�����i�*m��ٯ\$11�K�.���d\?�  �IDAT4�����ϯ���f�r�t�ԩf͚VVV_|�E��9������[������;�EM%y�����՜9srrr����ի]�A~~�����+W*?�'�|�������9���<|����lٲe������[�֬Y�!���ܼy�z��۶mS�՗/_�W�^pp�&�W��5���mll���4d�2��B����d/]�����yyyfff��mȐ!۷o���Ш��u@�<y2��	&(�F�u����ŋ�k��I~~��5k<==4mڴK�.iii��_��@�U�^�_��c�iXKV|]���Ԣ233|��6l޼y```nnn�d�ԩS��L���ԪU��ϯ0C||���K%�4���ԯ_��� ���:t��Mد��k
Va.�L�����ȸk�BX����T�J��c�6,==}���!!!��ͫ��D����ѯ_?oo�ʿ��]�{Lv��m``ЧO��3g:88h�~e< ���U��J�_���a5���f5�ҥK
�]��u��/h�x��N�:U|��7x��J�GX��V�IIIi޼��9s*m���u��#���MIIi׮ݷ�~������s��Ν;�����k�V�=²����h®5�Kfggw��k���vvvEܾ}�ĉ�U*U%_r�42���$WW�T�K�k������7o�@__�U�VÇ���Ԅ��u@��ï]�fcccmm=u�Ԅ�kk뼼<��+���`�!w%�P999666�g�V�T�N�������S�Ց��IIIj���ɓ���j�����6����+3����~@����6m�e˖J�W)w-�19~�����={�j��[�:w�<q�D�+�)�2���_M8 �0����Ο?ߥK###++����/�|��ٳg�fkkkjjڤI�9s����WN��ի���������S���~@�m���77�J��v-�1)�o�֭����ի7v��ȸ_M8 �4�j��},4�a"""��{�DD��X��H���Nc!$""��BHDD:�����t!�4B""�i,�DD��X��H���Nc!$""��BHDD:�����t!�غu�͛7�NA$&B"-�BHTAX�4ݐ!CRRR�r�J��	�+�i�R��/������H@<#$""��BHDD:�����t!��Z�jFF��)���BH�����1}�t��	��%""��3B""�i,�DD��X��H���Nc!$""��o��ۜ    IEND�B`�PK      vZ�N�B�H                       mimetypePK      vZ�N�T�D    
             5   format.txtPK      vZ�N�?yL�$  �$               t  content.xmlPK      vZ�N"���VE  VE  
             o+  image1.pngPK      �   �p    