PK     d��N�B�H         mimetypetext/x-wxmathmlPK     d��N�T�D    
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/andrejv/wxmaxima.
It also is part of the windows installer for maxima
(http://maxima.sourceforge.net).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using an text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     d��N�)F�  �     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created by wxMaxima 18.02.0   -->
<!--https://andrejv.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="100" activecell="5">

<cell type="code">
<input>
<editor type="input">
<line>S(x):=[x,cos(x),1,%e^(x/%pi)];</line>
<line>f(x):=x+2*x^2;</line>
</editor>
</input>
<output>
<mth><lbl>(%o41) </lbl><fn><r><fnm>S</fnm></r><r><p><v>x</v></p></r></fn><t>:=</t><t>[</t><v>x</v><t>,</t><fn><r><fnm>cos</fnm></r><r><p><v>x</v></p></r></fn><t>,</t><n>1</n><t>,</t><e><r><s>%e</s></r><r><f><r><v>x</v></r><r><s>%pi</s></r></f></r></e><t>]</t><lbl>(%o42) </lbl><fn><r><fnm>f</fnm></r><r><p><v>x</v></p></r></fn><t>:=</t><v>x</v><v>+</v><n>2</n><h>*</h><e><r><v>x</v></r><r><n>2</n></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>A: genmatrix(lambda([i,j],float(integrate( (S(x)[i])*(S(x)[j]) ,x,0,%pi )) ), 4, 4);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="A">(%o43) </lbl><tb><mtr><mtd><n>10.33542556009993</n></mtd><mtd><v>−</v><n>2.0</n></mtd><mtd><n>4.934802200544679</n></mtd><mtd><n>9.869604401089358</n></mtd></mtr><mtr><mtd><v>−</v><n>2.0</n></mtd><mtd><n>1.570796326794896</n></mtd><mtd><n>0.0</n></mtd><mtd><v>−</v><n>1.074678198508553</n></mtd></mtr><mtr><mtd><n>4.934802200544679</n></mtd><mtd><n>0.0</n></mtd><mtd><n>3.141592653589793</n></mtd><mtd><n>5.398141569083773</n></mtd></mtr><mtr><mtd><n>9.869604401089358</n></mtd><mtd><v>−</v><n>1.074678198508553</n></mtd><mtd><n>5.398141569083773</n></mtd><mtd><n>10.03590585188679</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>b:makelist( integrate( (f(x))*(S(x)[i]),x,0,%pi),i,1,4);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="b">(%o44) </lbl><t>[</t><f><r><n>3</n><h>*</h><e><r><s>%pi</s></r><r><n>4</n></r></e><v>+</v><n>2</n><h>*</h><e><r><s>%pi</s></r><r><n>3</n></r></e></r><r><n>6</n></r></f><t>,</t><v>−</v><n>4</n><h>*</h><s>%pi</s><v>−</v><n>2</n><t>,</t><f><r><n>4</n><h>*</h><e><r><s>%pi</s></r><r><n>3</n></r></e><v>+</v><n>3</n><h>*</h><e><r><s>%pi</s></r><r><n>2</n></r></e></r><r><n>6</n></r></f><t>,</t><n>2</n><h>*</h><s>%e</s><h>*</h><e><r><s>%pi</s></r><r><n>3</n></r></e><v>−</v><n>4</n><h>*</h><e><r><s>%pi</s></r><r><n>3</n></r></e><v>+</v><e><r><s>%pi</s></r><r><n>2</n></r></e><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>b:float(b);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="b">(%o45) </lbl><t>[</t><n>59.03997107710115</n><t>,</t><v>−</v><n>14.56637061435917</n><t>,</t><n>25.60565332074455</n><t>,</t><n>54.41209461635496</n><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>y:invert(A).b;</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="y">(%o46) </lbl><tb><mtr><mtd><v>−</v><n>6.498616228471292</n></mtd></mtr><mtr><mtd><v>−</v><n>1.455349139239274</n></mtd></mtr><mtr><mtd><v>−</v><n>22.05723587816783</n></mtd></mtr><mtr><mtd><n>23.52103729650935</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>S(x).y;</line>
</editor>
</input>
<output>
<mth><lbl>(%o66) </lbl><v>−</v><n>1.455349139239274</n><h>*</h><fn><r><fnm>cos</fnm></r><r><p><v>x</v></p></r></fn><v>+</v><n>23.52103729650935</n><h>*</h><e><r><s>%e</s></r><r><f><r><v>x</v></r><r><s>%pi</s></r></f></r></e><v>−</v><n>6.498616228471292</n><h>*</h><v>x</v><v>−</v><n>22.05723587816783</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>wxplot2d([S(x).y,f(x)], [x,0,%pi])$</line>
</editor>
</input>
<output>
<mth><lbl>(%t67) </lbl><img>image1.png</img>
</mth></output>
</cell>

</wxMaximaDocument>PK     d��N\~N��1  �1  
   image1.png�PNG

   IHDR  X  �   �W��   bKGD � � �����    IDATx���y\T���3,8;����"Ȧ������(j�a�X�JK��\�Ms)�*]0-�f
B������>��,.��80���k��0�3�y����Cs�yy���{Ι˲   Wi�   @�  8E  ��"  NC  �� ��P�  �i(B  �4!  p�  8E  ��"  N��D"�P(���
		�x3�ݻw��s�N�1 @�h����ճg�޾}�ҥK�b�Y�LMMW�Z������Z���iFFFds ���dGX^^>w���˗kkk;::Λ7/99�a�������iӦ���'$$���N
  *���L&sttܵkWII	�07n,**:t�P�>}~���N�Ңdg �;zya�Q<���J�K�,ihh8u���oii�����O�ׯ��̌��{�(���}#b0(5A߈J�Ǫ JN�v(//������QQQ|>�a�-�0���}EE�t  ���)���t??���]]]�a���\\\<!//��Ύ\@  PE�aUU����U�������kjjD"QEEŶm��ry||�����H'  �B�y�͛7oڴ��̚5+**�ҥKAAAYYY}��		y��W;=�����#b0(5A߈J�Ǫ J�ܸq�ƍ��H$�x�<�k0(�@߈�vT���(�� �GQ�fRr�  @1(B  �4!  p� @	���[###�PX]]��c���� � �`߾}�ׯ/,,433��Q,ˆ��O�:U*��\6x:! ����������o6n�XXX���ی3���뒒##����j̘1x���Z[[����Ow�؁W2��" x^��ɶ�����۷oߓ����P]]������q���G�2��������o��x���`��`�%/� P}����333;v�X,f����Gs��zv�  =���?�|������N(<
E �L�t\d����hx6! �2���___߾}R�������3� �IOO���߿���iLL̢E�H'�g��SF�� �(��L� ��P�  �i(B  %����noo��W_��φ� (SLLLjj����e2�رc�N����N:<� @�����_kkkkkk���߾}�t"x���Qew@�*HKK������
���(ek&� t.!!A$	�B++����K����3k --m���~�srr���{0q���I'P6� �Duu��ٳ�o߾t�R�X<k�,SSS���(--��K�ZZZ>�^����/_���_~=z��_߼y�H$Z�fM����|>��J�! @'����Ν�|�rmmmGG�y��%''������ͫ�������/<(
KKK�	733���,++[�lYLL�#-��N=������#�B�X`Y� �)d2����]�X��>}z~~���wSS˲������EEEB�022�e�w�}WKKK�?�>w����������nǟ�tx'6mb�7��i���WeI�>wV�P�fRu�Sa�]� %�J�K�,ihh8u�������8;;kjj����ٳ�I��֭[ì^�z�ȑo���0���x]9\E��3'�WO�)x�w��&Mk&N� <Qyy����Ϗ��z��155]�payy�믿��o���kmmmmm-LLL:����x�*`Y&l�O34����Ѡ�8h �����{xx���EDD���v��������ŋ>�®{��{��5i�2���1FF��(N	2N��c���\]]���:����gjj��s�R��ѣ,X���9���o˖-]�+���^�x�nм1��?7���_�m�$y�Re��  �		yd��5k�#�Y�f����L&cY�ҥK|>?))���s�;�*�gug�X���_�lͤ��E�o7  �����l�q�V���8F�םS�f��  й��Q�w~0.��p҇� �¢�ύ���Y�'��g�Q  xT��&���|�Y�'�,=���
��|7 ��c��^�cj��N�@ٚ�S�  ��eb�}�),��9D:K/A �_�DM-�oT������KP�  �_�L�X�;�mE�2��<��(;� �����W��5v_>�����TFa�M* @w�Hؤ-]���}�)[3qj  �(�OGjW���D:(B  �;�<ʻ Ԩ ��죦��" ���N_.?~Bgp?�Y��;�  p��̺~oͩY���l�Y��ꂧ�(�� �-M�/�vuzk��lͤj0
�lR ��e���k5�:��b45�u,ek&� p���9��Zܸ���� �s�|{i�������3"��<�, �-�WnY�YX�%�b�#�,*���
��|7 ����k�0��䙢��&���TFa�M* @�X�97t���vt�Q��S��P�f�!  W���kpU�����ӂ�A p/�t��B���k* �E��f  ���[���=��gK:�ʡ�<��(;� ��u��E��w�M�|C��L��0�& ��6i[ƀ��8y_١��Iٚ�S�  4�(Z�jh����t�EO&$$�D"�Phee��ۊX,�4i�@ ���=y�$�  �*qyؠܸ�W"4�\�������gϞ�bŊ�����������N.�ϙ3gܸq555{��]�hQAA�  �$so���xQ'�}Ԟ������v�
�x�nݺ[�n����3f̨�����ff�ҥ���۷o�p��w �'��}<˂�=��3���)[3)����=h������X���\{{��d���!++�\F �^�TyO:��<��{��CI> �J_y�����\����I ��u������I��.88�7� ([{�<wԒ*kO���z���{��[�BU�����������(>�/$Ƀ�*�H�Bᓎe�E j*�c#�y\�^)�-88�}�R��ꠧ���=<<���"""tuu�qtt��r���	999NNND3 ���o���>3��~��fWQr�������5(((  ��+zzz���,ظq�������\\\?��� �M׿����$q�C����_DٚIɎ���w���裏��k�����}��%ss��z+44�� �@YLv�����kO� }�ju�Q�� pM����ś����ek&U�Qe�
 ��z���jr�Ӭ�����HٚI�`F٤ ��l�%�fv|�ϚZ�����L�V ��R��C�V�{i\�� }P�  �*#(���Q��K�&|�Y�%w� pM��wo�;xb��9�,�E �~��Y�r���G�΢����0�.� �Z���"�6y��+��lͤj0
�lR�b�}Y��酆�/�}M���)[3���(�T �˦���XZ;��w����l��]�  j#k~�f~�}�9�-H! �z��ayHr&�����]��F �@ix�ٖ�����cA:mP�  ��&1W?�?�Y�@:����0�.� M����C=����߱����ek&U�Qe�
 �hon[M�0}^�&R/�xek&U�Qe�
 �hoϴ_p�I�Sr���25Hݚ��F TT���Uu��"T��" PE�olӻx���c|�D��]�  *���#�w7�zj��1�,�C ���_�~��`G��9V��pU<Fم_ P_�sڼ'_z/���Hgy"��L��0�& �����:�D���#Hgy��L��0�& ��,)���j9���U�%���lͤj0
�lR@������K�̦�����,���x�  y�ߩ�%�*��� }P�  ��,��=���s��X�	� �����}~=��h�O:G�u�  Ĕ}m���N9M�G:w� ȸs�`��O�[6�tN����Qv ����y��']X���g���m���TFa�M* �8iqe��q&m\pj�,��lͤj0
�lR@��kn��$۾�ʕ�*���'�lͤj0
�lR@e���B�]��_����([3���(�T PQry�ü�:�	7~��f�a��L�� �W�l��[�����u�E �r���z%s@�Y�j{J�R(B �W��_z'��qx�U�� ���/����R�����@'P�  =�f�i�O��89f�5�,�9! @O�s�`���~�yύtx"�n�Ue��*��p]>yZ²0u|���lͤj0
�lR���������y���Y���5���(��I �dwo���n�������L��0�& ���+���2�A�G�ކAٚI�`F٤ )l���vz����outH��1���TFa�M* ��֖;b~i�����(�2Cݚ�w� P�����j*[ǈ��nA�� � c�zyn���q���Ig��A <��9k]�7J��o' ��E �\�l�9��ƅ�]�HgE�D6 ����o��-�O;��,� ! ��
�<f�mH��ǩ�����P�  ���/����+���h�,�\P�  �V��9���_Ey����P{(B ���-�ϲ��}p�g��Y@	�zw �Q�.	 �sn���9kF�[?���,�Y��l�Ď ��j�_�|q慀�\nA��V�iiiFF��g��ݼ��ܹ�`6 Pk���mSg$.�57l6�,�L�loY�=x��u뚛��Ri�.\hcc�jժ��FFF����K�6 ��^f�d�����E�F:y�����?���;vlڴ��/&%%M�6���?!!�� x�������c�CR��"������z������JGG����իW�X�"22�I���.88�7B�ʓUV�q�����_�Z�Y�=�t(%�j{�0̕+WƏ���hKK���^ǟׯ_�����Q�m�@YZ�֗��9��9����q�
�lͤ|b� �0���� �zi�n(���m��bZ�f4�mTT���˃�yyyvvv� ���k.1;�@4=k�6>a�j4�H$���ضm�\.���?p�@PP�P �����κ�����SG��Kb������,::�رcFFF���{��8q"�P ������μ�3|�x��Z�~T]�Te~@a�F�x�7��N�G>	ek&�;B �na%-��/�`�x�b/�!Z�  �Vz?�a�ͶA�?��$p& �a[��CgW�7���o���[�# �c��s�ϫ�o�)h���E ��ޗe9.�nx��7Dr� 8��5�yaM��{��Bc���� �b�˲F�\S�1��S�yw���XYkֈ�kdny��Ђ��" �ae��6���Y萎���[:Z��N�,>f`�! pI�T�9|ac�M|\h�'T^7
 \��J��f-��������-�L�-��?����v� @?ySK���zY��EhAx� (�� ɱ�}�5�c��`�(! �LVל7��;^�z�x5�� ��r�������v\�!]}��s(B �S�ͺ�a�*��O�ߧ�����8 �B���+���=9k��>k�E �i߹�<�d贩�_k`��g�� �JC��j�)7�f��}��V� E ��{�f��w�hɤ�_��jE ���X"M.�����?Hgu�" �����x���SA����A��+��՜>�0 dұդ���A�z�9��?{�U�'�{�,��P� ��2��`�Ƭ��ڹ�tPW(B PWW>�������tPcx�= PK����v�;��N�
���E �'qy�����s��B:�=! ������9��4&~ش��� P� �Nο�ՠ3��	��HgJ�@=���_p_cQ�d��`:ܜt�� ԀL�v��M��"˼��I���@�5ݑ�:-hj-��3�#h���J�[PWd�+71Ur-=E ��fJU�����C����3X�#P� ��
���6Χ�g����>cz� TQ֑�>3}�����#�t�� T����-�|��x��I:���@�Ŀ��˷o����� _�Y�P� �B������/���F:p� TB��=��]v�Fr������ ���<iS[�ӊ��f��֦�� �������3��jܳ��k.$8w� I�r�n�N�c�ǩ�Z�@ 1��4������󣆎6�8�Q(B  #�@��,�{�W�>�o�����a�rF��=��Ig���2 ����|��ys�	��cHg@@/���g�|0,�$��8D4�t �A@����R��l*6͹ bF:��! �uyV�m���x� �! ����%����^xD[�K:�ߠ�g��Л�S�p�g��&�P9�G	 =���خ���ɶ���2	PQ�aZZ���у�b�xҤI������ɓ�pM{;sr�n�]o4�r^H:��S�,ˆ��O�:U*�v|E.�ϙ3gܸq555{��]�hQAAِ !m����4/%Y�K:���X�%�A96o�|������6ttaRRҌ3jkk����Y�t�������?�ǣ�� @\UAc��Ez�����kaH:(ek&=;������//�_��͵���hA�a�������MN"��.�Ђ��)BKKK��߷���I <x(����t8��{.* ��%�����;��v1�����C:�����2�@"�<x(�H��'~�M�| "N�<�v����pz�tP�����7�u!�E���(��r���&�0999NNN�CP���=5�����Ɯ��L:@��sj�q����6m��dgΜ9~�x@@ �P ������
�)�J�5D�PCC#::z�ʕ�����桡�...�CP��ruݤy&���k
�����n�Ue���;����U�]������BٚI�Q �!,�_r�i���-;\�-j��S� ��K��q[���y���Ѥ� </! tíi��#�B��]�~�� (N�@W�E�Uۏ0�gw3-�@@��mL�?ϓ��B�yz�p]�N��3��2�M�?�¦��C#�M%@�P� �45��SF���|U��E3�!�� (� �(�B�=�����O�
�} ��5B �\��t�IcSDn�GЂ@1! <���9��㈵�[�~��%���h�S� �7��޿�aLe,s挍>��"���+o~a���}��^�67"�7�� �W�?�
��e��s��;Z�;B `�ژ/�p&�i�!��)�� �*! �Ք6ey,w��&qL:@oéQ N��_3T�?�pH�EC� p����x�ĠW}�+ܯ����!����zy�O'��~�t��A: I(B ��>]�4{����I�5~S�q éQ n������©��hA ;B �h��Fyo���)=�̗t U�"���s��g-i����ZY���Bpj�~|r�h�{�����tЂ �! �Z$l��S�|��ݿ]W�$@��%�x���e��Z��)f�����pj�Nq�&����</@<v� �i�o;�qLޡ��ޘL:��CP%'�T����Ġ𚅕9�8 j �F����}_�9�ץ�Z�k�#�A]����uN���<o�8 �;B �w9,������}˯Bt�@���1���84Ї}c�[n�̀t" ��S� �(��t��n�E��D�=�8 �
;B ��ᬎh���Cn_2F<��̽ڶ�?����iw��;/����P� ��ʏ9z+^�hmP�i>��t ��(�zhkeO���f���5עc:hA %��@��W��^6L�{1��Ît �`G��X��Y~�h�(-�q�w.���;B �u��޵)��>-��ѯ�#�N���3�<w��y?M�;��zv� *��L�:��J�7~��}5>V�gaG�Z�}��`;j��-��t;� @�Î@U�ܔ\����#u[��l�C: W`G���]��e�Z�_�a��E�V]&I������u[���zv� $�ے�h�f��3,���  ��QKzyf�ȬC��ߎ�x.�8 ܅! �/�r�{��a�uG�  Q����7$�37{�߼{����  v� ��e�JI�  �IDAT��5�Z�:�ScT�=-��#�%W��^Z�Xw�u߿ݗO& ��!@�jogN�������J��v�� ������D�/yǦ�R��I7�1�� @'�#�-�m�3wY��x{�N�PU�(_ʮd�o�3�b._v=�t x!�2��o���G.7"�6|�e�8 �l��ݽ{7�!;w�$��%�31+��;:
�of���eY�z�mllV�Z�����H__���x����e�[��e[�o��|�<P��5��aRRRQQѴi����:mA���p�%V<�_�3a���khA �Cs���VVV:::FGG�^�zŊ���Oz2��{1)�%�e�W��8���V��s��d0O�O:���C:��Q��}\KK���^ǟׯ_������(��C/�=!nx���-Œ/w;��K:@��lͤyG�0̃d��޾���`�C}yӟ���;w��;�!-��h.¨�(��������uǶ�	��b� l����)�e�/@P{4�H$���ضm�\.���?p�@PP�P�����4����������#� ���"433���>v옑�Q``�޽{'N�H:����s#�1y��ދ��է_�I: (U<Fم_PY�<�������.t>bhmL:�J�l����]���gk�
�%'N���D: �!��J�
�_�dPMz��-���;�P��k� �U[P��Z��CdY���! �0��Iv~�.����*c�_�p|�� o�	(B�:�����/w��u/�k�I����̱/�P �{p�8-cO�և�1l͎���I� P��Q7�s����nF���=w��Ӡ�}���pj8��ղ���П=Y2�ۼ&�k�"�  ���CKj.z��3��d�����'��� ���Zj$f~�j;\Z/m��=��FV�C�J@�Z�e�`1L#+��T������ 
 Tn�jɥ�)����3]�ua��M: �"!P�]�vu͡~��i�����˘�"҉ @u��*l��ں����)Oת�˃c׎'� T�(��ɯ�a��g<~���P���N �Ej�Ajh���x��'�yxY  t����v���mA��P����em���{`Og * ���L[��ڻ�~������� �EjCz�^��ٜ�)7r�����5^� P�����~s�������V�����3�D @����;���G�#��\�P��S��+Z �
E*�䷫���hx���%Ws��};�w�P @!�ò9;b۾�ڴ���7�6>��J�t& ��eY�����s ��I����ɏ;����Z��e=mҡ ����TFa�M��i�)�{�{����1k֌��G��Tek&N�I7J�	�cUg�R��d��� �� Vz?����YI�ͩ��F��0����
�l��ʚ*���;��g�d�{ޟ����a �ek&v��+X�4<�n�^��s�m��Ώ{u�L  �"��&�]�����}�V�M߷��
�YP P)TmoF�6_ET�t+d�]��}gi��r�?�k�� T�l�Ď�LVY���a��Bۤm^���M�� �DT���(���-��Lݶ�!�+�_�|+�k�x�@%��L��y5���c�����SM���lH: @W�AA�����~���P��w/h���׻��4I� �&!t�\^z�v�!���É�w,BfͰ�! @A(B誻qie_�l��soPŤ ��]>��C </!<C��¢�"��~n��*��83&`�H�)6 Ђ�;F�PJ!�Q�|D�d�A}�Uۅ��E�tuI� @ٚI�`F٤>iɭ�/�iG����j9�Y�����Ƹ �BٚI�`F٤*@����QG�Ug]����7����惰�NP�fR5�Q6�]w/��p�1�?~3�/�j�R�\��N3�-� �T���TFa�M�3��ݸ��=�&��5Wg���������q� t	ek&U�Qe��)V�R|�L��A��Mr�\{?��1�E!n ��l��&�rME����8e[_�7�����v_lo�� `EH'���HꭰӔ���E�,|��_��nD: �ʡj{�0:��5��E���Μ�-={G˲l�L��o��c�� e�c�|���(L}'�����_geQqV�j�Is-���L��4[��� ��w��U�Q�zM��nc���'L��ܜ�c:�Q4��i#8j�� ���k�|&��0՟�撻E�c/��$h�+0]��c<w��rO!�t: ��_3����(L5'��Yq��$&1�q����l�q�.�fM�l���y bTs�TU�Q��L��ZRx�jݩd�km�\j�iYN���2��0|��N�� ��"k��P5������mE�_��IaRS-n^�RX��T=ԓ��9�OK�A�	 ��(+B��H������v6 �Ӛt���������v��|_(��nD�%�k�x~��9[088XI�U���Aю�VWX�v��Ɣ^����~?%C7?��v����zCk��.�F����k����������Z�oD��cUU�Q�R&U&cn�ܽs6��j�fn�qe��$�M�_a��h�5��d����q�'e�L;`Pj��1�R�UAT�qb���7�LII�ׯ�Ν;_z�N���Imog*�ͷ�
��e�b��B���<-��r#�&+G���x�����)i(�C�?��Z�oD��cU��5*���̙3o޼���������_�vmذa��&U��[iU����lQ1��q]�@i�9S+�3�1�k��3Kd�`0q���~�=4  �T��#���f̘Q[[����0�ҥK��ͷo���3y<^�/�yU��rYi�����������1���j�%�ۭm��m�F�gǷ��x�>�������Z�oD��cU�;���\{{��d����ܹsOz�W���Z+��J��"��0��N�cx*��
à�}#b0(��\�MMM���C�@�����3i��  ����
�D��D"
��  �
���b�\.�x������D6  ���������bӦM2��̙3Ǐ 
  T��544���W�\innnnn���B:  ��n�  �.�O�  <�  8E  ��"  NC  ���a�b�I�����ɓ'I�Q�3��{�n�Cv����!�"--��Ȉt���!P0M			"�H(ZYY�����}��3���,
mmm���K:Q,絵�988|���---���� ??�t����,X�������F"Q�G{{{XX�������,
z��}��޽khh*�ɲ��������C:T�ue�>S���|>?66�eٴ�4==��/�E��MLL��ח�d֮]K6Rwue������>|�رc��^Ϩ!!!��λv�R�"|��}����^��׮]�x�b�yӕQ��L�,[[[�񇄄�P���M6A85�ɧ5eee���]�Biiiee���ctt��իW�XI"�s	������"DqO������������X����QP0S�߻w��������7�ttt$����b����iM*�C<x�D"���c���&==��ﾛ3gNo}>����#<����i� �J�,Yҿ��+W�΢�'������ׯ����ə6m����W�^M:���iM]B��i;���WTT�R8�:������Ǉ��GEE��|�q��Q�1SNNN������+�8Ġi���g!**��7��˳���Ո�tLSzz������_DD���.�8
z�((����X77�%���1�<���HI�\.6l��|���ӧO���gdd��=O3g233Y��{�����֭[���Ξ=+
���	�VTjj���,��!�4M����������)���I��'�������633ۺu�L&���7008y�$�PĠY�e�b�)Slmm���q�����׭[�����d�H���occs��!rI�}EH�4���<���Y�H��'����bY655���K__�����&�$|  p�  �� ��P�  �i(B  �4!  p�  8E  ��"  NC  �� ��P�  �i(B  �4!  p�  8E  ��"  NC���
����R�a������*++I��>�@͟?������wss;|���ٳI'��@���8;;kjj����ٳ�t ���(�*255]�payy�믿N: �#PEiii&L�3gNNNNJJ���6�D �@�H�Ҁ����?,,L&��N@3�TNPPЙ3g�^����}��eoo���x///ҹ �"  NéQ  �4!  p�  8E  ��"  N����� I]    IEND�B`�PK      d��N�B�H                       mimetypePK      d��N�T�D    
             5   format.txtPK      d��N�)F�  �               t  content.xmlPK      d��N\~N��1  �1  
             �  image1.pngPK      �   �G    