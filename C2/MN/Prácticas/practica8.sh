# a
n=10;
d=2:2:2*n;
v=ones(n-1,1);
A=-diag(v,-1)+diag(d)+3*diag(v,1);

# b
cond(A)

# c
b=2.^(0:n-1);
X=A\b';

# d
Relativo=norm(A*X-b')
Absoluto=norm(A*X-b')/norm(X)



function s= spline32natural (x,y)
   n=length(x)-1;
   for i=1:n
      h(i)=x(i+1)-x(i);
      p(i)=(y(i+1)-y(i))./h(i);
   endfor
   A(1,1) = 2;
   A(1,2) = 1;
   b(1)=3*p(1);
   for i=2:n
      A(i,i-1)=h(i);
      A(i,i)=2*(h(i)+h(i-1));
      A(i,i+1)=h(i-1);
      b(i)=3*(h(i).*p(i-1)+h(i-1).*p(i));
   endfor
   A(n+1,n)=1;
   A(n+1,n+1)=2;
   b(n+1)=3*p(n);
   # d=A\b';
   # s=spline32simple(x,y,d(1),0);
endfunction


















            )
