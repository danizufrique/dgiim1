# Ejercicio 1.
# a) Calcula el polinomio qde grado menor o igual que 3 que mejor aproxima
# (en el sentido de los mínimos cuadrados) la tabla de datos.
# b) Representa gráficamente el polinomio anterior junto con los datos de la tabla
# (1) en el intervalo [0,20].
x=[1 2 4 8 16];
y=[30 20 10 50 100];
q=polyfit(x,y,3);
dx= 0:0.01:20;
plot(x,u,'o',dx,polyval(q,dx),'b')

# Ejercicio 2
# a) Calcula el polinomio impar de grado menor o igual que 3; es decir: de la forma
# p(t)=at^3 + bt que mejor aproxima (en el sentido de los mínimos cuadrados) los datos de la tabla (1).
# b) Representa gráficamente el polinomio anterior junto ocn los datos de la tabla (1) en el intervalo [0,20]

G =[x.^3,x];
q=G\y';
p=[q(1) 0 q(2) 0]; # También puede hacerse con una función implícita
polyout(q,'x')     # f=@x p(1)*x.^3 + p(2)*x;
plot(dx, polyval(p,dx));

# Ejercicio 3
# a) Calcula la mejor aproximación por mínimos cuadrados discreta de los datos de
# la siguiente tabla mediante una función de la forma: p(t) = a_2*sin(t) + a_1*cos(t) + a_0
# b) Representa gráficamente los datos de la tabla y la función que has calculado.

x=[10 20 40 50 60];
y=[8 12 18 13 7];
G = [sin(x) cos(x) pones(1,5)]';
f=@t p(1)*sin(t) + p(2)*cos(t) + p(3);
