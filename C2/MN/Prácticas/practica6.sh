# Ejercicio 1
# a)
f=mkpp([0 2 5 7 10], [1 -5 7; 3 13 1; 0 4 67; -1 -4 75]);

# b)
ppval(f,0:10)

# c)
dx=0:0.1:10
plot(dx, ppval(f,dx))

# Ejercicio 2
# a)
s=csape([0 1 2.5 3 4], [1.4 0.6 1 0.65 0.6 1], 'variatonal')

# b)
ppval(s, 1.5)

# c)
dx = 0:0.1:4;
plot(dx, ppval(s,dx))

# Ejercicio 3
# a)
x=-3:0.5:3;
s=spline(x,sin(x))

# b)
dx=-3:0.1:3;
plot(x,y,'o',dx,sin(dx), 'r', dx, ppval(s,dx), 'b')

# c)
ddx=-3:0.00001:3;
max(abs(ddx)-ppval(s,ddx))


# Ejercicio 4
# a)
dx=[1 3 5 7 9];
dy=[20 40 50 30 10];
s=spline)(dx, dy, [3 1 -2 -1 0])

# b)
ppval(s,4)

# c)
plot(dx, dy,'o')

# d)
dd=dev(dx);
plot(dx,dd);
