# Ejercicio 1
function [p nv na] = polintercern(x,y,v,a)
   h=x(2)-x(1);
   d=(y(2)-y(1))/h;
   c=(d-v-a*h/2)/h.^2;
   p=[c a/2 v y(1)];
   nv= v+a*h+3*c*h.^2;
   na= a+6*c*h;
endfunction

[u v a] = polintercen([1,3] [10 6] 4 -5)

# Ejercicio 2
function s= spline32simple (x,y,v,a)
   n=length(x)-1; # número de subintervalos
   for i=1:n
      [s(i,:) v a] = polintercern([x(i),x(i+1)], [y(i),y(i+1)],v,a);
   endfor
endfunction


x=[1 2 3 5];
y=[10 25 40 30];
spline32simple(x,y,4,-1)
