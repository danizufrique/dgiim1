# Notas

## Tema 1: Diagonalización de Endomorfismos

- Las matrices de un mismo endomorfismo son semejantes: tienen el mismo *determinante, rango* y *traza*.

- Una matriz cuadrada A es **diagonalizable** si es semejante a una matriz diagonal.

- Un vector $v$ es un **vector propio** de $f$ asociado a $\lambda$ si verifica: $f(v) = A\cdot v = \lambda v$.

- La aplicación **giro** no tiene ningún valor propio.

- $V_0 = Ker(f)$.

- Si $\{\lambda_1, \cdots, \lambda_n\}$ son valores propios diferentes, los vectores $v_i$ asociados a ellos son **linealmente independientes**.

- $f$ es **diagonalizable** $\iff$ tiene $n$ valores propios distintos.

- $A$ y $C$ son **semejantes** $\iff $ tienen el mismo polinomio característico.

- $P_A(A)=0_n$.

- $M\cdot Adj(M)^t = det(M)\cdot I$.

- $g_\lambda = n -rg(A-\lambda I_n)$.

- $$
  P_f(\lambda) = \lambda^2-traza(f)\cdot \lambda + det(f) \quad \quad \{n=2\}
  $$

- $$
  P_f(\lambda)=-\lambda^3 + traza(f)\cdot \lambda^2 - (A_{11}+A_{22}+A_{33})\cdot \lambda + det(f) \quad \quad \{n=3\}
  $$

  

- El valor propio de $P_A(\lambda)$ coincide con el de $P'_A(\lambda)$.

- $g$ sem. pos $\or g$ sem. neg $\Rightarrow  Cg=Rad(g)$.



## Tema 2: Formas bilineales y cuadráticas

- $dim(B(v))=n^2$.

- Matrices **congruentes**:

  $b(u,w)=(a_1, \cdots, a_n)\cdot M(b,B) \cdot \begin{pmatrix} b_1 \\ \vdots \\ b_n \end{pmatrix} = (a'_1, \cdots, a'_n)\cdot M(b,B') \cdot \begin{pmatrix} b'_1 \\ \vdots \\ b'_n \end{pmatrix} \iff M(b.B)=P^t\cdot M(b,B')\cdot P$ 

- Las matrices de una forma bilineal en dos bases distintas son congruentes.

- Dos matrices congruentes tienen el mismo **rango** y signo del determinante.

- La simetría se conserva en la congruencia.

- A simétrica, B antisimétrica $\Rightarrow$ A y C no congruentes.

- Semejanza no implica congruencia y viceversa.

- $C\subseteq V \Rightarrow C^{\perp} $ es s.v. de $V$.

- $g$ es no degenerada $\iff det(M(g,B)) \neq 0$.

- $dim(Rad(g))=n-rg(g)$.

  

- | Clasificación de $g_A$ |   Criterio   |
  | ---------------------- | ---- |
  | semidefinida positiva | $det(A)=0, a\ge0, c\ge0$ |
  | semidefinida negativa | $det(A)=0, a\le0, c\le0$ |
  | euclídea | $det(A)>0, a>0$ |
  | definida negativa | $det(A)>0, a<0$ |
  | indefinida | $det(A)<0$ |

- $g$ no degenerada no implica $g_{\vert U}$ degenerada.

- $Rad(g)\subseteq U^{\perp}$.

- $U\subseteq W \Rightarrow W^{\perp} \subseteq U^{\perp}, \quad (U+W)^{\perp} = U^{\perp} \cap W^{\perp}$.

- Para métricas no degeneradas:

  $dim(V)=dim(U)+dim(U^{\perp}), \quad V=U\ominus U^{\perp}$

  $(U^{\perp})^{\perp} = U$.

  $(U \cap W)^{\perp} = U^{\perp}+W^{\perp}$.

- $S=\{v_1, \cdots, v_k\}$ subconjunto ortogonal, $w_g(v_i)\neq 0 \Rightarrow S$ es l.i.

- En todo e.v. métrico existen bases ortonormales.

  ### Isometrías

- $f$ isometría si $g'(f(u),f(v))=g(u,v)$.

- Se verifican:

  $g,g'$ isometrías no degeneradas, $f$ sobreyectiva $\Rightarrow$ $f$ isometría.

  $g,g'$ isometrías euclídeas o def. negativas, $dim(V)=dim(V')$ $\Rightarrow$ $f$ isometría.

- $f$ es una isometría sii $M(g',B'), M(g,B)$ congruentes:
  $$
  M(f,B,B')^t\cdot M(g',B')\cdot M(f,B,B')=M(g,B)
  $$

- $B$  base ortonormal, $M(f,B)^t \cdot M(f,B) = I_N \Rightarrow M(f,B)\in O(n) \Rightarrow f$ isometría.

- $dim(V)=dim(V')=n, rg(g)=rg(g'), índice(g)=índice(g' )\Rightarrow f $ isometría $\Rightarrow f$ lleva bases ortonormales en bases ortonormales. 

- Que se conserve la forma cuadrática no implica que haya isometría.

- $(Iso(V,g), \circ) \subset (Aut(V), \circ)$.

- Los valores propios de $A\in O(n)$ son $\pm 1$. El determinante también puede ser $1$ ó $-1$.



## Tema 3: Espacios vectoriales euclídeos

- **Gram-Schmidt:**
  $$
  v_i=u_i-\frac{g(u_i,v1)}{\Vert v_1 \Vert^2}v_1 - \cdots - \frac{g(u_i, v_{i-1})}{\Vert v_{i-1}\Vert^2}v_{i-1}
  $$

### Proyecciones y simetrías

- **Pitágoras:** $\Vert u+v \Vert = \Vert u-v \Vert \iff u\perp v; \quad \Vert u+v\Vert²=\Vert u\Vert² + \Vert v\Vert²$.

- **Cauchy-Schwarz:** $\vert g(u,v) \vert \le \Vert u \Vert \Vert v \Vert; \quad \vert g(u,v)\vert = \Vert u \Vert \Vert v \Vert \iff \{u,v\} $ l.d.

- **Desigualdad Minkowski:** $\Vert u+v \Vert \le \Vert u \Vert \Vert v \Vert; \quad \Vert u+v \Vert = \Vert u \Vert \Vert v \Vert \iff u=0 \or v=0 \or u=\lambda v (\lambda>0)$.

- Si se da la igualdad de Minkowski $\Rightarrow$ desigualdad de Cauchy-Schwar $\Rightarrow$ $\{u,v\}$ l.d.

- $d(u,v)= \Vert u-v \Vert$.

- $g(u,v)=\Vert u \Vert \Vert v \Vert \cos(\theta); \quad \angle (u,v) = \pi \Rightarrow \{u,v\}$ l.d.

  

- **Proyecciones:**
  
  - $g(P(u),v)=g(u,P(v))$.
  - $f^2 =f$.
  - $\Vert v-P_u(v)\Vert \le \Vert v-u \Vert$.
  - $v-P_u(v) = P_{u\perp} (v)$.
  - $P_u(v) \circ P_{u{\perp}} = 0$
  - La matriz de $P_u$ en cualquier base ortonormal de $V$ es simétrica. 
  
  
  
-  **Simetrías:**
  
  - $g(S(u),v)=g(u,S	(v)); \quad f^2 =I_V  ; \quad S_u(v) \circ S_{u{\perp}} = -I_V$
  - $S(u)$ es una isometría.
  - $f\in Iso(V,g) $, diagonalizable $\Rightarrow f$ simetría ortogonal.
    - $\lambda = 1 \Rightarrow f=I_V = S_V$.
    - $\lambda = -1 \Rightarrow f=-I_V = S_{\{0\}}$
    - $\lambda = \pm 1 \Rightarrow V=V_1 \oplus V_{-1}$ (simetría ortogonal).​
  
- La matriz de $S_u$ en cualquier base ortonormal de $V$ es simétrica y ortogonal. 

### Endomorfismo Autoadjunto

- $M(\Phi, B, B^*)=M(g,B); \quad \widehat{f} = \Phi^{-1} \cdot f^t \cdot \Phi, f\in End(V)$.
- $M(\widehat{f}, B)=M(\Phi^{-1},B^*, B) \cdot M(f^t, B^*)\cdot M(\Phi,B,B^*)$.
- $f$ autoadjunto 

$$
\begin{align}
  &\begin{aligned}
    \iff M(f,B)=M(g, B)^{-1} \cdot M(f, B)^t \cdot M(g,B)\\
  \end{aligned}\\
  &\begin{aligned}
    \iff M(g,B)\cdot M(f,B)=M(f, B)^t\cdot M(g, B)\\
  \end{aligned}\\
  &\begin{aligned}
    \iff M(g,B)\cdot M(f,B)\in S_n(\mathbf{R})\\
  \end{aligned}\\
\end{align}
$$

- $\Phi(v)(u) = g(u,v)$.

- $f=\widehat{f}, \qquad dim(Ker(\widehat{f}))=dim(Ker(f)),\qquad dim(Im(\widehat{f}))=dim(Im(f))$.
- $g(v,f(u))=g(\widehat{f}(v),u)=g(f(v),u)$.
- $B$ base ortonormal. $f$ es autoadjunto $\iff M(f,B)\in S_n(\mathbf{R}) \iff M(\widehat{f},B)=M(f,B)^t$.

- $Im(\widehat{f}) = Ker(f)^{\perp}, \qquad Ker(\widehat{f}) = Im(f)^{\perp} $.


| Propiedades |
| ----------- |
|     $\widehat{g\circ f} = \widehat{f}\circ \widehat{g};$        |
|        $f^{-1} \circ f = f \circ f^{-1} = I_V$     |
|      $\widehat{f^{-1}\circ f} = \widehat{f\circ f^{-1}} = \widehat{I_V} = I_V$       |
|        $\widehat{f^{-1}} = \widehat{f}^{-1}$     |
|    $\widehat{\widehat{f}} = f$         |



- $P_f(\lambda)=P_{\widehat{f}}(\lambda)$.
- $f $ automorfismo $\Rightarrow f$ isometría $\iff \widehat{f} = f^{-1}$.

- **Caracterización:**
  $$
  \forall B:
  M(f,B)=M(g,B)^{-1}\cdot M(f,B)^t \cdot M(g,B) \iff M(g,B)\cdot M(f,B) \in S_n(\mathbf{R})
  $$

- **Lema:** $f(U)\subseteq U \Rightarrow \widehat{f}(U^{\perp})\subseteq U^{\perp}\Rightarrow f(U^{\perp})\subseteq U^{\perp}  \Rightarrow f_{|U}$ autoadjunto en $(U,g_{|U})$.
- **Lema:** $V_{\lambda_1} \ominus \cdots \ominus V_{\lambda_k}$.
- **Lema:** $A\in S_n(\mathbf{R}) \Rightarrow P_A(\lambda) \in \mathbf{R}[x]$ tiene $n$ raíces reales.
- **TEOREMA:** Sea $(V,g)$ e.m.e., $dim(V)=n, f\in End(V)$ tal que $\widehat{f} = f $. Entonces: $\exists B$ base ortonormal de $(V,g)$ formada por vectores propios de $f$.

- $f$ diagonalizable $\Rightarrow \exists g: f:(V,g)\rightarrow (V,g)$ es  autoadjunto.

- Toda matriz simétrica es diagonalizable por semejanza y congruencia.


### Clasificación de isometrías

- Las isometrías conservan ángulos, perpendicularidad , norma y todo lo que te imagines, excepto las ganas de vivir.



#### Espacio bidimensional

- $det(f)=1$:

| Requisitos                                                   | Función                                  |
| ------------------------------------------------------------ | ---------------------------------------- |
| $f=I_V$                                                      | Identidad                                |
| $f=-I_V$                                                     | Simetría central                         |
| $f=  \begin{vmatrix}cos(\theta) & -sen(\theta)\\sen(\theta) &-cos(\theta)\end{vmatrix}$ | Rotación de ángulo $\theta \in [0,2\pi]$ |

- $det(f)=-1:$

| Requisitos                                    | Función                    |
| --------------------------------------------- | -------------------------- |
| $f=  \begin{vmatrix}1 &0\\0 &-1\end{vmatrix}$ | Simetría axial o reflexión |



#### Espacio tridimensional

- $det(f)=1$:

  - $dim(V_1)=3: f=I_V$

  - $dim(V_1)=1: $

    - $dim(V_{-1})=2: f=$ simetría axial respecto $V_1$

    - $dim(V_{-1})=0: f$ giro o rotación respecto a $V_1$ de ángulo $\theta$
  
      
  
- $det(f)=-1$:

  - $dim(V_{-1})=3: f=-I_V$

  - $dim(V_{-1})=1: $

    - $dim(V_{1})=2: f=$ simetría especular respecto $V_1$

    - $dim(V_{1})=0: f$ simetría especular compuesta con rotación respecto $V_1^{\perp}$.