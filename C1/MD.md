#Resumen Markdown
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Resumen Markdown](#resumen-markdown)
	- [PRÓLOGO](#prlogo)
	- [BASES](#bases)
		- [Saltos de línea](#saltos-de-lnea)
		- [Títulos](#ttulos)
		- [Citas](#citas)
		- [Listas](#listas)
		- [Raya horizontal](#raya-horizontal)
		- [Tablas](#tablas)
		- [Gráficos](#grficos)
		- [Enlaces](#enlaces)
		- [Bloque de código](#bloque-de-cdigo)
		- [Imágenes/Emojis](#imgenesemojis)
		- [Tipos de letra](#tipos-de-letra)
		- [Pies de página](#pies-de-pgina)
		- [Listas de definiciones](#listas-de-definiciones)
		- [Checkbox](#checkbox)
	- [MATEMÁTICAS](#matemticas)
		- [Letras griegas](#letras-griegas)
		- [Operadores](#operadores)
		- [Índices](#ndices)
		- [Fracciones y binomios](#fracciones-y-binomios)
		- [Raíces](#races)
		- [Sumas e Integrales](#sumas-e-integrales)
		- [Notación](#notacin)
		- [Paréntesis](#parntesis)
		- [Matrices](#matrices)

<!-- /TOC -->
##PRÓLOGO
Esta guía es válida para cualquier uso de un procesador de Markdown, pero está especialmente enfocado para Atom. Para el correcto funcionamiento de este deberás instalar los paquetes:
- atom-latex
- markdown-preview-enhanced

##BASES
###Saltos de línea
Para generar un nuevo párrafo se separa el texto mediante una línea en blanco.
Al igual que sucede con HTML, Markdown no soporta dobles líneas en blanco, así que si intentas generarlas estas se convertirán en una sola al procesarse.
Para realizar un salto de línea y empezar una frase en una línea siguiente dentro del mismo párrafo, tendrás que pulsar dos veces la barra espaciadora antes de pulsar una vez intro.

Se podrá usar *\&nbsp;* para generar una línea en blanco.

###Títulos
Se usará el símbolo *#* tantas veces sea necesario.

***Ej:*** ##Título2
***Nota:*** dependiendo del procesador será necesario dejar un espacio entre # y el texto.
***Nota:*** para dotar de mayor importancia a una palabra se puede escribir en la línea siguiente "--"

###Citas
>¿Te has leído el PDF?
>> Nope

###Listas
- elemento1
- elemento2
  - elemento 2.1

1. elemento1
2. elemento2
  2.1 elemento 2.1

###Raya horizontal
***
---
___

###Tablas

|Título1|Título2|Título3|
|---|---|---|
|---|---|---|
|---|---|---|
|---|---|---|

###Gráficos
~~~~mermaid
graph TD
A[Christmas] -->|Get money| B(Go shopping)
B --> C{Let me think}
C -->|One| D[Laptop]
C -->|Two| E[iPhone]
C -->|Three| F[fa:fa-car Car]
~~~~

###Enlaces
[Gitlab](http://www.gitlab.com)

Soy el capitán y tengo un repositorio sobre la máquina enigma: [Garete][repos].
En dicho [repositorio][repos] recopilo artículos sobre todo lo relacionado con automatización, gestión y eficiencia.

[repos]: http://limni.net/blog/

###Bloque de código
Para añadir un bloque de código se usará:
~~~bash
mkdir ej1		
~~~					

###Imágenes/Emojis
![<Descripción>](/ruta/a/la/imagen.jpg)
![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")

- Clásico: :wink: :cry: :laughing: :yum:
- Abreviaciones: :-) :-( 8-) ;)


###Tipos de letra
- *cursiva* o _cursiva_
- **negrita**
- ***negrita cursiva***
-	~~tachado~~
- ==destacado==

###Pies de página

PiePagina 1 link[^uno].
PiePagina 2 link[^dos].

Inline footnote^[Hola caracola] definition.

[^uno]: **puede tener margen y estilo**

    y más de un párrafo.

[^dos]: text.

###Listas de definiciones
- **Estilo usual:**

Término1

:   Definicion 1
puedes continuar aquí.

Término 2

:   Definición 2

        { texto con margen }

    Otro párrafo.

- **Estilo compacto**

Término 1
  ~ Definición 1

Término 2
  ~ Definición 2a
  ~ Definición 2b

###Checkbox
¿El 0 es natural?:
- [x] Verdadero
- [ ] Falso

##MATEMÁTICAS
Para el uso de cualquier expresión matemática será necesario introducir la expresión entre *\$* para escribir sobre la misma línea y *$$* para escribir de forma centrada y aislada.

Como verás, el símbolo *\{}* está reservado, así que para usarlo deberás escribir *\\* antes de él.

###Letras griegas
1. $\alpha$
2. $A$
3. $\beta$
4. $B$
5. $\gamma$
6. $\Gamma$
7. $\pi$
8. $\Pi$
9. $\phi$
10. $\Phi$
11. $\varphi$
12. $\theta$
13. $\Psi$
14. $\psi$
15. $\epsilon$
15. $\varepsilon$
17. $\nu$
18. $\mu$
19. $\omega$
19. $\Omega$
21. $\lambda$
21. $\Lambda$
23. $\upsilon$
24. $\theta$
24. $\Theta$

###Operadores
1.  $\cos$
2. $ \sin$
3. $ \lim$
4. $ \exp$
5. $ \to$
6. $ \infty$
7. $ \equiv$
8. $ \bmod$
9. $ \times$
10. $\le$
11. $\ge$
12. $<$
13. $>$
14. $\leftarrow$
15. $\rightarrow$
16. $\equiv$
17. $\sim$
18. $\neq$
19. $\subseteq$
20. $\supseteq$
21. $\mid$
22. $\approx$
23. $\cong$
24. $\in$
25. $\div$
26. $\circ$
27. $\times$

###Índices
1- $k_{n+1}$
2- $n^2$
3- $k_n^2$

***Nota:*** nótese que para agrupar varios elementos es necesario usar *{}*.

###Fracciones y binomios
1. $\frac{n!}{k!(n-k)!}$
&nbsp;
2. $\binom{n}{k}$
&nbsp;
3. $\frac{\frac{x}{1}}{x - y}$
&nbsp;
4. $^3/_7$

###Raíces
1. $\sqrt{k}$
2. $\sqrt[n]{k}$
3. $\sqrt{b^2 - 4ac}$

###Sumas e Integrales
1. $\sum_{i=1}^{10} t_i$
2. $\int_0^\infty \mathrm{e}^{-x}\,\mathrm{d}x$
3. $\sum$
4. $\prod$
5. $\coprod$
6. $\bigoplus$
7. $\bigotimes$
8. $\bigodot$
9. $\bigcup$
10. $\bigcap$
11. $\biguplus$
12. $\bigsqcup$
13. $\bigvee$
14. $\bigwedge$
15. $\int$
16. $\oint$
17. $\iint$
18. $\iiint$
19. $\int\limits_a^b$


***Nota:*** los elementos que tengas "big" en su nomenclatura tienen un equivalente más pequeño eliminando "big" de la declaración.
###Notación
1. $a',  a^{\prime}$
2. $a''$
3. $\hat{a}$
4. $\bar{a}$
5. $\grave{a}$
6. $\acute{a}$
7. $\dot{a}$
8. $\ddot{a}$
9. $\not{a}$
10. $\mathring{a}$
11. $\overrightarrow{AB}$
12. $\overleftarrow{AB}$
13. $a′′′	a’’’$
14. $	\overline{aaa}$
15. $\check{a}$
16. $\vec{a}$
17. $\underline{aaa}$
18. $\color{red}x$
19. $\pm$
20. $\mp$
21. $\int y \mathrm{d}x$
22. $\,$
23. $\:$
24. $\;$
25. $!$
26. $\int y\, \mathrm{d}x$
27. $\dots$
28. $\ldots$
29. $\cdots$
30. $\vdots$
31. $\ddots$
32. $\cdot$

###Paréntesis
1. $(a)$
2. $[a]$
3. ${a}$
4. $\langle f \rangle$
5. $\lfloor f \rfloor$
6. $\lceil f \rceil$
7. $\ulcorner f \urcorner$
8. $\sum_{i=1}^{n}\left( \frac{X_i}{Y_i} \right)$ *(ajusta tamaño automáticamente)*


###Matrices
1. $\begin{array}
{rrr}
1 & 2 & 3 \\
4 & 5 & 6 \\
7 & 8 & 9
\end{array}
$
&nbsp;
2. $\mathbf{X} = \left[\begin{array}
{rrr}
1 & 2 & 3 \\
4 & 5 & 6 \\
7 & 8 & 9
\end{array}\right]
$
