##### 5.- ¿Coincide la característica de transferencia con la esperada teóricamente?

Sí, coinciden:

- Para $V_i$ menores que $1,5V$ aproximadamente (el voltaje umbral) el voltaje es nulo en $R_D$ puesto que todo el voltaje se queda entre la fuente y el drenador.
- A partir de $1,5V$ el transistor estará en la región lineal y habrá caída de tensión en $R_D$. La tensión entre la fuente y el drenador irá disminuyendo progresivamente.
- Por último, a partir de $3V$ el transistor estará en saturación y el voltaje entre la fuente y el drenador permanecerá constante en $0V$.

##### 6.- Para el montaje de la figura 5.3, mida e indique el valor experimental de $R_D$.

$R_D=9,96K\Omega$

##### 9.- Con la ayuda de Excel, realice un ajuste lineal de los datos de la columna E $(\sqrt{I_D})$ frente a los de la columna B $(V_{GS})$. Usando dicho ajuste, calcule los valores de la siguiente tabla.

|$V_{th} (V)$ | $\mu_n C_{ox}\frac{W}{L} (mA/V^2)$ |coef. de correlación del ajuste|
|---|---|---|
|0,9368411079|0,3331536465|0,99298577354154|



==Mª Isabel Ruiz Martínez y Daniel Zufrí Quesada==

