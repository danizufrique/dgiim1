#Tema 1
##Campo Eléctrico
######Campos escalares
- Variaciones entre superficies equiescalares (**gradiente**): $\overrightarrow{\nabla}U = \frac{\partial U}{\partial x} \hat{i} + \frac{\partial U}{\partial y} \hat{j} + \frac{\partial U}{\partial z} \hat{k}$

  ``Ejemplo``
  $$U(x,y) = x^2+y^2, \frac{\partial U}{\partial x} = 2x + 0, \frac{\partial U}{\partial y} = 0 + 2y, \frac{\partial U}{\partial z} = 0 + 0$$$$\overrightarrow{\nabla}U = 2x\hat{i} + 2y\hat{j}$$

######Campos Vectoriales
- **Circulación** de un vector $\overrightarrow v$ a través de la curva C que une A y B: $\oint_A^B = \overrightarrow vd\overrightarrow r $

``Campo conservativo``  $\oint \overrightarrow vd\overrightarrow r = 0 $



> ___Apuntes de Clase___
Forma general: $\overrightarrow r = \overrightarrow r_0 + t(\overrightarrow r_B - \overrightarrow r_0)$
Que la circulación no dependa del recorrido es una causa necesaria (pero no suficiente) para que el campo sea conservativo.

- **Potencial**: a cada punto del *campo vectorial conservativo* se le asigna un potencial.  $\oint_A^B = \overrightarrow vd\overrightarrow r = V_A - V_B = -(V_B-V_A) = -\Delta V$
- **Flujo** (Weber): $\phi = \int_S \overrightarrow v d\overrightarrow S = \int_S |\overrightarrow v| · |d\overrightarrow S| · cos(\hat{\overrightarrow v d \overrightarrow S})$

``Nota``
Si $\overrightarrow v$ es el mismo para cualquier punto de la superficie y $\overrightarrow v$ y $\overrightarrow S$ son paralelos:  $\phi = |\overrightarrow v| |\overrightarrow S|$
&nbsp;
######Campo Eléctrico
- **Fuerza**:
$$\overrightarrow F = K \frac{Q_1Q_2}{r^2}\hat e_r (N)$$
- **Intensidad de campo**: fuerza referida a la unidad característica de dicho campo colocada en ese punto.
$$\overrightarrow E=\frac{\overrightarrow F}{q}=K \frac{Qq}{qr^2}\hat e_r= \frac{Q}{r^2}\hat e_r  (N/C)$$
- **Energía potencial eléctrica**:
$$ W_{A \rightarrow B} = \int_A^B \overrightarrow F · d\overrightarrow r (Julios)$$
- **Variación de energía potencial**: trabajo realizado por la fuerza central al trasladar su punto de aplicación desde B hasta A.
$$\Delta Ep = E_{pA} − E_{pB} = W_{A \rightarrow B} = −W_{B\rightarrow A} $$

``Cargas puntuales`` $ \Delta E_p = E_{pA} - E_{pB} = KQq(\frac{1}{r_A}-\frac{1}{r_B})$
- **Energía potencial eléctrica**: trabajo que realizaría la fuerza eléctrica para trasladar la carga sobre la que actúa, desde un punto punto ($r_A$) hasta el infinito ($r_B$).

``Cargas puntuales`` $E_p=K\frac{Qq}{r_A}$
- **Potecial eléctrico**: trabajo realizado por la fuerza eléctrica para trasladar la unidad de carga positiva desde un punto ($r_A$) hasta el infinito ($r_B$).

``Cargas puntuales`` $V_A = K\frac{Q}{r_A} (Voltio = \frac{Julios}{Culombio})$

- **Diferencia de potencial**: trabajo realizado para trasladar la unidad de carga positiva desde un punto a otro.

``Cargas puntuales`` $ V_B - V_A = KQ(\frac{1}{r_B} - \frac{1}{r_A})$
- **Potencial e intensidad de campo**.
  &nbsp;
  - Potencial a partir de campo:
  $$ V_B - V_A = \frac{-W_{AB}}{q} = -\frac{1}{q}\int_A^B\overrightarrow F · d\overrightarrow r = -\frac{1}{q}\int_A^B q\overrightarrow E · d\overrightarrow r$$
  >$r_B \rightarrow \infty$

  $$ -V_A = -\int_A^\infty \overrightarrow E · d\overrightarrow r \rightarrow V_A = -\int_\infty^A \overrightarrow E · d\overrightarrow r$$
  - Campo a partir del potencial:
  $$-\overrightarrow E = \nabla V = \frac{\partial U}{\partial x} \hat{i} + \frac{\partial U}{\partial y} \hat{j} + \frac{\partial U}{\partial z} \hat{k}$$

- **Potencia e intensidad de campo**: el sentido del campo es el de los *potenciales decrecientes*: $ \overrightarrow E = -\nabla V$
&nbsp;
######Superposición
$$ \overrightarrow E_T= K\sum_{i=1}^{n} \frac{Q_i}{r_i^2} \overrightarrow e_{r_i}$$   $$V_T= K\sum_{i=1}^{n} \frac{Q_i}{r_i}$$
- Distribuciones contínuas de carga:
&nbsp;
  -  Volumétrica:  $\rho = lim_{\Delta V \to 0} \frac{\Delta Q}{\Delta V} = \frac{dQ}{dV} (\frac{C}{m^3}) \to Q=\int_V \rho dV$
  &nbsp;
  - Superficial:  $\sigma = lim_{\Delta S \to 0} \frac{\Delta Q}{\Delta S} = \frac{dQ}{dS} (\frac{C}{m^2}) \to Q=\int_S \sigma dS$
  &nbsp;
  - Lineal:  $\lambda = lim_{\Delta L \to 0} \frac{\Delta Q}{\Delta L} = \frac{dQ}{dL} (\frac{C}{m}) \to Q=\int_L \lambda dL$
  &nbsp;

- **Teorema de Gauss**: el flujo de un campo eléctrico ($\overrightarrow E$) a través de una superficie cerrada (S) es igual a:
$$\phi = \oint \overrightarrow E · d\overrightarrow S = \frac{\sum Q}{\epsilon_0}$$
``Pasos``
1. Estimamos la dirección y sentido del vector intensidad de campo eléctrico.
2. Elegimos una superficie de integración que pase por el punto donde
queremos calcular el campo y que simplifique $\oint \overrightarrow E· d\overrightarrow S$.
3. Calculamos el flujo según su definición $\phi =\oint \overrightarrow E· d\overrightarrow S$
4. Para la superficie de integración elegida calculamos $\frac{\sum Q}{\epsilon_0}$
5. Calculamos el valor del campo eléctrico aplicando el *Teorema de
Gauss*.
&nbsp;
######Campo eléctrico en la materia
- **Dieléctricos**:
$\overrightarrow E^{tot} = \overrightarrow E^{ext} + \overrightarrow E^{ind}$
$\overrightarrow E^{tot} = \overrightarrow E^{ext} - (1-\frac{\epsilon_0}{\epsilon})\overrightarrow E^{ext} = \frac{\epsilon_0}{\epsilon}\overrightarrow E^{ext}$

- **Conductores**:
`Ejemplo`

Aplicamos Gauss:
1. $\phi = \oint_S \overrightarrow E·d\overrightarrow S = \oint_S EdS = E \oint_SdS = E4\pi r^2$
2. *Teorema de Gauss:* $\phi = \frac{Q}{\epsilon_0}$
3. $E4\pi r^2 = \frac{Q}{\epsilon_0}$
4. $E=\frac{Q}{4 \pi \epsilon_0 r^2} = K\frac{Q}{r^2}$

Usando la relación entre potencial y campo:
5. $V=\int_r^\infty \overrightarrow E · d\overrightarrow r = \int_r^\infty Edr$
6. *Aplicamos el resultado anterior:* $V=\int_r^\infty K\frac{Q}{r^2}dr$
7. $V=K\frac{Q}{r}$

- **Capacidad**: $$C=\frac{Q}{V} (\frac{C}{V} = Faradio)$$
&nbsp;
`Cálculo de la capacidad` $$C=\frac{Q}{V_1 - V_2}$$
&nbsp;
`Cálculo de equivalente en serie`
$$\frac{1}{C} = \sum_{i=1}^{n}\frac{1}{C_n}$$

  `Cálculo del equivalente en paralelo`
$$Q = \sum_{i=1}^{n}Q \to C = \sum_{i=1}^{n}C$$

  `Energía almacenada en un condensador`

$$U=\frac{1}{2}C(V_1-V_2)^2 = \frac{1}{2}\frac{Q^2}{C} = \frac{1}{2}Q(V_1-V_2)$$
&nbsp;
##Campo Magnético
- **Corriente eléctrica**:  conjunto de las cargas que circulan a través de un conductor. La *conducción* es el proceso por el cual la carga se transporta.
- **Intensidad de corriente**: velocidad a la que se transporta la carga
por un punto dado en un sistema conductor. Es la cantidad de carga por
unidad de tiempo que atraviesa un conductor.
$$I=\frac{dQ}{dt} (\frac{Culombio}{segundo} = Amperio)$$
- **Ley de Ohm**:
$$V_1 - V_2 = IR$$
- **Asociación de resistencias**:

![](/assets/Captura_lqmz9lwpk.PNG)![](assets/Fisica-7590d269.PNG)
- **Energía consumida por una corriente U que atraviesa una resistencia**: La caída de potencial ($V_1 − V_2$) entre dos puntos de un hilo conductor es la pérdida de energía potencial de la unidad de carga, cuando pasa de un punto a otro. Si la carga transportada es q, la pérdida de energía potencial U sería:
$$U=I(V_1 - V_2)t = I^2Rt = \frac{V_1-V_2}{R}t$$
- **Potencia consumida por una corriente U que atraviesa una resistencia**: Es la energíaa de la corriente en cada unidad de tiempo.
$$P=I(V_1 -V_2) = I^2R = \frac{(V_1-V_2)}{R} (Watio)$$
&nbsp;
######Fuerza electromotriz
>La fuerza electromotriz (fem) (\epsilon) es toda causa capaz de mantener una diferencia de potencial entre dos puntos de un conductor o de producir una corriente eléctrica que lo atraviese.

&nbsp;

######Inducción magnética
- **Fuerza Lorentz**

Fuerza experimentada por una **carga puntual** en movimiento.
$$\overrightarrow F = q\overrightarrow v \times \overrightarrow B$$

>`Unidades del campo` $\frac{Weber}{m^2}= Tesla$
`Módulo de la fuerza` $qvBsin\varphi $
`Dirección y sentido` Regla de la mano derecha.
&nbsp;

Fuerza experimentada por una **corriente**:
$$\overrightarrow F = I\overrightarrow l \times \overrightarrow B$$

>Si $\overrightarrow l$ cambia, $d\overrightarrow F = Id\overrightarrow l \times \overrightarrow B$
`Módulo de la fuerza` $IlB\varphi $
`Dirección y sentido` Regla de la mano derecha.

- **Campo creado por una carga puntual en movimiento**:
$$ \overrightarrow B = \frac{\mu}{4\pi}\frac{q\overrightarrow v \times \hat e_r}{r^2} = \frac{\mu}{4\pi}\frac{q\overrightarrow v \times \overrightarrow r}{r^3} (T) $$

&nbsp;

- **Cálculo del vector Inducción Magnética Ley de Biot y Savart**:
$$d\overrightarrow B = \frac{\mu_0}{4\pi}\frac{Id\overrightarrow l \times \overline r}{r^3}$$

La inducción magnética ($\overrightarrow B$) producida por un elemento de corriente estacionaria en un punto del espacio, es un vector perpendicular al plano determinado por el elemento de corriente y el punto; de sentido dado por la regla de la mano derecha cuando el pulgar indica el avance de la corriente.

`Pasos`
1. Para calcular el vector Inducción
Magnética total aplicamos el Principio
de superposición.
2. Si tenemos un conductor finito: sumo
a todos los trozos de conductor ⇒ uso $\int$.
3. Aplicable a cargas en movimiento ⇒ sustituyo $I\overrightarrow d l$ por $q\overrightarrow v$.
&nbsp;
- **Ley de Ampere**: forma más facil de calcular el campo.
$$\oint \overrightarrow B · d\overrightarrow S=0$$
>- En el campo magnético las *líneas de campo son cerradas*.
>- No podemos usar Gauss.
>- El campo no es conservativo.
>- No podemos definir potencial.

`Ley de Ampere`

En un campo magnético, la circulación del vector inducción a lo largo de una curva cerrada C es igual a $µ_0$ veces la intensidad de corriente que corta el área de dicha curva.
$$\oint_C \overrightarrow B · d\overrightarrow l = \mu_0 I$$
> - La Ley de Ampère sólo es válida para corrientes estacionarias.
>- Para poner signo a las corrientes usamos la regla de la mano derecha-

`Ejemplos`

>Si $r>a$:
>
>$\oint_C \overrightarrow B·d \overrightarrow l = \oint_C Bdl = B\oint_C dl = B2\pi r$
$\oint_C \overrightarrow B · d\overrightarrow l = \mu_0 I$
$B=\frac{\mu_0 I}{2\pi r}$

>Si $r<a$:
>
>$\oint_C \overrightarrow B·d \overrightarrow l = \oint_C Bdl = B\oint_C dl = B2\pi r$
$\oint_C \overrightarrow B · d\overrightarrow l = \mu_0 I'$ ,siendo $I'=I\frac{\pi r^2}{\pi a^2}$
$B=\frac{\mu_0 I_r}{2\pi a^2}$

- **Fuerzas entre corrientes paralelas**:

![](/assets/Captura2_2uwi2m1il.PNG)![](assets/Fisica-9901eacf.PNG)
$$F=\frac{\mu_0 I I'l}{2\pi a}$$

######Autoinducción
> - Solo hay corriente mientras haya movimiento.
>- Esta se produce por una *fem* que depende de la intensidad y el sentido del campo magnético.
>- Es debida a la variación del flujo magnético.

- **Ley de Lenz**: El flujo producido por la corriente inducida se opone a la variación del flujo inductor.
- **Ley de Faraday**: Siempre que varía el flujo magnético que atraviesa un circuito, se origina en él una corriente inducida. La fuerza electromotriz de inducción que origina dicha corriente inducida es el valor de la velocidad de variación del flujo. La corriente de inducción existe mientras existe variación de flujo.
$$\varepsilon = -\frac{d\phi}{dt}$$

>Razones de cambio del flujo ($\phi = \overrightarrow B · \overrightarrow S$)
>1. Cambie $\overrightarrow B$
>2. Cambie \overrightarrow S$
>3. Cambie el ángulo entre $\overrightarrow B$ y $\overrightarrow S$
- **Autoinducción**: Se produce el fenómeno de autoinducción en un conductor cuando se forman en él corrientes inducidas debidas a las variaciones de flujo en el propio conductor.

`Circuito rígido`

$$\frac{d\phi}{dt} = \frac{d\phi}{dI}\frac{dI}{dt}$$

El coeficiente de autoinducción de un circuito ($L$) es la variación que experimenta el flujo a través del circuito debida a las variaciones de la corriente que lo recorre y depende únicamente de los parámetros geométricos del circuito.
$$L=\frac{d\phi}{dI} (Henrios)$$

>  Un Henrio es la autoinducción de un conductor en el cual la variación de la corriente en un amperio por segundo induce una fem de un voltio.

$$\varepsilon = -L\frac{dI}{dt}$$

- **Asociación de bobinas**:

`Serie` $$L_{eq} = \sum_{i=1}^n L_N$$
`Paralelo` $$\frac 1{L_{eq}} = \sum_{i=1}^n \frac 1{L_N}$$
