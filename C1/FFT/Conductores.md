# Tema 4: Dispositivos Semiconductores

<center> <h3>Diodo</h3> </center>

$$ I_d=I_S (e^{\frac{qVd}{k_B T}}-1)$$

####Característica de transferencia
Es la representación de la salida de un circuito en función de la entrada. (No confundir con función de transferencia).
![](assets/Conductores-e9a2132f.PNG)

<center> <h3>Mosfet</h3> </center>

>**CORTE:** no hay canal.
- $V_G-V_S = V_{GS}<V_T$
&nbsp;

>**LINEAL:** Hay canal en toda la zona entre D y S.
- $V_{GS}>V_T$ y $V_{DS}<(V_{GS-V_T})$
$$I_D=\frac{k}{2}[2(V_{GS}-V_T)V_{DS}-V_{DS} ^2]$$
&nbsp;

>**SATURACIÓN:** Hay canal pero no ocupa toda la zona entre D y S.
- $V_{GS}>V_T$ y $V_{DS}>(V_{GS}-V_T)$
$$I_D=\frac{k}{2}(V_{GS}-V_T)^2$$


<center><img src="assets/Conductores-9ff83f63.PNG"></center>

#Tema 5: Electrónica Digital

<center> <img src="assets/Conductores-a7bf033a.PNG"></center>



<center> <h3>CMOS</h3> </center>

<center> <img src="assets/Conductores-db0c0c5c.PNG"> </center>
<center><img src="assets/Conductores-5335352b.PNG"></center> </center>

<center><img src="assets/Conductores-3b442f6e.PNG"></center> </center>
