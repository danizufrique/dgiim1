# Tema 3: Circuitos en Corriente Alterna
- **Energía transportada**: $U=vit \to Y_{perdidas}=Ri^2t$
&nbsp;
######Números complejos
$j\equiv\sqrt{-1}$

`Forma binomial` $z=a+jb$
`Forma polar` $z = |z| e^{j\theta}$
`Forma trigonométrica` $z=|z|cos\theta + j|z|sin\theta$

######Señal sinusoidal
$v(t)=V_0sin(\omega t+\alpha) = V_0cos(\omega t+\alpha-\frac{\pi}2)$

$v(t)=V_0sin(2\pi ft+\alpha)=V_0cos(2\pi ft+\alpha-\frac{\pi}2)$

$v(t)=V_0sin(\frac{2\pi t}{T} + \alpha -\frac{\pi}2)$
>$\omega $: frecuencia angular.
$f$: frecuencia.
$T$: periodo

`Diferencia de fase entre 2 señales`

- $x_1(t)$ adelantada si $0<\alpha_1 - \alpha_2 < \pi$.
- $x_2(t)$ adelantada si $-\pi < \alpha_1 - \alpha_2 < 0$.

 `Valor eficaz`
 $$r.m.s = \sqrt{\frac{1}{T}\int_0^T v^2(t)dt} = \frac{V_0}{\sqrt{2}} $$
&nbsp;
 `Representación de señal sinusoidal con complejos`

 Sólo nos interesa una parte:
 $$v(t) = V_0(cos(\omega t+\alpha)+j sin(\omega t+\alpha))$$

 `Representación fasorial`

 $v(t)=V_0e^{j(\omega t+\alpha)}$

 $V=V_0e^{j\alpha} \rightarrow v(t) = Ve^{j\omega t}$$

 >El fasor ($V_0e^{j\alpha}$ es un número complejo que representa el módulo $V_0$ y la fase inicial $e^{j\alpha}$ de una señal sinusoidal $v(t)$.

&nbsp;

######Corriente alterna
 Se denomina corriente alterna a la corriente eléctrica en la que la magnitud y dirección varían cíclicamente. La forma de onda de la corriente alterna más comúnmente utilizada es la de una onda senoidal. Sin embargo, en ciertas aplicaciones se utilizan otras formas de onda periódicas, tales como la triangular o la cuadrada.
&nbsp;
######Impedancia
$$v(t) = V_0cos(\omega t+\alpha) \rightarrow v(t)= Real(V_0e^{j(\omega t+ \alpha)}) \rightarrow v(t) = V_0e^{j(\omega t+ \alpha)}$$

==Añadiremos el término $e^{j\omega t}$ al final==

`Generalización de la Ley de Ohm`
$$v(t)=Zi(t) (\Omega)$$

`Impedancia de una Resistencia`
$$v(t)=Ri(t) \rightarrow v(t)=V_0e^{j(\omega t+ \alpha)} \rightarrow V_0e^{j(\omega t+ \alpha)} = Ri(t)$$

$$Z_R=R$$
- $i(t)=\frac{V_0}{R}e^{j(\omega t+ \alpha)} \rightarrow i(t)$ y $ v(t)$ tienen *misma frecuencia angular y distinto módulo*.

`Impedancia de un condensador`  
$$i(t)=C\frac{dv(t)}{dt} \rightarrow v(t)= V_0e^{j(\omega t+ \alpha)} \rightarrow i(t)=C\frac{d(V_0e^{j(\omega t+ \alpha)})}{dt} = Cj\omega V_0e^{\omega t+\alpha} = Cj\omega v(t)$$

$$v(t)=\frac{1}{j\omega C}\rightarrow Z_C = \frac {1}{j\omega C} = \frac{-j}{\omega C} = \frac{1}{\omega C}e^{-j\frac{\pi}{2}}$$

- $i(t)=Cj\omega v(t) = C\omega V_0e^{j(\omega t+ \alpha+\frac{\pi}{2})} \rightarrow i(t)$ y $v(t)$ tienen *misma frecuencia angular pero distinto módulo* ($C\omega V_0$) y *distinta fase* ($\alpha + \frac{\pi}{2}$) .

`Impedancia de una bobina`
$$v(t)=L\frac{di(t)}{dt} \rightarrow v(t) = V_0e^{j(\omega t+ \alpha)} \rightarrow i(t)=\frac{1}{L} \int v(t)dt = \frac{1}{L}\int V_0e^{j(\omega t+\alpha)}dt = \frac {1}{L}\frac{1}{j\omega}V_0e^{j(\omega t+\alpha)} $$
$$v(t)=j\omega L i(t) \rightarrow Z_L=j\omega L = \omega Le^{j\frac{\pi}{2}}$$
- $i(t)=\frac{1}{j\omega L} v(t) = \frac{V_0}{\omega L} e^{j(\omega t + \alpha - \frac{\pi}{2})} \rightarrow i(t)$ y $v(t)$ tienen *misma frecuencia angular* pero *distinto módulo* ($\frac{V_0}{\omega L}$) y *distinta fase* ($\alpha - \frac{\pi}{2}$).

`Asociación`
- Serie: $Z_{equivalente} = \sum_i^n Z_i$
&nbsp;
- Paralelo: $\frac{1}{Z_{equivalente}} = \sum_i^n \frac{1}{Z_i}$

==Mirar ejemplos de transparencias ;)==
&nbsp;
######Potencia
>Suponemos:
$v(t)=Ve^{j(\omega t + \alpha_V)}$
$i(t)=Ie^{j(\omega t + \alpha_I)}$

- No puedo multiplicar los números complejos y quedarme con la parte real.
- Procedimiento adecuado:
$$P(t) = V I cos(\omega t+ \alpha_V ) cos(\omega t+\alpha_I ) = \frac{V I}{2} [cos(2\omega t + \alpha_V + \alpha_I ) + cos(\alpha_V − \alpha_I )]$$
- La potencia varía con el tiempo con una frecuencia doble a la de las señales del circuito.
- La potencia tiene una parte independiente del tiempo que es la que da lugar al valor medio de la potencia distinto de cero.
- La potencia media disipada en una bobina o un condensador es cero.
&nbsp;
######Superposición
El Principio de Superposición es especialmente útil en CA para resolver circuitos en los que hay varias fuentes que operan a distintas frecuencias. En este caso, no podemos emplear ningún otro método de resolución de los aprendidos en CC.

![](/assets/3_1_m9ya7sluf.PNG)![](assets/Fisica_Tema3-c7de0d95.PNG)![](/assets/3_2_0g5iofiyk.PNG)![](assets/Fisica_Tema3-81a8fadc.PNG)![](/assets/3_3.PNG)![](assets/Fisica_Tema3-3856a019.PNG)

La intensidad o diferencia de potencial entre los extremos de cualquier elemento del circuito del Ejemplo 3 se puede calcular como la suma de las intensidades o diferencias de potenciales que se obtienen al resolver los circuitos de los ejemplos *3a* y *3b*.

`Resolución`
- Resuelvo *3a* usando $\omega=10^{6\frac{rad}{s}}$
- Resuelvo *3b* usando $\omega = 2·10^{6\frac{rad}{s}}$
- Sumo las soluciones finales: las que dependen del tiempo y son funciones coseno.
&nbsp;
######Thevenin y Norton (copión!

  [](/assets/3_4.PNG)![](assets/Fisica_Tema3-d3b10b02.PNG)

- La formulación de ambos teoremas es similar a la vista en CC. Ahora hablaremos de *Impedancia Thevenin* y de *Impedancia Norton* en lugar de Resistencias Thevenin y Norton.
- Las impedancias Thevenin y Norton son ahora *números complejos* ⇒ complejidad de determinación experimental.
- Las impedancias Thevenin y Norton son ahora funciones de la frecuencia.
&nbsp;
######Filtros
==Mirar apuntes y comprenderéis por qué no están aquí :cry:==
