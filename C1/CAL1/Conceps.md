## Numerabilidad
**1.42 Proposición.** Sea A un conjunto infinito de números naturales. Entonces existe una única biyección creciente de N sobre A.
**1.44 Proposición.** Un conjunto es numerable si, y sólo si, es finito o es equipotente a N.
**1.45 Proposición.** Un conjunto no vacío A es numerable si, y sólo si, hay una aplicación sobreyectiva de N sobre A.
**1.46 Proposición.** $ \N * \N  $ Y $ \Z * \N $ son equipotentes a $\N$.
**1.47 Proposición.** El conjunto de los números racionales es numerable.

######Demostración

Es consecuencia de las dos proposiciones anteriores y de que la aplicación $ f :
Z × N → Q $ dada por $f(p, q) = p/q$  es sobreyectiva.

Por ser $Q$ numerable infinito sabemos que $Q ∼ N$, es decir, existen biyecciones de $N$ sobre $Q$.
Hemos respondido en parte a nuestra pregunta inicial con un resultado muy sorprendente: ¡hay
tantos números racionales como números naturales! Nos falta todavía dar alguna información del
tamaño de $R$ \ $Q$.

**1.50 Proposición.** $R$ y $R$ \ $Q$ son conjuntos no numerables.

######Demostración.
Evidentemente todo subconjunto de un conjunto numerable también es numerable.

Como acabamos de ver que hay subconjuntos de $R$ que no son numerables (por los intervalos encajados) deducimos que $R$ no es numerable. Puesto que $R = Q$ $∪$ $($$R$ \ $Q)$ y sabemos que $Q$ es numerable y $R$ no lo es, deducimos que $R$ \ $Q$ no es numerable.  El teorema anterior demuestra no solamente que $R$ \ $Q$ no es vacío sino que “hay muchos más números irracionales que racionales” pues mientras que podemos enumerar los racionales no podemos hacer lo mismo con los irracionales ya que no hay biyecciones de $N$ sobre $R$ \ $Q$.

Nótese que ésta es una demostración de existencia de números irracionales bastante sorprendente pues, de una parte, prueba, ¡sin necesidad de dar ningún ejemplo concreto!, que hay números irracionales pero, de hecho, prueba mucho más pues nos dice que el conjunto $R$ \ $Q$ tiene muchos más elementos que $Q$. Deducimos también la siguiente estrategia “para probar que un conjunto $A ⊂ R$ no es vacío es suficiente probar que su complemento $R$ \ $A$ es numerable” (¡con lo cual, de hecho, estamos probando que $A$ es infinito no numerable!).

Esta técnica de demostración de existencia es debida a Cantor, el creador de la teoría de conjuntos. También se debe a Cantor el concepto de “número transfinito” o “cardinal” o “potencia” de un conjunto. Todos los conjuntos equipotentes a N tienen el mismo cardinal que se representa por ℵ~0~ (la letra ℵ es la primera letra del alfabeto hebreo y se lee “aleph”) y todos los conjunto Conjuntos infinitos y conjuntos numerables equipotentes a $R$ tienen el mismo cardinal que se representa por la letra $c$ y se llama “la potencia del continuo”. Aunque claramente representan “números infinitos” creo que aceptarás que ℵ~0~$<c$, desigualdad que no se pretende explicar y que sólo se pone para motivar la curiosidad por estos temas; ¡ya decíamos al principio que unos infinitos son más grandes que otros!

El siguiente resultado nos dice que si hacemos la unión de una “cantidad numerable” de conjuntos numerables obtenemos un conjunto que sigue siendo numerable. El enunciado del teorema precisa estas ideas.

## Sucesiones. Convergencia
**2.2 Definición.** Se dice que una sucesión {$x_n$} converge a un número real $x$ si, dado cualquier número real $ε > 0$, existe un número natural $m_ε$ tal que si $n$ es cualquier número natural mayor o igual que $m_ε$ se cumple que $|x_n− x| < ε$.

$$|xn− x| < ε ⇐⇒ x − ε < xn < x + ε ⇐⇒ xn ∈]x − ε, x + ε[$$

De esto resulta que {$x_n$} converge a $x$ si dado cualquier número $ε > 0$ se verifica que todos los términos de la sucesión a partir de uno en adelante están en el intervalo $]x − ε, x + ε[$. También podemos reformular la definición dada considerando para cada $ε > 0$ el conjunto de números naturales $A_ε = (n∈N : |xn − x| > ε)$. Tenemos entonces que: $lım(x_n) = x ⇐⇒ ∀ε > 0$ el conjunto $A_ε = (n∈N : |xn − x| > ε)$ es finito.

Esta forma de expresar la convergencia puede ser útil para probar que una sucesión dada, {$x_n$} no converge a $x$. Para ello basta encontrar un número $ε > 0$ tal que el conjunto $A_ε$ sea infinito.

La *definición 2.2* es típica del Análisis pues en ella se está definiendo una igualdad, $lım(x_n) = x$ , en términos de desigualdades: $|xn − x| < ε$ siempre que $n > m_ε$.

**2.6 Proposición.** Una sucesión convergente tiene un único límite.
######Demostración.
Sea {$x_n$} $→ x e y 6= x$. Tomemos $ε > 0$ tal que $]x − ε, x + ε[∩]y − ε, y + ε[= Ø$.
Puesto que {$x_n$} $→ x$ el conjunto $A_ε = (n∈N : |x − xn| > ε)$ es finito. Puesto que si $|x − xn| <
ε$ entonces $|y − x_n| > ε$, tenemos que $B_ε = (n∈N : |y − xn| > ε) ⊃ N$ \ $A_ε$ y, por tanto, $B_ε$ es
infinito, lo que prueba que {$x_n$} no converge a $y$.

**2.8 Proposición (Principio de las sucesiones encajadas).** Supongamos que $(Xn), (Yn), (Zn)$ son sucesiones tales que $lım(Xn) = lım(Zn) = α$ y existe un número natural m~0~ tal que para todo $n > m$~0~ se verifica que $Xn \le Yn \le Zn$, entonces la sucesión $(Yn)$ es convergente y $lım(Yn) = α$.


**2.10 Corolario.** Sean {$x_n$} e {$y_n$} sucesiones cuyos términos son iguales a partir de uno en adelante, es decir, hay un número natural $m_0$ tal que para todo $n > m_0$ es $x_n = y_n$. Entonces {$x_n$} converge si, y sólo si, {$y_n$} converge en cuyo caso las dos sucesiones tienen igual límite.

**2.14 Teorema.** Toda sucesión monótona y acotada es convergente. Más concretamente, si una sucesión {$x_n$} es:
i) creciente y mayorada, entonces $lım(x_n) = β$ donde $β = sup(x_n : n ∈ N)$. Además se verifica
que $x_n < β$ para todo $n ∈ N$, o bien que todos los términos a partir de uno en adelante son
iguales a $β$.
ii) decreciente y minorada, entonces $lım(x_n) = α$ donde $α = ınf(x_n : n ∈ N)$. Además se
verifica que $α < x_n$ para todo n ∈ N, o bien que todos los términos a partir de uno en adelante
son iguales a $α$.

######Demostración.
Probaremos *i)* quedando la demostración de *ii)* como ejercicio.

La hipótesis de que {$x_n$} es mayorada garantiza, por el principio del supremo, la existencia del número real
$β = sup(x_n : n ∈ N)$. Dado $ε > 0$, como $β − ε < β$, tiene que haber algún término $x_m$ de la
sucesión tal que $β − ε < x_m$. Puesto que la sucesión es creciente para todo $n > m$ se verificará
que $x_m \le x_n$ y, por tanto $β − ε < x_n$. En consecuencia $β − ε < x_n < β + ε$ para todo $n > m$.
Hemos probado así que $lım(x_n) = β$. Finalmente, si hay algún término igual a $β, x_m0 = β$,
entonces para todo $n > m_0 $tenemos que $β = x_m0 \le x_n \le β$ por lo que $x_n = β$.

## LOG
$  log(x) = lım_{(n→∞)}($n$\sqrt[n]{x} - 1)$

##e
$e = lim(1 + \frac{1}{n})^{n+1}$

## Adherencia

Se dice que un número real $z$ es un valor de adherencia de una sucesión {$x_n$} si hay alguna sucesión parcial de {$x_n$} que converge a $z$.

## Bolzano
**2.43 Teorema (Teorema de Bolzano -Weierstrass).** Toda sucesión acotada de números reales tiene alguna sucesión parcial convergente. Equivalentemente, toda sucesión acotada de números reales tiene al menos un valor de adherencia.
**2.44 Proposición.** Una sucesión acotada no convergente tiene al menos dos valores de adherencia.
**2.45 Corolario.** Una sucesión acotada es convergente si y sólo si tiene un único valor de adherencia.

##Cauchy
**2.46 Definición.** Se dice que una sucesión {$x_n$} satisface la condición de Cauchy, si para cada número positivo, $ε > 0$, existe un número natural $m_ε$, tal que para todos $p, q ∈ N$ con $p > m_ε$ y $q > m_ε$ se verifica que $|xp− xq| < ε.$
**2.47 Teorema** (Teorema de complitud de R). Una sucesión de números reales es convergente si, y sólo si, verifica la condición de Cauchy.

Puesto que, evidentemente, para cada $h∈N$ se tiene que $|xn+h−xn| \le ρn$ para todo $n∈N$, si {$x_n$} satisface la condición de Cauchy entonces para cada h∈N se verifica $lım_{n→∞}(x_n+h− x_n) = 0$.
Es importante observar que una sucesión {$x_n$} puede verificar esta última condición y no ser convergente, es decir, no satisfacer la condición de Cauchy. Un ejemplo de ello lo proporciona la serie armónica, esto es, la sucesión {$H_n$} dada por $H_n = \sum_{k=1}^{n}\frac{1}{k}$. Hemos visto que dicha sucesión no es convergente y, por tanto, no verifica la condición de Cauchy

##Límites superior e inferior
**2.61 Teorema.** Una sucesión acotada es convergente si, y sólo si, su límite superior y su límite inferior son iguales, en cuyo caso ambos coinciden con el límite de la sucesión.
**2.63 Proposición.** Sea {$x_n$} una sucesión cualquiera de números positivos. Se verifica que:
$$\underline{lım}(\frac{x_n +1}{x_n}) \le \underline{lim}(\sqrt[n]{x_n}) \le \overline{lim}(\sqrt[n]{x_n}) \le \overline{lim}(\frac{x_{n+1}}{x_n})$$
