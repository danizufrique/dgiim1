#GUÍA MAESTRA ABSOLUTA EXTREME 4K DE SERIES Y SUCESIONES RELOADED
***Daniel Zufrí Quesada
con la colaboración de Daniel Pérez***

-----

##Sucesiones convergentes
>**Definición 2.2**

Se dice que una sucesión $\{x_n\}$ converge a un número real $x$ si, dado cualquier número real $\varepsilon > 0$, existe un número natural $m_\varepsilon$ tal que si $n$ es cualquier número natural mayor o igual que $m_\varepsilon$ se cumple que $\mid x_n − x\mid < \varepsilon$.

Esto es:

$\lim\{x_n\} = x \leftrightarrow [\forall \varepsilon > 0]$ el conjunto $A_\varepsilon=\{n\in \N:\mid x_n - x\mid \ge \varepsilon\}$ es finito.
&nbsp;
 >**Proposición 2.8: Sucesiones Encajadas**

 Supongamos que $\{x_n\}, \{y_n\}, \{z_n\}$ son sucesiones tales que $\lim \{x_n\} = \lim \{z_n\} = \alpha$ y existe un número natural $m_0$ tal que $\forall n \ge m_0$ se verifica que $x_n \le yn \le zn$, entonces la sucesión $\{y_n\}$ es convergente y $\lim\{y_n\} = \alpha$.
&nbsp;
##Sucesiones monótonas

>**Teorema 2.14**

Toda sucesión monótona y acotada es convergente.
&nbsp;

## Equivalencias asintóticas

>{$X_n$} $\rightarrow 0$

- $\log(1+X_n)\sim X_n$
- $(1+X_n)^{\alpha}-1 \sim \alpha X_n$
- $e^{X_n}\sim X_n +1$

&nbsp;
>{$X_n$} $\rightarrow 1$

- $\log X_n\sim X_n -1$
&nbsp;
- $\{X_n^{Y_n}\}\rightarrow e^L \iff \{Y_n (X_n - 1)\} \rightarrow 1$
&nbsp;
- $\{X_n^{Y_n}\}\rightarrow \infty \iff \{Y_n (X_n - 1)\} \rightarrow \infty$
&nbsp;
- $\{X_n^{Y_n}\}\rightarrow 0 \iff \{Y_n (X_n - 1)\} \rightarrow -\infty$
&nbsp;
- $e^{X_n -1}\sim X_n$
&nbsp;
- $X_n ^\alpha -1 \sim \alpha (X_n-1)$
&nbsp;
- $X_n^{Y_n} = e^{\log(X_n^{Y_n})}$

&nbsp;

>Otras:

- $\{1 + \frac{1}{2} + \dots + \frac{1}{n} \} \sim \{\log n\}$
&nbsp;
- $Y_n \log (X_n) = Y_n (X_n -1)$
&nbsp;
- $\frac{1}{n+1} < \log(1 +\frac 1 n)< \frac 1 n$
&nbsp;
- $\log n! \sim n \log n $
&nbsp;

##Logaritmos y Exponenciales

>**Proposición 2.33**

- $\frac{x}{1+x}\le \log(1+x)\le x $ para todo $x>-1$, y la desigualdad es estricta si $x\ne0$.
&nbsp;
- $\mid \frac{\log(1+x)}{x}-1\mid > \frac{|x|}{1+x}$ para todo $x>1, x\ne0$.
&nbsp;
- $x\le e^x -1 \le xe^x$ para todo número real $x$, y la desigualdad es estricta si $x\ne 0$.
&nbsp;
- $\mid\frac{e^x -1}{x} -1 \mid < \mid e^x-1 \mid$ para todo $x\ne 0$.
&nbsp;

>**Proposición 2.34**
- Una sucesión $\{x_n\} \rightarrow x\in \R \leftrightarrow \{e^{x_n}\} \rightarrow e^x$.
- Una sucesión de números positivos $\{y_n\} \rightarrow r\in \R \leftrightarrow \{e^{x_n}\}\rightarrow e^x$
&nbsp;

>**Teorema 2.35**

- Para toda sucesión $\{x_n\}$ tal que $-1<x_n\ne 0$ para todo $n\in \N$ y $\lim \{x_n\} = 0$, se verifica que $$\lim_{n\rightarrow \infty} \frac{\log(1+x_n)}{x_n} = 1, \lim_{n\rightarrow \infty} (1+x_n)^{\frac{1}{x_n}} = e$$
- Para toda sucesión $\{x_n\}$ tal que $\lim \{x_n\} = 0$ y $X_n \ne 0$ para todo $n\in \N$, se verifica que $$\lim_{n\rightarrow \infty} \frac{e^{x_n}-1}{x_n} = 1$$
&nbsp;
>**Corolario 2.36**
- Para toda sucesión $\{x_n\}$ tal que $0<X_n \ne 1$ para todo $n\in \N$ y $lim \{x_n\} = 1$ se verifica que $$\lim_{n\rightarrow \infty} \frac{log(x_n)}{x_n -1} = 1$$
- Para toda sucesión $\{x_n\}$ tal que $x_n \notin [-1,0]$ para todo $n \in \N$ y $lim \frac{1}{x_n} = 0$, se verifica que $$\lim_{n\rightarrow \infty} (1+\frac{1}{x_n})^{x_n} = e$$


&nbsp;
##Sucesiones parciales y valores de adherencia

 >**Proposición 2.40**

Si $\lim \{x_n\} = x$, toda sucesión parcial de $\{x_n\}$ también converge a $x$. En particular, una sucesión convergente tiene como único valor de adherencia su límite.
Para probar que una sucesión *no es convergente* basta con que tenga una sucesión parcial no convergente o dos sucesiones parciales convergentes a límites distintos.

&nbsp;
<center> <h3>Bolzano-Weiertrass</h3> </center>

>**Lema 2.42**

Toda sucesión de números reales tiene una sucesión parcial monótona.
Toda sucesión parcial de una sucesión acotada es una sucesión acotada.

>**Teorema 2.43 (BOLZANO-WEIERSTRASS)**

Toda sucesión acotada de números reales tiene alguna sucesión parcial convergente. Equivalentemente, toda sucesión acotada de números reales tiene al menos un valor de adherencia.

>**Proposición 2.44**

Una sucesión acotada no convergente tiene al menos dos valores de adherencia.

>**Corolario 2.45**

Una sucesión acotada es convergente si y sólo si tiene un único valor de adherencia.

&nbsp;
<center> <h3>Cauchy</h3> </center>

>**Definición 2.46**

Se dice que una sucesión $\{x_n\}$ satisface la condición de Cauchy, si para cada número positivo, $\varepsilon > 0$, existe un número natural $m_\varepsilon$, tal que para todos $p, q \in \N$ con $p \ge m_\varepsilon$ y $q \ge m_\varepsilon$ se verifica que $\mid x_p− x_q\mid < \varepsilon$.

Una sucesión de números reales es convergente si, y solo si, verificala condición de Cauchy.
&nbsp;

##Sucesiones divergentes

>**Proposición 2.49**

Para una sucesión $\{x_n\}$ de números reales equivalen las siguientes afirmaciones:
- $\{x_n\}$ no tiene ningún valor de adherencia.
- $\{x_n\}$ no tiene ninguna sucesión parcial acotada.
- Para todo número real $K > 0$ existe un número natural $m_K \in \N$, tal que $\forall n\in \N$ con $n \ge m_K$ se verifica que $\mid x_n\mid \ge K$.
&nbsp;
>**Proposición 2.51**

- Supuesto $x_n \ne 0$ para todo $n\in \N$, se verifica que $\{\mid x_n\mid \} \rightarrow +\infty$ si, y sólo si, $\{1/x_n\} \rightarrow 0$.
- $\{x_n\} \rightarrow +\infty$ si, y sólo si, $\{−x_n\} \rightarrow −\infty$.
&nbsp;
>**Proposición 2.52**

- Para toda sucesión $\{x_n\}$ se verifica que:
  - $\{x_n\} \rightarrow +\infty$ si, y sólo si, $\{e^{x_n} \} \rightarrow +\infty$.
  - $\{x_n\} \rightarrow −\infty$ si, y sólo si, $\{e^{x_n} \} \rightarrow 0$.
  &nbsp;

- Para toda sucesión de números positivos $\{x_n\}$ se verifica que:
  - $\{x_n\} \rightarrow +\infty$ si, y sólo si, $\{\log (x_n)\} \rightarrow +\infty$.
  - $\{x_n\} \rightarrow 0$ si, y sólo si, $\{\log (x_n)\} \rightarrow −\infty$.
&nbsp;
>**Nota**
Llas sucesiones del tipo $\{x_n + y_n\}$ donde $\{x_n\} \rightarrow +\infty, \{y_n\} \rightarrow −\infty$, requieren un estudio particular en cada caso. Tales sucesiones suele decirse que son una indeterminación del tipo $“∞−∞”$.
Análogamente, si sabemos que $\{x_n\} \rightarrow 0$ y que $\{y_n\}$ es divergente, ello no proporciona ninguna información sobre el comportamiento de la sucesión $\{x_ny_n\}$; la cual se dice que es “una indeterminación del tipo $“0∞”$. Las indeterminaciones que aparecen al estudiar el cociente de dos sucesiones divergentes o de dos sucesiones que convergen a cero, las llamadas indeterminaciones de los tipos $“∞/∞”$, $“0/0”$, pueden reducirse a una indeterminación del tipo $“0∞”$.

>**Nota**
La convergencia de $\{x_n ^{y_n}\}$ vendrá determinada por la de $\{y_n \log (x_n)\}$

  &nbsp;
>**Teorema 2.56: ==CRITERIO DE EQUIVALENCIA LOGARÍTMICA==**

Sean $\{x_n\}$ una sucesión de números positivos distintos de $1$ que converge a $1$, $\{y_n\}$ una sucesión cualquiera y $L$ un número real. Entonces se tiene que:

- $\lim \{x_n ^{y_n}\} = e^L \leftrightarrow \lim \{y_n(x_n -1)\} = L$
- $\{x_n^{y_n} \} \rightarrow +\infty \leftrightarrow \{y_n(x_n − 1)\} \rightarrow +\infty$.
- $\{x_n^{y_n}\} \rightarrow 0 \leftrightarrow \{y_n(x_n − 1)\} \rightarrow −\infty$.

  &nbsp;
>**Teorema 2.57: ==CRITERIO DE STOLZ==**

Sea $\{y_n\}$ una sucesión positivamente divergente y estrictamente creciente y sea $\{x_n\}$ cualquier sucesión. Supongamos que $$\frac{x_{n+1}-x_n}{y_{n+1}-y_n} \rightarrow L$$ donde $L \in \R$, ó $L = +\infty$, ó $L = −\infty$. Entonces se verifica también que $\frac{x_n}{y_n} \rightarrow L$

>**Nota**
Este criterio da una condición suficiente pero no necesaria de la convergencia o divergencia de $\frac{x_n}{y_n}$.

&nbsp;
>**Proposición 2.58: ==CRITERIO DE LA MEDIA ARITMÉTICA==**

Supongamos que $\{a_n\} \rightarrow L$ donde $L \in \R$ , ó $L = +\infty$, ó $L = −\infty$. Entonces se verifica que $$\{\frac{a_1+a_2+\dots + a_n}{n}\} \rightarrow L$$

&nbsp;
>**Proposición 2.58: ==CRITERIO DE LA MEDIA GEOMÉTRICA==**

Supongamos que $\{a_n\} \rightarrow L$ donde $\{a_n\}$ es una sucesión de números positivos y $L \in \R$ , ó $L = +\infty$. Entonces se verifica que $$\{ \sqrt[n]{a_1 a_2 \dots a_n}\} \rightarrow L$$

&nbsp;
>**Corolario 2.60**

Supongamos que $\frac{x_{n+1}}{x_n} \rightarrow L$ donde $\{x_n\}$ es una sucesión de números positivos y $L \in \R$ o bien $L = +\infty$. Entonces se verifica que $$\{\sqrt[n]x_n\} \rightarrow L$$
&nbsp;
>**Teorema 2.61:**

Una sucesión acotada es convergente si, y sólo si, su límite superior e inferior coinciden, en cuyo caso ambos son el límite de la sucesión.
  &nbsp;
>**Proposición 2.63:**

Sea $\{x_n\}$ una sucesión cualquiera de números positivos. Se verifica que $$\lim\{\frac{x_{n+1}}{x_n}\} \le \lim\{\sqrt[n]x_n\} \le \lim\{\sqrt[n]x_n\} \le \lim\{\frac{x_{n+1}}{x_n}\} $$
&nbsp;
#SERIES

>**Ejemplo 3.2: ==SERIE GEOMÉTRICA==**

$$\sum_{n\ge 0}x^n \Rightarrow \{1+x+x^2+\dots + x^n\}$$

- Converge si, y sólo si, $\mid x \mid < 1$, en cuyo caso se verifica que $\sum_{n\ge 0}x^n = \frac{1}{1-x}$.
- La comvergencia de la serie geométrica equivale a la convergencia de la sucesión $\{x^n\}$.

  &nbsp;
>**Ejemplo 3.2: ==SERIE ARMÓNICA==**

$$H_n=\sum_{k=1}^n \frac 1 k = \sum_{n\ge1} \frac 1 k$$

- Diverge positivamente, ya que $H_n>\log (n+1)$.
- $\{H_n\} \sim \{\log(n)\} $

  &nbsp;

>**Ejemplo 3.2: ==SERIE ARMÓNICA ALTERADA==**

$$\sum_{n\ge 1} \frac{(-1)^{n+1}}{n}$$
- Es convergente y su suma es igual a $log 2$.

>**Nota**
Modificando el orden de los términos en una serie convergente podemos obtener otra serie convergente con distinta suma. *Consultar Ejemplo 3.5 [pg. 77]*.

  &nbsp;

> **==SUMA DE FACTORIALES==**

$$ H_{n!}=\sum_{n\ge1}{\frac{1}{n!}}$$

- La serie converge al número $e$

$$
\sum_{n=1}^{\infin}{\frac{1}{n!}} = e
$$
>**Nota**
Toda sucesión $\{a_n\}$ es la serie definida por la sucesión de sus diferencias.

  &nbsp;
>**Proposición 3.6:**

Sean $\{a_n\}$ y $\{b_n\}$ dos sucesiones y supongamos que hay un número $q \in \N$ tal que para $n > q + 1$ es $a_n = b_n$. Entonces se verifica que las series $\{a_1 + a_2 + \dots + a_n\}$ y $\{b_1 + b_2 + \dots + b_n\}$ o bien convergen ambas o no converge ninguna, y en el primer caso se verifica que:
$$\sum_{n=1}^\infty a_n - \sum_{j=1}^q a_j = \sum_{n=1}^\infty b_n  - \sum_{j=1}^q b_j$$

&nbsp;
>**Proposición 3.7:**

Para que la serie $\sum a_n$ sea convergente es necesario que $\lim \{a_n\} = 0$.

&nbsp;
##Series de términos positivos

>**Proposición 3.9: ==CRITERIO BÁSICO DE CONVERGENCIA==**

Una serie de términos $\sum_{n\ge 1}a_n$ es convergente si, y sólo si, está mayorada, en cuyo caso la suma viene dada por $\sum_{n=1}^\infty a_n = sup\{\sum_{k=1}^n a_k : n\in \N\}$.
&nbsp;
>**Proposición 3.11:**

La serie $\sum_{n\ge 0 }\frac 1 {n!}$ es convergente y su suma es el número $e$.
&nbsp;
>**Proposición 3.9: ==CRITERIO BÁSICO DE COMPARACIÓN==**

Sean $\sum_{n\ge 1}a_n$ y $$\sum_{n\ge 1}a_n$ dos series de términos positivos. Supongamos que hay un número $k \in \N$ tal que $a_n \le b_n$ para todo $n>k$. Entonces se verifica que si la serie $\sum_{n\ge 1} b_n$ es convergente, también $\sum_{n\ge 1} a_n$ es convergente o, equivalentemente, si la serie $\sum_{n\ge 1} a_n$ es divergente también $\sum_{n\ge 1} b_n$ es divergente.
&nbsp;
>**Proposición 3.14: ==CRITERIO LÍMITE DE COMPARACIÓN==**

Sean $\sum_{n\ge1}{a_n}$ y $\sum_{n\ge1}{b_n}$  dos series de términos positivos, y supongamos que:

$$
\left\{\frac{x_n}{y_n}\right\} \rightarrow L \in \R_{0}^{+} \cup\{+\infin\}
$$

* Si $L= +\infin$ y $\sum_{n\ge1}{b_n}$ es divergente, entonces $\sum_{n\ge1}{a_n}$ es divergente.
* Si $L= 0$ y $\sum_{n\ge1}{b_n}$ es convergente, entonces $\sum_{n\ge1}{a_n}$ es convergente.
* Si $L\in\R^+$, ambas series $\sum_{n\ge1}{b_n}$ y $\sum_{n\ge1}{a_n}$ son convergentes o divergentes.

En particular, si dos sucesiones $\{a_n\}$ y $\{b_n\}$ son asintóticamente equivalentes, las series $\sum_{n\ge1}{a_n}$ y $\sum_{n\ge1}{b_n}​$ convergen o divergen.
&nbsp;
>**Proposición 3.16: ==CRITERIO DE CONDENSACIÓN DE CAUCHY==**

Sea $\{a_n\}$ una sucesión decreciente de números positivos. Se verifica que las series $\{A_n\}$ y $\{B_n\}_{n\in\N_0}$donde:

$$
A_n = a_{1}+a_2+a_3+\ldots+a_n , Bn = a_1+2a_2+4a_3+\ldots+2^na_{2^n}
$$

* Ambas convergen o ambas divergen.

>**Nota**
Para usar los criterios de comparación necesitamos conocer ejemplos de series convergentes con las que comparar la serie dada, como las que se explican a continuación.

&nbsp;
>**Proposición 3.17: ==SERIES DE RIEMANN==**


$$
\sum_{n\ge1}{\frac{1}{n^\alpha}} : \alpha \in \R
$$

  * La serie solamente converge si $\alpha > 1$.
&nbsp;

>**Proposición 3.17: ==SERIES DE BERTRAND==**

$$
\sum_{n\ge2}{\frac{1}{n^\alpha·log(n)^\beta}}
$$

- Converge si:
  * $\alpha > 1$ cualquiera sea $\beta$.
  * $\alpha = 1$ y $\beta > 1$
  * En cualquier otro caso, la serie diverge
&nbsp;
>**Proposición 3.21: ==CRITERIO DEL COCIENTE O DE D'ALEMBERT==**

  Si se cumple que:

$$
\lim\left\{\frac{a_{n+1}}{a_n}\right\} \rightarrow L \in \R_{0}^{+} \cup\{+\infin\}
$$

* Si $L < 1 $ entonces la serie $\sum_{n\ge1}{a_n}$ es convergente.
* Si $L > 1$ o si $L = +\infin$ , la sucesión $\{a_n\}$ no converge a 0 y por tanto, la serie $\sum_{n\ge1}{a_n}$ no es convergente.

* Cuando $L = 1$, la serie puede ser convergente o divergente.
&nbsp;
>**Proposición 3.21: ==CRITERIO DE LA RAÍZ O DE CAUCHY==**

* Sea $\sum_{n\ge1}{a_n}$  una serie de términos positivos. Si se verifica que:

$$
\lim\left\{\sqrt[n]{a_n}\right\} \rightarrow L \in \R_{0}^{+} \cup\{+\infin\}
$$

* Si $L < 1 $ entonces la serie $\sum_{n\ge1}{a_n}$ es convergente.
* Si $L > 1$ o si $L = +\infin$ , la sucesión $\{a_n\}$ no converge a 0 y por tanto, la serie $\sum_{n\ge1}{a_n}$ no es convergente.

- Cuando $L = 1$, la serie puede ser convergente o divergente.
&nbsp;
>**COMPARACIÓN CRITERIOS COCIENTE Y RAÍZ**

$$\underline{\lim}{\left\{\frac{a_{n+1}}{a_n}\right\}}\le \underline{\lim}{\left\{\sqrt[n]{a_n}\right\}}\le \overline{\lim}{\left\{\sqrt[n]{a_n}\right\}} \le \overline{\lim}{\left\{\frac{a_{n+1}}{a_n}\right\}}$$

* Siempre que el criterio del cociente de información, el de la raíz también lo proporciona.
* Si el criterio del cociente no da información, es posible que el de la raíz sí lo haga.
&nbsp;
>**Proposición 3.25: ==CRITERIO DE RAABE==**


Sea $a_n > 0$ para todo $n \in \N$, y pongamos:

$$
R_n=n\left( 1-\frac{a_{n+1}}{a_n}\right)
$$

Supongamos que $\lim\{R_n\} = L \in \R \cup\{\pm\infin\}$:
  * Si $L > 1$ ó $L = +\infin$ , la serie $\sum_{n\ge1}{a_n}$ es convergente.
  * Si $L < 1$ ó $L = -\infin$, o si existe algún $k\in\N$ tal que $k\le R_n$  tal que $R_n \le 1$ para todo $n \ge k$ , entonces la serie $\sum_{n\ge1}{a_n}$ es divergente.
&nbsp;
  >**Proposición 3.25: ==CRITERIO DE RAABE ALTERNATIVO==**

Sea ${a_n} > 0$  $\forall n \in \N$  una serie de términos positivos tal que $\lim \frac{a_{n+1}}{a_n} = 1$, y pongamos $S_n = {\left(\frac{a_{n+1}}{a_n}\right)}^n$:
  * Si $S_n\rightarrow e^L$ con $L > 1$ o si  $S_n\rightarrow +\infin$, la serie $\sum_{n\ge1}{a_n}$ es convergente.
  * Si $S_n\rightarrow e^L$ con $L < 1$ o si  $S_n\rightarrow 0$ la serie $\sum_{n\ge1}{a_n}$ es divergente.

&nbsp;
##SERIES CONMUTATIVAMENTE CONVERGENTES

- Se dice que una serie $\sum_{n\ge 1} a_n$ es **conmutativamente convergente** si para toda biyección $\pi : \N \rightarrow \N$ se verifica que la serie definida por la sucesión $\{a_{\pi (n)}\}$, es decir la serie $\sum_{n \ge 1} a_{\pi (n)} = \{a_{\pi (1)} + a_{\pi (2)} + \dots + a_{\pi (n)}\}$ es convergente.
- Si la serie $\sum a_n$ es conmutativamente convergente entonces ella misma es convergente. En otras palabras, una serie es conmutativamente convergente, cuando es convergente y también son convergentes todas las series que se obtienen de ella por reordenación de sus términos.
- La suma de una serie de términos positivos convergente es igual al extremo superior del conjunto de todas las sumas parciales


- Se dice que una serie $\sum_{n\ge 1} a_n$ es **absolutamente convergente** si la serie $\sum_{n \ge 1} \mid a_n \mid$ es convergente.
- $\{x_n\} \rightarrow x \iff \{\mid x_n\mid\} \rightarrow \mid x\mid$.
- $\sum_{n \ge 1} \mid a_n \mid = \{\mid a_1 \mid + \mid a_2 \mid + \dots + \mid a_n \mid\}$

&nbsp;
>**Teorema 3.27**

- Toda serie absolutamente convergente es conmutativamente convergente. Además, si la serie $\sum_{n\ge 1} a_n$  es absolutamente convergente, entonces para toda biyección $\pi : \N \rightarrow \N$ se verifica que:
$$\sum_{n=1}^\infty a_n = \sum_{n=1}^\infty a_{\pi(n)}$$

- Toda serie conmutativamente convergente es absolutamente convergente.

&nbsp;
>**Teorema 3.28: ==TEOREMA DE RIEMANN==**

Sea $\sum_{n\ge 1} a_n$ una serie convergente pero no absolutamente convergente y sea $\alpha \in \R$. Entonces existe una biyección $\pi : \N \rightarrow \N$ tal que la serie $\sum_{n\ge 1} a_{\pi(n)}$ es convergente y $\sum_{n\ge 1}^\infty a_{\pi(n)} = \alpha$.

&nbsp;
>**Proposición 3.29: ==CRITERIO DE LEIBNIZ PARA SERIES ALTERNADAS==**

Supongamos que la sucesión $\{a_n\}​$ es decreciente y convergente a cero. Entonces la serie alternada $\sum_{n\ge1}{(-1)^{n+1}a_n}​$ es convergente. Además, si:

  *  $S_n =\sum_{k\ge1}{(-1)^{k+1}a_k}$
  * $S =\sum_{n\ge1}^{\infin}{(-1)^{n+1}a_n}​$

Entonces, $\forall n \in \N$, se verifica que $|S - S_n| \le a_{n+1}$


$$\left( \frac 1 n} \right) $$

$$\frac{\tg({\frac 1 n})}{\frac 1 n} \rightarrow 1$$

$$M(I*, B_u* \leftarrow \overline B)^t = M(I, B \leftarrow B_u) \rightarrow  M(I, B \leftarrow B_u)^{-1} =  M(I, B_u \leftarrow  B)$$
