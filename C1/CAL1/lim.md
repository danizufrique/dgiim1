$$\underline{\lim}\{x_n\} \overline{\lim}\{y_n\} \le \overline{\lim}\{x_ny_n\}\le \overline{\lim}\{x_n\}\overline{\lim}\{y_n\}$$

**a)**

Sean:
$A_n = \{x_k : k \ge n\} \rightarrow \alpha = inf(A_n), \rho = sup(A_n) $
$B_n = \{y_k : k \ge n\} \rightarrow \beta = sup(B_n)$
$C_n = \{x_ky_k : k \ge n\} \rightarrow \gamma = sup(C_n)$


$\underline{\lim}\{x_n\}  = \lim\{\alpha\}$

$\overline{\lim}\{x_n\} = \lim\{\rho\}$

$\overline{\lim}\{y_n\} = \lim\{\beta\}$

$\overline{\lim}\{x_n y_n\} = \lim\{\gamma\}$

&nbsp;
> $\overline{\lim}\{x_ny_n\}\le \overline{\lim}\{x_n\}\overline{\lim}\{y_n\}$

$\forall k>n:$
- $\rho \ge x_k$
- $\beta \ge y_k$

$\rho \beta \ge x_k y_k \rightarrow \rho \beta \ge \gamma $

&nbsp;
> $\underline{\lim}\{x_n\} \overline{\lim}\{y_n\} \le \overline{\lim}\{x_ny_n\}$

$\forall k>n:$

- $\gamma \ge x_k y_k
 \ge \alpha  y_k \rightarrow \gamma / \alpha \ge y_k \rightarrow \gamma \ge \alpha \beta$

&nbsp;
 **b)** Deduce  que si el $\lim\{x_n\} = x>0$, entonces $\overline{\lim}\{x_n y_n\} = x·\overline{\lim\{y_n\}}$

 Como $\lim\{x_n\}=x, \underline{\lim}\{x_n\} = \overline{\lim}\{x_n\} = {\lim}\{x_n\} = x$.

 Sustituyendo en la expresión del enunciado obtenemos:
 $$\underline{\lim}\{x_n\} \overline{\lim}\{y_n\} \le \overline{\lim}\{x_ny_n\}\le \overline{\lim}\{x_n\}\overline{\lim}\{y_n\}$$ $$x \overline{\lim}\{y_n\} \le \overline{\lim}\{x_ny_n\}\le x\overline{\lim}\{y_n\}$$

 Como las expresiones de los extremos de la desigualdad son iguales, concluimos que ${\lim}\{x_n\} = x$.

&nbsp;
**c)**

$$\{X_n\} = \{1/2, 1, 2, 1/2, 1 , 2\dots\}$$

$$\{Y_n\} = \{0,2,1,0,2,1, \dots\}$$

$$\{X_nY_n\} = \{0,2,2,0,2,2, \dots\}$$
