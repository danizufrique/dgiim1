\part{Tema 5. Lógica de Primer Orden}

\section{Lenguajes de Primer Orden.}

\begin{ndef}[Lenguaje de Primer Orden]
Un lenguaje de primer orden, \textbf{L}, consta de los siguientes símbolos:

\begin{center}
\begin{tabular}{ |c|c|c|c|  }
\hline
\hline
\textbf{Tipo de símbolo} & \textbf{Elementos} & \textbf{Conjunto} & \textbf{Otras representaciones} \\
\hline
Variable & $x_1, \cdots , x_n$ & $V$ infinito numerable & $y,u,v,w$ \\
Constante & $a_1, \cdots , a_n$ & $C$, pudiendo ser vacío & $b,c$ \\
Función & $f_k^n$ & $F; \quad n,k \in \mathbb{N}, n \neq 0$ & $g,h$ \\
Relación & $r_k^n$ & $R; \quad n,k \neq 0 \in \mathbb{N}$ & $s,p,q$ \\
Lógico & $\rightarrow, \neg, \vee, \land, \leftrightarrow, \forall, \exists$ & - & - \\
Auxiliar & $),(,$ y $,$ & - & - \\
\hline
\end{tabular}
\end{center}
Llamaremos \textbf{expresión de L} a cada sucesión finita de sus símbolos (incluimos la \textit{expresión vacía}.
\end{ndef}

\begin{nnote}
Supondremos siempre que estamos en un \textit{lenguaje numerable}, esto es, $C,F,R$ son numerables.
\end{nnote}

\subsection{Géneros.}
Distinguimos 3 géneros entre las expresiones de \textbf{L}:

\begin{ndef}[Términos]
Dados $f_k^n$ símbolo de función y $t_1, \cdots , t_n$ términos, son términos:
\begin{enumerate}
    \item Los símbolos de constante y variable.
    \item $f_k^n(t_1, \cdots , t_n)$
    \item No hay otros términos además de los ya descritos.
\end{enumerate}
El conjunto de términos de \textbf{L} es representado por $Term(\textbf{L})$.
\end{ndef}

\begin{ndef}[Fórmulas atómicas]
Las fórmulas atómicas de L son las expresiones de la forma $r_n^k(t_1,\cdots, tn)$, donde $r_n^k$ es un \textit{símbolo de predicado} y $t_i$ son \textit{términos}. El conjunto de las fórmulas atómicas de \textbf{L} es representado por $Atom(\textbf{L})$.
\end{ndef}

\begin{ndef}[Fórmulas]
Dadas $\alpha$ y $\beta$ fórmulas y $x$ variable, las fórmulas de \textbf{L} son las expresiones defininidas por las siguientes condiciones:
\begin{enumerate}
    \item Las fórmulas atómicas son fórmulas.
    \item $\neg \alpha$ es una fórmula.
    \item $(\alpha \rightarrow \beta), (\alpha \vee \beta), (\alpha \land \beta), (\alpha \leftrightarrow \beta)$ son fórmulas.
    \item $((\forall x)\alpha), ((\exists x)\alpha)$ son fórmulas.
    \item No hay otras formulas además de las ya descritas.
\end{enumerate}
El conjunto de las fórmulas de \textbf{L} es representado por $Form(\textbf{L})$
\end{ndef}

\begin{nnote}
Cualquier fórmula (resp. fórmula atómica, término) no puede ser escrita más que en una forma. Ello nos permitirá realizar razonamientos por inducción sobre fórmulas.
\end{nnote}

\subsection{Ocurrencias y sentencias.}
\begin{ndef}[Ocurrencias]
Una ocurrencia de un símbolo de variable $x$ en la fórmula $\alpha$ es:
\begin{itemize}
    \item \textbf{Ligada} si en la escritura de $\alpha$ dicha ocurrencia está inmediatamente precedida de un cuantificador $\forall$ o de un cuantificador $\exists$, o bien la ocurrencia tiene lugar en el radio de acción de un cuantificador $\forall x$ o $\exists x$.
    \item \textbf{Libre} cuando no es ligada.
\end{itemize}
Un \textbf{símbolo de variable} en una fórmula $\alpha$ será \textbf{libre} (resp. \textbf{ligado} si tiene una ocurrencia libre (resp \textbf{ligada}). Observar que un símbolo de variable puede ser simultáneamente libre y ligado en una fórmula
\end{ndef}

\begin{nnote}
Dada $\varphi$ fórmula, $v_{i1}, \cdots, v_{ik} \in V:$ \newline
$\varphi (v_{i1}, \cdots, v_{ik})$ indicará que \textit{algunas} de las variables $v_{ik}$ son libres en $\varphi$, pudiendo esta contener otras variables libres.
\end{nnote}

\begin{ndef}[Sentencia]
Llamamos sentencia a toda aquella fórmula en la que cada ocurrencia de cada una de sus variables es \textit{ligada}. El conjunto de las sentencias del lenguaje será representado por $Sent(\textbf{L})$.
\end{ndef}

\begin{nexa}
Sea $\varphi = \forall x(r(g(x,a),y))\rightarrow r(g(x,f(y)),f(g(x,y))))$: 
\begin{itemize}
    \item El radio de acción del único $\forall x$ que aparece es $r(g(x,a),y)$ que llamaremos $\alpha$
    \item Es ligada la única ocurrencia de $x$ que hay en  por estar en el radio de acción de un cuantificador $\forall x$.
    \item La ocurrencia de $y$ en $\alpha$ no es ligada en $\varphi$, ya que no está en el radio de acción de un cuantificador $\forall y$.
    \item Ninguna ocurrencia de las variables en $\alpha$ son ligadas en $\alpha$, ya que en $\alpha$ no ocurre cuantificador alguno.
\end{itemize}
\end{nexa}

\begin{nexa}
Sea $\phi = \forall y(\forall x(r(g(x,a),y))\rightarrow r(g(x,f(y)),f(g(x,y))))$ 
\begin{itemize}
    \item En $\phi$ no ninguna ocurrencia de $y$ que sea libre.
    \item $\phi$ no es sentencia, aunque sí lo es por ejemplo $\forall x \forall y(\forall x(r(g(x,a),y))\rightarrow r(g(x,f(y)),f(g(x,y))))$
\end{itemize}
\end{nexa}

\begin{nnote}
Si en una fórmula no hay ocurrencia de variables se trata de una sentencia.
\end{nnote}

\begin{nnote}
Por convenio, si una ocurrencia de un símbolo de variable está en el radio de acción de varios cuantificadores que pueden ligarla, es el más interno el que la liga, mientras que el resto no tienen sobre ella el más mínimo efecto.
\end{nnote}

\section{Interpretaciones, satisfacibilidad y verdad.}