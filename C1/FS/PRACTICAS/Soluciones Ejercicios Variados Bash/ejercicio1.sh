#!/bin/bash
# Título:		ejercicio1.sh
# Fecha:		15/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Realiza una copia de sí mismo en un archivo llamado backup.sh
# Opciones: 		Ninguna
# Uso:			./ejercicio1.sh

cat $0 > backup.sh
