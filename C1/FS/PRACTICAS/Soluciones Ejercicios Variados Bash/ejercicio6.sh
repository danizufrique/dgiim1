#!/bin/bash
# Título:		ejercicio6.sh
# Fecha:		15/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Devuelve los usuarios del sistema y su shell predeterminado.
# Opciones: 		Ninguna
# Uso:			./ejercicio6.sh

ls ~/..
for usuario in $(ls ~/..)
do
	grep -o /$usuario.* /etc/passwd
done
