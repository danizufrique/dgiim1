# Título:		ejercicio11.sh
# Fecha:		16/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Junta varias imágenes en una sola
# Opciones: 		Ninguna
# Uso:			./ejercicio11.sh nombre_final imagen1 imagen2 ...

primera=$1
if [ -f $1 ]
then printf "El nombre de archivo destino ya existe."
else
shift 1
cat $* > $primera
fi
