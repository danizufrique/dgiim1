#!/bin/bash
# Título:		ejercicio4.sh
# Fecha:		15/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Elimina todos los comentarios de otro script (o archivo)
# Opciones: 		Ninguna
# Uso:			./ejercicio4.sh file

cut -d'#' -f1 $1 > temp
mv temp $1
