#!/bin/bash
# Título:		ejercicio10.sh
# Fecha:		16/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Instala el ejercicio9.sh dentro de un directorio dado como argumento
# Opciones: 		Ninguna
# Uso:			./ejercicio10.sh directorio

tail -n +17 $0 > $1/temp
head -16 $1/temp > $1/ejercicio9.sh
head -33 $1/temp | tail -16 > $1/ejercicio11.sh
tail -11 $1/temp > $1/ejercicio3.sh
chmod u+x $1/*
rm $1/temp
exit
#!/bin/bash
# Título:		ejercicio9.sh
# Fecha:		16/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Compacta varios archivos en uno solo
# Opciones: 		Ninguna
# Uso:			./ejercicio9.sh compactado archivo1 archivo2 ...
if [ -f $1 ]
then
printf "El nombre para el arcivo $1 ya pertenece a otro, por favor introduzca uno válido.\n"
else
compactado=$1
shift 1
cat $* > $compactado
fi

#!/bin/bash
# Título:		ejercicio11.sh
# Fecha:		16/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Junta varias imágenes en una sola
# Opciones: 		Ninguna
# Uso:			./ejercicio11.sh nombre_final imagen1 imagen2 ...

primera=$1
if [ -f $1 ]
then printf "El nombre de archivo destino ya existe."
else
shift 1
cat $* > $primera
fi

#!/bin/bash
# Título:		ejercicio3.sh
# Fecha:		15/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Elimina todas las líneas en blanco de otro script (o archivo)
# Opciones: 		Ninguna
# Uso:			./ejercicio3.sh file

grep . $1 > temp
mv temp $1
