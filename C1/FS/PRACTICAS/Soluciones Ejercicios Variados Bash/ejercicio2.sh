#!/bin/bash
# Título:		ejercicio2.sh
# Fecha:		15/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Comprueba que hay pendrives conectados al sistema, y en casi de que los haya los lista.
# Opciones: 		Ninguna
# Uso:			./ejercicio2.sh

valor=$( ls /media/$USER | wc -l )
if [ $valor != 0 ]; then ls /media/$USER; else echo "No hay ningún pendrive conectado al sistema"; fi
