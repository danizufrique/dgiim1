#!/bin/bash
# Título:		ejercicio14.sh
# Fecha:		16/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Realiza el determinante de una matriz
# Opciones: 		Ninguna
# Uso:			./ejercicio14.sh Matriz.txt

for i in {1..3}
do
for n in {1..3}
do
let fila=$i%3
let param=($n-1)*3+$i
if [ $fila -eq 0 ]
then fila=3
fi
valor[$param]=`cat $1 | cut -d';' -f$fila | head -$n | tail -1`
done
done
positivo=`echo "${valor[1]}*${valor[5]}*${valor[9]}+${valor[2]}*${valor[6]}*${valor[7]}+${valor[3]}*${valor[4]}*${valor[8]}" | bc`
negativo=`echo "${valor[3]}*${valor[5]}*${valor[7]}+${valor[2]}*${valor[4]}*${valor[9]}+${valor[1]}*${valor[6]}*${valor[8]}" | bc`
det=`echo "$positivo-$negativo" | bc`
echo $det
