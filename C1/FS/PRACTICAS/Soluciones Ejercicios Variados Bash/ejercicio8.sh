#!/bin/bash
# Título:		ejercicio8.sh
# Fecha:		15/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Dados los coeficientes A, B y C de una ecuación Ax^2 + Bx + C = 0 devuelve los valores de x para los que es cierta.
# Opciones: 		Ninguna
# Uso:			./ejercicio8.sh
if [ $# == 3 ];
then
	let valor=-$3
	let valorB=-$2
	if [ $1 == 0 ];
		then if [ $2 == 0 ];
			then if [ $3 == 0 ];
				then printf "Ha introducido todo 0 y esa ecuación es cierta siempre.";
				else printf "La ecuación $3 = 0 no tiene soluciones.";
				fi
			else let respuesta=$valor/$2
				printf "La única solución es $respuesta .";
			fi
		elif [ $2 == 0 ];
			then if [ $3 == 0 ];
				then respuesta=0
					printf "La única solución es $respuesta .";
				else let division=$valor/$1
					if (( $division < 0 ));
						then printf "La ecuación no tiene soluciones reales.";
						elif [ $division == 0 ];
							then respuesta=0
							printf "La ecuación solo tiene la solución $respuesta .";
						else respuesta1=$( echo "scale=2;sqrt($division)" | bc )
							let respuesta2=-$respuesta1
							printf "Las dos soluciones son x= $respuesta1 y x= $respuesta2 .";
					fi
				fi
		elif [ $3 == 0 ];
			then let respuesta1=0
				let respuesta2=$valorB/$1
				printf "La ecuación tiene dos soluciones: x= $respuesta1 y x= $respuesta2 .";
			else let cuadradoB=$2*$2
				let resta=-4*$1*$3
				let radicando=$cuadradoB+$resta
				if (( $radicando < 0 ));
					then printf "La ecuación no tiene soluciones reales.";
					else
						let raiz=$( echo "scale=2;sqrt($radicando)" | bc )
						let numerador1=$valorB+$raiz
						let numerador2=$valorB-$raiz
						let denominador=2*$1
						let respuesta1=$numerador1/$denominador
						let respuesta2=$numerador2/$denominador
						if [ $radicando == 0 ];
							then printf "La ecuación tiene una única solución: x= $respuesta1 .";
							else printf "La ecuación tiene dos soluciones: x= $respuesta1 y x= $respuesta2 .";
						fi
				fi;fi
else printf "Número de argumentos erróneo."
fi
printf "\n"
