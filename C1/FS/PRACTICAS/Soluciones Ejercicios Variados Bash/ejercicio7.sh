#!/bin/bash
# Título:		ejercicio7.sh
# Fecha:		15/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Devuelve la fecha y hora a la que se hizo un último log.
# Opciones: 		Ninguna
# Uso:			./ejercicio7.sh

lastlog -u $USER
