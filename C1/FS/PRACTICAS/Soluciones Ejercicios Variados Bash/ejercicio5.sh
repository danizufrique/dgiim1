#!/bin/bash
# Título:		ejercicio5.sh
# Fecha:		15/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Elimina todos los comentarios de otro script (o archivo) y sus líneas en blanco
# Opciones: 		Ninguna
# Uso:			./ejercicio5.sh file

~/FS/Ejercicios/ejercicio4.sh $1
~/FS/Ejercicios/ejercicio3.sh $1

