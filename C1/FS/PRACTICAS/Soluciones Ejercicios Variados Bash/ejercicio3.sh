#!/bin/bash
# Título:		ejercicio3.sh
# Fecha:		15/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Elimina todas las líneas en blanco de otro script (o archivo)
# Opciones: 		Ninguna
# Uso:			./ejercicio3.sh file

grep . $1 > temp
mv temp $1
