#!/bin/bash
# Título:		ejercicio9.sh
# Fecha:		16/11/2014
# Autor:		Antonio Checa Molina
# Version:		1.0
# Descripcion: 		Compacta varios archivos en uno solo
# Opciones: 		Ninguna
# Uso:			./ejercicio9.sh compactado archivo1 archivo2 ...

if [ -f $1 ]
then
printf "El nombre para el arcivo $1 ya pertenece a otro, por favor introduzca uno válido.\n"
else
compactado=$1
shift 1
cat $* > $compactado
fi
