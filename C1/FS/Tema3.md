#Tema 3. Compilación y enlazado de programas.
##1 Lenguajes de programación
###1.1 Concepto de lenguaje de programacion
Un lenguaje de programación es un conjunto de símbolos y de reglas para combinarlos que se usan para expresar algoritmos. Los lenguajes de programación poseen un léxico (vocabulario o conjunto de símbolos permitidos), una sintaxis que nos indica como realizar construcciones del lenguaje y una semántica que determina el significado de cada construcción correcta. Son independientes de la arquitectura física de ordenador, luego no obligan al programador a conocer los detalles del computador.

- **Lenguajes de primera generación:**
  -  *Lenguaje máquina:* Esta escrito en binario, es lo que entiende el ordenador de forma directa.
  - *Ensamblador:* Símbolos que son traducidos al binario.

- **Lenguajes de segunda generación:**
  - *Macro ensamblador:* Son secuencias de instrucciones que se empaquetaban todas juntas formándose una macro. Consta del repertorio y de las distintas macros. Se traduce de igual manera que el ensamblador. Tiene una serie de instrucciones y cierta estructura.

- **Lenguajes de tercera generación:** Los primeros lenguajes que intentaban alejarse del ensamblador eran extremadamente ineficientes en el proceso de traducción, se generaba mucho código basura que hacía que fuesen un desastre. Este paso al lenguaje de tercera generación es un desarroll![1](/assets/1.PNG)o un poco mayor del macro ensamblador.

Los *lenguajes de alto nivel* son lenguajes que combinan símbolos y estructura. La estructura a su vez está compuesta por datos e instrucciones. Aparecen los tipos de datos, formalizados matemáticamente.

Un tipo de dato está compuesto por una terna (G, O,P):
- **G** representa del género del tipo conjunto de valores que puede tomar dentro de ese tipo (es la representación).
- **O** que indica las operaciones que soporta el tipo.
- **P** representa las propiedades de esas operaciones.

Los lenguajes de programación, o lenguajes de alto nivel, están específicamente diseñados para programar computadores. Sus características son:
- Son independientes de la arquitectura física del computador. Por tanto, no obligan al programador a conocer los detalles del computador que utiliza, y permiten utilizar los mismos programas en computadores diferentes, con distinto lenguaje de máquina (portabilidad).
- Una sentencia en un lenguaje de alto nivel da lugar, tras el proceso de traducción, a varias instrucciones en lenguaje máquina.
- Algo expresado en un lenguaje de alto nivel utiliza notaciones más cercanas a las habituales en el ámbito en el que se usan.

![](/assets/1_mygvaaoj5.PNG )![](assets/Tema3-783b3c39.PNG)
