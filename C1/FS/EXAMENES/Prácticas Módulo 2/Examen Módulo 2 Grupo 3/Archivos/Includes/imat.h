// -------------------------
// ignore estas dos líneas:
#ifndef IMAT
#define IMAT
// ------------------------

#include <iostream>


using namespace std;

int isum(int, int );

int ires(int, int );

int imul(int, int );

int idiv(int, int );

int imod(int, int );

int icuad(int);


// -------------------------
// ignore esta línea:
#endif
// ------------------------
