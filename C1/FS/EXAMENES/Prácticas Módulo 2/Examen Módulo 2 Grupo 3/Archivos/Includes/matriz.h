// -------------------------
// ignore estas dos líneas:
#ifndef MATRIZ
#define MATRIZ
// ------------------------

#include <iostream>

using namespace std;

class matriz {
  public:
   double m[9];
};


matriz mcero(void);

matriz mident(void);

matriz msum(matriz, matriz);

matriz mres(matriz, matriz);

void mprint(matriz);


// -------------------------
// ignore esta línea:
#endif
// ------------------------
