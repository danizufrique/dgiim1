#include "imat.h"

// -----------------
int isum(int a, int b)
{
   int c = a + b;
   return c;
}


// -----------------
int ires(int a, int b)
{
   int c = a - b;
   return c;
}


// -----------------
int imul(int a, int b)
{
   int c = a * b;
   return c;
}


// -----------------
int idiv(int a, int b)
{
   if (b == 0)
   {
      cout << "Error: División por 0." << endl;
      return 0;
   }
   int c = a / b;

   return c;
}


// -----------------
int imod(int a, int b)
{
   int c = a - b;
   return c;
}


// -----------------
int icuad(int a)
{
   int c = a * a;
   return c;
}
