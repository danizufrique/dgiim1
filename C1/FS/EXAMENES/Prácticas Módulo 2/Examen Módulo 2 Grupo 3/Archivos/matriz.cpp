#include "matriz.h"


// -----------------
matriz mzero(void)
{
   matriz m;
   
   for (int c = 0; c < 9; c++)
      m.m[c] = 0;

   return m;
}


// -----------------
matriz mident(void)
{
   matriz m;
   
   for (int c = 0; c < 9; c++)
      if ( (c == 0) || (c == 4) || (c == 8) )
	 m.m[c] = 1;
      else
	 m.m[c] = 0;

   return m;
}


// -----------------
matriz msum(matriz a, matriz b)
{
   matriz m;

   for (int c = 0; c < 9; c++)
      m.m[c] = a.m[c] + b.m[c];

   return m;
}


// -----------------
matriz mres(matriz a, matriz b)
{
   matriz m;

   for (int c = 0; c < 9; c++)
      m.m[c] = a.m[c] - b.m[c];

   return m;
}


// -----------------
void mprint(matriz m)
{
   cout << "(" << endl;
   for (int c = 0; c < 3; c++) 
      cout << " (" << m.m[c*3] << ", " << m.m[c*3+1] << ", " << m.m[c*3+2] << ")" << endl;
   cout << ")" << endl;
}
