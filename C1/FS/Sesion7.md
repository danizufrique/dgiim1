- El amplio uso de la CA viene determinada por su facilidad de transformaci´on, que no existe en CC.
- Elevaci´on de tensi´on en CC: conexi´on de generadores en serie ⇒ poco
pr´actico.
- Elevaci´on de tensi´on en CA: uso de transformadores ⇒ eficiente
porque se eleva el voltaje hasta altos valores (alta tensi´on),
disminuyendo en igual proporci´on la intensidad de corriente.
- Energ´ıa transportada: U = vit. La misma energ´ıa puede ser
distribuida a largas distancias con bajas intensidades de corriente y,
por tanto, con bajas p´erdidas (Uperdidas = Ri2
t).
- Una vez en el punto de consumo o en sus cercan´ıas, el voltaje puede
ser de nuevo reducido para su uso industrial o dom´estico de forma
c´omoda y segura.
