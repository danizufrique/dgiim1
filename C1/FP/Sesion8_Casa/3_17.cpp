#include <iostream>
#include <string>

using namespace std;

int main(){
	const int MAY_TO_MIN = 'A'- 'a';
	string nombre;
	string nick;
	int n;
	int contador = 0;
	
	cout << "Introduce el nombre: ";
	getline(cin,nombre);
	
	cout << "Introduce N: ";
	cin >> n;
	
	/*Se a�aden las letras del nombre introducido (pasadas a min�scula) al nick mientras el contador no
	supere al tope	se�alado (n) y no sea un espacio, en cuyo caso el contador se reseteara y ya habra
	introducido la palabra	completa tal y como se pide*/
	
	for (int i=0; i<nombre.length(); i++){
		if(nombre[i] != ' ' && contador < n){
			if('A'<=nombre[i] && nombre[i] <= 'Z'){
				nick += nombre[i] - MAY_TO_MIN ;
				contador++;	
			}
			else{
				nick += nombre[i] ;
				contador++;
			}		
		}
		else if (nombre[i] == ' '){
			contador = 0;
		}
	}
	
	cout << "\nNICK: " << nick;
}
