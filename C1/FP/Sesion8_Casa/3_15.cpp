#include <iostream>
using namespace std;

void PedirMatriz(int matriz[100][100], int& filas, int& columnas){
	cout << "\nIntroduce las dimensiones de la matriz: ";
	cin >> filas >> columnas;
	
	cout << "Introduce la matriz por filas: ";
	for(int i=0; i<filas; i++){
		for (int j=0; j<columnas; j++){
			cin >> matriz[i][j];
		}
	}
}

void RepresentarMatriz(int matriz[100][100], string nombre, int filas, int columnas){
	cout << "\n\nLa matriz " << nombre << " es: \n";
	
	for(int i=0; i<filas; i++){
		cout << "\n";
		for (int j=0; j<columnas; j++){
			cout << matriz[i][j] << " ";
		}
	}
}

/*Se intercambia cada elemento por su opuesto en la matriz*/
void Transpuesta(int matriz[100][100], int trans[100][100], int filas, int columnas){
	for (int i=0; i<filas; i++){
		for (int j=0; j<columnas; j++){
			trans[j][i] = matriz[i][j];
		}
	}
}

/*Se supone que el mayor es el primero, y si en esa fila se encuentra uno mayor se sustituye el valor del mayor*/
int MaximoFila(int matriz[100][100], int fila, int n_columnas){
	int mayor = matriz[fila][0];
	for (int i=1; i<n_columnas; i++){
		if(matriz[fila][i] > mayor){
			mayor = matriz[fila][i];
		}
	}
	return mayor;
}

/*Se supone que el menor es el primero, y si en esa columna se encuentra uno menor se sustituye el valor del menor*/
int MinimoColumna(int matriz[100][100], int columna, int n_filas){
	int menor = matriz[0][columna];
	for (int i=1; i<n_filas; i++){
		if(matriz[i][columna] < menor){
			menor = matriz[i][columna];
		}
	}
	return menor;
}


/*El elemento ij de la matriz resultado es la suma del producto del elemento k de i por el elemento k de j,
dandose que el numero de productos que se suman (indice) es igual a las columnas de la matriz1 o bien a las filas de 
la matriz 2*/
void ProductoMatrices(int resultado[100][100], int matriz1[100][100], int matriz2[100][100], int filas, int columnas, int indice){
	int temporal = 0;
	
	for (int i = 0 ; i < filas ; i++ ){ 
		for (int j = 0 ; j < columnas ; j++ ){ 	
	        temporal = 0 ;
	        for (int k = 0 ; k < indice ; k++ ){
	            temporal += matriz1[i][k] * matriz2[k][j];
	            resultado[i][j] = temporal ;
	        }
	    }
	}	
}

int main(){
	int matriz[100][100];
	int trans[100][100];
	int util_filas, util_columnas;
	
	
	//ENTRADA  DATOS
	PedirMatriz(matriz, util_filas, util_columnas);
	RepresentarMatriz(matriz, "introducida", util_filas, util_columnas);
	
	//Apartado A
	cout << "\n\n___APARTADO A___\n";
	Transpuesta(matriz, trans, util_filas, util_columnas);
	RepresentarMatriz(trans, "transpuesta", util_columnas,  util_filas);
	
	//Apartado B
	cout << "\n\n___APARTADO B___\n";
	
	int menorFila;
	int menores[util_filas];
	int mayorMenor;
	int posicionFila;
	int posicionColumna;
	
	//Se calcula el menor de cada fila y se introduce en el vector "menores"
	for (int i=0; i<util_filas; i++){
		menorFila = matriz[i][0];
		for (int j=1; j<util_columnas; j++){
			if(matriz[i][j] < menorFila){
				menorFila = matriz[i][j];
			}
		}
		menores[i] = menorFila;
	}
	
	//Se busca cual de ellos es el mayor y su fila
	cout << "\nLa lista de los menores de cada fila es: ";
	mayorMenor = menores[0];
	for (int i=0; i<util_filas; i++){
		cout << menores[i] << ", ";
		if(menores[i] > mayorMenor){
			mayorMenor = menores[i];
			posicionFila = i;
		}
	}
	
	//Obtengo sus coordenadas sabiendo la fila en la que esta
	for (int i=0; i<util_columnas; i++){
		if(matriz[posicionFila][i] == mayorMenor){
			posicionColumna = i;
		}
	}
	
	cout << "\nEl mayor menor es " << mayorMenor << " y se encuentra en la posicion (" << posicionFila << "," << posicionColumna << ")";
	
	//Apartado C
	
	//Ejemplo de matriz sin Maximin: [10,20,30,0,70,60,0,30,15]
	//Ejemplo de matriz con Maximin: [1,2,3,4,5,6,7,8,9]
	
	cout << "\n\n___APARTADO C___\n";
	
	bool maximin = false;
	int nMaximin;
	
	for (int i=0; i<util_filas && maximin == false; i++){
		for (int j=0; j<util_columnas && maximin == false; j++){
			if((MinimoColumna(matriz, j, util_filas)) == (MaximoFila(matriz, i, util_columnas))){
				maximin = true;
				nMaximin = (MinimoColumna(matriz, j, util_filas));
			}
		}
	}
	
	maximin ? cout << "Existe elemento Maximin: " << nMaximin : cout << "No hay ningun elemento Maximin.";
	
	//Apartado D
	
	cout << "\n\n___APARTADO D___\n";
	
	int matriz2[100][100];
	int util_filas2, util_columnas2;
	int filasProducto;
	int columnasProducto;
	int producto[100][100];
	
	
	PedirMatriz(matriz2, util_filas2, util_columnas2);
	RepresentarMatriz(matriz2, "a multiplicar", util_filas2, util_columnas2);
	
	filasProducto = util_filas;
	columnasProducto = util_columnas2;
	
	if(util_columnas == util_filas2){
		ProductoMatrices(producto, matriz, matriz2, filasProducto, columnasProducto, util_columnas);
		RepresentarMatriz(producto, "producto",  filasProducto, columnasProducto);
	}
	else{
		cout << "\n\nEstas matrices no se pueden multiplicar";
	}
	
}
