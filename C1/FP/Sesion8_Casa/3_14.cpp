#include <iostream>
using namespace std;

void PedirMatriz(float matriz[100][100], int& filas, int& columnas){
	cout << "\nIntroduce las dimensiones de la matriz: ";
	cin >> filas >> columnas;
	
	cout << "Introduce la matriz por filas: ";
	for(int i=0; i<filas; i++){
		for (int j=0; j<columnas; j++){
			cin >> matriz[i][j];
		}
	}
}

void RepresentarMatriz(float matriz[100][100], int filas, int columnas){
	cout << "\nLa matriz es: \n";
	for(int i=0; i<filas; i++){
		cout << "\n";
		for (int j=0; j<columnas; j++){
			cout << matriz[i][j] << " ";
		}
	}
}

int main(){
	float matriz[100][100];
	int filas, columnas;
	
	//ENTRADA  DATOS
	PedirMatriz(matriz, filas, columnas);
	RepresentarMatriz(matriz, filas, columnas);
	
	//C�LCULO
	int suma_filas[filas];
	int suma_columnas[columnas];
	int suma;
	
	/*Se suman los elementos de cada l�nea*/
	for (int i=0; i<filas; i++){
		suma = 0;
		for (int j=0; j<columnas; j++){
			suma += matriz[i][j];
		}
		suma_filas[i] = suma;
	}
	
	/*Se suman los elementos de cada columna*/
	for (int i=0; i<columnas; i++){
		suma = 0;
		for (int j=0; j<filas; j++){
			suma += matriz[j][i];
		}
		suma_columnas[i] = suma;
	}
	
	//RESULTADOS
	cout << "\n\nLa suma de los elementos de cada fila es:";
	for (int i=0; i<filas; i++){
		cout << "\n\tFila " << i+1 << " : " << suma_filas[i];
	}
	
	cout << "\n\nLa suma de los elementos de cada columna es:";
	for (int i=0; i<columnas; i++){
		cout << "\n\tColumna " << i+1 << " : " << suma_columnas[i];
	}
}
