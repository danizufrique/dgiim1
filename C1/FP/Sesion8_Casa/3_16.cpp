#include <iostream>
using namespace std;

void PedirMatriz(int matriz[100][100], int& filas, int& columnas){
	cout << "\nIntroduce las dimensiones de la matriz: ";
	cin >> filas >> columnas;
	
	cout << "Introduce la matriz por filas: ";
	for(int i=0; i<filas; i++){
		for (int j=0; j<columnas; j++){
			cin >> matriz[i][j];
		}
	}
}

void RepresentarMatriz(int matriz[100][100], string nombre, int filas, int columnas){
	cout << "\n\nLa matriz " << nombre << " es: \n";
	
	for(int i=0; i<filas; i++){
		cout << "\n";
		for (int j=0; j<columnas; j++){
			cout << matriz[i][j] << " ";
		}
	}
}

bool ComprobarSimetrica(int matriz[100][100], int filas, int columnas){
	bool simetrica = true;
	
	if (filas != columnas){
		simetrica = false;
	}
	
	else{
		for (int i=0; i<filas; i++){
			for (int j=0; j<columnas; j++){
				if(matriz[i][j] != matriz[j][i]){
					simetrica = false;
				}
			}
		}
	}
	
	return simetrica;
}

int main(){
	
	int matriz[100][100];
	int filas, columnas;
	bool simetrica;
	int contador = 0;
	int vector[1000];
	
	// Apartado A
	cout << "__APARTADO A___\n";
	
	do{
		PedirMatriz(matriz, filas, columnas);
		simetrica = ComprobarSimetrica(matriz, filas, columnas);
	}while(simetrica == false);
	
	RepresentarMatriz(matriz, "introducida", filas, columnas);
	
	/*Los valores que se buscan est�n en forma escalonada, asi que conforme avanzo de fila
	aumento en uno numero de valores que tomare de ella*/
	for (int i=0; i<filas; i++){
		for (int j=0; j<=i; j++){
			vector[contador] = matriz[i][j];
			contador++;	
		}
	}

	cout << "\n\nEl vector pedido es: (";
	for (int i=0; i<contador; i++){
		if(contador-i > 1){
			cout << vector[i] << ",";
		}
		else{
			cout << vector[i] << ")";
		}
		
	}
	
	//Apartado B
	cout << "\n\n___APARTADO B___\n";
	
	int vector_usuario[1000];
	int matriz2[100][100];
	int longitud;
	int fila = 0, tope_columna = 0;
	
	contador = 0;
	
	
	cout << "Introduce la longitud del vector: ";
	cin >> longitud;
	
	cout << "Introduce el vector separado por espacios: ";
	for (int i=0; i<longitud; i++){
		cin >> vector[i];
	}
		
	/*Se van colocando valores de forma escalonada de forma an�loga
	a como se obtuvo el vector en el apartado anterior*/
	
	while (contador < longitud){
		for (int i=0; i<=tope_columna; i++){
			matriz2[fila][i] = vector[contador];
			matriz2[i][fila] = vector[contador];
			contador++;
		}
		fila++;
		tope_columna++;
	}
	
	RepresentarMatriz(matriz2, "pedida", fila, tope_columna);
}
