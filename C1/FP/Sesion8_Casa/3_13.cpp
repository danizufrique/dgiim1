#include <iostream>
using namespace std;


void PedirMatriz(int matriz[100][100], char nombre, int& filas, int& columnas){
	cout << "\nIntroduce las dimensiones de la matriz " << nombre << ": ";
	cin >> filas >> columnas;
	
	cout << "Introduce la matriz por filas: ";
	for(int i=0; i<filas; i++){
		for (int j=0; j<columnas; j++){
			cin >> matriz[i][j];
		}
	}
}

void RepresentarMatriz(int matriz[100][100], char nombre, int filas, int columnas){
	cout << "\n\nLa matriz " << nombre << " es:";
	for(int i=0; i<filas; i++){
		cout << "\n";
		for (int j=0; j<columnas; j++){
			cout << matriz[i][j] << " ";
		}
	}
}

/*Si algun elemento [m][n] no coincide con el de la misma posicion en la otra matriz se deja de comprobar
y se obtiene que la matriz es diferente (false)*/

bool MatricesIguales(int matriz1[100][100], int matriz2[100][100], int filas1, int columnas1, int filas2, int columnas2){
	bool iguales = true;
	
	if(filas1 != filas2 || columnas1 != columnas2){
		iguales = false;
	}
	
	else{
		for (int i=0, j=0; i<filas1 && j<filas2 && iguales == true; i++, j++){
			for (int x=0, y=0; x<columnas1 && y<columnas2 && iguales == true; x++, y++){
				if(matriz1[i][x] != matriz2[j][y]){
					iguales = false;
				}
			}
		}	
	}
	
	return iguales;
}

int main(){
	int matriz1[100][100];
	int matriz2[100][100];	
	int filas1, filas2;
	int columnas1, columnas2;
	bool iguales;
	
	//ENTRADA DATOS
	PedirMatriz(matriz1, '1', filas1, columnas1);
	PedirMatriz(matriz2, '2', filas2, columnas2);
	
	RepresentarMatriz(matriz1, '1', filas1, columnas1);
	RepresentarMatriz(matriz2, '2', filas2, columnas2);
	

	//C�LCULO
	iguales = MatricesIguales(matriz1, matriz2, filas1, columnas1, filas2, columnas2);
	
	//RESULTADO
	iguales ? cout << "\n\nLas matrices son iguales" : cout << "\n\nLas matrices son diferentes";
}
