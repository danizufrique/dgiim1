#include <iostream>
using namespace std;

const int MAX_LONG = 1000;

struct Permutacion{
    int datos[MAX_LONG];
    int longitud;
};

//Comprueba que est�n todos los n�meros hasta N sin repetir
bool PermutacionCorrecta(Permutacion permutacion){
    bool correcto = true;
    bool encontrado = false;
    
    for (int n = 1; n <= permutacion.longitud && correcto ; n++){
        encontrado = false;
        for (int i = 0; i < permutacion.longitud && !encontrado; i++){
            if (permutacion.datos[i] == n){
                encontrado = true;
            }
        }
        if (!encontrado){
            correcto = false;
        }
    }
    return correcto;
}

Permutacion Composicion(Permutacion permutacion1, Permutacion permutacion2){
    Permutacion nueva_permutacion;
    nueva_permutacion.longitud = permutacion1.longitud;
    
    for (int i = 0; i < permutacion1.longitud; i++){
        nueva_permutacion.datos[i] = permutacion2.datos[permutacion1.datos[i]-1];
    }
    
    return nueva_permutacion;
}

Permutacion Potencia(Permutacion permutacion, int potencia){
    Permutacion nueva_permutacion;
    nueva_permutacion.longitud = permutacion.longitud;
    
    for (int i = 0; i < permutacion.longitud; i++){
        nueva_permutacion.datos[i] = permutacion.datos[i];
    }
    
    for (int i = 1; i < potencia; i++){
       nueva_permutacion = Composicion(nueva_permutacion, permutacion);
    }
    
    return nueva_permutacion;
}


int main(){
    Permutacion permutacion;
    Permutacion potencia;
    int exp;
    
    cout << "Introduce  de la permutacion: ";
    cin >> permutacion.longitud;
    
    do{
        cout << "Introduce los datos de la permutacion: ";
        for (int i = 0; i < permutacion.longitud; i++){
            cin >> permutacion.datos[i];
        }
    } while (! PermutacionCorrecta(permutacion));
    
    
    cout << "Introduzca el exponente: ";
    cin >> exp;
    
    potencia = Potencia(permutacion, exp);
    
    
    cout << "La potencia " << exp<< " de la permutacion es: ";
    for (int i = 0; i < potencia.longitud;i++){
        cout << potencia.datos[i] << " ";
    }
}
