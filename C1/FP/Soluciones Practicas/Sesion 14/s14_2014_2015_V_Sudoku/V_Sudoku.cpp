////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
Sudoku es un juego muy popular que consiste en rellenar
una cuadr�cula de 9 � 9 celdas que est� dividida en subcuadr�culas de 3 � 3 (deno-
minadas regiones) con cifras del 1 al 9. Un sudoku se considera resuelto si verifica
que:
� En cada fila aparecen todos los n�meros del 1 al 9 (sin repetir)
� En cada columna aparecen todos los n�meros del 1 al 9 (sin repetir)
� En cada regi�n aparecen todos los n�meros del 1 al 9 (sin repetir)

Realizar un programa que lea todos los elementos de un sudoku y determine si est�
resuelto o no.
Implemente la soluci�n con una clase Sudoku con un dato miembro matriz de doble
corchete y a�ada un programa principal de prueba.
*/


#include <iostream>
#include <cmath>

using namespace std;

class SecuenciaEnteros{
private:
   static const int TAMANIO = 50;
   int vector_privado[TAMANIO];
   int total_utilizados;
public:
   SecuenciaEnteros()
      :total_utilizados(0) {
   }
   int TotalUtilizados(){
      return total_utilizados;
   }
   int Capacidad(){
      return TAMANIO;
   }
   void Aniade(int nuevo){
      if (total_utilizados < TAMANIO){
         vector_privado[total_utilizados] = nuevo;
         total_utilizados++;
      }
   }
   int Elemento(int indice){
      return vector_privado[indice];
   }
   void EliminaTodos(){
      total_utilizados = 0;
   }
   
   void Modifica(int indice_componente, int nuevo){
		if (indice_componente >= 0  &&  indice_componente < total_utilizados)
			vector_privado[indice_componente] = nuevo;
	}	
	
	int PrimeraOcurrenciaEntre (int pos_izda, int pos_dcha, int buscado){
      int i = pos_izda;
      bool encontrado = false;

      while (i <= pos_dcha  &&  !encontrado)
         if (vector_privado[i] == buscado)
            encontrado = true;
         else
            i++;

      if (encontrado)
         return i;
      else
         return -1;
   }
   
   int PrimeraOcurrencia (int buscado){
      return PrimeraOcurrenciaEntre (0, total_utilizados - 1, buscado);
   }
};



class Sudoku{
private:
	// Se considera tablero 9x9. 
	static const int TAMANIO_TABLERO = 9;
	int tablero[TAMANIO_TABLERO][TAMANIO_TABLERO];
	int filas_aniadidas;
   
   ///////////////////////////////////////////////////////////////////////////
   
   
	// Hemos considerado regiones de tama�o arbitrario distinto de 3
	// por lo que el tama�o se pasa como par�metro.
	// En cualquier caso, no es necesario ya que no se ped�a en el enunciado. 

	bool CheckRegion_repite_codigo_vs1 (int fila_inicial, int col_inicial, int tamanio_region){
		bool encontrado = true;
		int tope_fil = fila_inicial + tamanio_region;
		int tope_col = col_inicial + tamanio_region;

		for (int a_buscar = 1; a_buscar < 10 && encontrado; a_buscar++){
		   encontrado = false;
		   		   
			for (int fila = fila_inicial; fila < tope_fil && !encontrado; fila++)
				for (int col = col_inicial; col < tope_col && !encontrado; col++)
					encontrado = (tablero[fila][col] == a_buscar);                                          
		}

		return encontrado;
	}
	
	// Si alg�n entero entre 1 y 9 no est� en la fila, 
	// no se cumple la condici�n de resoluci�n del Sudoku.
	// Como s�lo hay 9 componentes en cada fila, si encontramos todos
	// los d�gitos entre 1 y 9 => No puede haber repetidos.

	bool CheckFila_repite_codigo_vs1(int indice_fila){
		bool encontrado = true;

		for (int a_buscar = 1; a_buscar < 10 && encontrado; a_buscar++){
		   encontrado = false;
		   
			for (int i=0; i<TAMANIO_TABLERO && !encontrado; i++)
				encontrado = (tablero[indice_fila][i] == a_buscar);             
		}

		return encontrado;
	}

	bool CheckColumna_repite_codigo_vs1(int indice_col){
		bool encontrado = true;

		for (int a_buscar = 1; a_buscar < 10 && encontrado; a_buscar++){
		   encontrado = false;
		   		   
			for (int i=0; i<TAMANIO_TABLERO && !encontrado; i++)
				encontrado = (tablero[i][indice_col] == a_buscar);         
		}

		return encontrado;
	}
	
	/*
      El c�digo de CheckFila y CheckColumna es bastante parecido.
      Si quisi�ramos unificarlo tendr�amos varias alternativas:
      
      La primera que vamos a ver pasa por construir un objeto SecuenciaEnteros 
		y usamos el m�todo PrimeraOcurrencia que ya est� implementado en la clase. 
      Dicho objeto ser� una fila, una columna, o una regi�n.
      Para ello, a�adimos el m�todo Region que construye una secuencia de enteros
      con la matriz correspondiente a la regi�n indicada.
      
      El �nico incoveniente de esta soluci�n ser�a que tenemos que construir
		la secuencia completa (fila, columna o regi�n) y despu�s volvemos a recorrerla
		para comprobar si contiene o no los enteros entre 1 y 9.
   */

	bool CheckFila_repite_codigo_vs2(int indice_fila){
		bool encontrado = true;
		SecuenciaEnteros fila(Fila(indice_fila));  

		for (int a_buscar = 1; a_buscar < 10 && encontrado; a_buscar++)
			encontrado = fila.PrimeraOcurrencia(a_buscar) != -1;
		
		return encontrado;
	}
	
	bool CheckColumna_repite_codigo_vs2(int indice_col){
		bool encontrado = true;
		SecuenciaEnteros columna(Columna(indice_col));  

		for (int a_buscar = 1; a_buscar < 10 && encontrado; a_buscar++)
			encontrado = columna.PrimeraOcurrencia(a_buscar) != -1;
		
		return encontrado;
	}
	
	bool CheckRegion_repite_codigo_vs2 (int fila_inicial, int col_inicial, int tamanio_region){
		bool encontrado = true;
		SecuenciaEnteros region(Region(fila_inicial, col_inicial, tamanio_region));  

		for (int a_buscar = 1; a_buscar < 10 && encontrado; a_buscar++)
			encontrado = region.PrimeraOcurrencia(a_buscar) != -1;
		
		return encontrado;
	}
	
	/*
	Mejor a�n: 
	
		Podemos definir un �nico m�todo Check al que previamente
		le pasamos una secuencia cualquiera de enteros. De esa forma evitamos
		completamente la duplicidad de c�digo.
	*/
	
	bool Check(SecuenciaEnteros a_comprobar){
	   bool encontrado = true;
	   
	   for (int a_buscar = 1; a_buscar < 10 && encontrado; a_buscar++)
			encontrado = a_comprobar.PrimeraOcurrencia(a_buscar) != -1;
		
		return encontrado; 
	}
	
	bool CheckFila(int indice_fila){
		return CheckAlternativo(Fila(indice_fila));
	}
	
	bool CheckColumna(int indice_col){
		return CheckAlternativo(Columna(indice_col));
	}
	
	bool CheckRegion (int fila_inicial, int col_inicial, int tamanio_region){
		return CheckAlternativo(Region(fila_inicial, col_inicial, tamanio_region));
	}
	
	/*
   Ampliaci�n.
   
   	La ventaja de haber aislado en el m�todo Check la tarea de comprobar
   	la validez de la secuencia es que podemos modificar el algoritmo
   	que lo resuelve cambiando �nicamente este m�todo.
   	
      Una mejora del algoritmo podr�a ser la siguiente
      pasa por usar la misma idea que el contador de may�sculas.
      
      Utilizamos un vector de frecuencias (n�mero de veces que aparece un n�mero)
         int frecuencias[9];
      frecuencias[i] contendr� el n�mero de apariciones de i+1
      Recorremos los valores de la secuencia (fila, columna o regi�n) 
      y directamente incrementamos frecuencias[i-1]
      Una vez hecho lo anterior, recorremos el vector de frecuencias 
		y si hay alguno mayor o igual que dos, es que est�
      repetido y por tanto falta alg�n valor.
      
      Esta soluci�n s�lo es v�lida si tenemos la garant�a de que los valores
      que hay en el Sudoku son enteros entre 1 y 9.      
   */
   
   bool CheckAlternativo(SecuenciaEnteros a_comprobar){
      int frecuencias[9] = {0};
      int tope = a_comprobar.TotalUtilizados();
      bool check = true;
      
      for (int i=0; i<tope; i++)
         frecuencias[a_comprobar.Elemento(i-1)] ++;
      
      for (int i=0; i<tope && check; i++)
         check = frecuencias[i] == 1;

		return check; 
	}
 
                     
               
	/*
	Ampliaci�n.
	  
		Si hubiese una restricci�n de memoria muy importante y no podemos crear
	   un objeto SecuenciaEnteros copia de una fila, columna o regi�n, entonces,
		para no repetir c�digo, podr�amos definir un m�todo gen�rico PRIVADO 
		pas�ndole como par�metro los valores que me permitan
	   distinguir entre filas, columnas y regiones.
	   
	   	bool Check_EficienteMemoria(int fil_inicial, int fil_final, int col_inicial, int col_final)
	   	
	 	Es algo parecido a lo que se vio con la clase TraccionBicicleta.
   	Pero volvemos a destacar que debe ser privado ya que no queremos que
      el cliente de la clase pueda llamarlo con cualquier par�metro actual.

	   Dependiendo de los tipos de datos con los que trabajemos, el tipo
   	de comparaciones a realizar, el tama�o de los bloques a procesar, etc, 
   	optaremos por una soluci�n u otra. 
   */
   
	
	// B�squeda en una fila f ->    f, f, 0, total_utilizados
	// B�squeda en una columna c -> 0, total_utilizados, c, c
	// B�squeda en una regi�n
	// con esquina superior derecha f,c ->   f, f+tamanio_region, c, c+tamanio_region
	bool Check_EficienteMemoria(int fil_inicial, int fil_final, int col_inicial, int col_final){
		bool encontrado = true;
		
		for (int a_buscar = 1; a_buscar < 10 && encontrado; a_buscar++){
      	encontrado = false;
      	
      	for (int i=fil_inicial; i<=fil_final && !encontrado ; i++)
				for (int j=col_inicial; j<=col_final  && !encontrado; j++)
					encontrado = (tablero[i][j] == a_buscar);  
		}
		
		return encontrado;
	}
			     	

  
   
public:
	Sudoku ()
		:filas_aniadidas(0)
	{}
	void AniadeFila (SecuenciaEnteros fila){
		if (filas_aniadidas < TAMANIO_TABLERO){
			for (int col=0; col < TAMANIO_TABLERO; col++)
				tablero[filas_aniadidas][col] = fila.Elemento(col);

			filas_aniadidas++;
		}
	}
	
	int Tamanio(){
	   return TAMANIO_TABLERO;
	}
	
	SecuenciaEnteros Fila(int indice_fila){
      SecuenciaEnteros fila;
	   
      for (int j=0; j<TAMANIO_TABLERO; j++)
         fila.Aniade(tablero[indice_fila][j]); 
	     
	   return fila;
	}
	
	SecuenciaEnteros Columna(int indice_col){
      SecuenciaEnteros columna;
	   
      for (int i=0; i<TAMANIO_TABLERO; i++)
         columna.Aniade(tablero[i][indice_col]); 
	     
	   return columna;
	}

   SecuenciaEnteros Region(int fila_inicial, int col_inicial, int tamanio_region){
      SecuenciaEnteros region;
      int tope_fil = fila_inicial + tamanio_region;
		int tope_col = col_inicial + tamanio_region;
		
      for (int fila = fila_inicial; fila < tope_fil; fila++)
			for (int col = col_inicial; col < tope_col; col++)
			   region.Aniade(tablero[fila][col]);
		
		return region;
   }
   
	int Digito (int fil, int col){
		return tablero[fil][col];
	}

	bool Resuelto(){
		bool resuelto = true;
		int tamanio_region = 3;

		for (int fil = 0; fil < TAMANIO_TABLERO && resuelto; fil++)
			resuelto = CheckFila(fil);

		for (int col = 0; col< TAMANIO_TABLERO && resuelto; col++)
			resuelto = CheckColumna(col);

		for (int fil_region = 0; 
			fil_region < TAMANIO_TABLERO && resuelto; 
			fil_region = fil_region + tamanio_region)

			for (int col_region = 0; 
				col_region < TAMANIO_TABLERO && resuelto; 
				col_region = col_region + tamanio_region)

				resuelto = CheckRegion(fil_region, col_region, tamanio_region);

		return resuelto;
	}
	
	bool Resuelto_EficienteMemoria(){
		bool resuelto = true;
		int tamanio_region = 3;

		for (int fil = 0; fil < TAMANIO_TABLERO && resuelto; fil++)
			resuelto = Check_EficienteMemoria(fil, fil, 0, TAMANIO_TABLERO);	
			
		for (int col = 0; col< TAMANIO_TABLERO && resuelto; col++)
			resuelto = Check_EficienteMemoria(0, TAMANIO_TABLERO, col, col);

		for (int fil_region = 0; 
			fil_region < TAMANIO_TABLERO && resuelto; 
			fil_region = fil_region + tamanio_region)

			for (int col_region = 0; 
				col_region < TAMANIO_TABLERO && resuelto; 
				col_region = col_region + tamanio_region)

				resuelto = Check_EficienteMemoria(fil_region, fil_region + tamanio_region, col_region, col_region + tamanio_region);

		return resuelto;
	}

};

int main(){
   int casilla;
   Sudoku mi_sudoku;
   SecuenciaEnteros fila;
   bool resuelto;
   
   for (int i=0; i< 9; i++){
      fila.EliminaTodos();
      
      for (int j=0; j<9; j++){
         cin >> casilla;
         fila.Aniade(casilla);
      }  
      
      mi_sudoku.AniadeFila(fila);
   }  
 	
   resuelto = mi_sudoku.Resuelto_EficienteMemoria();

   if (resuelto)
      cout << "\nResuelto";
   else
      cout << "\nNo resuelto";
      
   // 1 2 3 4 5 6 7 8 9    4 5 6 7 8 9 1 2 3    7 8 9 1 2 3 4 5 6    2 3 4 5 6 7 8 9 1   5 6 7 8 9 1 2 3 4    8 9 1 2 3 4 5 6 7     3  4 5 6 7 8 9 1 2    6 7 8 9 1 2 3 4 5    9 1 2 3 4 5 6 7 8
} 
