////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
Definid la clase MatrizRectangularEnteros usando una matriz de doble corchete como dato miembro privado.

   int matriz_privada[MAXIMO_FILAS][MAXIMO_COLUMNAS];

Definid m�todos para:

a) Obtener el n�mero de filas y columnas utilizadas, as� como el dato que haya en
una fila y columna.
b) Devolver una fila completa como un objeto de la clase SecuenciaEnteros.
c) A�adir una fila entera. La fila ser� un objeto de la clase SecuenciaEnteros.
d) Comprobar si es igual a otra matriz.
e) Obtener la traspuesta.
f) Comprobar si es sim�trica. Hacedlo primero calculando la traspuesta de la matriz
   y viendo si es igual a su sim�trica, usando los m�todos anteriores.
   Hacedlo tambi�n comprobando directamente si cada componente es igual a su
   sim�trica y parando el recorrido en cuanto encuentre una componente que no lo
   verifique.
g) Multiplicar dos matrices.
*/

/*
Sobre el ejercicio anterior, construid un m�todo que busque la fila de la matriz que
m�s se parezca a una secuencia de enteros, a la que llamaremos referencia. La
similitud entre dos secuencias x = (x1���xp) e y = (y1���yp) vendr� dada por la
distancia eucl�dea entre ambas:
   dist(x,y) = sqrt((x1- y1)�2+ ��� + (xp- yp)�2)
Adem�s, la b�squeda solo se har� sobre las filas de la matriz enumeradas en una
segunda secuencia llamada filas_a_comparar.
Por ejemplo, dada la matriz M (7 � 4),
!  3 1 0 8
   4 5 1 5
!  5 7 1 7
   7 9 6 1
!  4 9 5 5
!  2 8 2 2
   7 3 2 5
y las secuencias referencia = 2,8,1,1 y filas_a_comparar = 0,2,4,5, el
programa deber� encontrar 5 como la fila m�s cercana a referencia.
*/


#include <iostream>
#include <cmath>
using namespace std;


class SecuenciaEnteros{
private:
   static const int TAMANIO = 50;
   int vector_privado[TAMANIO];
   int total_utilizados;
   
   void IntercambiaComponentes_en_Posiciones(int pos_izda, int pos_dcha){
      int intercambia;

      intercambia = vector_privado[pos_izda];
      vector_privado[pos_izda] = vector_privado[pos_dcha];
      vector_privado[pos_dcha] = intercambia;
   }
public:
   SecuenciaEnteros()
      :total_utilizados(0) {
   }
   int TotalUtilizados(){
      return total_utilizados;
   }
   int Capacidad(){
      return TAMANIO;
   }
   void Aniade(int nuevo){
      if (total_utilizados < TAMANIO){
         vector_privado[total_utilizados] = nuevo;
         total_utilizados++;
      }
   }
   int Elemento(int indice){
      return vector_privado[indice];
   }
   void EliminaTodos(){
      total_utilizados = 0;
   }
   
   int ProductoEscalar (SecuenciaEnteros otra_secuencia){
   	int producto_escalar = 0;
   	
   	if (otra_secuencia.TotalUtilizados() == total_utilizados){
   		for (int i=0; i<total_utilizados; i++)
   			producto_escalar = producto_escalar + vector_privado[i] * otra_secuencia.Elemento(i);
   	}
   	
   	return producto_escalar;
   }
   
   double DistanciaEuclidea(SecuenciaEnteros otra_secuencia){
      double distancia;
      
      if (total_utilizados == otra_secuencia.TotalUtilizados()){
         distancia = 0;
         
         for (int i=0; i<total_utilizados; i++){
            double diferencia;
            
            diferencia = vector_privado[i] - otra_secuencia.Elemento(i);
            diferencia = diferencia * diferencia;
            distancia = distancia + diferencia;
         }
         
         distancia = sqrt(distancia);
      }
      else
         distancia = -1;
      
      return distancia;
   }
};




class MatrizRectangularEnteros{
private:
   static const int MAX_FIL = 50;
   static const int MAX_COL = 40;
   int matriz_privada[MAX_FIL][MAX_COL];
   int util_fil;
   const int util_col;
public:
   // Prec: 0 < numero_de_columnas <= MAX_COL(40)
   MatrizRectangularEnteros(int numero_de_columnas)
      :util_fil(0), util_col(numero_de_columnas)
   { 
   }
   MatrizRectangularEnteros(SecuenciaEnteros primera_fila)
      :util_fil(0), util_col(primera_fila.TotalUtilizados())
   { 
   }
   int CapacidadFilas(){
      return MAX_FIL;
   }
   int FilasUtilizadas(){
      return util_fil;
   }
   int ColUtilizadas(){
      return util_col;
   }
   int Elemento(int fila, int columna){
      return matriz_privada[fila][columna];
   }
   SecuenciaEnteros Fila(int indice_fila){
      SecuenciaEnteros fila;

      for (int col = 0; col < util_col; col++)
         fila.Aniade(matriz_privada[indice_fila][col]);

      return fila;
   }
   
   void Aniade(SecuenciaEnteros fila_nueva){
      int numero_columnas_nueva;

      if (util_fil < MAX_FIL){
         numero_columnas_nueva = fila_nueva.TotalUtilizados();

         if (numero_columnas_nueva == util_col){
            for (int col = 0; col < util_col ; col++)
               matriz_privada[util_fil][col] = fila_nueva.Elemento(col);

            util_fil++;
         }
      }
   }
   
   bool EsIgual_a (MatrizRectangularEnteros otra_matriz){
      bool son_iguales;
      
      son_iguales = util_fil == otra_matriz.FilasUtilizadas() &&
      				  util_col == otra_matriz.ColUtilizadas();	
      
      for (int i=0; i<util_fil && son_iguales; i++)
         for (int j=0; j<util_col && son_iguales; j++)
            son_iguales = matriz_privada[i][j] == otra_matriz.Elemento(i,j);
      
      return son_iguales;
   }
   
   SecuenciaEnteros Columna(int indice_columna){
      SecuenciaEnteros columna;
      
      for (int i=0; i<util_fil; i++)
         columna.Aniade(matriz_privada[i][indice_columna]);
      
      return columna;
   }
   
   MatrizRectangularEnteros Traspuesta(){
      MatrizRectangularEnteros traspuesta(util_fil);
 
      for (int j=0; j<util_col; j++)
         traspuesta.Aniade(Columna(j));
         
      return traspuesta;
   }
   
   // versi�n ineficiente pero que aprovecha otros m�todos
   bool EsSimetrica_vs1(){      
      return EsIgual_a(Traspuesta());      
   }
   
   // versi�n eficiente. Se sale del bucle en cuanto encuentro dos elementos discordantes
   bool EsSimetrica_vs2(){      
      bool es_simetrica;
      
      for (int i=0; i<util_fil && es_simetrica; i++)
         for (int j=i+1; j<util_col && es_simetrica; j++)
            es_simetrica = matriz_privada[i][j] == matriz_privada[j][i];
      
      return es_simetrica;
   }
   
   MatrizRectangularEnteros Multiplica_por_vs1 (MatrizRectangularEnteros otra_matriz){
      int columnas_otra_matriz = otra_matriz.ColUtilizadas();
      MatrizRectangularEnteros producto(columnas_otra_matriz);
      SecuenciaEnteros fila_producto;
      int casilla_producto;
      
      if (util_col == otra_matriz.FilasUtilizadas()){
	      for (int i=0; i<util_fil; i++){      
	   		for (int j=0; j<columnas_otra_matriz; j++){           
	            casilla_producto = 0;
	            
	            for (int k=0; k<util_col; k++)
	               casilla_producto = casilla_producto +  matriz_privada[i][k] * otra_matriz.Elemento(k,j);
	            
	            fila_producto.Aniade(casilla_producto);
	         } 
	         
	         producto.Aniade(fila_producto);
	         fila_producto.EliminaTodos();
	      }    
   	}
      
		return producto;
   }
   
   // Esta versi�n de Multiplica es mejor ya que hemos situado la tarea del c�mputo
   // del producto de dos vectores en el sitio que le corresponde: la secuencia de enteros
   MatrizRectangularEnteros Multiplica_por_vs2 (MatrizRectangularEnteros otra_matriz){
      int columnas_otra_matriz = otra_matriz.ColUtilizadas();
      MatrizRectangularEnteros producto(columnas_otra_matriz);
      SecuenciaEnteros fila_izda, fila_producto;
      int casilla_producto;
      
      if (util_col == otra_matriz.FilasUtilizadas()){
	      for (int i=0; i<util_fil; i++){      
	      	fila_izda = Fila(i);
	      	
	   		for (int j=0; j<columnas_otra_matriz; j++){           
	            casilla_producto = fila_izda.ProductoEscalar(otra_matriz.Columna(j));
	            fila_producto.Aniade(casilla_producto);
	         } 
	         
	         producto.Aniade(fila_producto);
	         fila_producto.EliminaTodos();
	      }    
   	}
      
		return producto;
   }
   
   
   int PosFilaMasCercana(SecuenciaEnteros referencia, SecuenciaEnteros filas_a_comparar){
      int pos_fila_mas_cercana;
      double dist_minima;
      int util_filas_a_comparar = filas_a_comparar.TotalUtilizados(); 
      
      if (util_filas_a_comparar > 0 && referencia.TotalUtilizados() == util_col){
         int pos_fila_a_comparar = filas_a_comparar.Elemento(0);
         pos_fila_mas_cercana = pos_fila_a_comparar;
         dist_minima = Fila(pos_fila_a_comparar).DistanciaEuclidea(referencia);
         
         for (int i=1; i<util_filas_a_comparar; i++){
            pos_fila_a_comparar = filas_a_comparar.Elemento(i);
            double dist =  Fila(pos_fila_a_comparar).DistanciaEuclidea(referencia);
            
            if (dist < dist_minima){
               dist_minima = dist;
               pos_fila_mas_cercana = pos_fila_a_comparar; 
            }
         }
      }
            
      return pos_fila_mas_cercana;
   }
};


int main(){
	int filas_a_leer_matriz, columnas_a_leer_matriz;	
   int casilla;
   SecuenciaEnteros fila;
   
   // Leemos los datos de la matriz original
	// No etiquetamos las entradas porque suponemos que leemos de un fichero
	// Supondremos que util_filas y util_columnas son > 0

	cin >> filas_a_leer_matriz;
	cin >> columnas_a_leer_matriz;
	
	MatrizRectangularEnteros matriz(columnas_a_leer_matriz);

	for (int i=0; i<filas_a_leer_matriz; i++){
      for (int j=0; j<columnas_a_leer_matriz; j++){
         cin >> casilla;
         fila.Aniade(casilla);
      }  
      
      matriz.Aniade(fila);
      fila.EliminaTodos();
   }
   
   // Imprimimos la matriz original
   /*
   Nota:
      Las variables util_fil_matriz (util_col_matriz) representan lo mismo 
      que filas_a_leer_matriz (columnas_a_leer_matriz)
      Hemos usado variables distintas para enfatizar que filas_a_leer_matriz
      representa el n�mero de filas que vamos a leer desde un fichero externo.
      Una vez que tengamos las filas introducidas en la matriz, podr�amos leer
      m�s filas, borrar otras, etc, y FilasUtilizadas() de la matriz cambiar�a autom�ticamente.
   */
   
  	int util_fil_matriz, util_col_matriz;	
	
	util_fil_matriz = matriz.FilasUtilizadas();
	util_col_matriz = matriz.ColUtilizadas();
	
	cout << "\n\n";
   cout << "Matriz original:\n";   
   
   for (int i=0; i<util_fil_matriz; i++){
      cout << "\n";
      
      for (int j=0; j<util_col_matriz; j++)
         cout << matriz.Elemento(i,j) << " ";
   }  
	

         
   ///////////////////////////////////////////////////////////////////////////
   // Traspuesta
   
   // Creamos la matriz traspuesta
   
   MatrizRectangularEnteros traspuesta(matriz.Traspuesta());
   
   // Imprimimos la traspuesta
   
   int util_fil_traspuesta, util_col_traspuesta;
   util_fil_traspuesta = traspuesta.FilasUtilizadas();
   util_col_traspuesta = traspuesta.ColUtilizadas();
   
   cout << "\n\n";
   cout << "Matriz traspuesta:\n";   
    
   for (int i=0; i<util_fil_traspuesta; i++){
      cout << "\n";
      
      for (int j=0; j<util_col_traspuesta; j++)
         cout << traspuesta.Elemento(i,j) << " ";
   }  
   

   ///////////////////////////////////////////////////////////////////////////
   // Multiplicaci�n
   
   // Leemos los datos de la matriz derecha
   
   int filas_a_leer_dcha, columnas_a_leer_dcha;
   cin >> columnas_a_leer_dcha;
   filas_a_leer_dcha = util_col_matriz;
   
   MatrizRectangularEnteros matriz_dcha(columnas_a_leer_dcha); 
   
   fila.EliminaTodos();
      
	for (int i=0; i<filas_a_leer_dcha; i++){
		for (int j=0; j<columnas_a_leer_dcha; j++){
			cin >> casilla;
			fila.Aniade(casilla);
		}
		
		matriz_dcha.Aniade(fila);
		fila.EliminaTodos();
   }
	
   // Creamos la matriz producto	   
		   
   MatrizRectangularEnteros matriz_producto(matriz.Multiplica_por_vs2(matriz_dcha)); 
 
   // Imprimimos la matriz producto
  	
	int util_fil_producto, util_col_producto;
   util_fil_producto = matriz_producto.FilasUtilizadas();
   util_col_producto = matriz_producto.ColUtilizadas();

   cout << "\n\n";
   cout << "Matriz producto:\n";
   
   for (int i=0; i<util_fil_producto; i++){
      cout << "\n";
      
      for (int j=0; j<util_col_producto; j++)
         cout << 	matriz_producto.Elemento(i,j) << " ";
   }  
 
 
   ///////////////////////////////////////////////////////////////////////////
   // Posici�n de fila m�s cercana
   
   // Leemos la fila con la que vamos a comparar y los �ndices de las filas que se van a comparar
   
   SecuenciaEnteros referencia;
   SecuenciaEnteros filas_a_comparar;
   
   for (int i=0; i<util_col_matriz; i++){
      cin >> casilla;
      referencia.Aniade(casilla);
   }
   
   int numero_filas_a_comparar;
   cin >> numero_filas_a_comparar;
   
   for (int i=0; i<numero_filas_a_comparar; i++){
      cin >> casilla;
      filas_a_comparar.Aniade(casilla);
   }
   
   int pos_mas_cercano;
   
   pos_mas_cercano = matriz.PosFilaMasCercana(referencia, filas_a_comparar);
   
   cout << "\n\nPosici�n m�s cercana: " << pos_mas_cercano;
   
   cout << "\n\n";
	system("pause");
	
	// Matriz de 7x4
	// Matriz dcha de 4x2
   // referencia  2,8,1,1
   // filas_a_comparar = 0,2,4,5
   
	// 7 4   3 1 0 8   4 5 1 5    5 7 1 7   7 9 6 1   4 9 5 5   2 8 2 2   7 3 2 5          2    3 4   6 7   7 8    1 2             2 8 1 1    4    0 2 4 5    
   
   // 3 4    9 7 4 5    2 18 2 12    7 9 1 5      2      1 2   3 4   5 6   7 8
}
