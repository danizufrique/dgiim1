////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

/*
Se quiere trabajar con una tabla de datos en el que todas las
filas tienen el mismo n�mero de columnas y los datos son de tipo int. Esta clase se
llamar� ImagenBlancoNegro y contendr� valores enteros que representan un nivel
de gris (0 ser�a negro y 255 blanco). Se supone que todos los valores deben ser posi-
tivos aunque por problemas de captaci�n y registro algunos de ellos son negativos. Es
preciso corregir estos valores err�neos y se propone sustituirlos por el valor promedio
de sus ocho vecinos m�s cercanos espacialmente (arriba, abajo, izquierda, derecha y
esquinas). Debe considerar que entre estos vecinos pudiera haber valores negativos,
y en este caso no intervendr�n en el c�lculo del valor promedio:

Si hubiera un s�lo valor negativo en la vecindad, se sumar�n los valores de
los 7 vecinos v�lidos y la suma se dividir� entre 7. Si hubiera dos valores
negativos en la vecindad, se sumar�n los valores de los 6 vecinos v�lidos y la
suma se dividir� entre 6. ...Si no hubiera ning�n valor v�lido, se sustituir�
por un cero.

Implemente un m�todo para que dada una imagen, devuelva otra imagen corregida.
La imagen original no se modifica.
Para la implementaci�n debe considerar:

a) El algoritmo debe ser simple y claro.

b) Para simplificar el problema, las casillas de los bordes no se modifican, aunque
s� se usan para efectuar las correcciones oportunas. En definitiva, la primera y
la �ltima fila as� como la primera y la �ltima columna son iguales entre la matriz
original y la corregida.
Cread un programa principal de prueba.
*/

class SecuenciaEnteros{
private:
   static const int TAMANIO = 50;
   int vector_privado[TAMANIO];
   int total_utilizados;
public:
   SecuenciaEnteros()
      :total_utilizados(0) {
   }
   int TotalUtilizados(){
      return total_utilizados;
   }
   int Capacidad(){
      return TAMANIO;
   }
   void Aniade(int nuevo){
      if (total_utilizados < TAMANIO){
         vector_privado[total_utilizados] = nuevo;
         total_utilizados++;
      }
   }
   int Elemento(int indice){
      return vector_privado[indice];
   }
   void EliminaTodos(){
      total_utilizados = 0;
   }
   
   void Modifica(int indice_componente, int nuevo){
		if (indice_componente >= 0  &&  indice_componente < total_utilizados)
			vector_privado[indice_componente] = nuevo;
	}	
	
	int PrimeraOcurrenciaEntre (int pos_izda, int pos_dcha, int buscado){
      int i = pos_izda;
      bool encontrado = false;

      while (i <= pos_dcha  &&  !encontrado)
         if (vector_privado[i] == buscado)
            encontrado = true;
         else
            i++;

      if (encontrado)
         return i;
      else
         return -1;
   }
   
   int PrimeraOcurrencia (int buscado){
      return PrimeraOcurrenciaEntre (0, total_utilizados - 1, buscado);
   }
};


class Imagen{
private:
	static const int MAX_FIL = 50;
   static const int MAX_COL = 40;
   int matriz_privada[MAX_FIL][MAX_COL];
   int util_fil;
   const int util_col;
public:	
   Imagen(int numero_de_columnas)
      :util_fil(0), util_col(numero_de_columnas)
   { 
   }
   Imagen(SecuenciaEnteros primera_fila)
      :util_fil(0), util_col(primera_fila.TotalUtilizados())
   { 
   }
   
   int CapacidadFilas(){
      return MAX_FIL;
   }
   
   int FilasUtilizadas(){
      return util_fil;
   }
   
   int ColUtilizadas(){
      return util_col;
   }
   
   	// Prec: fil y col en el rango adecuado
	double Pixel(int fil, int col){
		return matriz_privada[fil][col];
	}

   SecuenciaEnteros Fila(int indice_fila){
      SecuenciaEnteros fila;

      for (int col = 0; col < util_col; col++)
         fila.Aniade(matriz_privada[indice_fila][col]);

      return fila;
   }
   
   void Aniade(SecuenciaEnteros fila_nueva){
      int numero_columnas_nueva;

      if (util_fil < MAX_FIL){
         numero_columnas_nueva = fila_nueva.TotalUtilizados();

         if (numero_columnas_nueva == util_col){
            for (int col = 0; col < util_col ; col++)
               matriz_privada[util_fil][col] = fila_nueva.Elemento(col);

            util_fil++;
         }
      }
   }
   
   void Modifica(int fil, int col, int nuevo){
      matriz_privada[fil][col] = nuevo;
   }
   
	// Devuelve una imagen suavizada, es decir, sustiyendo cada pixel por
	// el promedio de sus vecinos correctos -> Mean Filter for Smoothing
	Imagen Suavizada(){
		/*
		Algoritmo:
			Copiamos todos los pixels originales en pixels_suavizada
			Recorremos todos los pixels de la copia, sustituyendo cada pixel incorrecto
			por la media de sus vecinos originales
		*/
		/*
		AVANZADO:
			Hemos copiado toda la imagen original en la suavizada y luego modificamos los
			valores incorrectos. En el caso de que la imagen fuese muy grande y tuvi�semos
			usualmente muchos valores incorrectos, se podr�a evitar tener que copiar todos los datos
			al principio, por lo que deber�amos ir almacenando los valores (corregidos o no) uno a uno.
		*/	
		
      // Creamos una copia de la imagen
      		
		Imagen suavizada(util_col);
	
		for (int i=0; i<util_fil; i++)
         suavizada.Aniade(Fila(i));
         
      // Tambi�n podr�amos haber hecho la copia de esta forma (se ver� en el segundo cuatrimestre)
		// Imagen suavizada(*this); 
         
      // Sustituimos cada pixel incorrecto por la media de sus vecinos correctos
        
		int ultima_fila = util_fil - 2;
		int ultima_colu = util_col - 2;
			
		for (int i=1; i <= ultima_fila; i++)
			for (int j=1; j <= ultima_colu; j++)
				if (! EsPixelCorrecto(i, j))
					suavizada.Modifica(i, j, MediaVecinosCorrectos(i,j));   // double --> int
	  
	  
	  return suavizada;
   }


	bool EsPixelCorrecto(int fil, int col){
		/*
			Como necesitamos comprobar que un pixel es correcto en varios sitios,
			definimos un m�todo que se encargue de dicha tarea.
			Lo hemos puesto como un m�todo porque el criterio de ser pixel correcto
			o no podr�a cambiar en el futuro. De esta forma, s�lo hay que modificar
			la l�nea correspondiente en este m�todo. 
		*/
		return matriz_privada[fil][col] >= 0;
	}


	// Calcula la media de los vecinos correctos.
	// Si todos son incorrectos, devuelve 0
	double MediaVecinosCorrectos(int fil, int col){
		/*
		Algoritmo:
			Recorremos los pixels de las tres filas y columnas que hay alrededor 
			del pixel que han pasado exceptuando el propio pixel.
			Si es un pixel correcto lo contabilizamos para la media aritm�tica.
			
		Para recorrer los vecinos, NO haremos algo de la forma siguiente:

			if (pixels[fil-1}[col-1] >= 0){
				suma_vecinos = suma_vecinos + matriz_privada[fil-1][col-1];
				total_vecinos++;
			}
			if (pixels[fil-1][col] >= 0){
				suma_vecinos = suma_vecinos + matriz_privada[fil-1][col];
				total_vecinos++;
			}
			if (pixels[fil-1][col+1] >= 0){
				suma_vecinos = suma_vecinos + matriz_privada[fil-1][col+1];
				total_vecinos++;
			}
			......
		
      Usaremos dos bucles anidados en la forma:
      
         for (int i = fil - 1; i <= fil + 1; i++)
			   for (int j = col - 1; j <= col + 1; j++)
			      if (i != j)	
			         ......
      */
      

		double suma_vecinos = 0.0;
		int total_vecinos = 0;

		for (int i = - 1; i <= 1; i++){
			for (int j =  - 1; j <= 1; j++){
				if (! (i == 0 && j == 0)){
					int indice_fila = i + fil;
					int indice_columna = j + col;
					
					if (EsPixelCorrecto(indice_fila, indice_columna)){
						suma_vecinos = suma_vecinos + matriz_privada[indice_fila][indice_columna];
						total_vecinos++;
					}
			   }
		   }
	   }
	
		if (total_vecinos == 0)
			return 0;
		else
			return suma_vecinos / total_vecinos;
		
		/*
   		Para excluir del c�mputo de la media el propio pixel, 
   		debemos realizar la comprobaci�n i != j
   		En el ejercicio se pide que se calcule la media de los vecinos
   		de aquellos pixeles que son incorrectos.
   		Por tanto, cuando i == j, el pixel es incorrecto; por lo tanto 
   		podr�amos suprimir la comprobaci�n if (i != j)
   		ya que posteriormente se realiza la comprobaci�n if (EsPixelCorrecto(i, j))
   
   		Sin embargo, es mejor mantener la comprobaci�n if (i != j)
   		ya que el m�todo MediaVecinosCorrectos podr�a ser llamado
   		en cualquier otra situaci�n, como por ejemplo, el c�mputo
   		de la media de los vecinos de un pixel que S� sea correcto.
		*/
	}


	// Aqu� ir�an todos los m�todos u operaciones que quisi�ramos hacer con una imagen.
};



int main(){
	SecuenciaEnteros fila;
	int filas_a_leer, columnas_a_leer, pixel;
	
	cin >> filas_a_leer;
   cin >> columnas_a_leer;
   
   Imagen imagen(columnas_a_leer);
	
	for (int i=0; i<filas_a_leer; i++){
	   fila.EliminaTodos();
	   
	   for(int j=0; j<columnas_a_leer; j++){
	      cin >> pixel;
         fila.Aniade(pixel);
      }  
      
      imagen.Aniade(fila);
   }  
   
   Imagen imagen_suavizada(imagen.Suavizada());
	
	////////////////////////////////////////////////////////
	
	cout << "\nImagen original:\n";
		
	for (int i=0; i<filas_a_leer; i++){
	   for(int j=0; j<columnas_a_leer; j++)
         cout << imagen.Pixel(i,j) << " ";
      cout << "\n";
   }
	
	cout << "\n\nImagen suavizada:\n";
	
	for (int i=0; i<filas_a_leer; i++){
	   for(int j=0; j<columnas_a_leer; j++)
         cout << imagen_suavizada.Pixel(i,j) << " ";
      cout << "\n";
   }

	cout << "\n\n";
	system("pause");
	
	// 5 6     -1 -1 -1 1  9 1     -2 -1 -1 8 -1 1     -1 -1  -1 1  3 1    -1 -1 -1 4 4 4      1 5 5 5 5 1
}
