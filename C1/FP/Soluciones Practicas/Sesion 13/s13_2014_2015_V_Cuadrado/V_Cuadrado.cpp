////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

// Cuadrado, Punto, Segmento y Circunferencia

/*
Recuperad las implementaciones de las clases Punto2D, SegmentoDirigido y
Circunferencia vistas en clase de teor�a. Definid tambi�n la clase Cuadrado.
Un cuadrado vendr� determinado por el punto correspondiente a la esquina inferior izquierda
y por la longitud de cualquiera de sus lados (estos ser�n sus datos miembros).
A�adid m�todos para observar los valores de los datos miembros y para calcular el
�rea y el per�metro del cuadrado.

Definid sobre la clase Cuadrado los siguientes m�todos:

Cuatro m�todos para devolver cada uno de los lados del cuadrado como objetos
de la clase SegmentoDirigido.

 Obtener el punto central interior al cuadrado:
   Punto2D Centro()
Para calcular las coordenadas basta sumar la mitad de la longitud del cuadrado
a las coordenadas de la esquina inferior izquierda.

 Obtener la circunferencia inscrita al cuadrado (la que est� por dentro):
   Circunferencia CircunferenciaInscrita()
Esta circunferencia es la que tiene como centro el centro del cuadrado y como
radio la mitad de la longitud del cuadrado.

 Obtener la circunferencia circunscrita al cuadrado (la que est� por fuera):
   Circunferencia CircunferenciaCircunscrita()
Esta circunferencia es la que tiene como centro el centro del cuadrado y como
radio, la longitud del segmento que une el centro con la esquina inferior izquierda.
Obtened la longitud creando el objeto de la clase SegmentoDirigido y a
continuaci�n llamad al m�todo Longitud.

Cread un programa principal que llame a dichos m�todos.
Finalidad: Trabajar con el constructor de copia y con m�todos que devuelven objetos.
Dificultad Baja.
*/

#include <iostream>
#include <cmath>
using namespace std;

const double PI = 3.1415927;

bool SonIguales(double uno, double otro){
    return abs(uno-otro) <= 0.000001;
}

class Punto2D{
private:
	double abscisa;
	double ordenada;
public:
	Punto2D(double abscisaPunto, double ordenadaPunto)
	{	
		SetCoordenadas(abscisaPunto, ordenadaPunto);
	}
	double Abscisa(){
		return abscisa;
	}
	double Ordenada(){
		return ordenada;
	}
	void SetCoordenadas(double abscisaPunto, double ordenadaPunto){
		abscisa = abscisaPunto;
		ordenada = ordenadaPunto;
	}
	bool EsIgual_a (Punto2D otro_punto){
		return (SonIguales(abscisa, otro_punto.abscisa) && SonIguales(ordenada, otro_punto.ordenada));
	}
};

class SegmentoDirigido{
private:
   double x_1, y_1, x_2, y_2;
   
   bool SonCorrectos(double origen_abscisa, double origen_ordenada,
                     double final_abscisa,  double final_ordenada){
 
      return !(origen_abscisa == final_abscisa && origen_ordenada == final_ordenada); 
   }
   
   void AsignaCoordenadas(double origen_abscisa, double origen_ordenada,
                          double final_abscisa,  double final_ordenada){
      x_1 = origen_abscisa;
      y_1 = origen_ordenada;
      x_2 = final_abscisa;
      y_2 = final_ordenada;                           
   }
   
public:
   SegmentoDirigido(double origen_abscisa, double origen_ordenada,
                    double final_abscisa, double final_ordenada){
      
      if (SonCorrectos(origen_abscisa, origen_ordenada,
                       final_abscisa, final_ordenada)){
         AsignaCoordenadas(origen_abscisa, origen_ordenada,
                           final_abscisa, final_ordenada);
      }
      else{
         x_1 = y_1 = x_2 = y_2 = NAN;
      }      
   }
   
   void SetCoordenadas(double origen_abscisa, double origen_ordenada,
                       double final_abscisa,  double final_ordenada){
      if (SonCorrectos(origen_abscisa, origen_ordenada,
                       final_abscisa, final_ordenada)){
         AsignaCoordenadas(origen_abscisa, origen_ordenada,
                           final_abscisa, final_ordenada);
      }
   }
   double OrigenAbscisa(){
      return x_1;
   }
   double OrigenOrdenada(){
      return y_1;
   }
   double FinalAbscisa(){
      return x_2;
   }
   double FinalOrdenada(){
      return y_2;
   }
   double Longitud(){
      return sqrt((x_2 - x_1)*(x_2 - x_1) +
                  (y_2 - y_1)*(y_2 - y_1));
   }
   void TrasladaHorizontal(double unidades){
      x_1 = x_1 + unidades;
      x_2 = x_2 + unidades;
   }
   void TrasladaVertical(double unidades){
      y_1 = y_1 + unidades;
      y_2 = y_2 + unidades;
   }
};

class Circunferencia{
private:
   double centro_x;
   double centro_y;
   double radio;
      
   bool EsCorrectoRadio(double longitud_radio){
      return longitud_radio > 0; 
   }
public:
	Circunferencia(double abscisa_centro, double ordenada_centro, double longitud_radio){
      centro_x = abscisa_centro;
      centro_y = ordenada_centro;
			
      if (EsCorrectoRadio(longitud_radio)) 
         radio = longitud_radio;
		else{
			radio = centro_x = centro_y = NAN;
		}
   }

   double AbscisaCentro(){
      return centro_x;
   }
   double OrdenadaCentro(){
      return centro_y;
   }
   double Radio(){
      return radio;
   }
   double Longitud(){
      return 2*PI*radio;
   }
   double Area(){
      return PI*radio*radio;
   }
   void Traslada(double en_horizontal, double en_vertical){
      centro_x = centro_x + en_horizontal;
      centro_y = centro_y + en_vertical;
   }
};

class Cuadrado{
private:
	double esquina_abscisa;
	double esquina_ordenada;
	double longitud;
	
   bool EsCorrectaLongitud(double longitud){
      return longitud > 0; 
   }
public:
	Cuadrado(double esquina_abscisa_cuadrado, 
				double esquina_ordenada_cuadrado,
				double longitud_cuadrado){

		esquina_abscisa = esquina_abscisa_cuadrado;
		esquina_ordenada = esquina_ordenada_cuadrado;
		
		if (EsCorrectaLongitud(longitud_cuadrado))
			longitud = longitud_cuadrado;
		else
			longitud = esquina_abscisa = esquina_ordenada = NAN;	
	}

	double AbscisaEsquina(){
		return esquina_abscisa;
	}

	double OrdenadaEsquina(){
		return esquina_ordenada;
	}

	double Longitud(){
		return longitud;
	}

	// Area es un valor que se obtiene a partir de la longitud.
	// Por tanto, Area no ser� un dato miembro.
	double Area(){
		return longitud * longitud;
	}

	double Perimetro(){
		return 4 * longitud;
	}

   SegmentoDirigido Lado1(){
      SegmentoDirigido lado(esquina_abscisa, esquina_ordenada, 
                            esquina_abscisa + longitud, esquina_ordenada);
      return lado;
   }
   
   SegmentoDirigido Lado2(){
      SegmentoDirigido lado(esquina_abscisa + longitud, esquina_ordenada, 
                            esquina_abscisa + longitud, esquina_ordenada + longitud);
      return lado;
   }
   
   SegmentoDirigido Lado3(){
      SegmentoDirigido lado(esquina_abscisa + longitud, esquina_ordenada + longitud, 
                            esquina_abscisa, esquina_ordenada + longitud);
      return lado;
   }
   
   SegmentoDirigido Lado4(){
      SegmentoDirigido lado(esquina_abscisa, esquina_ordenada + longitud, 
                            esquina_abscisa, esquina_ordenada);
      return lado;
   }
   
	Punto2D Centro(){
		double mitad_longitud = longitud / 2.0;

		Punto2D centro(esquina_abscisa + mitad_longitud, 
					      esquina_ordenada + mitad_longitud);

		return centro;
	}
	
	Circunferencia CircunferenciaInscrita(){
		Punto2D centro_circunf(Centro());  // Constructor de copia.
		double long_radio_circunf;

		long_radio_circunf = longitud / 2.0;

		Circunferencia circunf_inscrita(
				centro_circunf.Abscisa(),
				centro_circunf.Ordenada(), 
				long_radio_circunf);

		return circunf_inscrita;
	}
	
	Circunferencia CircunferenciaCircunscrita(){
		Punto2D centro_circunf(Centro());  // Constructor de copia.
		double long_radio_circunf;

		SegmentoDirigido radio_circunf(
				centro_circunf.Abscisa(),
				centro_circunf.Ordenada(), 
				esquina_abscisa,
				esquina_ordenada);		

		long_radio_circunf = radio_circunf.Longitud();

		Circunferencia circunf_circunscrita(
				centro_circunf.Abscisa(),
				centro_circunf.Ordenada(), 
				long_radio_circunf);

		return circunf_circunscrita;
	}

	bool MayorArea_que(Cuadrado otro_cuadrado){
		return Area() > otro_cuadrado.Area();

		// O bien, por eficiencia:
		// return longitud > otro_cuadrado.Longitud();
	}

	bool Contiene(Cuadrado otro){
		double x_otro = otro.AbscisaEsquina();
		double y_otro =otro.OrdenadaEsquina();
		double long_otro = otro.Longitud();

		return  (esquina_abscisa <= x_otro && 
					x_otro + long_otro <= esquina_abscisa + longitud && 
			      esquina_ordenada <= y_otro && 
					y_otro + long_otro <= esquina_ordenada + longitud );
	}
};


int main(){
	double esquina_x, esquina_y, long_cuadrado;

	cout << "Introduzca la abscisa de la esquina inferior izquierda ";
	cin >> esquina_x;
	cout << "\nIntroduzca la ordenada de la esquina inferior izquierda ";
	cin >> esquina_y;
	cout << "\nIntroduzca la longitud de la parcela ";
	cin >> long_cuadrado;

	// Creamos el objeto parcela 

	Cuadrado parcela(esquina_x, esquina_y, long_cuadrado);
	Circunferencia circunf_interior_parcela (parcela.CircunferenciaInscrita());
	Circunferencia circunf_exterior_parcela (parcela.CircunferenciaCircunscrita());
	
	cout << circunf_interior_parcela.Longitud() << "\n";
	cout << circunf_exterior_parcela.Longitud() << "\n";

	Cuadrado parcela2(esquina_x + 1, esquina_y + 1, long_cuadrado - 2);
	cout << "\n�rea parcela 1: " << parcela.Area();
	cout << "\n�rea parcela 2: " << parcela2.Area();

	if (parcela.Contiene(parcela2))
		cout << "\nParcela 1 contiene a parcela 2";
	else
		cout << "\nParcela 1 no contiene a parcela 2";

   cout << "\n\n";
	system("pause");
}
