////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
 (Examen Septiembre 2014) Sobre la clase SecuenciaCaracteres implemente el
algoritmo Counting Sort para ordenar sus valores. El m�todo no modificar� las com-
ponentes del vector privado sino que debe construir una secuencia nueva y devolverla.
El algoritmo funciona de la siguiente forma:

� Calculad los caracteres m�nimo y m�ximo del vector. Por ejemplo, si el vector contiene
   c b b a b c c a g c b g c
el m�nimo es 'a' y el m�ximo 'g'.

� Construid un vector auxiliar de frecuencias con los conteos de todos los carac-
teres que hay entre el m�nimo y el m�ximo. Con el ejemplo anterior, el vector de
conteos ser�
   2 4 5 0 0 0 2
que corresponden a las frecuencias de las letras que hay entre 'a' y 'g'.

� Recorrer el vector de frecuencias almacenando cada car�cter tantas veces como
indique su frecuencia (2 veces el 'a', cuatro veces el 'b', etc)
   a a b b b b c c c c c g g
Haced lo mismo pero parametrizando el m�todo CountingSort para que ordene
s�lo los valores de la secuencia que hay entre un car�cter izquierda y otro car�cter
derecha. Por ejemplo, si izquierda = 'b' y derecha = 'g' el resultado ser�a:
   b b b b c c c c c g g
*/

#include <iostream>
#include <cmath>
using namespace std;

class  SecuenciaCaracteres{
private:
	static  const  int  TAMANIO  =  50;
	char  vector_privado[TAMANIO];
	int  total_utilizados;
public:
	SecuenciaCaracteres()
		:total_utilizados(0)        
	{    
	}

	int  TotalUtilizados(){
		return  total_utilizados;
	}

	void  Aniade(char  nuevo){
		if (total_utilizados  <  TAMANIO){
			vector_privado[total_utilizados]  =  nuevo;
			total_utilizados++;
		}
	}

	void AniadeCadena(string nuevo){
		int tope = nuevo.size();

		for (int i = 0; i < tope; i++)
			Aniade(nuevo[i]);
	}

	char Elemento(int  indice){
		return  vector_privado[indice];
	}

	void Modifica(int indice_componente, char nuevo){
		if (indice_componente >= 0  &&  indice_componente < total_utilizados)
			vector_privado[indice_componente] = nuevo;
	}	


	int PrimeraOcurrenciaEntre(int pos_izda, int pos_dcha, char buscado){
		int i = pos_izda; 
		bool encontrado = false;

		while (i <= pos_dcha  &&  !encontrado)
			if (vector_privado[i] == buscado)
				encontrado = true;
			else
				i++;

		if (encontrado)
			return i;
		else
			return -1;
	}

	int PrimeraOcurrencia (char buscado){
		return PrimeraOcurrenciaEntre(0, total_utilizados - 1, buscado);
	}


   // Ordenaci�n por el m�todo CountingSort
   // Prec: M�nimo del vector <= min <= max <= M�ximo del vector
   SecuenciaCaracteres CountingSortEntre(char min, char max){
	   /*                                                             
      Ejemplo:												     
         p c b b a b c c a g c b g c f 
  	      min = 'b', max = 'f' (los caracteres 'a', 'g' y 'p' no entran)
  		
  		Usamos un vector local conteos                                                        
  		                                                         
         �ndices de conteos -> 0 1 2 3 4
         corresponden a:    -> b c d e f
         valores en conteos -> 4 5 0 0 1
           		                                                         
  		Calcular en un vector auxiliar conteos, el n�mero                          
  			de veces que cada valor entre min y max se repite     
  		La componente i de conteos contendr� el n�mero de
  			repeticiones del valor min + i        
         -> conteos[2] = repeticiones de 'b'+2 = repeticiones de 'd' = 0                   
  		Construir el vector resultado recorriendo conteos y a�adiendo
  			min + i  tantas veces como diga conteos[i]                 

  		Nota.
  			Para calcular el n�mero de veces que cada valor entre min y max se repite
  			podemos plantear dos algoritmos:

			Recorrer cada car�cter entre min y max (for (caracter = min; caracter <= max; caracter++))
				Recorrer el vector original para contar todas las apariciones
				del car�cter en dicho vector
				Asignar a conteos[caracter-min] dicho conteo

			Este algoritmo es ineficiente ya que implica recorrer el vector
			original muchas veces.
			Mejor usamos el siguiente algoritmo:

			Inicializar a cero todos los valores del vector conteos
			Recorrer cada car�cter del vector original
				Incrementar en 1 el valor de conteos[caracter - min]
		
		En resumen, nos queda:
		
      Algoritmo (Counting Sort entre min y max):
      
         Calcular en un vector auxiliar conteos, el n�mero                          
  			de veces que cada valor entre min y max se repite     			
  			Para ello:  			   
  			 	inicializar a cero todos los valores del vector conteos
			   Recorrer cada car�cter del vector original
				   Incrementar en 1 el valor de conteos[caracter - min]
			
         Construir el vector resultado recorriendo conteos y a�adiendo
  			min + i  tantas veces como diga conteos[i]    
  	   */

		int conteos[TAMANIO] = {0};
		SecuenciaCaracteres ordenado;
		int posicion_en_conteos;

		for (int i = 0; i < total_utilizados; i++){
			posicion_en_conteos = vector_privado[i] - min;
			conteos[posicion_en_conteos] ++;
		}
		
		int entre_min_y_max = max - min + 1;

		for (int i = 0; i<entre_min_y_max; i++){
			int repeticiones = conteos[i];

			for (int j = 0; j<repeticiones; j++)
				ordenado.Aniade(min + i);
		}

		return ordenado;
	}

	int PosMaximo(){
		int pos_maximo = 0;

		for (int i=0; i<total_utilizados; i++)
			if (vector_privado[i] > vector_privado[pos_maximo])
				pos_maximo = i;

		return pos_maximo;
	}

	int PosMinimo(){
		int pos_minimo = 0;

		for (int i=0; i<total_utilizados; i++)
			if (vector_privado[i] < vector_privado[pos_minimo])
				pos_minimo = i;

		return pos_minimo;
	}

	// CountingSort sobre todo el vector.
	// Debemos calcular el m�nimo y el m�ximo del vector

	SecuenciaCaracteres CountingSort(){
      char maximo = vector_privado[PosMaximo()];
		char minimo = vector_privado[PosMinimo()];
		SecuenciaCaracteres ordenado = CountingSortEntre(minimo, maximo);

		return ordenado;
	}
};



int main(){
	SecuenciaCaracteres cadena, cadena_ordenada;
	
   cadena.AniadeCadena("pcbbabccagcbgcf"); 
	cadena_ordenada = cadena.CountingSort();

	int util_ordenado = cadena_ordenada.TotalUtilizados();

	for (int i=0; i<util_ordenado; i++)
		cout << cadena_ordenada.Elemento(i);
	
	cout << "\nTama�o cadena original: " << cadena.TotalUtilizados();
	cout << "\nTama�o cadena construida: " << cadena_ordenada.TotalUtilizados();
	
}
