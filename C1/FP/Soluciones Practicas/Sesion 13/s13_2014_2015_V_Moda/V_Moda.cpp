////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
Se quiere calcular la moda de una secuencia de caracteres, es decir, el car�cter que
m�s veces se repite. Por ejemplo, si la secuencia fuese
{'l','o','s',' ','d','o','s',' ','c','o','f','r','e','s'}
los caracteres que m�s se repiten son 'o' y 's' con un total de 3 apariciones. La
moda ser�a cualquiera de ellos, por ejemplo, el primero encontrado 'o'. Sobre la
clase SecuenciaCaracteres, se pide construir el m�todo Moda que devuelva un
struct del tipo:
   struct FrecuenciaCaracter{
      char caracter;
      int  frecuencia;
   };

en el que el campo caracter contendr� el car�cter en cuesti�n ('o') y en el campo
frecuencia el conteo de la moda (3).
*/

#include <iostream>
#include <vector>
using namespace std;

struct FrecuenciaCaracter{
   char caracter;
   int  frecuencia;
};
   
class SecuenciaCaracteres{
private:
	static  const  int  TAMANIO  =  50;
	char  vector_privado[TAMANIO];
	int  total_utilizados;
public:
	SecuenciaCaracteres()
		:total_utilizados(0)        
	{    
	}

	int  TotalUtilizados(){
		return  total_utilizados;
	}

	void  Aniade(char  nuevo){
		if (total_utilizados  <  TAMANIO){
			vector_privado[total_utilizados]  =  nuevo;
			total_utilizados++;
		}
	}

	void AniadeCadena(string nuevo){
		int tope = nuevo.size();

		for (int i = 0; i < tope; i++)
			Aniade(nuevo[i]);
	}

	char Elemento(int  indice){
		return  vector_privado[indice];
	}

	void Modifica(int indice_componente, char nuevo){
		if (indice_componente >= 0  &&  indice_componente < total_utilizados)
			vector_privado[indice_componente] = nuevo;
	}	


	int PrimeraOcurrenciaEntre(int pos_izda, int pos_dcha, char buscado){
		int i = pos_izda; 
		bool encontrado = false;

		while (i <= pos_dcha  &&  !encontrado)
			if (vector_privado[i] == buscado)
				encontrado = true;
			else
				i++;

		if (encontrado)
			return i;
		else
			return -1;
	}

	int PrimeraOcurrencia (char buscado){
		return PrimeraOcurrenciaEntre(0, total_utilizados - 1, buscado);
	}

	FrecuenciaCaracter Moda(){
	   /*
	   Primera idea:
	     Almacenamos en un vector local los conteos de cada uno de los caracteres
	     de la tabla ASCII y buscamos el m�ximo.
	     Esta soluci�n no es extensible si trabaj�semos con enteros en vez de caracteres
        ya que el vector requerir�a mucho espacio.
   
      Mejor si almacenamos en un vector local cada uno de los caracteres 
      de la secuencia original que ya se haya procesado (contado el n�mero de ocurrencias)
      Por cada uno de los caracteres de la secuencia original, vemos
      si se encuentra en el vector local de procesados y en caso negativo,
      lo procesamos.
      Como tendremos que hacer una b�squeda en el vector local,
      usamos un objeto SecuenciaCaracteres que ya contiene
      el m�todo PrimeraOcurrencia
	     
	   
      Algoritmo (Moda de una secuencia de caracteres):
          
         Almacenaremos en un vector local los caracteres ya procesados
                         
         Recorrer todos los caracteres de la secuencia original
            Buscar el car�cter en el vector local procesados
            Si no est� (no ha sido procesado a�n), 
               lo a�adimos y actualizamos, en su caso, la moda
	   */
	   
		char caracter_actual;
		SecuenciaCaracteres procesados;
		int  conteo_parcial;
      FrecuenciaCaracter moda;

		procesados.Aniade(vector_privado[0]);
		moda.caracter = vector_privado[0];
		moda.frecuencia = 1;
		
		for (int actual=1 ; actual < total_utilizados ; actual++){
		   caracter_actual = vector_privado[actual];
		   
			if (-1 == procesados.PrimeraOcurrencia(caracter_actual)){
				procesados.Aniade(caracter_actual);				
				conteo_parcial = 0;

				for (int i=actual; i < total_utilizados ; i++)
					if (caracter_actual == vector_privado[i])
						conteo_parcial++;

				if (conteo_parcial > moda.frecuencia) {
					moda.frecuencia = conteo_parcial;
					moda.caracter   = caracter_actual;
				}
			}
		}

		return moda;
	}
};

int main(){
   SecuenciaCaracteres cadena;
   FrecuenciaCaracter moda_cadena;
	int tope;

   cadena.AniadeCadena("Hollaaaooolaaa");
	tope = cadena.TotalUtilizados();

   for (int i=0; i<tope; i++)
      cout  << cadena.Elemento(i) << " ";
      
   moda_cadena = cadena.Moda();
   
   cout << "\n\n" << "Moda = " << moda_cadena.caracter 
        << "\nFrecuencia = " << moda_cadena.frecuencia;            

   cout << "\n\n";
   system ("pause");
}

