////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
Sge�n un etsduio de una uivenrsdiad ignlsea, no ipmotra el odren en el que las ltears
etsan ersciats, la �icna csoa ipormtnate es que la pmrirea y la �tlima ltera ets�n ecs-
ritas en la psioci�n cocrrtea. El rsteo peuden estar ttaolmntee mal y a�n pord�s lerelo
sin pobrleams. Etso es pquore no lemeos cada ltera por s� msima snio la paalbra cmoo
un tdoo.

Se pide crear la clase Palabra que permita almacenar un conjunto de caracteres
que representar�n una palabra. Definid un m�todo EsIgual al que se le pase como
par�metro otra palabra y determine si son iguales atendiendo al siguiente criterio: La
primera letra de ambas palabras es igual, la �ltima letra de ambas palabras tambi�n
es igual y el resto de las letras son las mismas pero no est�n necesariamente en las
mismas posiciones.
*/

#include <iostream>
#include <cctype>
using namespace std;


class SecuenciaCaracteres{
private:
	static  const  int  TAMANIO  =  50;
	char  vector_privado[TAMANIO];
	int  total_utilizados;
public:
	SecuenciaCaracteres()
		:total_utilizados(0)        
	{    
	}

	int  TotalUtilizados(){
		return  total_utilizados;
	}

	void  Aniade(char  nuevo){
		if (total_utilizados  <  TAMANIO){
			vector_privado[total_utilizados]  =  nuevo;
			total_utilizados++;
		}
	}

	void AniadeCadena(string nuevo){
		int tope = nuevo.size();

		for (int i = 0; i < tope; i++)
			Aniade(nuevo[i]);
	}

	char Elemento(int  indice){
		return  vector_privado[indice];
	}

	void Modifica(int indice_componente, char nuevo){
		if (indice_componente >= 0  &&  indice_componente < total_utilizados)
			vector_privado[indice_componente] = nuevo;
	}	


	int PrimeraOcurrenciaEntre(int pos_izda, int pos_dcha, char buscado){
		int i = pos_izda; 
		bool encontrado = false;

		while (i <= pos_dcha  &&  !encontrado)
			if (vector_privado[i] == buscado)
				encontrado = true;
			else
				i++;

		if (encontrado)
			return i;
		else
			return -1;
	}

	int PrimeraOcurrencia (char buscado){
		return PrimeraOcurrenciaEntre(0, total_utilizados - 1, buscado);
	}

   int NumeroOcurrencias(char a_buscar, int izda, int dcha){
      int numero_ocurrencias = 0;
      
      for (int i=izda; i <= dcha; i++)
         if (vector_privado[i] == a_buscar)
            numero_ocurrencias++;
      
      return numero_ocurrencias;
   }
   
   bool EsSimilar(SecuenciaCaracteres otra_palabra){
      /*

      Llamaremos palabra principal al vector dato miembro privado
         
      Comparar primera con primera y �ltima con �ltima         
      Si son iguales:
         Recorrer las letras de la palabra principal entre primera+1 y �ltima-1
            Contar el n�mero de apariciones de la letra en cada palabra
            Si el n�mero de apariciones es distinto, no son similares
      
		Mejora: Usamos un vector local de procesados para
			     no volver a contar aquellas letras que ya se hayan procesado      
			     
              	
      Algoritmo (Palabras similares) 
      
         Llamaremos palabra principal al vector dato miembro privado
         Usamos un vector local de letras ya procesadas
         
         Comparar primera con primera y �ltima con �ltima         
         Si son iguales:
            Recorrer las letras de la palabra principal entre primera+1 y �ltima-1
               Si la letra no ha sido procesada:
                  Contar el n�mero de apariciones de la letra en cada palabra
                  Si el n�mero de apariciones es distinto, no son similares			     
      */
      bool es_similar;
      SecuenciaCaracteres procesados;
      
      if (total_utilizados != otra_palabra.TotalUtilizados())
         es_similar = false;
      else{
         int ultimo = total_utilizados - 1;
         es_similar = vector_privado[0] == otra_palabra.Elemento(0) 
			 				 && 
							 vector_privado[ultimo] == otra_palabra.Elemento(ultimo);
         
         for (int i=1; i<ultimo && es_similar; i++){
            char actual = vector_privado[i];
            
            if (procesados.PrimeraOcurrencia(actual) == -1){
               procesados.Aniade(actual);
               es_similar = NumeroOcurrencias(actual, 1, ultimo-1) 
					       		 == 
								    otra_palabra.NumeroOcurrencias(actual, 1, ultimo-1);					
			   }
         }
      }
            
      return es_similar;
   }
};

int main(){
   SecuenciaCaracteres palabra;
   SecuenciaCaracteres otra_palabra;
   bool es_similar;

	
   palabra.AniadeCadena("abcbefg");
   otra_palabra.AniadeCadena("abfbceg");

   /*
   int tope;
   tope = palabra.TotalUtilizados();

   for (int i = 0; i < tope; i++)
      cout  << palabra.Elemento(i) << " ";
   */
      
   es_similar = palabra.EsSimilar(otra_palabra);

   if (es_similar)
      cout << "\nEs similar";
   else
      cout << "\nNo es similar";
      
   cout << "\n\n";
   system ("pause");
}

