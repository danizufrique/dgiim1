////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
(Examen Septiembre 2014) Existe un m�todo para la clase string de C++, deno-
minado replace, que cambia n caracteres de una cadena cad1, empezando en
una determinada posici�n pos, por los caracteres presentes en una segunda cade-
na cad2. La llamada al m�todo es cad1.replace(pos, n, cad2). Ejemplos del
funcionamiento de replace son:
   string cad1="Fundamental Programaci�n";
   
   cad1.replace(9,2,"os de la");
   
      // "al" -> "os de la"
      // Ahora cad1 tiene "Fundamentos de la Programaci�n"
      
   cad1.replace(12,5,"en");
   
      // "de la" -> "en"
      // Ahora cad1 tiene "Fundamentos en Programaci�n"
      
Puede observar que, dependiendo de la cadena a insertar y de las posiciones especi-
ficadas, la secuencia final puede ser m�s grande o m�s peque�a que la original.
Sobre la clase SecuenciaCaracteres construid un m�todo llamado Reemplaza
con la misma funcionalidad que replace (el m�todo no devuelve ning�n objeto sino
que modifica los datos de �ste).
Restricciones para este ejercicio: No se puede utilizar la clase string en ninguna
parte del programa, debe hacerse lo m�s eficiente posible y no puede utilizarse un
vector o secuencia auxiliar en el que se vaya almacenando el resultado, es decir, las
modificaciones deben hacerse directamente sobre los datos originales.
*/


#include <iostream>
#include <string>

using namespace std;
  
class SecuenciaCaracteres{
private:
	static  const  int  TAMANIO  =  50;
	char  vector_privado[TAMANIO];
	int  total_utilizados;
public:
	SecuenciaCaracteres()
		:total_utilizados(0)        
	{    
	}

	int  TotalUtilizados(){
		return  total_utilizados;
	}

	void  Aniade(char  nuevo){
		if (total_utilizados  <  TAMANIO){
			vector_privado[total_utilizados]  =  nuevo;
			total_utilizados++;
		}
	}

	void AniadeCadena(string nuevo){
		int tope = nuevo.size();

		for (int i = 0; i < tope; i++)
			Aniade(nuevo[i]);
	}

	char Elemento(int  indice){
		return  vector_privado[indice];
	}

	void Modifica(int indice_componente, char nuevo){
		if (indice_componente >= 0  &&  indice_componente < total_utilizados)
			vector_privado[indice_componente] = nuevo;
	}	


	int PrimeraOcurrenciaEntre(int pos_izda, int pos_dcha, char buscado){
		int i = pos_izda; 
		bool encontrado = false;

		while (i <= pos_dcha  &&  !encontrado)
			if (vector_privado[i] == buscado)
				encontrado = true;
			else
				i++;

		if (encontrado)
			return i;
		else
			return -1;
	}

	int PrimeraOcurrencia (char buscado){
		return PrimeraOcurrenciaEntre(0, total_utilizados - 1, buscado);
	}


	void Reemplaza(int inicio, int n_a_quitar, SecuenciaCaracteres a_insertar){
		/*
		Llamemos diferencia_componentes
		a la diferencia entre el tama�o del vector a insertar y las posiciones a quitar

		Si el vector a insertar es m�s peque�o que el n�mero de componentes a quitar
			es decir, si diferencia_componentes < 0 
			hay que acortar el vector => 
			Desplazar  hacia la izquierda todas las componentes que hay entre
			(pos_inicio + numero_posiciones_a_quitar) y el final	
		else
			hay que agrandar el vector =>
			Desplazar hacia la derecha todas las componentes que hay entre
			(pos_inicio + numero_posiciones_a_quitar) y el final	

		Finalmente, volcaremos el vector a insertar en las posiciones indicadas.

		Nos quedar�a:
		
		Algoritmo (Reemplazar cadena):
		
			diferencia_componentes = diferencia entre las posiciones a quitar y el tama�o del vector a insertar
			
			Si (diferencia_componentes > 0) => Hay que acortar el vector
				Trasladamos a la izda las componentes que hay 
				por encima de pos_inicio + numero_posiciones_a_quitar
			else
				Trasladamos a la derecha las componentes que hay 
				por encima de pos_inicio + numero_posiciones_a_quitar	
			
			Volcamos el vector a insertar en el "hueco" que ha quedado
			
      -------------------------------------------------------------------------------------			
		�C�mo realizamos los desplazamientos?
		
		Opci�n Ineficiente:
			Para acortar el vector: 
			Repetir para todas y cada una de las componentes -i- que haya que eliminar
				Desplazar a la izquierda una posici�n todas las componentes que hay a la derecha de -i-

			Para agrandar el vector:
			Repetir para todas y cada una de las componentes -i- que haya que insertar nuevas
				Desplazar a la derecha una posici�n todas las componentes que hay a la derecha de -i-
		
		Esta opci�n es ineficiente tal y como se indic� en los ejercicios
		Eliminar May�sculas y Eliminar Repetidos de la Relaci�n de Problemas IV.
		En su lugar, debemos ir colocando directamente cada componente en la situaci�n que le corresponde.

	
         abcdefg  inicio=1  n_a_quitar=3  inserta=jklmn  Resultado=ajklmndefg 		        
         a[bcd]efg  (3)
         [jklmn]   (5)
         => 5-3 = 2 posiciones m�s (tendr� el vector)
         g -> La colocamos en la posici�n 6 + 2 
         f -> La colocamos en la posici�n 5 + 2
         e -> La colocamos en la posici�n 4 + 2
         Volcamos [jklmn] en el vector a partir de inicio.
         
         
         abcdefg  inicio=1  n_a_quitar=4  inserta=jk  Resultado=ajkfg 		        
         a[bcde]fg  (4)
         [jk]      (2)
         => 4-2 = 2 posiciones menos (tendr� el vector)
         f -> La colocamos en la posici�n 5 - 2 
         g -> La colocamos en la posici�n 6 - 2
         Volcamos [jk] en el vector a partir de inicio.
        
      Vemos que los dos recorridos son similares, pero:
         - Si hay que agrandar, debemos empezar los desplazamientos desde la �ltima hacia atr�s
         - Si hay que acortar, debemos empezar los desplazamientos desde la primera hacia delante
      Para ello, usamos una variable siguiente que valdr� +1 o -1 y
      dos variables de posici�n lectura y escritura que ir�n actualiz�ndose seg�n diga siguiente
	   */
	   		   
	   int diferencia_componentes, tamanio_a_insertar, lectura, escritura, siguiente;
	   	   
	   tamanio_a_insertar = a_insertar.TotalUtilizados();
	   
		if (tamanio_a_insertar + total_utilizados <= TAMANIO  && 
			n_a_quitar <= total_utilizados - inicio       &&
			inicio <= total_utilizados){
	   
	   	diferencia_componentes = tamanio_a_insertar - n_a_quitar;
	   	
			if ( diferencia_componentes < 0 ){   // Hay que acortar el vector
				siguiente = 1;
				lectura   = inicio + n_a_quitar;
				escritura = inicio + tamanio_a_insertar;
			}
			else{
				siguiente = -1;
				lectura   = total_utilizados - 1;
				escritura = total_utilizados - 1 + diferencia_componentes;
			}

			int numero_componentes_a_desplazar = total_utilizados - n_a_quitar - inicio;
         
			for (int i=0; i < numero_componentes_a_desplazar; i++){
				vector_privado[escritura] = vector_privado[lectura];
				lectura   = lectura   + siguiente;
				escritura = escritura + siguiente;
			}

			for (int i=0; i < tamanio_a_insertar; i++)
				vector_privado[inicio + i] = a_insertar.Elemento(i);	   
      } 
	   
      total_utilizados = total_utilizados + diferencia_componentes;	// Si la diferencia es positiva, habr� m�s componentes
	}
};

int main(){
	SecuenciaCaracteres palabra;
   SecuenciaCaracteres para_insertar;

   palabra.AniadeCadena("abcdefg");
   para_insertar.AniadeCadena("jklmn");
   palabra.Reemplaza(0,6,para_insertar);
	
	int tope = palabra.TotalUtilizados();

	for (int i=0; i<tope; i++)
		cout << palabra.Elemento(i);

	cout << "\n\n";
	system("pause");
}
