////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
En las transparencias se enfatiza que las tareas necesarias para realizar las operaciones
de E/S de los datos de un objeto, se realizar�n en clases espec�ficas que
implementen dichas responsabilidades.
Vamos a aplicarlo sobre la clase SecuenciaCaracteres. Para ello, vamos a crear
sendas clases:

 La clase ImpresorSecuenciaCaracteres servir� para imprimir los caracteres
de un objeto de la clase SecuenciaCaracteres. Si la secuencia contiene
los caracteres h o l a, por ejemplo, en pantalla saldr� lo siguiente:
{h,o,l,a}
En general, se dar� la posibilidad de delimitar los caracteres con otros s�mbolos
que no sean { } ,
La clase ImpresorSecuenciaCaracteres contendr� el siguiente m�todo:
void Imprime (SecuenciaCaracteres secuencia_a_imprimir)

 La clase LectorSecuenciaCaracteres para leer los datos de un objeto de
la clase SecuenciaCaracteres. La lectura de datos parar� cuando se llegue
a un terminador, que ser� un car�cter especial.
La clase LectorSecuenciaCaracteres contendr� al menos el siguiente m�todo:
SecuenciaCaracteres Lee()
que ser� el encargado de hacer la lectura de los datos y construir un objeto de la
clase SecuenciaCaracteres.

Cread un programa principal que vaya leyendo datos de un fichero. El fichero contendr�
al principio el n�mero de filas de caracteres que hay y a continuaci�n las filas.
Cada fila ser� una serie de caracteres con un punto al final (�ste es el terminador de
la fila).

3
Esto es una fila.
Esta es otra.
Tercera y �ltima fila.

Para realizar la lectura se usar� un �nico objeto de la clase
LectorSecuenciaCaracteres. Cada fila se almacenar� en un objeto de la
clase SecuenciaCaracteres. Cada vez que leamos una fila la a�adiremos a un
objeto de la clase Texto. Utilizad la implementaci�n incluida en las transparencias en
el apartado Tabla dentada usando un vector de objetos, bajo el nombre Texto_vs2.
Este objeto Texto habr� que definirlo en la funci�n main.
Una vez le�das todas las filas, crearemos un objeto de la clase
ImpresorSecuenciaCaracteres y lo usaremos para imprimir por pantalla
todas las filas.
*/


#include <iostream>
using namespace std;


class SecuenciaCaracteres{
private:
	static  const  int  TAMANIO  =  50;
	char  vector_privado[TAMANIO];
	int  total_utilizados;
public:
	SecuenciaCaracteres()
		:total_utilizados(0)        
	{    
	}

	int  TotalUtilizados(){
		return  total_utilizados;
	}

	void  Aniade(char  nuevo){
		if (total_utilizados  <  TAMANIO){
			vector_privado[total_utilizados]  =  nuevo;
			total_utilizados++;
		}
	}

	void AniadeCadena(string nuevo){
		int tope = nuevo.size();

		for (int i = 0; i < tope; i++)
			Aniade(nuevo[i]);
	}

	char Elemento(int  indice){
		return  vector_privado[indice];
	}

	void Modifica(int indice_componente, char nuevo){
		if (indice_componente >= 0  &&  indice_componente < total_utilizados)
			vector_privado[indice_componente] = nuevo;
	}	


	int PrimeraOcurrenciaEntre(int pos_izda, int pos_dcha, char buscado){
		int i = pos_izda; 
		bool encontrado = false;

		while (i <= pos_dcha  &&  !encontrado)
			if (vector_privado[i] == buscado)
				encontrado = true;
			else
				i++;

		if (encontrado)
			return i;
		else
			return -1;
	}

	int PrimeraOcurrencia (char buscado){
		return PrimeraOcurrenciaEntre(0, total_utilizados - 1, buscado);
	}

   int NumeroOcurrencias(char a_buscar, int izda, int dcha){
      int numero_ocurrencias = 0;
      
      for (int i=izda; i <= dcha; i++)
         if (vector_privado[i] == a_buscar)
            numero_ocurrencias++;
      
      return numero_ocurrencias;
   }
   
      // Si pequenia est� vac�a devuelve -1
   int PosContiene (SecuenciaCaracteres pequenia){
      int i, j, pos_contiene, tope, utilizados_pequenia;
      bool encontrado, iguales_parcial;
      
      utilizados_pequenia = pequenia.TotalUtilizados();
      pos_contiene = -1;
      encontrado = false;
      tope = total_utilizados - utilizados_pequenia;
      i = 0;
      
      while (i <= tope && !encontrado){
         if (vector_privado[i] == pequenia.Elemento(0)){
            pos_contiene = i;
            iguales_parcial = true;
            j = 1;
            
            while (!encontrado && iguales_parcial){
               if (j == utilizados_pequenia)
                  encontrado = true;
               else if (pequenia.Elemento(j) == vector_privado[i+j])
                  j++;
               else
                  iguales_parcial = false;
            }
         }

         if (!encontrado)
            i++;
      }

      if (encontrado)
         return pos_contiene;
      else
         return -1;
   }
};


class ImpresorSecuenciaCaracteres{
private:
	string inicio, final, intermedio;
public:
	ImpresorSecuenciaCaracteres(string cadena_inicio, string cadena_final, string cadena_intermedio)
		:inicio(cadena_inicio), final(cadena_final), intermedio(cadena_intermedio)
	{
	}
   void Imprime(SecuenciaCaracteres secuencia_a_imprimir){
		int tope = secuencia_a_imprimir.TotalUtilizados() - 1;

		cout << inicio;

      for (int i=0; i<tope; i++)
         cout << secuencia_a_imprimir.Elemento(i) << intermedio;

      cout << secuencia_a_imprimir.Elemento(tope) << final;
   }
};

class LectorSecuenciaCaracteres{
private:
	char terminador;
public:
	LectorSecuenciaCaracteres(char caracter_terminador)
		:terminador(caracter_terminador)
	{
	}
	SecuenciaCaracteres Lee(){
		SecuenciaCaracteres a_leer;
		char caracter;

		caracter = cin.get();

		while (caracter == '\n')
			caracter = cin.get();

		while (caracter != terminador){
			a_leer.Aniade(caracter);
			caracter = cin.get();
		}		

		return a_leer;
	}
};


class Texto{
private:
   static const int MAX_FIL = 50;
   SecuenciaCaracteres vector_privado[MAX_FIL];
   int util_fil;
public:
   // Prec: 0 < numero_de_columnas <= Capacidad de SecuenciaCaracteres
   Texto()
      :util_fil(0)
   {	
   }
   int CapacidadFilas(){
      return MAX_FIL;
   }
   int FilasUtilizadas(){
      return util_fil;
   }
   int ColUtilizadas(int indice_fila){
      return vector_privado[indice_fila].TotalUtilizados();
   }
   char Elemento(int fila, int columna){
      return vector_privado[fila].Elemento(columna);
   }
   SecuenciaCaracteres Fila(int indice_fila){
      return vector_privado[indice_fila];
   }
   void Aniade(SecuenciaCaracteres fila_nueva){
      if (util_fil < MAX_FIL){
         vector_privado[util_fil] = fila_nueva;
         util_fil++;
      }
   }
 
   void Inserta(SecuenciaCaracteres fila_nueva, int pos_fila_insercion){
      if (util_fil < MAX_FIL && 
         pos_fila_insercion >= 0 && 
         pos_fila_insercion <= util_fil){
         
         for (int i = util_fil ; i > pos_fila_insercion ; i--)
            vector_privado[i] = vector_privado[i-1];
         
         vector_privado[pos_fila_insercion] = fila_nueva;      
         util_fil++;
      }  
   }	
};


int main(){
   LectorSecuenciaCaracteres lector_secuencia_caracteres('.');
	ImpresorSecuenciaCaracteres impresor_secuencia_caracteres("{","}",",");
	SecuenciaCaracteres linea;	
	Texto texto;
	int numero_filas;

	cin >> numero_filas;	

	for (int i=0; i<numero_filas; i++){
		linea = lector_secuencia_caracteres.Lee();                 
		texto.Aniade(linea);
	}

	for (int i=0; i<numero_filas; i++){
		impresor_secuencia_caracteres.Imprime(texto.Fila(i));
		cout << "\n";
	}

   cout << "\n\n";
	system("pause");
	
   // 3Esto es una fila.Esta es otra.Tercera y �ltima fila.
}
