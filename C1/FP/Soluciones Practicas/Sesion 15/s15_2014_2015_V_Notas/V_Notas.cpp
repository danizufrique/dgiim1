////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
Se quiere almacenar el nombre de un alumno junto con
las notas que ha sacado en varias asignaturas. El n�mero de asignaturas puede variar
de un alumno a otro y las notas son de tipo de dato entero. Con estos datos, se quiere
construir un conjunto de alumnos

Ana de Gober, (9,7,8,9)
Sergio Garc�a, (3,4,2)
David Rodriguez, (5,4)

Defina la clase Alumnos que contendr� los siguientes datos miembro:

 Un vector de SecuenciaCaracteres con los nombres de los alumnos:
[ {Ana de Gober} , {Sergio Garc�a} , {David Rodriguez} ]

 Un vector de SecuenciaEnteros con las notas de cada alumno:
[ {9,7,8,9} , {3,4,2} , {5,4} ]

A�ada m�todos para:

 Obtener el n�mero total de alumnos
 Obtener el nombre de un alumno. Al m�todo se le pasar� un �ndice de componente
y devolver� un objeto SecuenciaCaracteres.
 Obtener todas las notas de un alumno. Al m�todo se le pasar� un �ndice de
componente y devolver� un objeto SecuenciaEnteros.
 Obtener los alumnos cuyo nombre contenga una determinada cadena de caracteres.
Al m�todo se le pasar� un objeto SecuenciaCaracteres y devolver� un
objeto SecuenciaEnteros con las posiciones correspondientes.
 Ordenar los datos de menor a mayor seg�n la media aritm�tica de sus calificaciones.
Con los datos del anterior ejemplo, la nota media de Ana de Gober ser�a
8.25, la de Segio Garc�a 3 y la de David Rodriguez 4.5 , por lo que los
datos ordenados quedar�an como sigue:
Sergio Garc�a, (3,4,2)
David Rodriguez, (5,4)
Ana de Gober, (9,7,8,9)

Cread un programa principal sencillo de prueba.
*/


#include <iostream>
#include <string>
using namespace std;



class SecuenciaEnteros{
private:
   static const int TAMANIO = 50;
   int vector_privado[TAMANIO];
   int total_utilizados;
public:
   SecuenciaEnteros()
      :total_utilizados(0) {
   }
   int TotalUtilizados(){
      return total_utilizados;
   }
   int Capacidad(){
      return TAMANIO;
   }
   void Aniade(int nuevo){
      if (total_utilizados < TAMANIO){
         vector_privado[total_utilizados] = nuevo;
         total_utilizados++;
      }
   }
   int Elemento(int indice){
      return vector_privado[indice];
   }
   void EliminaTodos(){
      total_utilizados = 0;
   }
   
   void Modifica(int indice_componente, int nuevo){
		if (indice_componente >= 0  &&  indice_componente < total_utilizados)
			vector_privado[indice_componente] = nuevo;
	}	
	
	int PrimeraOcurrenciaEntre (int pos_izda, int pos_dcha, int buscado){
      int i = pos_izda;
      bool encontrado = false;

      while (i <= pos_dcha  &&  !encontrado)
         if (vector_privado[i] == buscado)
            encontrado = true;
         else
            i++;

      if (encontrado)
         return i;
      else
         return -1;
   }
   
   int PrimeraOcurrencia (int buscado){
      return PrimeraOcurrenciaEntre (0, total_utilizados - 1, buscado);
   }
   
   double Media(){
      double media = 0;
      
      for (int i=0; i<total_utilizados; i++)
         media = media + vector_privado[i];
         
      return media/total_utilizados;   
   }
};



class SecuenciaCaracteres{
private:
	static  const  int  TAMANIO  =  50;
	char  vector_privado[TAMANIO];
	int  total_utilizados;
public:
	SecuenciaCaracteres()
		:total_utilizados(0)        
	{    
	}

	int  TotalUtilizados(){
		return  total_utilizados;
	}

	void  Aniade(char  nuevo){
		if (total_utilizados  <  TAMANIO){
			vector_privado[total_utilizados]  =  nuevo;
			total_utilizados++;
		}
	}

	void AniadeCadena(string nuevo){
		int tope = nuevo.size();

		for (int i = 0; i < tope; i++)
			Aniade(nuevo[i]);
	}

	char Elemento(int  indice){
		return  vector_privado[indice];
	}

	void Modifica(int indice_componente, char nuevo){
		if (indice_componente >= 0  &&  indice_componente < total_utilizados)
			vector_privado[indice_componente] = nuevo;
	}	


	int PrimeraOcurrenciaEntre(int pos_izda, int pos_dcha, char buscado){
		int i = pos_izda; 
		bool encontrado = false;

		while (i <= pos_dcha  &&  !encontrado)
			if (vector_privado[i] == buscado)
				encontrado = true;
			else
				i++;

		if (encontrado)
			return i;
		else
			return -1;
	}

	int PrimeraOcurrencia (char buscado){
		return PrimeraOcurrenciaEntre(0, total_utilizados - 1, buscado);
	}

   int NumeroOcurrencias(char a_buscar, int izda, int dcha){
      int numero_ocurrencias = 0;
      
      for (int i=izda; i <= dcha; i++)
         if (vector_privado[i] == a_buscar)
            numero_ocurrencias++;
      
      return numero_ocurrencias;
   }
   
      // Si pequenia est� vac�a devuelve -1
   int PosContiene (SecuenciaCaracteres pequenia){
      int i, j, pos_contiene, tope, utilizados_pequenia;
      bool encontrado, iguales_parcial;
      
      utilizados_pequenia = pequenia.TotalUtilizados();
      pos_contiene = -1;
      encontrado = false;
      tope = total_utilizados - utilizados_pequenia;
      i = 0;
      
      while (i <= tope && !encontrado){
         if (vector_privado[i] == pequenia.Elemento(0)){
            pos_contiene = i;
            iguales_parcial = true;
            j = 1;
            
            while (!encontrado && iguales_parcial){
               if (j == utilizados_pequenia)
                  encontrado = true;
               else if (pequenia.Elemento(j) == vector_privado[i+j])
                  j++;
               else
                  iguales_parcial = false;
            }
         }

         if (!encontrado)
            i++;
      }

      if (encontrado)
         return pos_contiene;
      else
         return -1;
   }
};



class ImpresorSecuenciaCaracteres{
private:
	string inicio, final, intermedio;
public:
	ImpresorSecuenciaCaracteres(string cadena_inicio, string cadena_final, string cadena_intermedio)
		:inicio(cadena_inicio), final(cadena_final), intermedio(cadena_intermedio)
	{
	}
   void Imprime(SecuenciaCaracteres secuencia_a_imprimir){
		int tope = secuencia_a_imprimir.TotalUtilizados() - 1;

		cout << inicio;

      for (int i=0; i<tope; i++)
         cout << secuencia_a_imprimir.Elemento(i) << intermedio;

      cout << secuencia_a_imprimir.Elemento(tope) << final;
   }
};

class LectorSecuenciaCaracteres{
private:
	char terminador;
public:
	LectorSecuenciaCaracteres(char caracter_terminador)
		:terminador(caracter_terminador)
	{
	}
	SecuenciaCaracteres Lee(){
		SecuenciaCaracteres a_leer;
		char caracter;

		caracter = cin.get();

		while (caracter == '\n')
			caracter = cin.get();

		while (caracter != terminador){
			a_leer.Aniade(caracter);
			caracter = cin.get();
		}		

		return a_leer;
	}
};


class ImpresorSecuenciaEnteros{
private:
	string inicio, final, intermedio;
public:
	ImpresorSecuenciaEnteros(string cadena_inicio, string cadena_final, string cadena_intermedio)
		:inicio(cadena_inicio), final(cadena_final), intermedio(cadena_intermedio)
	{
	}
   void Imprime(SecuenciaEnteros secuencia_a_imprimir){
		int tope = secuencia_a_imprimir.TotalUtilizados() - 1;

		cout << inicio;

      for (int i=0; i<tope; i++)
         cout << secuencia_a_imprimir.Elemento(i) << intermedio;

      cout << secuencia_a_imprimir.Elemento(tope) << final;
   }
};


class LectorSecuenciaEnteros{
private:
	int terminador;
public:
	LectorSecuenciaEnteros(int terminador)
		:terminador(terminador)
	{
	}
	SecuenciaEnteros Lee(){
		SecuenciaEnteros a_leer;
		int dato;

		cin >> dato;
				
		while (dato != terminador){
		   a_leer.Aniade(dato);
			cin >> dato;
		}		

		return a_leer;
	}
};


class Alumnos{
private:
   static const int TAMANIO = 50;
	SecuenciaCaracteres nombres[TAMANIO];	
	SecuenciaEnteros notas[TAMANIO];
   int numero_alumnos;
   
   void Intercambia(int indice_uno, int indice_otro){
      SecuenciaCaracteres cambia_nombre;
      SecuenciaEnteros cambia_nota;
      
      cambia_nombre = nombres[indice_uno];
      nombres[indice_uno] = nombres[indice_otro];
      nombres[indice_otro] = cambia_nombre;
      
      cambia_nota = notas[indice_uno];
      notas[indice_uno] = notas[indice_otro];
      notas[indice_otro] = cambia_nota;            
   }
public:
   Alumnos()
      :numero_alumnos(0)
   {
   }
	void Aniade(SecuenciaCaracteres nombre_alumno, SecuenciaEnteros notas_alumno){
	   if (numero_alumnos < TAMANIO){
         nombres[numero_alumnos] = nombre_alumno;
         notas[numero_alumnos] = notas_alumno;
         numero_alumnos++;
      }
	}
	
   int NumeroAlumnos(){
      return numero_alumnos;
   }
   
   SecuenciaCaracteres Nombre(int indice_alumno){
      return nombres[indice_alumno];
   }
   
   SecuenciaEnteros Notas(int indice_alumno){
      return notas[indice_alumno];
   }
   
   SecuenciaEnteros PosicionesContiene (SecuenciaCaracteres pequenia){
      SecuenciaEnteros posiciones_contiene;
     
      for (int i=0; i<numero_alumnos; i++)
         if (nombres[i].PosContiene(pequenia) != -1)
            posiciones_contiene.Aniade(i);         
      
      return posiciones_contiene;
   }
   
	void Ordena_por_notamedia (){
		// Almacenamos en un vector local las notas medias
		// notas_medias[i] contendr� la nota media del alumno i
		// Cuando intercambiemos dos alumnos, tambi�n tendremos
		// que intercambiar las componentes respectivas del vector de medias

		double notas_medias[TAMANIO];
		int pos_min;
		int dcha;		
		double nota_min;
		double intercambia;

		for (int i=0; i<numero_alumnos; i++)
			notas_medias[i] = notas[i].Media();

		for (int izda = 0 ; izda < numero_alumnos ; izda++){
			pos_min = izda;
			nota_min = notas_medias[izda];

			for (dcha = izda; dcha < numero_alumnos; dcha++){
				if (notas_medias[dcha] < nota_min){
					pos_min = dcha;
					nota_min = notas_medias[pos_min];
				}
			}
         
         intercambia = notas_medias[izda];
         notas_medias[izda]  = notas_medias[pos_min];
         notas_medias[pos_min] = intercambia;
         
         Intercambia(izda, pos_min);
		}
	}
};


int main(){   
   LectorSecuenciaCaracteres lector_caracteres(',');
   LectorSecuenciaEnteros lector_enteros(-1);
   ImpresorSecuenciaCaracteres impresor_caracteres("","","");
   ImpresorSecuenciaEnteros impresor_enteros("[","]",",");
   SecuenciaCaracteres nombre_alumno;
   SecuenciaEnteros notas_alumno;
   
   Alumnos claseFP;
   int numero_alumnos;
   
   cin >> numero_alumnos;
   
   for (int i=0; i<numero_alumnos; i++){
      nombre_alumno = lector_caracteres.Lee();
      notas_alumno = lector_enteros.Lee();
      claseFP.Aniade(nombre_alumno, notas_alumno);
   }
   
   claseFP.Ordena_por_notamedia();   
   numero_alumnos = claseFP.NumeroAlumnos();  // Se espera que no haya cambiado!
   
   cout << "\n";
   
   for (int i=0; i<numero_alumnos; i++){
      impresor_caracteres.Imprime(claseFP.Nombre(i));
      cout << " ";
      impresor_enteros.Imprime(claseFP.Notas(i));
      cout << "\n";
   }
   
   cout << "\n\n";
   system ("pause");
   // 3Sergio Garcia,3 4 2 -1Ana de Gober,9 7 8 9 -1David Rodriguez,5 4 -1  
}

