////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
   Pasar un car�cter de may�scula a min�scula.
   En caso contrario, dejadlo tal cual.
*/

#include <iostream>
using namespace std;

int main(){
   char letra_convertida, letra_original;
   const int DISTANCIA_MAY_MIN = 'a'-'A';

   cout << "\nIntroduzca una letra  --> ";
   cin >> letra_original;


	/*
	if ((letra_original >= 'A') && (letra_original <= 'Z')){
		letra_convertida = letra_original + DISTANCIA_MAY_MIN
		cout << letra_convertida;
	}
	else{
		cout << letra_original << " no es una may�scula";
	}
	*/

	/*
	El problema de la anterior soluci�n es que se mezclan C/S.
	Para resolverlo, usamos una variable adicional que indique
	si la letra en cuesti�n es o no una may�scula.
	Le asignamos un valor en un bloque -de c�mputos-
	y luego OBSERVAMOS su valor en otro bloque -de salida de resultados-
	�De qu� tipo de dato declaramos esa variable que indica si la letra es una may�scula?
	Vamos a verlo de dos formas:

	- Con un string  :-(
	- Con un bool    :-)
	*/


   // Con un string :-(
   /*
   string tipo_letra;

   if ((letra_original >= 'A') && (letra_original <= 'Z'))
      tipo_letra = "es may�scula";

   if (tipo_letra == "es may�scula")
      letra_convertida = letra_original + DISTANCIA_MAY_MIN;
   else
      letra_convertida = letra_original;

   cout << "\nEl car�cter " << letra_original << " una vez convertido es: " << letra_convertida;
   */
	/*
      La anterior soluci�n es propensa a errores.
      Hay muchas probabilidades de que asignemos un valor y luego comparemos con otro:
         tipo_letra = "es may�sculas";
         ......
         if (tipo_letra == "es may�scula")
            .....
      Moraleja: Jam�s usaremos un bool cuando queramos identificar una situaci�n
      que s�lo tiene dos posibles valores: es o no es una may�scula.
      Mucho mejor un bool.
   */
   

   // Con un bool :-)
	/*
	bool es_mayuscula;

	if (letra_original >= 'A') && (letra_original <= 'Z')
      es_mayuscula = true;

   if (es_mayuscula)
      letra_convertida = letra_original + DISTANCIA_MAY_MIN;
   else
      letra_convertida = letra_original;
   */

	/*
   Sin embargo, hay un error l�gico ya que la variable es_mayuscula
   se podr�a quedar sin un valor asignado (en el caso de que
   la expresi�n l�gica del primer condicional fuese false)
	El compilador lo detecta y da el error al inicio de la sentencia  if (es_mayuscula)

   Para resolverlo debemos poner lo siguiente:
	*/

	/*
   if (letra_original >= 'A') && (letra_original <= 'Z')
      es_mayuscula = true;
   else
      es_mayuscula = false;
	*/

	// O mucho mejor, de una forma m�s concisa:

   bool es_mayuscula;

	es_mayuscula = (letra_original >= 'A') && (letra_original <= 'Z');

	if (es_mayuscula)
		letra_convertida = letra_original + DISTANCIA_MAY_MIN;
	else
		letra_convertida = letra_original;

	cout  << "\nEl car�cter " << letra_original 
         << " una vez convertido es: " << letra_convertida;

	cout << "\n\n";
	system("pause");
}
