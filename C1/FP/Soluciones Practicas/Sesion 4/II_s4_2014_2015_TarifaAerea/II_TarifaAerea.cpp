////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
Una compa��a a�rea establece el precio del billete como sigue: en primer lugar se fija
una tarifa base de 150 euros, la misma para todos los destinos. Si el destino est� a
menos de 200 kil�metros, el precio final es la tarifa inicial. Para destinos a m�s de 200
Km, se suman 10 c�ntimos por cada kil�metro de distancia al destino (a partir del Km
200).

En una campa�a de promoci�n se va a realizar una rebaja lineal de 15 euros a todos
los viajes. Adem�s, se pretenden a�adir otras rebajas y se barajan las siguientes
alternativas de pol�ticas de descuento:

a) Una rebaja del 3% en el precio final, para destinos a m�s de 600Km.
b) Una rebaja del 4% en el precio final, para destinos a m�s de 1100Km. En este
caso, no se aplica el anterior descuento.
c) Una rebaja del 5% si el comprador es cliente previo de la empresa.

Cread un programa para que lea el n�mero de kil�metros al destino y si el billete
corresponde a un cliente previo de la empresa. Calcular el precio final del billete con
las siguientes pol�ticas de descuento:

 - Aplicando c) de forma adicional a los descuentos a) y b)
 - Aplicando c) de forma exclusiva con los anteriores, es decir,
   que si se aplica c), no se aplicar�a ni a) ni b)
*/

// NOTA: En un programa real no usar�amos double para representar una cuant�a monetaria
//			debido a los problemas de precisi�n y redondeo.


#include <iostream>
#include <cctype>
using namespace std;

int main(){
   const int    LIMITE_KMS_TARIFICACION_ADICIONAL = 200;
   const double TARIFA_POR_KM_ADICIONAL = 0.1;
   const int    MINIMO_KMS_TRAYECTO_MEDIO = 600;
   const int    MINIMO_KMS_TRAYECTO_LARGO = 1100;
   const double INDICE_VARIACION_TRAYECTO_MEDIO = 1 - 0.03;
	const double INDICE_VARIACION_TRAYECTO_LARGO = 1 - 0.04;
	const double INDICE_VARIACION_CLIENTE_PREVIO = 1 - 0.05;
   const double REBAJA_LINEAL = 15.0;
   const double TARIFA_BASE = 150.0;

   double   tarifa_con_km_adicionales,    
            tarifa_final_descuentos_acumulados, 
				tarifa_final_descuentos_excluyentes;
   int distancia_recorrido;
   bool es_cliente_previo;
   char opcion_es_cliente_previo;
   bool es_trayecto_medio, es_trayecto_largo;
	double indice_variacion_final;

   cout << "\nIntroduzca la distancia del recorrido del viaje: ";
   cin >> distancia_recorrido;
   cout << "\n�Es un cliente previo (s/n)? ";
   cin >> opcion_es_cliente_previo;

   opcion_es_cliente_previo = toupper(opcion_es_cliente_previo);
   es_cliente_previo = (opcion_es_cliente_previo == 'S');  // En el caso de que introduzca algo distinto de 'S', se considera que no es cliente previo

	/*
	///////////////////////////////////////////////////////////////////////////////////////////////
	Opci�n primera:
		Descuentos a) y b) son mutuamente excluyentes entre s� => if-else-if
   	Descuento c) acumulativo => if a continuaci�n del anterior   
   
   El objetivo es aplicar la f�rmula siguiente:
		TarifaFinal = IVF * TarifaBase - REBAJA_LINEAL;  // IVF = �ndice de variaci�n final
	
	Algoritmo:
   	Seg�n sea la longitud del trayecto
	   	Calcular IVF
	   Seg�n sea cliente previo
	   	Actualizar IVF	   	
	   TarifaFinal = IVF * TarifaBase - REBAJA_LINEAL; 


	Para conseguir calcular el �ndice de variaci�n final, lo podemos hacer usando dos criterios:
	
		1. El descuento por ser cliente previo se aplica sobre la tarifa sin descuentos anteriores:
			Por ejemplo 3% de trayecto medio y 5% de cliente previo => 8%
			Al trabajar con IV en vez de porcentajes, la cuenta a realizar ser�a la siguiente:
			IVF = 1.03 + 1.05 -1 = 1.08
			
		2. El descuento por ser cliente previo se aplica sobre la tarifa con descuentos anteriores:
			Por ejemplo 3% de trayecto medio y 5% de cliente previo => 3% de (5% de tarifa)
			Al trabajar con IV en vez de porcentajes, la cuenta a realizar ser�a la siguiente:
			IVF = 1.03 * 1.05
		  
	///////////////////////////////////////////////////////////////////////////////////////////////
   Opci�n segunda:
      Descuentos a) y b) son mutuamente excluentes entre s�
      Descuento c) mutuamente excluyente con los anteriores
      => a) b) y c) son mutuamente excluyentes entre s�
      => if-else-if-else-if
      
   Algoritmo:
   	Seg�n sea cliente previo y la longitud del trayecto
	   	Calcular IVF
	   TarifaFinal = IVF * TarifaBase - REBAJA_LINEAL; 

   Es importante el orden en el que se establecen los if-else
   ya que cuando el programa entre en un if, no debe entrar en el resto de los if.
   Lo razonable es que la compa��a aplique el m�s beneficioso para el cliente,
   en caso de que le sea aplicable m�s de uno.
   Por tanto, los if-else debemos ordenarlos de mayor a menor descuento.
	*/

	//	Usando constantes que reflejan el �ndice de variaci�n:

	tarifa_con_km_adicionales = TARIFA_BASE;

   if (distancia_recorrido > LIMITE_KMS_TARIFICACION_ADICIONAL)
      tarifa_con_km_adicionales = tarifa_con_km_adicionales +
                                  TARIFA_POR_KM_ADICIONAL *
                                  (distancia_recorrido - LIMITE_KMS_TARIFICACION_ADICIONAL) ;

   es_trayecto_medio = (distancia_recorrido >= MINIMO_KMS_TRAYECTO_MEDIO)
                       && (distancia_recorrido < MINIMO_KMS_TRAYECTO_LARGO);
   es_trayecto_largo = (distancia_recorrido >= MINIMO_KMS_TRAYECTO_LARGO);

	
	if (es_trayecto_largo)
		indice_variacion_final = INDICE_VARIACION_TRAYECTO_LARGO;
	else if (es_trayecto_medio)
		indice_variacion_final = INDICE_VARIACION_TRAYECTO_MEDIO;
	else
		indice_variacion_final = 1;

   if (es_cliente_previo)
		indice_variacion_final = indice_variacion_final * INDICE_VARIACION_CLIENTE_PREVIO;
		// indice_variacion_final = indice_variacion_final + INDICE_VARIACION_CLIENTE_PREVIO - 1;  // Si el descuento por cliente se aplicase directamente sobre la tarifa sin descuentos por trayecto

	tarifa_final_descuentos_acumulados = indice_variacion_final * tarifa_con_km_adicionales - REBAJA_LINEAL;

   if (tarifa_final_descuentos_acumulados < 0)                     
      tarifa_final_descuentos_acumulados = 0.0;

	cout << "\n\nTarifa final aplicando todos los descuentos: ";
   cout << tarifa_final_descuentos_acumulados;

	/////////////////////////////////////////////////////////////////////////////////////////////////


   if (es_cliente_previo)			// Importante: Se aplica primero el descuento m�s ventajoso para el cliente
      indice_variacion_final = INDICE_VARIACION_CLIENTE_PREVIO;
   else if (es_trayecto_largo)
      indice_variacion_final = INDICE_VARIACION_TRAYECTO_LARGO;
   else if (es_trayecto_medio)
      indice_variacion_final = INDICE_VARIACION_TRAYECTO_MEDIO;
	else
		indice_variacion_final = 1;

	tarifa_final_descuentos_excluyentes = indice_variacion_final * tarifa_con_km_adicionales - REBAJA_LINEAL;

   if (tarifa_final_descuentos_excluyentes < 0)                     
      tarifa_final_descuentos_excluyentes = 0.0;

   cout << "\n\nTarifa final aplicando el descuento por ser cliente previo, de forma excluyente ";
   cout << tarifa_final_descuentos_excluyentes;

   cout << "\n\n";
   system("pause");
}
