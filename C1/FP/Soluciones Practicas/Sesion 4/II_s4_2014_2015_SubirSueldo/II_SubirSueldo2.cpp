////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
   Subir el sueldo un 4% a los jubilados o j�venes.
	Si, adem�s, tienen ingresos inferiores a 300 euros, subir tambi�n un 3%
*/
/*
	En el ejercicio anterior hac�amos lo siguiente:
	
	if (es_aplicable_subida)
		ingresos_finales = ingresos * INDICE_VARIACION_SUBIDA;
	else
		ingresos_finales = ingresos;
	
	Como ahora vamos a trabajar con varios incrementos, mejor hacemos lo siguiente:
	
	Calculamos el IVF (�ndice de variaci�n final) seg�n sean las distintas condiciones.
	Una vez hecho lo anterior, simplemente basta calcular:
		ingresos_finales = ingresos * IVF;
		
	Algoritmo:
		Establecer los criterios de aumento (edad especial e ingresos bajos)

		Si es edad especial
			IVF = IV_Edad
			Si tiene ingresos bajos
				Actualizar IVF       // <- Ver c�mo hacerlo abajo
		Si no
			IVF = 1
			
		ingresos_finales = ingresos * IVF;
*/
			

/*
	Para actualizar IVF:
	Se supone que la subida se calcula sobre los ingresos iniciales.
	Por tanto, si se aplican los dos criterios, hay que subir un 7%
	Si trabajamos con �ndices de variaci�n 1.04 y 1.03 habr� que hacer la cuenta:
		IVF = 1.04 + 1.03 - 1 = 1.07
		Ing = Ing * IVF
	Si la subida del 3% se realizase sobre la subida del 4% habr�a que multiplicar
	los �ndices de variaci�n:
		IVF = 1.04 * 1.03
		Ing = Ing * IVF 	
*/

#include <iostream>
using namespace std;

int main(){	
   const double INDICE_VARIACION_JOVEN_JUBILADO = 1.04;
   const double INDICE_VARIACION_INGRESOS_BAJOS = 1.03;
   const int MAXIMO_INGRESOS = 300;
   const int EDAD_JUBILACION = 65;
   const int EDAD_MAX_JOVEN = 35;
   
   double ingresos, ingresos_finales;
   double indice_variacion_final;
	int edad;
	bool es_edad_especial, tiene_ingresos_bajos, es_aplicable_subida;
	
	cout << "\nIntroduce la edad de la persona: ";
	cin >> edad;
	cout << "\nIntroduce los ingresos: ";
	cin >> ingresos;	

	// C�MPUTOS:

	es_edad_especial = edad > EDAD_JUBILACION  ||  edad < EDAD_MAX_JOVEN;
	tiene_ingresos_bajos = ingresos < MAXIMO_INGRESOS;

	if (es_edad_especial){
		indice_variacion_final = INDICE_VARIACION_JOVEN_JUBILADO;
		
		if (tiene_ingresos_bajos)
			indice_variacion_final = indice_variacion_final + INDICE_VARIACION_INGRESOS_BAJOS - 1;
	}
	else
		indice_variacion_final = 1;
	
	es_aplicable_subida = es_edad_especial;
	
	ingresos_finales = ingresos * indice_variacion_final;
	
	// SALIDA RESULTADOS:

	if (! es_aplicable_subida)
		cout << "\nNo es aplicable la subida";

	cout << "\nSalario Final: " << ingresos_finales;
	cout << "\n\n";
	system("pause");
	
	/*
	Mejor poner el c�digo anterior que el siguiente:
	
	if (es_edad_especial){
		es_aplicable_subida = true;
		indice_variacion_final = INDICE_VARIACION_JOVEN_JUBILADO;
		
		if (tiene_ingresos_bajos)
			indice_variacion_final = indice_variacion_final + INDICE_VARIACION_INGRESOS_BAJOS - 1;
	}
	else{
		es_aplicable_subida = false;
		indice_variacion_final = 1;
	}
	
	En el c�digo anterior, se nos puede olvidar darle un valor a la variable es_aplicable_subida
	Adem�s, en el c�digo correcto, queda muy claro que al poner 
		es_aplicable_subida = es_edad_especial;
	el criterio de es_aplicable_subida s�lo depende de la edad.
	*/
}
