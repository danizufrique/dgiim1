////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
   Cread un programa que ofrezca en pantalla la siguiente salida:
   1 2 3 4 5 6
   2 3 4 5 6
   3 4 5 6
   4 5 6
   5 6
   6

   Cread un programa que ofrezca en pantalla la siguiente salida:
   1 2 3 4 5 6
   2 3 4 5 6 7
   3 4 5 6 7 8
   4 5 6 7 8 9
   5 6 7 8 9 10
   6 7 8 9 10 11

   Modificad los dos ejercicios anteriores para que 
	se lea desde teclado el valor inicial y el n�mero de filas a imprimir. 
	En los ejemplos anteriores, el valor inicial era 1 y se imprim�an un total de 6 filas.
*/

#include <iostream>
using namespace std;

int main(){
   int inicio, final,  fila, col, final_fila;
	unsigned int numero_filas;

   /*
		Valores en forma de pir�mide:

      Algoritmo:
			El valor final a imprimir es inicio + numero_filas - 1

         Recorrer todos los enteros -fila- entre inicio y final
            Recorrer todos los enteros -col- entre fila y final
               Imprimir col
   */

	inicio = 1;
	numero_filas = 6;
	final = inicio + numero_filas - 1;

	for (fila = inicio ; fila <= final ; fila++){
		for (col = fila ; col <= final ; col++)
			cout << col << " ";

		cout << "\n";
	}

   cout << "\n\n";

   /*
		Valores en forma de cuadrado:

      Algoritmo:
			El valor final a imprimir es inicio + numero_filas - 1

         Recorrer todos los enteros -fila- entre inicio y final
            Calcular el entero -final_fila- que hay numero_filas por encima de fila
            Recorrer todos los enteros -col- entre fila y final_fila
               Imprimir col
   */

	for (fila = inicio ; fila <= final ; fila++){
		final_fila = fila + numero_filas - 1;

		for (col = fila ; col <= final_fila ; col++)
			cout << col << " ";

		cout << "\n";
	}

   /*
      Comprobad que funciona correctamente cuando numero_filas es un valor extremo como 1 o 0.
   */	

   /*
		�Qu� pasar�a si en el anterior bucle sustituy�semos final_fila por final?
		Entrar�a en un bucle sin fin, al estar modificando en cada iteraci�n el valor final 
		del bucle m�s externo
	*/

   cout << "\n\n";
	system("pause");
}
