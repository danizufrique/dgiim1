///////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Autor: Juan Carlos Cubero
//
///////////////////////////////////////////////////////

/*
   Cread un programa que ofrezca en pantalla la siguiente salida:
   1 2 3 4 5 6
   2 3 4 5 6
   3 4 5 6
   4 5 6
   5 6
   6

   Cread un programa que ofrezca en pantalla la siguiente salida:
   1 2 3 4 5 6
   2 3 4 5 6 7
   3 4 5 6 7 8
   4 5 6 7 8 9
   5 6 7 8 9 10
   6 7 8 9 10 11

   Modificad los dos ejercicios anteriores para que los valores inicial (1) y final (6) puedan
   ser cualesquiera introducidos desde el teclado
*/

#include <iostream> 
using namespace std;  
   
int main(){    
   int inicio, final, numero_valores, fila, col, final_fila;

   /*
      Algoritmo:
         Recorrer todos los enteros -fila- entre inicio y final
            Recorrer todos los enteros -col- entre fila y final
               Imprimir col
   */

	inicio = 5;
	numero_valores = 9;
	final = inicio + numero_valores - 1;

	for (fila = inicio ; fila <= final ; fila++){     
		for (col = fila ; col <= final ; col++)
			cout << col << " ";

		cout << "\n";
	}

   cout << "\n\n";

   /*
      Algoritmo:
         Recorrer todos los enteros -fila- entre inicio y final
            Calcular el entero -final_fila- que hay numero_valores por encima de fila
            Recorrer todos los enteros -col- entre fila y final_fila
               Imprimir col
   */

	for (fila = inicio ; fila <= final ; fila++){     
		final_fila = fila + numero_valores - 1;

		for (col = fila ; col <= final_fila ; col++)     
			cout << col << " ";

		cout << "\n";
	}

   /*
      Comprobad que funciona correctamente cuando numero_valores es un valor extremo como 1 o 0.
   */	

   /*
		�Qu� pasar�a si en el anterior bucle sustituy�semos final_fila por final?
		Entrar�a en un bucle sin fin, al estar modificando en cada iteraci�n el valor final 
		del bucle m�s externo
	*/

   cout << "\n\n";
	system("pause");
}
