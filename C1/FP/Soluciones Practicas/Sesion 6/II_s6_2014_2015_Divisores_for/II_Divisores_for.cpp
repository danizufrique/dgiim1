////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programación
// ETS Informática y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computación e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/* Calcular los divisores de un número entero positivo >0 */

#include <iostream>
using namespace std;  

int main(){
	int n, ultimo_posible_divisor, divisor;

	do{
		cout << "Introduce un numero entero positivo mayor que 0: ";
		cin >> n;
	}while (n <= 0);

	ultimo_posible_divisor = n/2;
	
	for (divisor = 2; divisor <= ultimo_posible_divisor; divisor++)
		if (tope % divisor == 0)
			cout << divisor << " es un divisor de " << tope << "\n";

	cout << "\n\n";
	system("pause");
}
