///////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Autor: Juan Carlos Cubero
//
///////////////////////////////////////////////////////

/*
	C�mputo del precio final de un autom�vil, conociendo
	el precio base, el porcentaje de ganancia del vendedor y el IVA
*/

#include <iostream>   
#include <cmath>      

using namespace std; 

int main(){ 
	/*
	Optamos por declarar el porcentaje de ganancia y el IVA como constantes.
	Si se prev� que puedan variar, habr�a que declarlas como variables
	y leer dichos datos con cin.
	
   */
	const int IVA                          = 16;
	const int PORCENTAJE_GANANCIA_VENDEDOR = 20;	
	double precio_base, precio_final;	
	
	cout << "\n\nIntroduzca el precio base del autom�vil ";
	cin >> precio_base;
	
	precio_final = precio_base * (1 +  (IVA + PORCENTAJE_GANANCIA_VENDEDOR)/100.0);
	
	/*
   Es posible que la expresi�n sin sacar factor com�n sea m�s legible
	en cuyo caso, tambi�n hubiese sido aceptable:
	precio_final = precio_base + precio_base * (IVA + PORCENTAJE_GANANCIA_VENDEDOR)/100.0;
	*/
	
	cout << "\nEl precio final del autom�vil es " << precio_final;
	cout << "\nSe ha aplicado lo siguiente:";
	cout << "\nIVA = " << IVA << "%";
	cout << "\nPorcentaje de ganancia del vendedor = " << PORCENTAJE_GANANCIA_VENDEDOR << "%\n\n";	
	system("pause");
}


  
