////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
   Medidas contra la crisis.

   * Bajar un 3% la retenci�n fiscal a los aut�nomos
   * Para las rentas de trabajo:
      = Se sube un 1% la retenci�n fiscal a todos los pensionistas.
      = Al resto de trabajadores se le sube un 2% la retenci�n fiscal. Una vez hecha
         esta subida lineal del 2 %, se le aplica (sobre el resultado anterior) las
         siguientes subidas adicionales, dependiendo de su estado civil y niveles de
         ingresos:
         - Subir un 6% la retenci�n fiscal a las rentas de trabajo inferiores a 20.000 euros
         - Subir un 8% la retenci�n fiscal a los casados con rentas de trabajo superiores a 20.000 euros
         - Subir un 10% la retenci�n fiscal a los solteros con rentas de trabajo superiores a 20.000 euros
   
   Ejemplo (Supongamos retenci�n del 20%)
      Si es aut�nomo --> nueva retenci�n (-3% de 20) = 19.4 Nueva retenci�n = 19.4%
      Si es no aut�nomo, no pensionista (+2% de 20) = 20.4% Casado y con ingresos altos (+8% de 20.4) = 22.032  Nueva retenci�n = 22.032%      
*/

/*
   C�mputo de porcentajes de incremento (Matem�ticas de tercero de la ESO).

   Supongamos un producto cuyo precio es P.
   Un incremento del 3%, por ejemplo, se traduce en que el precio final del producto ser� P * (1 + 0.03) = 1.03 * P
   1.03 se denomina "�ndice de variaci�n"
   Una rebaja del 3%, por ejemplo, se traduce en que el precio final del producto ser� P * (1 - 0.03) = 0.97 * P
   En este caso, el �ndice de variaci�n es 0.97
   La aplicaci�n sucesiva de incrementos supone ir multiplicando sucesivamente los �ndices de variaci�n.
   Si subimos un 3% y al resultado le aplicamos otra subida del 5%, el precio final es P * 1.03 * 1.05

   Hay que remarcar que en este ejemplo los �ndices de variaci�n se est�n aplicando sobre porcentajes de retenci�n.
   Es decir, que si partimos de una retenci�n del 20% y al final se le sube un 3% de ese 20%, significa que la nueva retenci�n es:
      20 * (1 + 0.03) = 20 * 1.03 = 20.6   (nueva retenci�n = 20.6%)
   Por tanto, podemos hablar del �ndice de variaci�n de la retenci�n iv_retencion = 1.03
      nueva retenci�n = antigua retenci�n * iv_retencion 
      
   Finalmente, la renta neta ser�:
      r_neta = r_bruta * (1 - nueva retenci�n/100.0)
*/

#include <iostream>
#include <cctype>
using namespace std;

int main(){
	const int LIMITE_SALARIO_BAJO = 20000;
   const double IV_RETENCION_PENSIONISTA = 1.01;        // IV_RETENCION: �ndice de Variaci�n.
   const double IV_RETENCION_AUTONOMO = 0.97;
   const double IV_RETENCION_SALARIO_BAJO = 1.06;
   const double IV_RETENCION_SALARIO_NORMAL_SOLTERO = 1.1;
   const double IV_RETENCION_SALARIO_NORMAL_CASADO = 1.08;
   const double IV_RETENCION_INCREMENTO_LINEAL = 1.02;	
   
   double renta_bruta, renta_neta, retencion, retencion_final, iv_retencion;
	bool es_autonomo, es_soltero, es_pensionista;
   char opcion; 
   
 	///////////////////////////////////////////////////////////////////////////////////
 	//  Entradas de datos

   /*
   Aunque el ejercicio no lo ped�a, usamos filtros de entrada para los datos
   */
   
	cout << "\nRenta Bruta/Neta\n";
	
	do{
		cout << "\nIndique si la persona es un trabajador aut�nomo o no (s/n) ";		
		cin >> opcion;
		opcion = toupper(opcion);
	}while (opcion != 'S' && opcion != 'N');	
	
	es_autonomo = opcion == 'S';
	
	do{
		cout << "\nIndique si la persona es pensionista o no (s/n) ";		
		cin >> opcion;
		opcion = toupper(opcion);
	}while (opcion != 'S' && opcion != 'N');	
	
	es_pensionista = opcion == 'S';
	
	do{
		cout << "\nIndique si la persona est� soltera o no (s/n) ";		
		cin >> opcion;
		opcion = toupper(opcion);
	}while (opcion != 'S' && opcion != 'N');	
	
	es_soltero = opcion == 'S';
	
	cout << "\nIndique la Renta Bruta: ";		
	cin >> renta_bruta;
	cout << "\nIndique la Retenci�n a aplicar (en %): ";		
	cin >> retencion;
	
	///////////////////////////////////////////////////////////////////////////////////
	// C�mputos
	
	/*
	Algoritmo:
	
	  Asignar un valor inicial al �ndice de variaci�n de la retenci�n iv_retencion
	     en funci�n de si es aut�nomo o pensionista
	     
	     Actualizar iv_retencion en funci�n del salario y el estado civil.
	   
	   Actualizar la retencion a partir de iv_retencion 
      Calcular la renta neta en funci�n de la  la renta bruta y la retenci�n	
	*/

	if (es_autonomo)
		iv_retencion = IV_RETENCION_AUTONOMO;
	else if (es_pensionista)
		iv_retencion = IV_RETENCION_PENSIONISTA;
	else{
      iv_retencion = IV_RETENCION_INCREMENTO_LINEAL;

      if (renta_bruta < LIMITE_SALARIO_BAJO)
         iv_retencion = iv_retencion * IV_RETENCION_SALARIO_BAJO;
      else if (es_soltero)
         iv_retencion = iv_retencion * IV_RETENCION_SALARIO_NORMAL_SOLTERO;
      else
         iv_retencion = iv_retencion * IV_RETENCION_SALARIO_NORMAL_CASADO;
   }

	retencion_final = retencion * iv_retencion;
	renta_neta = renta_bruta * (1 - retencion_final/100.0);

	///////////////////////////////////////////////////////////////////////////////////
	// Salida de resultados
	
	cout << "\nRenta final = " << renta_neta;
   cout << "\n\n";
	system("pause");
}

