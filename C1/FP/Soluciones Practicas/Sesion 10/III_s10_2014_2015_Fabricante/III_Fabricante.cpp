///////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Autor: Juan Carlos Cubero
//
///////////////////////////////////////////////////////

#include <iostream>
#include <string>
using namespace std;

/*
Se quiere construir una clase Nomina para realizar la funcionalidad descrita en el
ejercicio 12 de la relaci�n de problemas I sobre la n�mina del fabricante y dise�ador
(p�gina RP-I.5). Cread los siguientes programas (entregad un fichero por cada uno de
los apartados):

a) Suponed que s�lo gestionamos la n�mina de una empresa en la que hay un fabri-
cante y tres dise�adores. Los salarios brutos se obtienen al repartir los ingresos
de la empresa, de forma que el dise�ador cobra el doble de cada fabricante.
El programa leer� el valor de los ingresos totales y calcular� los salarios brutos
de los fabricantes y dise�ador, llamando a los m�todos oportunos de la clase
Nomina.

b) Supongamos que se aplica una retenci�n fiscal y que �sta es la misma para
los fabricantes y el dise�ador. En el constructor se establecer� el porcentaje de
retenci�n fiscal (de tipodouble) y posteriormente no se permitir� que cambie, de
forma que todas las operaciones que se hagan ser�n siempre usando la misma
retenci�n fiscal. Los salarios netos se obtienen al aplicar la retenci�n fiscal a los
salarios brutos (despu�s de repartir los ingresos totales de la empresa):

salario_neto = salario_bruto - salario_bruto * retencion_fiscal / 100.0

El programa leer� el valor de los ingresos totales y la retenci�n fiscal a aplicar y
calcular� los salarios brutos y netos de los fabricantes y dise�ador, llamando a
los m�todos oportunos de la clase Nomina.

c) Supongamos que gestionamos las n�minas de varias sucursales de una empre-
sa. Queremos crear objetos de la clase Nomina que se adapten a las caracter�s-
ticas de cada sucursal:

� En cada sucursal hay un �nico dise�ador pero el n�mero de fabricantes es
distinto en cada sucursal. Por tanto, el n�mero de fabricantes habr� que es-
pecificarlo en el constructor y posteriormente no podr� cambiarse.

� La forma de repartir el dinero es la siguiente: el dise�ador se lleva una parte
del total y el resto se reparte a partes iguales entre los fabricantes. En los
apartados anteriores, por ejemplo, la parte que se llevaba el dise�ador era
2/5 y el resto (3/5) se repart�a entre los tres fabricantes. La parte que el di-
se�ador se lleva puede ser distinta entre las distintas sucursales (2/5, 1/6,
etc), pero no cambia nunca dentro de una misma sucursal. Por tanto, el por-
centaje de ganancia (2/5, 1/6, etc) habr� que especificarlo en el constructor
y posteriormente no podr� cambiarse.

� Las retenciones fiscales de los fabricantes y dise�ador son distintas. Adem�s,
se prev� que �stas puedan ir cambiando durante la ejecuci�n del programa.
Por lo tanto, no se incluir�n como par�metros en el constructor.
El programa leer� los siguientes datos desde un fichero externo:

� El n�mero de sucursales.
� Los siguientes valores por cada una de las sucursales:
� Ingresos totales a repartir
� N�mero de fabricantes
� Parte que se lleva el dise�ador
� Retenci�n fiscal del dise�ador
� Retenci�n fiscal de los fabricantes
Por ejemplo, el siguiente fichero indica que hay dos sucursales. La primera tiene
unos ingresos de 300 euros, 3 fabricantes, el dise�ador se lleva 1/6, la retenci�n
del dise�ador es del 20% y la de cada fabricante un 18%. Los datos para la
segunda son 400 euros, 5 fabricantes, 1/4, 22% y 19%.
2
300 3 6 20 18
400 5 4 22 19

El programa tendr� que imprimir el salario neto del dise�ador y de los fabricantes
por cada una de las sucursales, llamando a los m�todos oportunos de la clase
Nomina.
*/


/*
a) 
   S�lo gestionamos la n�mina de una empresa 
   Hay un fabricante y tres dise�adores. 
   El dise�ador cobra el doble de cada fabricante.
*/

class Nomina_a{
private:
   double ingresos_totales;

   bool EsCorrecto(double ingresos){
      return ingresos > 0;
   }
public:   
	Nomina_a(double ingresos_totales_nomina)
	{
	  if (EsCorrecto(ingresos_totales_nomina))
	     ingresos_totales = ingresos_totales_nomina;
     else
        ingresos_totales = 0;	
	}
 
   void SetIngresosTotales(double ingresos_totales_nuevos){
      if (EsCorrecto(ingresos_totales_nuevos))
         ingresos_totales = ingresos_totales_nuevos;
      // else
      //     lanzar excepci�n
   }

   double Ingresos(){
      return ingresos_totales;
   }


   double SalarioBrutoFabricante(){
      return ingresos_totales / 5.0;
   }

   double SalarioBrutoDiseniador(){
      return 2.0 * SalarioBrutoFabricante();
   }
};


/*
b) 
   Se aplica una retenci�n fiscal y �sta es la misma para los fabricantes y el dise�ador.
   	salario_neto = salario_bruto - salario_bruto * retencion_fiscal / 100.0
   Una vez que le demos un valor a la retenci�n fiscal, �sta ya no cambia. 
   Por lo tanto, no incluiremos ning�n m�todo SetRetencionFiscal
   Adem�s, tambi�n podemos forzar a que sea una constante a nivel de objeto
   (no es est�tica ya que la retenci�n puede variar de una sucursal a otra)
*/

class Nomina_b{
private:
   double ingresos_totales;
   const double PORCENTAJE_RETENCION_FISCAL;

   double AplicaRetencion(double valor_bruto){
	   return valor_bruto * (1 - PORCENTAJE_RETENCION_FISCAL/100.0);
   }
   bool EsCorrecto(double ingresos){
      return ingresos > 0;
   }
public:   
	Nomina_b(double ingresos_totales_nomina, double porcentaje_retencion_fiscal_nomina)
		:PORCENTAJE_RETENCION_FISCAL(porcentaje_retencion_fiscal_nomina)
	{
	  if (EsCorrecto(ingresos_totales_nomina))
	     ingresos_totales = ingresos_totales_nomina;
     else
        ingresos_totales = 0;	
	}
 
   void SetIngresosTotales(double ingresos_totales_nuevos){
      if (EsCorrecto(ingresos_totales_nuevos))
         ingresos_totales = ingresos_totales_nuevos;
      // else
      //     lanzar excepci�n
   }

   double Ingresos(){
      return ingresos_totales;
   }

   double PorcentajeRetencionFiscal() {
      return PORCENTAJE_RETENCION_FISCAL;
   }

   double SalarioBrutoFabricante(){
      return ingresos_totales / 5.0;
   }

   double SalarioBrutoDiseniador(){
      return 2.0 * SalarioBrutoFabricante();
   }

   double SalarioNetoDisenador(){
      return AplicaRetencion(SalarioBrutoDiseniador());
   }

   double SalarioNetoFabricante(){
      return AplicaRetencion(SalarioBrutoFabricante());
   }
};


/*
	Como norma general no debemos usar funciones globales (en Java se implementan a trav�s de clases "est�ticas")
	Sin embargo, hay casos en los que s� puede justificarse su uso.
	Por ejemplo, cuando trabajamos con datos simples (int, double, etc) y son funciones
	muy gen�ricas que se usar�n en muchos otros programas.
	Tal es el caso de la funci�n AplicaRetencion.
	Decidimos por tanto ponerla como una funci�n p�blica.
*/

/*
   Podr�amos poner la siguiente funci�n.
   Observad que ahora tenemos que pasar como par�metro la retenci�n.

   double AplicaRetencion(double valor_bruto, double retencion_en_tanto_por_ciento){
	  return valor_bruto * (1 - retencion_en_tanto_por_ciento/100.0);
   }
   
   Pero m�s general es la siguiente:
*/

double AplicaDisminucionPorcentaje(double base, double porcentaje){
   return base * (1 - porcentaje/100.0);
}    
   

/*
c) 
   Gestionamos las n�minas de varias sucursales de una empresa. 
   En cada sucursal hay un �nico dise�ador pero el n�mero de fabricantes es
   distinto en cada sucursal. 
   El dise�ador se lleva una parte del total y el resto se reparte 
   a partes iguales entre los fabricantes seg�n un porcentaje de ganancia (2/5, 1/6, etc)
   
   Lo que distingue la n�mina de una sucursal de otra es el 
   n�mero de fabricantes y el porcentaje de ganancia del dise�ador.
   Por lo tanto, lo pasaremos en el constructor y no permitiremos su modificaci�n posterior

   Las retenciones fiscales de los fabricantes y dise�ador son distintas. Adem�s,
   se prev� que �stas puedan ir cambiando durante la ejecuci�n del programa.
   Por lo tanto, las retenciones fiscales ser�n par�metros de los m�todos que calculan los salarios netos.
   
   Con este ejemplo se quiere poner de manifiesto que ser�n las caracter�sticas
   de nuestro problema las que nos har�n determinar qu� son datos miembro y qu� son
   par�metros de los m�todos.
   
   En el primer caso ten�amos que las retenciones fiscales eran las mismas para todos
   los trabajadores y no variaban => Datos miembro Ctes a nivel de objeto.
   En el �ltimo caso tenemos que las retenciones var�an de unos trabajadores 
   a otros y adem�s pretendemos ejecutar los m�todos que calculan los salarios
   netos con distintos tipos de retenciones => Par�metros a dichos m�todos.
*/


class Nomina_c{
private:
   const int NUMERO_FABRICANTES;
   const double PORCENTAJE_GANANCIA_DISENADOR;
   double ingresos_totales;
   
   bool EsCorrecto(double ingresos){
      return ingresos > 0;
   }
public:   
	Nomina_c(double ingresos_totales_nomina, int numero_fabricantes, double porcentaje_ganancia_disenador)
		:NUMERO_FABRICANTES(numero_fabricantes), 
       PORCENTAJE_GANANCIA_DISENADOR(porcentaje_ganancia_disenador)
	{
      if (EsCorrecto(ingresos_totales_nomina))
         ingresos_totales = ingresos_totales_nomina;
      else
         ingresos_totales = 0;	
   }

   void SetIngresosTotales(double ingresos_totales_nuevos){
      if (EsCorrecto(ingresos_totales_nuevos))
         ingresos_totales = ingresos_totales_nuevos;
      // else
      //     lanzar excepci�n
   }
   
   double Ingresos(){
      return ingresos_totales;
   }


   double SalarioBrutoDiseniador(){
      return PORCENTAJE_GANANCIA_DISENADOR * ingresos_totales;
   }

   double SalarioBrutoFabricante(){
      return (1 - PORCENTAJE_GANANCIA_DISENADOR) * ingresos_totales / NUMERO_FABRICANTES;
   }

   double SalarioNetoDisenador(double porcentaje_retencion_fiscal){
      return AplicaDisminucionPorcentaje(SalarioBrutoDiseniador(), porcentaje_retencion_fiscal);
   }

   double SalarioNetoFabricante(double porcentaje_retencion_fiscal){
      return AplicaDisminucionPorcentaje(SalarioBrutoFabricante(), porcentaje_retencion_fiscal);
   }
};

int main(){
   /*
   El programa leer� los siguientes datos desde un fichero externo:

   � El n�mero de sucursales.
   � Los siguientes valores por cada una de las sucursales:
   � Ingresos totales a repartir
   � N�mero de fabricantes
   � Parte que se lleva el dise�ador
   � Retenci�n fiscal del dise�ador
   � Retenci�n fiscal de los fabricantes
   Por ejemplo, el siguiente fichero indica que hay dos sucursales. La primera tiene
   unos ingresos de 300 euros, 3 fabricantes, el dise�ador se lleva 1/6, la retenci�n
   del dise�ador es del 20% y la de cada fabricante un 18%. Los datos para la
   segunda son 400 euros, 5 fabricantes, 1/4, 22% y 19%.
   2
   300 3 6 20 18
   400 5 4 22 19
*/

   const string MENSAJE_FABRICANTE = "\nSalario neto de cada fabricante: "; 
   const string MENSAJE_DISENIADOR = "\nSalario neto de cada dise�ador: "; 
   double ingresos, retencion_diseniador, retencion_fabricante;
	int porcentaje_ganancia_disenador;
	int numero_sucursales, numero_fabricantes;

   cin >> numero_sucursales;
   
   for (int i=0; i<numero_sucursales; i++){
      cin >> ingresos;
      cin >> numero_fabricantes;
      cin >> porcentaje_ganancia_disenador;
      cin >> retencion_diseniador;
      cin >> retencion_fabricante;
      
      Nomina_c nomina(ingresos, numero_fabricantes, 1.0/porcentaje_ganancia_disenador);  
      
      cout << "\n\nSucursal n�mero " << i+1;
      cout << "\nSalario bruto del fabricante: " << nomina.SalarioBrutoFabricante();
      cout << "\nSalario bruto del dise�ador:  " << nomina.SalarioBrutoDiseniador();
      cout << "\nSalario neto del fabricante:  " << nomina.SalarioNetoFabricante(retencion_fabricante);
      cout << "\nSalario neto del dise�ador:   " << nomina.SalarioNetoDisenador(retencion_diseniador);
   }
   
   // IMPORTANTE:
   // nomina es una variable con �mbito restringido al bucle.
   // Por tanto, cada vez que entra al bucle, se crea un nuevo objeto 
   // y se libera la zona de memoria que ocupaba el objeto de la iteraci�n anterior
   // Por tanto, la variable nomina NO exisitir� fuera del bucle
   // Si quisi�ramos guardar todos los objetos nomina, necesitar�amos un vector de Nomina
   // Se ver� en el �ltimo tema.
  
   cout << "\n\n";
	system("pause");
	
	// 2 300 3 6 20 18 400 5 4 22 19
}

