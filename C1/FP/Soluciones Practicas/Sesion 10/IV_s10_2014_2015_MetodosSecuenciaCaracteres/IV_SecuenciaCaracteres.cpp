////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

/*
   Pal�ndromo
   Modifica
   IntercambiaComponentes
   Invierte
   EliminaComponente
*/

#include <iostream>
#include <cctype>
using namespace std;

class  SecuenciaCaracteres{
private:
   static  const  int  TAMANIO  =  50;
   char  vector_privado[TAMANIO];
   int  total_utilizados;

	void IntercambiaComponentesDirectamente (int izda, int dcha){
      char intermedia;

      intermedia = vector_privado[izda];
      vector_privado[izda] = vector_privado[dcha];
      vector_privado[dcha] = intermedia;
   }

public:
   SecuenciaCaracteres()
      :total_utilizados(0)
	{
   }

   int  TotalUtilizados(){
      return  total_utilizados;
   }

   void  Aniade(char  nuevo){
      if (total_utilizados  <  TAMANIO){
         vector_privado[total_utilizados]  =  nuevo;
         total_utilizados++;
      }
   }

   char  Elemento(int  indice){
      return  vector_privado[indice];
   }

   bool EsPalindromo(){
      bool es_palindromo;
      int izda, dcha;

      es_palindromo = true;
      izda = 0;
      dcha = total_utilizados - 1;

      while (izda < dcha && es_palindromo){
         if (vector_privado[izda] != vector_privado[dcha])
            es_palindromo = false;
         else{
            izda++;
            dcha--;
         }
      }

      return es_palindromo;
		
		/*
      El siguiente bucle no funciona correctamente:
        
		for (int i=0; i<total_utilizados; i++){
			if (vector_privado[i] != vector_privado[total_utilizados- 1 - i ])
				es_palindromo = false;
			else
				es_palindromo = true;
		}

      1. No hay que llegar hasta total_utilizados sino hasta total_utilizados/2
      2. Al no salirse del bucle cuando ha encontrado dos componentes distintas,
         al final la variable es_palindromo se queda con el resultado de comparar
         las dos �ltimas posiciones centrales
      */
   }

   void Modifica (int indice_componente, char nuevo){
      if (indice_componente >= 0  &&  indice_componente < total_utilizados)
         vector_privado[indice_componente] = nuevo;
   }

	/*
		void IntercambiaComponentes (int izda, int dcha){
			char intermedia;

			if (izda >= 0  &&  izda < dcha  &&  dcha < total_utilizados){
				intermedia = vector_privado[izda];
				vector_privado[izda] = vector_privado[dcha];
				vector_privado[dcha] = intermedia;
			}
		}

		void Invierte(){
			int izda, dcha;

			izda = 0;
			dcha = total_utilizados - 1;

			while (izda < dcha){
				IntercambiaComponentes(izda, dcha);
				izda++;
				dcha--;
		}
	*/
	/*
		IntercambiaComponentes, al ser p�blico, comprueba que el acceso a las componentes es correcto.
		Esto supone que Invierte, que llama a IntercambiaComponentes ser� poco eficiente.
		Para resolverlo:
			- O bien intercambiamos directamente dentro de Invierte sin llamar a IntercambiaComponentes
			- O bien declaramos un m�todo privado IntercambiaComponentesDirectamente que no realice
			  la comprobaci�n de las condiciones y cambiamos las implementaci�n de IntercambiaComponentes
			  para que realice la comprobaci�n de las condiciones y luego llame a IntercambiaComponentesDirectamente

		*/

	void IntercambiaComponentes (int izda, int dcha){
      if (izda >= 0  &&  izda < dcha  &&  dcha < total_utilizados)
         IntercambiaComponentesDirectamente (izda, dcha);
	}

   void Invierte(){
		int izda, dcha;

		izda = 0;
		dcha = total_utilizados - 1;

		while (izda < dcha){
			IntercambiaComponentesDirectamente(izda, dcha);
			izda++;
			dcha--;
		}


		/*
		// �Qu� har�a el siguiente c�digo?

		for (int i=0; i < total_utilizados; i++)
			IntercambiaComponentes (i, total_utilizados - i - 1);
		*/
	}


	
	/*
   Tipos de borrados:
      - L�gico
         Usar un valor de componente especial y marcar la componente con dicho valor
         Un vector de edades -> valor -1
         Un vector de caracteres alfab�ticos -> '@'
         Ventajas: Muy r�pido
         
         Inconvenientes: Cualquier procesado posterior del vector
         debe tratar las componentes marcadas de una forma especial

      - F�sico
         Implica desplazar 1 posici�n a la izquierda, todas las componentes que hay a la derecha de
			la que queremos borrar.
         
         Tiene justo las ventajas e incovenientes contrarias que el m�todo anterior.

       En este ejercicio, implementamos el borrado f�sico.
	*/


   // Elimina una componente, dada por su posici�n
	void Elimina (int posicion){
      /*
      Algoritmo:

		   Recorremos de izquierda a derecha toda las componentes
		   que hay a la derecha de la posici�n a eliminar
			   Le asignamos a cada componente la que hay a su derecha
      */
      if (posicion >= 0 && posicion < total_utilizados){
         int tope = total_utilizados-1;

         for (int i = posicion ; i < tope ; i++)         // i < tope = total_utilizados-1 ya que la �ltima asignaci�n  ultimo <- ? no es necesaria
            vector_privado[i] = vector_privado[i+1];

         total_utilizados--;
      }

      // Nota:

      // En vez de usar la asignaci�n
      //    vector_privado[i] = vector_privado[i+1];
      // tambi�n podr�amos haber puesto lo siguiente:
      //    Modifica(i, Elemento(i+1));
      // Hemos preferido acceder directamente a las componentes con la notaci�n en corchete
      // para aumentar la eficiencia del m�todo Elimina, ya que si el vector es muy grande
      // tendr� que realizar muchos desplazamientos.

      // En general, desde dentro de la clase, los m�todos de la clase Secuencia
      // acceder�n directamente a las componentes con la notaci�n corchete

      // Adem�s, cuando entramos en la funci�n Elimina, comprobamos con el condicional
      // que los accesos a los �ndices son correctos.
      // Si usamos el m�todo Modifica, volver�amos a comprobar lo mismo.
      
      // Nota:
      
      // �Y si en vez de asignar vector_privado[i] = vector_privado[i+1];
      // llamamos a IntercambiaComponentesDirectamente(i, i+1) ?
      // La componente se eliminar�a pero realizando el doble de asignaciones
      // Obviamente, no es necesario intercambiar las componentes.
      // �nicamente debemos ir asignando v[i] = v[i+1] de izquierda a derecha.
	}
};

int main(){
   SecuenciaCaracteres cadena;
   int tope;
   bool es_palindromo;

	
   cadena.Aniade('H');
   cadena.Aniade('o');
   cadena.Aniade('o');
   cadena.Aniade('H');

   tope = cadena.TotalUtilizados();

   for (int i = 0; i < tope; i++)
      cout  << cadena.Elemento(i) << " ";

   es_palindromo = cadena.EsPalindromo();

   if (es_palindromo)
      cout << "\nEs un pal�ndromo";
   else
      cout <<"\nNo es un pal�ndromo";
	

   /////////////////////////////////////////////////////////////////

	
   cadena.Modifica(-1,'A');   // <- No modificada nada :-)
   cadena.Modifica(5,'A');    // <- No modificada nada :-)
   cadena.Modifica(1,'A');
   tope = cadena.TotalUtilizados();

   cout << "\n";

   for (int i = 0; i < tope; i++)
      cout  << ">" << cadena.Elemento(i) << "<";
	

   /////////////////////////////////////////////////////////////////

	
   cadena.IntercambiaComponentes(1,3);
   tope = cadena.TotalUtilizados();

   cout << "\n";

   for (int i = 0; i < tope; i++)
      cout  << cadena.Elemento(i) << " ";

   cadena.Invierte();
   tope = cadena.TotalUtilizados();

   cout << "\n";

   for (int i = 0; i < tope; i++)
      cout  << cadena.Elemento(i) << " ";
	

   cout << "\n\n";
   system ("pause");
}

