////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

// Erat�stenes

/*
(Examen Septiembre 2012) La criba de Erat�stenes (Cirene, 276 a. C. Alejandr�a,
194 a. C.) es un algoritmo que permite hallar todos los n�meros primos menores que
un n�mero natural dado n.

El procedimiento �manual� consiste en escribir todos los n�meros naturales compren
didos entre 2 y n y tachar los n�meros que no son primos de la siguiente manera: el
primero (el 2) se declara primo y se tachan todos sus m�ltiplos; se busca el siguiente
n�mero entero que no ha sido tachado, se declara primo y se procede a tachar todos
sus m�ltiplos, y as� sucesivamente. El proceso para cuando el cuadrado del n�mero
entero es mayor o igual que el valor de n.

El programa debe definir una clase llamada Eratostenes que tendr� tres m�todos:

� void CalculaHasta(int n) para que calcule los primos menores que n.
� int TotalCalculados() que devuelva cu�ntos primos hay actualmente almacenados.
� int Elemento(int indice) para que devuelva el indice-�simo primo.

El programa principal quedar�a de la forma:

Eratostenes primos;
int n = 100; 
int tope;

primos.CalculaHasta(n);
tope = primos.TotalCalculados();

for (int i=0; i<tope; i++)
	cout << primos.Elemento(i) << " ";

	Para almacenar los primos, se usar� un dato miembro de tipo vector.
*/

#include <iostream>
#include <cmath>

using namespace std;

class Eratostenes{
private:
   static const int MAXIMO = 100;
	int primos[MAXIMO];
	int numero_de_primos;

public:
   Eratostenes()
      :numero_de_primos(0)
   {         
   }
   
	// Calcula los primos menores o iguales que n
	void CalculaHasta(int n){
		/*
      Versi�n ineficiente:

      Guardar todos los enteros menores que tope en un vector
      Recorrer todos los enteros -i- de dicho vector
         Recorrer todos los enteros -j- mayores que i
            Si j es m�ltiplo de i, borrarlo f�sicamente, desplazando
            hacia la izquierda todas las componentes que hay a su derecha.
		*/

		/*
      Versi�n eficiente:

      Alternativa A. Usando un vector de enteros.
         Guardamos en un vector de enteros todos los enteros menores o iguales que n         
         "Tachar" significar� que pondremos a -1 la posici�n correspondiente.

      Alternativa B. Usamos un vector de bool.
         Si un entero es primo, su correspondiente posici�n en el vector de bool ser� true.
         
			Como el primer primo es 2 y los vectores tienen �ndice 0, la posici�n dentro del vector
         que corresponde a un entero ser� (entero - 2).         
			Para no tener que ir repitiendo esta cuenta (entero - 2), podemos hacer una correspondencia
			m�s intuitiva  entero == �ndice. El precio a pagar es que 
			las dos primeras no las usaremos (corresponden al 0 y al 1)

         "Tachar" significar� que pondremos a false la posici�n correspondiente.
         Si queremos guardar los primos en un vector, basta con recorrer posteriormente los no tachados.

      Tanto si seguimos la alternativa A, como la alternativa B, 
      la descripci�n del algoritmo es la misma:

      Recorremos todos los enteros hasta llegar a uno cuyo cuadrado sea mayor que n
         Si el entero no est� tachado
            Tachamos todos sus m�ltiplos 
            (el primer m�ltiplo es 2*entero y los siguientes se calculan sumando el entero sucesivamente)          

      Implementamos la alternativa B). La alternativa A) es similar.
		*/

	
		// Alternativa B:
		int primos_local[MAXIMO];	
      double tope; 
     
      // A�adimos 0 y 1 aunque no lo vayamos a usar
      // Inicializamos bastantes m�s valores de los que luego se usar�n.
       
		for (int i=0; i<=n; i++)   
			primos_local[i] = i;   
      
      tope = sqrt(n);
      			   
		for (int entero = 2; entero < tope; entero++){			
			if (primos_local[entero] != -1){

				for (int a_tachar = 2*entero; a_tachar <= n; a_tachar = a_tachar + entero)
					primos_local[a_tachar] = -1;
			}
		}

      
		for (int i=2; i<=n; i++)         // Como ya dijimos, los dos primeros no se usan
			if (primos_local[i] != -1){
				primos[numero_de_primos] = i;
				numero_de_primos++;
		   }	

		/*
		// Alternativa A:
		bool es_primo[MAXIMO];	 
      double tope; 

		for (int i=0; i<=n; i++)   
			es_primo[i] = true;    
      
      tope = sqrt(n);
    
		for (int entero = 2; entero < tope; entero++){			
			if (es_primo[entero]){

				for (int a_tachar = 2*entero; a_tachar <= n; a_tachar = a_tachar + entero)
					es_primo[a_tachar] = false;
			}
		}
      
      int ultimo_indice = 2;

		for (int i=2; i<=n; i++)   // Como ya dijimos, los dos primeros no se usan
			if (es_primo[i]){
				primos[numero_de_primos] = i;
				numero_de_primos++;
			}
		*/
	}

	// Devuelve cu�ntos primos hay actualmente almacenados
	int TotalCalculados(){
		return numero_de_primos;
	}

	// Devuelve el k-�simo primo
	int k_esimo(int k) {
		return primos[k];
	} 
};


int main(){
	int tope;
	int total_generados;
	Eratostenes primos;

	cout << "\nIntroduzca un entero: ";
	cin >> tope;

	primos.CalculaHasta(tope);
	total_generados = primos.TotalCalculados();

	for (int i=0; i<total_generados; i++)
		cout << primos.k_esimo(i) << " ";
}
