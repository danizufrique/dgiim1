////////////////////////////////////////////////////////////////////////////
//
// Fundamentos de Programaci�n
// ETS Inform�tica y Telecomunicaciones
// Universidad de Granada
// Departamento de Ciencias de la Computaci�n e Inteligencia Artificial
// Autor: Juan Carlos Cubero
//
////////////////////////////////////////////////////////////////////////////

// M�ximo de los m�nimos
// MaxiMin

/*
En este ejercicio, no hay que construir ninguna clase ni
funci�n. Es un ejercicio sobre recorridos de una matriz.
Leed desde teclado dos variables util_filas y util_columnas y leed los datos
de una matriz de enteros de tama�o util_filas x util_columnas. Sobre dicha
matriz, se pide lo siguiente:

a) Calcular la traspuesta de la matriz, almacenando el resultado en otra matriz.

b) (Examen Septiembre 2011) La posici�n de aquel elemento que sea el mayor de
entre los m�nimos de cada fila. Por ejemplo, dada la matriz M (3 � 4),
9 7 4 5
2 18 2 12
7 9 1 5
el m�ximo entre 4, 2 y 1 (los m�nimos de cada fila) es 4 y se encuentra en la
posici�n (0,2).

c) Ver si existe un valor MaxiMin, es decir, que sea a la vez, m�ximo de su fila y
m�nimo de su columna.

d) Leer los datos de otra matriz y multiplicar ambas matrices (las dimensiones de la
segunda matriz han de ser compatibles con las de la primera para poder hacer la
multiplicaci�n)
*/

#include <iostream>
using namespace std;

int main(){
	const int MAX_FIL = 10, MAX_COL = 10;
	double matriz[MAX_FIL][MAX_COL];
	double traspuesta[MAX_COL][MAX_FIL];
	
	int util_filas, util_columnas;
   double maximo, minimo;
	int fila_max_min, col_max_min;
	int columna_minimo;

	// No etiquetamos las entradas porque suponemos que leemos de un fichero
	// Supondremos que util_filas y util_columnas son > 0

	cin >> util_filas;
	cin >> util_columnas;

	for (int i=0; i<util_filas; i++)
		for (int j=0; j<util_columnas; j++)
			cin >> matriz[i][j];
			
   ///////////////////////////////////////////////////////////////////////////
   // Traspuesta
   
   int util_fil_traspuesta, util_col_traspuesta;
   
   util_fil_traspuesta = util_columnas;
   util_col_traspuesta = util_filas;
   
   for (int i=0; i<util_fil_traspuesta; i++)
      for (int j=0; j<util_col_traspuesta; j++)
         traspuesta[i][j] = matriz[j][i];
   
   cout << "\n\n";
   cout << "Matriz traspuesta:\n";   
    
   for (int i=0; i<util_fil_traspuesta; i++){
      cout << "\n";
      
      for (int j=0; j<util_col_traspuesta; j++)
         cout << traspuesta[i][j] << " ";
   }  
   
   
   ///////////////////////////////////////////////////////////////////////////
	// M�ximo de los m�nimos

	/*
		Algoritmo (M�ximo de los m�nimos de las filas de una matriz)
		
		Inicializar Max al m�nimo de la primera fila
		Recorrer el resto de filas -i-
			Calcular el m�nimo de la fila i
			y actualizar, en su caso, el m�ximo
	*/

	minimo = matriz[0][0];
	columna_minimo = 0;
	
	for (int j=0; j<util_columnas; j++)
		if (matriz[0][j] < minimo){
			minimo = matriz[0][j];
			columna_minimo = j;
		}

   maximo = minimo;
	fila_max_min = 0;
	col_max_min = columna_minimo;

   for (int i=1 ; i<util_filas ; i++){
		minimo = matriz[i][0];
		columna_minimo = 0;

      for (int j=0; j<util_columnas; j++){
			if (matriz[i][j] < minimo){
				minimo = matriz[i][j];
				columna_minimo = j;
			}
		}

		if (minimo > maximo){
			maximo = minimo;
			fila_max_min = i;
			col_max_min	 = columna_minimo;
		}
   }

	cout << "\n\nMaxiMin (primer algoritmo):\nFila: " << fila_max_min <<
		"\nColumna: " << col_max_min << "\nValor: " << maximo;

	/*
		El anterior algoritmo se puede mejorar:
		Si durante el recorrido de una fila f encontramos un elemento
		que es menor que el m�ximo del resto de las filas anteriores,
		el m�nimo de f ser� menor que dicho m�ximo por lo que
		�ste no cambiar�. Nos quedar�a:

		Algoritmo (M�ximo de los m�nimos de las filas de una matriz. Versi�n mejorada):
	
			Inicializar Max al m�nimo de la primera fila
			Recorrer el resto de filas -i-
				Calcular el m�nimo de la fila i
				saliendo del recorrido cuando encontremos
				un valor menor que Max
				Actualizar, en su caso, Max
	*/

	bool es_mayor_igual_que_Max;

	minimo = matriz[0][0];
	columna_minimo = 0;
	
	for (int j=0; j<util_columnas; j++)
		if (matriz[0][j] < minimo){
			minimo = matriz[0][j];
			columna_minimo = j;
		}

   maximo = minimo;
	fila_max_min = 0;
	col_max_min = columna_minimo;

   for (int i=1 ; i<util_filas ; i++){
		minimo = matriz[i][0];
		columna_minimo = 0;
		es_mayor_igual_que_Max = true;

      for (int j=0; j<util_columnas && es_mayor_igual_que_Max; j++){
         double dato_matriz = matriz[i][j];
         
			if (dato_matriz < maximo)
				es_mayor_igual_que_Max = false;
			else{
				if (dato_matriz < minimo){
					minimo = matriz[i][j];
					columna_minimo = j;
				}
			}
		}

		if (minimo > maximo && es_mayor_igual_que_Max){
			maximo = minimo;
			fila_max_min = i;
			col_max_min	 = columna_minimo;
		}
   }

	cout << "\n\nMaxiMin (segundo algoritmo):\nFila: " << fila_max_min <<
		"\nColumna: " << col_max_min << "\nValor: " << maximo;


   ///////////////////////////////////////////////////////////////////////////
   // Multiplicaci�n
   
   const int MAX_FIL_DCHA = MAX_COL, MAX_COL_DCHA = 30;
	double matriz_dcha[MAX_FIL_DCHA][MAX_COL_DCHA];
	double matriz_producto[MAX_FIL][MAX_COL_DCHA];

	int util_filas_dcha, util_columnas_dcha;
	int util_filas_producto, util_columnas_producto;

   util_filas_dcha = util_columnas;
   cin >> util_columnas_dcha;
      
	for (int i=0; i<util_filas_dcha; i++)
		for (int j=0; j<util_columnas_dcha; j++)
			cin >> matriz_dcha[i][j];
   
   util_filas_producto = util_filas;
   util_columnas_producto = util_columnas_dcha;
   
   for (int i=0; i<util_filas_producto; i++)
		for (int j=0; j<util_columnas_producto; j++){
         matriz_producto[i][j] = 0;
         
         for (int k=0; k<util_columnas_producto; k++)
            matriz_producto[i][j] += matriz[i][k] * matriz_dcha[k][j];
            // suma += b es la forma abreviada de:
            // suma = suma + b.
            // En este ejemplo: 
            // matriz_producto[i][j] = matriz_producto[i][j]  + matriz[i][k] * matriz_dcha[k][j];
      }  
   
   cout << "\n\n";
   cout << "Matriz producto:\n";
   
   for (int i=0; i<util_filas_producto; i++){
      cout << "\n";
      
      for (int j=0; j<util_columnas_producto; j++)
         cout << 	matriz_producto[i][j] << " ";
   }  
  
   
   cout << "\n\n";
	system("pause");
	
	// 3 4    9 7 4 5    2 18 2 12    7 9 1 5      2      1 2   3 4   5 6   7 8
}
