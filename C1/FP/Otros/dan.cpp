#include <iostream>
using namespace std;


int main(){
	char palabra[3];
	char texto[1000];
	char caracter;
	int longitud = 0;
	
	int contador = 0;
	int contadorLongitud = 0, contadorRecorrido = 0;
	bool encontrado;
	int coincidencias = 0;
	
	//ENTRADA DE DATOS
	cout << "Introduce la palabra a buscar: ";
	for (int i=0; i<3; i++){
		cin >> palabra[i];
	}
	
	cout << "Introduce el texto donde buscar:";
	cin >> caracter;
	
	while(caracter != '#'){
		texto[contador] = caracter;
		contador++;
		longitud++;
		cin >> caracter;
	}
	
	//OPERACIONES
		
	while(contadorLongitud<3){
		contadorRecorrido = 0;
		encontrado = false;
		while(encontrado == false && contadorRecorrido<longitud){
			if(palabra[contadorLongitud] == texto[contadorRecorrido]){
				encontrado = true;
				coincidencias++;
			}
			else{
				contadorRecorrido++;			
			}
		}
		contadorLongitud++;
	}
	
	//RESULTADO
	if(coincidencias == 3) {cout << "La palabra se ha podido encontrar";}
	else{cout << "La palabra no se ha podido encontrar";}
}
