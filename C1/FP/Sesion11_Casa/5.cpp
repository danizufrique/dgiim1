#include <iostream>
#include <iomanip>
using namespace std;

class Libro{
	private:		
		string titulo;
		string autor;
		string editorial;
		int anio;
		
	public:
		//Constructores
		Libro(string tit, string aut, string edit, int a){
			titulo = tit;
			autor = aut;
			editorial = edit;
			anio = a;
		}
		
		Libro(){
			titulo = "---";
			autor = "---";
			editorial = "---";
			anio = 0;	
		}
		
		//Mostrar
		void MostrarDatos(){
			cout << "\n\n- Titulo: " << titulo;
			cout << "\n- Autor:  " << autor;
			cout << "\n- Editorial:  " << editorial;
			cout << "\n- Anio:  " << anio;
		}
		
		//Set
		void SetDatos(){
			cout << "\nIntroduce el titulo del libro: ";
			getline(cin, titulo);
			cout << "\nIntroduce el autor: ";
			getline(cin, autor);
			cout << "\nIntroduce la editorial: ";
			getline(cin, editorial);
			cout << "\nIntroduce el anio de publicacion: ";
			cin >> anio;
		}
		
		//Get
		string GetTitulo(){
			return titulo;
		}
		
		string GetAutor(){
			return autor;
		}
		
		string GetEditorial(){
			return editorial;
		}
		
		int GetAnio(){
			return anio;
		}
};

void Comparar(Libro libro1, Libro libro2){
	cout << "\n\n";
    cout << setw(10) << "DATO" << setw(30) << "LIBRO 1" << setw(30) << "LIBRO 2" << endl << endl;
    cout << setw(10) << "Titulo" << setw(30) << libro1.GetTitulo() << setw(30) << libro2.GetTitulo() << endl;
    cout << setw(10) << "Autor" << setw(30) << libro1.GetAutor() << setw(30) << libro2.GetAutor() << endl;
    cout << setw(10) << "Editorial" << setw(30) << libro1.GetEditorial() << setw(30) << libro2.GetEditorial() << endl;
    cout << setw(10) << "Anio" << setw(30) << libro1.GetAnio() << setw(30) << libro2.GetAnio() << endl;
}



int main(){
	Libro libro1{"El Guardian Entre el Centeno", "J. D. Salinger", "Little, Brown and Company", 1951};
	Libro libro2;
	
	//Pide al usuario que cree un nuevo libro
	libro2.SetDatos();
	
	//Muestra ambos libros
	libro1.MostrarDatos();
	libro2.MostrarDatos();
	
	//Compara ambos libros;
	Comparar(libro1, libro2);
}
