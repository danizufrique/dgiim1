#include <iostream>
using namespace std;

class Conjunto{
	private:
		static const int MAXELEM = 100;
		int num_elem;
		int elementos[MAXELEM];
		
	public:
		//Constructor
		Conjunto(){
			num_elem = 0;
		}
		
		Conjunto(int array[MAXELEM], int longitud){
			bool ordenado;
			bool noRep;
			
			num_elem = longitud;
			
			for(int i=0; i<longitud; i++){
				elementos[i] = array[i];
			}
			
			ordenado = Ordenado();
			
			if(!ordenado){
				cout << "\nAVISO: Has creado un conjunto cuyos elementos no estan ordenados y se corregira automaticamente.";
				Ordenar();
			}
			
			noRep = NoRepeticion();
			
			if(!noRep){
				cout << "\nAVISO: Has creado un conjunto que tiene elementos repetidos y se corregira automaticamente..";
				EliminarRepetidos();
			}
		}
		
		//Mostrar
		void Mostrar(string nombre){
			cout << "\nEl vector " << nombre << " es: (";
			cout << elementos[0];
			for (int i=1; i<num_elem-1; i++){
				cout << ", " << elementos[i];
			}
			if(num_elem>1){
				cout << ", " << elementos[num_elem-1];	
			}
			cout << ")";		
		}
		
		//Buscar elemento en conjunto
		bool Buscar(int elemento){
			bool encontrado = false;
			
			for (int i=0; i<num_elem && encontrado == false; i++){
				if (elementos[i] == elemento){
					encontrado = true;
				}
			}
			
			return encontrado;
		}
		
		//Ordenados
		bool Ordenado(){
			bool ordenado = true;
			
			for (int i=1; i<num_elem && ordenado == true; i++){
				if(elementos[i-1] > elementos[i]){
					ordenado = false;
				}
			}
			
			return ordenado;
		}
		
		//5 2 4 3
		//Ordenar
		void Ordenar(){
			float temporal;
			
			for (int i=0; i<num_elem; i++){
				for (int j = 0; j< num_elem-1; j++){
					if (elementos[j] > elementos[j+1]){
						temporal = elementos[j]; 
						elementos[j] = elementos[j+1]; 
						elementos[j+1] = temporal;
					}
				}
			}
		}
		
		//No repetido
		bool NoRepeticion(){
			bool correcto = true;
			
			for (int i=1; i<num_elem && correcto == true; i++){
				if(elementos[i-1] == elementos[i]){
					correcto = false;
				}
			}
			
			return correcto;		
		}
		//A�adir elemento
		void Aniadir(int elemento){
			bool aniadido = false;
			bool presente;
			
			presente = Buscar(elemento);
			
			if(!presente){
				for (int i=0; i<num_elem && aniadido == false; i++){
					if(elementos[i] > elemento){
						for (int j=num_elem; j>=i; j--){
							elementos[j+1] = elementos[j];
						}
						elementos[i] = elemento;
						num_elem++;
						aniadido = true;
					}
				}
				
				if(!aniadido){				
					elementos[num_elem] = elemento;
					num_elem++;
				}									
			}
			else{
				cout << "\nEl elemento ya esta en el conjunto.";
			}

		}
		
		//Eliminar elemento
		void Eliminar(int posicion){
			if(posicion < num_elem && posicion>0){
				for (int i=posicion; i<num_elem; i++){
					elementos[i] = elementos[i+1];
				}
				num_elem--;			
			}
			else{
				cout << "Posicion no valida.";
			}

		}
		
		//Union
		Conjunto Union(Conjunto conjunto2){
		    Conjunto resultado;
		    bool pertenece;
		    resultado.num_elem = num_elem + conjunto2.num_elem;
		    
		    int i = 0;
			int j = 0;
			int contador = 0;
		    
		    while (i < num_elem || j < conjunto2.num_elem){
		        if (i < num_elem && j < conjunto2.num_elem){
		            if (elementos[i] < conjunto2.elementos[j]){
		                resultado.elementos[contador] = elementos[i];
		                i++;
		            } else {
		                resultado.elementos[contador] = conjunto2.elementos[j];
		                j++;
		            }
		            
		        }
		        else if (i < num_elem){
		            resultado.elementos[contador] = elementos[i];
		            i++;
		        } else {
		            resultado.elementos[contador] = conjunto2.elementos[j];
		            j++;
		        }
		        contador++;
		    }
		    
		    resultado.EliminarRepetidos();
		    
		    return resultado;
		}
		
		//Interseccion
		Conjunto Interseccion(Conjunto conjunto2){
		    Conjunto resultado;
		    
		    bool eliminados[MAXELEM];
		    bool pertenece = false;
		    int contador = 0;
		    
		    for (int i = 0; i < conjunto2.num_elem; i++){
		        eliminados[i] = false;
		    }
		    
		    for (int i = 0; i < num_elem; i++){
		        pertenece = false;
		        for (int j = 0; j < conjunto2.num_elem && !pertenece; j++){
		            if (!eliminados[j] && elementos[i] == conjunto2.elementos[j]){
		                pertenece = true;
		                eliminados[j] = true;
		                resultado.elementos[contador] = elementos[i];
		                contador++;
		            }
		        }
		    }    
		    resultado.num_elem = contador;
		    
		    return resultado;
		}
		
		//Eliminar Repetidos (Suponiendo que este ordenado)
		void EliminarRepetidos(){
			for (int i=0; i<num_elem-1; i++){
				if(elementos[i] == elementos[i+1]){
					Eliminar(elementos[i+1]);
				}
			}
		}
};

int main(){
	const int MAXELEM = 100;
	int array1[MAXELEM]={5,2,4,3};
	int array2[MAXELEM]={0,2,4,5,8};
	
	Conjunto c1{array1, 4};
	Conjunto c2{array2, 5};
	Conjunto cUnion;
	Conjunto cInter;
	
	c1.Ordenar();
	//C�lculo
	cUnion = c1.Union(c2);
	cInter = c1.Interseccion(c2);
	
	//Salida
	c1.Mostrar("1");
	c2.Mostrar("2");	
	cUnion.Mostrar("union");
	cInter.Mostrar("interseccion");
}
