#include <iostream>
using namespace std;

class Fecha{
	private:
		int dia;
		int mes;
		int anio;
		
	public:
		//Constructores
		Fecha (int a, int b, int c){
			dia = a;
			mes = b;
			anio = c;
		}
		
		Fecha(){
			dia = 1;
			mes = 1;
			anio = 0;
		}
		
		//Set
		void SetDia(int a){
			dia = a;
		}
		
		void SetMes(int a){
			mes = a;
		}
		
		void SetAnio(int a){
			anio = a;
		}
		
		void SetFecha(int a, int b, int c){
			dia = a;
			mes = b;
			anio = c;
		}
		
		//Mostrar
		void Mostrar(string nombre){
			cout << "\nLa fecha " << nombre << " es: " << dia << "/" << mes << "/" << anio;
		}
		
		//Fecha Valida
		bool FechaValida(){
			bool resultado;
			
			//Mes y d�a v�lidos
			if((0<mes && mes<=13) && dia>0){
				//Meses con 31 d�as
				if(mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12){
					if(dia <= 31){
						resultado = true;
					}
					else{
						resultado = false;
					}
				}
				//Mes con 28 o 29 d�as
				else if(mes == 2){
					//A�o bisiesto
					if((anio%400 == 0 || anio%4 == 0) && dia <= 29){
						resultado = true;
					}
					else if(dia <= 28){
						resultado = true;
					}
					else{
						resultado = false;
					}
				}
				//Meses con 30 d�as
				else{
					if(dia <= 30){
						resultado = true;
					}
					else {
						resultado = false;
					}
				}
			}
			else{
				resultado = false;
			}
			
			return resultado;
		}
		
		//Dia siguiente		
		void Siguiente(){
			bool valido = FechaValida();
			
			if (valido){
			
				//Meses con 31 d�as
				if(mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10){
					if(dia == 31){
						dia = 1;
						mes += 1;						
					}
					else{
						dia += 1;
					}
				}
				
				else if(mes == 12){
					if(dia == 31){
						dia = 1;
						mes = 1;
						anio++;
					}
				}
				
				//Mes con 28 o 29 d�as
				else if(mes == 2){
					//A�o bisiesto
					if((anio%400 == 0 || anio%4 == 0) && dia == 29){
						dia = 1;
						mes += 1;
					}
					else if (dia == 28){
						dia = 1;
						mes += 1;
					}
					else{
						dia += 1;
					}
				}
				//Meses con 30 d�as
				else{
					if(dia == 30){
						dia = 1;
						mes += 1;
					}
					else {
						dia += 1;
					}
				}
				
				Mostrar("");
			}
			
			else{
				cout << "\nLa fecha no era valida.";
			}
		}
		
		//Dia anterior
		void Anterior(){
			bool valido = FechaValida();
			
			if (valido){
			
				//Meses cuyo mes previo tiene 31 d�as
				if(mes == 2 || mes == 4 || mes == 6 || mes == 9 || mes == 11){
					if(dia == 1){
						dia = 31;
						mes -= 1;
					}
					else{
						dia -= 1;
					}
				}
				//Mes con mes previo a Febrero
				else if(mes == 3){
					//A�o bisiesto
					if((anio%400 == 0 || anio%4 == 0) && dia == 1){
						dia = 29;
						mes -= 1;
					}
					else if (dia == 1){
						dia = 28;
						mes -= 1;
					}
					else{
						dia -= 1;
					}
				}
				//Meses cuyo mes previo tiene 30 d�as
				else if(mes == 3 || mes == 5 || mes == 7 || mes == 10 || mes == 12){
					if(dia == 1){
						dia = 30;
						mes -= 1;
					}
					else {
						dia -= 1;
					}
				}
				//Es Enero
				else{
					if(dia == 1){
						dia = 31;
						mes = 12;
						anio -= 1;
					}
				}
				
				Mostrar("");
			}
			
			else{
				cout << "\nLa fecha no era valida.";
			}
		}
	};

int main(){
	Fecha fecha1{31,12,1990};
	Fecha fecha2{1,1,1992};
	bool valida1, valida2;
	int dia, mes, anio;
	
	valida1 = fecha1.FechaValida();
	valida2 = fecha2.FechaValida();
	
	cout << "__MOSTRAR FECHAS__";
	fecha1.Mostrar("1");
	fecha2.Mostrar("2");
	
	cout << "\n\n__COMPROBAR SI FECHAS CON VALIDAS__";
	valida1 ? cout << "\nLa primera fecha es valida" : cout << "\nLa primera fecha no es valida";
	valida2 ? cout << "\nLa segunda fecha es valida" : cout << "\nLa segunda fecha no es valida";
	
	cout << "\n\n__CALCULAR DIA SIGUIENTE DE LA PRIMERA__";
	fecha1.Siguiente();
	
	cout << "\n\n__CALCULAR DIA ANTERIOR DE LA SEGUNDA";
	fecha2.Anterior();
	
	cout << "\n\n__CAMBIAR LA PRIMERA FECHA__";
	
	cout << "\nIntroduce el dia, mes y anio: ";
	cin >> dia >> mes >> anio;
	fecha1.SetFecha(dia, mes, anio);
	fecha1.Mostrar("1 cambiada");
	
}
