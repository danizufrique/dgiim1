#include <iostream>
using namespace std;


char Minuscula(char caracter){
	const int MAY_MIN = 'a'-'A';
	
	if('A' <= caracter && caracter <= 'Z'){
		caracter += MAY_MIN;
	}
	
	return caracter;
}


int main(){
	char caracter;
	
	cout << "Introduce el caracter: ";
	cin >> caracter;
	
	caracter = Minuscula(caracter);
	cout << "El caracter resultado es: " << caracter;
}
