#include <iostream>
using namespace std;

int MCM(int a, int b){
	int mayor;
	int multiplo;
	
	if(a>b){
		mayor = a;
	}
	else{
		mayor=  b;
	}
	
	multiplo = mayor;
	
	while(multiplo != a*b){
		multiplo++;
	}
	
	return multiplo;
}


int main(){
	int a,b;
	int resultado;
	
	cout << "Introduce 2 valores: ";
	cin >> a >> b;
	
	resultado = MCM(a,b);
	
	cout << "El MCM de " << a << " y " << b << " es: " << resultado;
}
