#include <iostream>
using namespace std;

int FechaValida(int dia, int mes, int anio){
	bool resultado;
	
	//Mes y d�a v�lidos
	if((0<mes && mes<=13) && dia>0){
		//Meses con 31 d�as
		if(mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 10){
			if(dia == 31){
				resultado = true;
			}
			else{
				resultado = false;
			}
		}
		//Mes con 28 o 29 d�as
		else if(mes == 2){
			//A�o bisiesto
			if((anio%400 == 0 || anio%4 == 0) && dia <= 29){
				resultado = true;
			}
			else if(dia <= 28){
				resultado = true;
			}
			else{
				resultado = false;
			}
		}
		//Meses con 30 d�as
		else{
			if(dia <= 30){
				resultado = true;
			}
			else {
				resultado = false;
			}
		}
	}
	else{
		resultado = false;
	}
	
	return resultado;
}

int main(){
	int dia, mes, anio;
	bool valido;
	
	cout << "Introduce la fecha (dd/mm/aaaa): ";
	cin >> dia >> mes >> anio;
	
	valido = FechaValida(dia, mes, anio);
	
	if(valido){
		cout << "La fecha es valida.";
	}
	else{
		cout << "La fecha no es valida.";
	}
}
