#include <iostream>
using namespace std;


int SumaDivisores(int a){
	//Si un numero menor o igual que su mitad es divisor se suma al resultado
	int resultado = 0;
	
	for(int i=1; i<=a/2; i++){
		if(a%i == 0){
			resultado += i;
		}
	}
	
	return resultado;
}

bool Amigos(int a, int b){
	int divisoresA, divisoresB;
	bool amigos;
	
	divisoresA = SumaDivisores(a);
	divisoresB = SumaDivisores(b);
	
	amigos = (divisoresA == b && divisoresB == a) && a!=b;
	
	return amigos;
}


int main(){
	int a,b;
	bool amigos;
	
	cout << "Introduce 2 valores: ";
	cin >> a >> b;
		
	for(int j=a; j<b; j++){
		for (int i=j; i<b; i++){
			amigos = Amigos(j, i);
			if(amigos){
				cout << "Los numeros " << j << " y " << i << " del intervalo son amigos.\n";
			}
		}
	}
}
