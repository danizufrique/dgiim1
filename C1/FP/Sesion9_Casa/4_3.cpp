#include <iostream>
using namespace std;


bool Confirmar(){
	char respuesta;
	bool valido = false;
	bool confirmacion;
	
	while (valido == false){
		cout << "┐Confirmar (S/N)? ";
		cin >> respuesta;
		
		
		if (respuesta == 'S' || respuesta == 's'){
			valido = true;
			confirmacion = true;
		}
		else if (respuesta == 'N' || respuesta == 'n'){
			valido = true;
			confirmacion = false;
		}
	}
	
	return confirmacion;
}


int main(){
	bool confirmacion;
	
	confirmacion = Confirmar();
	cout << confirmacion;
}
