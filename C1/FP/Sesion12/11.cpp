#include <iostream>
#include <vector>
using namespace std;

class Bicicleta{
	private:
		int estrella, maxEstrella;
		int pinon, maxPinon; 
	
	public:
		//Constructor
		Bicicleta(){
			estrella = 1;
			pinon = 1;
			maxEstrella = 3;
			maxPinon = 7;
		}
		
		//Mover
		void MoverEstrella(int n){
			if(n == 1 && (estrella+n <= maxEstrella)){
				estrella += n;
			}
			else if(n == -1 && estrella+n>0){
				estrella += n;
			}
		}
		
		void MoverPinon(int n){
			if((n==1 || n==2) && pinon+n <= maxPinon){
				pinon += n;
			}
			else if((n==-1 || n==-2) && pinon+n>0){
				pinon += n;
			}
		}
		
		bool Seguro(char letra, int n){
			bool resultado = true;
			
			if(letra == 'E'){
				if(estrella+n==1 && pinon>=5){
					resultado = false;
				}
				else if(estrella+n==2 && (pinon==1 || pinon==7)){
					resultado = false;
				}
				else if(estrella+n==3 && pinon<=3){
					resultado = false;
				}
			}
			else if(letra == 'P'){
				if(estrella==1 && pinon+n>=5){
					resultado = false;
				}
				else if(estrella==2 && (pinon+n==1 || pinon+n==7)){
					resultado = false;
				}
				else if(estrella==3 && pinon+n<=3){
					resultado = false;
				}
			}
			
			return resultado;
		}
		
		void MoverGeneral(char letra, char movimiento){
			if(letra == 'E'){			
				if(movimiento == 'S'){
					if(Seguro('E',1)){
						MoverEstrella(1);
					}	
				}
				else if(movimiento == 'B'){
					if(Seguro('E',-1)){
						MoverEstrella(-1);
					}
				}
			}
			
			else if(letra == 'P'){
				if(movimiento == 'S'){
					if(Seguro('P',1)){
						MoverPinon(1);
					}		
				}
				else if(movimiento == 'B'){
					if(Seguro('P',-1)){
						MoverPinon(-1);
					}
				}
				else if(movimiento == 'T'){
					if(Seguro('P',2)){
						MoverPinon(2);
					}
				}
				else if(movimiento == 'C'){
					if(Seguro('P',-2)){
						MoverPinon(-2);
					}
				}
			}
		}
		
		//Leer lista
		void Cambios(vector<char> elementos){			
			for (int i=0, j=1; i<elementos.size(); i+=2, j+=2){
				MoverGeneral(elementos[i], elementos[j]);
			}
		}
		
		//Get
		vector<int> GetPosiciones(){
			vector<int> resultado(2);
			
			resultado[0] = estrella;
			resultado[1] = pinon;
			
			return resultado;
		}	
};

void MostrarVectorChar(vector<char> elementos, string nombre){
	cout << "\nEl vector " << nombre << " es (" << elementos[0];
	
	for (int i=1; i<elementos.size()-1; i++){
		cout << ", " << elementos[i] ;
	}
	
	if(elementos.size() > 1){
		cout << ", " << elementos[elementos.size()-1] << ")";
	}
	else{
		cout << ")";
	}
}

void MostrarVectorInt(vector<int> elementos, string nombre){
	cout << "\nEl vector " << nombre << " es (" << elementos[0];
	
	for (int i=1; i<elementos.size()-1; i++){
		cout << ", " << elementos[i] ;
	}
	
	if(elementos.size() > 1){
		cout << ", " << elementos[elementos.size()-1] << ")";
	}
}

void PedirDatos(vector<char>& elementos){
	char respuesta;
	
	cout << "Introduce la secuencia de movimientos: ";
	cin >> respuesta;
	while(respuesta!='#' && (respuesta == 'E' || respuesta == 'P' || respuesta == 'S' || respuesta == 'B' || respuesta == 'T' || respuesta == 'C')){
		elementos.push_back(respuesta);
		cin >> respuesta;
	}
}

int main(){
	vector<char> movimientos;
	Bicicleta bici;
	vector<int> resultado;
	
	//Datos
	PedirDatos(movimientos);
	MostrarVectorChar(movimientos, "movimientos");
	
	//Calculo
	bici.Cambios(movimientos);
	resultado = bici.GetPosiciones();
	
	//Salida
	MostrarVectorInt(resultado, "de las posiciones de estrella y pinion");
}
