#include <iostream>
#include <vector>
using namespace std;

class Examen{
	private:
		string asignatura;
		vector<string> enunciados;
		vector<char> respuestas;
		
	public:
		//Constructor
		Examen(string nombre){
			asignatura = nombre;			
		}
		
		//Datos
		void NuevaPregunta(string enunciado, char respuesta){
			enunciados.push_back(enunciado);
			respuestas.push_back(respuesta);
		}
		
		int NumPreguntas(){
			int numero = enunciados.size();
			return numero;
		}
		
		//Get
		string GetEnunciado(int numero){
			string enunciado = enunciados[numero];
			return enunciado;
		}
		
		char GetRespuesta(int numero){
			char respuesta = respuestas[numero];
			return respuesta;
		}
		
		//Mostrar
		void Mostrar(){
			cout << "El examen resuelto de " << asignatura << " es: ";
			for (int i=0; i<enunciados.size(); i++){
				cout << "\n" << i+1 << "- " << enunciados[i];
				cout << "\n" << respuestas[i];
			}
			
			cout << endl;
		}		
};

Examen CrearExamen(){	
	int numero;
	string asignatura, enunciado;
	char respuesta;
	
	cout << "Introduce el nombre de la asignatura: ";
	cin >> asignatura;
	
	Examen examen(asignatura);
	
	cout << "Introduce el numero de preguntas del examen: ";
	cin >> numero;
	
	for (int i=0; i<numero; i++){
		cout << "Introduce el enunciado de la pregunta " << i+1 << ": ";
		cin >> enunciado;
		cout << "Introduce su respuesta: ";
		cin >> respuesta;
		
		examen.NuevaPregunta(enunciado, respuesta);
	}
	
	cout << endl;
	return examen;
}

float CalificarExamen(Examen examen){
	char respuesta;
	int numeroPreguntas = examen.NumPreguntas();
	float calificacion = 0;
	
	cout << "Para dejar la pregunta sin contestar introducir #";
	
	for (int i=0; i<numeroPreguntas; i++){
		cout << "\n\nPREGUNTA " << i+1 << ": ";
		cout << "\n" << examen.GetEnunciado(i) << " ";
		cin >> respuesta;
		
		if(respuesta != '#'){
			if(respuesta == examen.GetRespuesta(i)){
				calificacion++;
				cout << "Respuesta correcta.";
			}
			else{
				calificacion--;
				cout << "Respuesta incorrecta. La solucion es " << examen.GetRespuesta(i);
			}
		}			
	}
	
	if(calificacion < 0){
		calificacion = 0;
	}
	else{
		calificacion = (calificacion/numeroPreguntas)*10;
	}
	
	return calificacion;
}

int main(){
	Examen examen1 = CrearExamen();
	float calificacion;
	
	//Datos
	examen1.Mostrar();
	
	//Calculo
	calificacion = CalificarExamen(examen1);
	
	//Salida
	cout << "\n\nLa nota del alumno es: " << calificacion << "/10";
}
