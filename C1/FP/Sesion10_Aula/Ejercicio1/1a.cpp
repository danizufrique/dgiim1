#include <iostream>
using namespace std;

class Recta{
	public:
		double A,B,C;		
};

void Datos(Recta& recta, string nombre){
	cout << "Introduce 3 valores para la recta " << nombre << ": " ;
	cin >> recta.A >> recta.B >> recta.C;
}

int main(){
	Recta recta1, recta2;
	double pendiente1, pendiente2;
	
	Datos(recta1, "1");
	Datos(recta2, "2");
	
	pendiente1 = -recta1.A/recta1.B;
	pendiente2 = -recta2.A/recta2.B;
	
	cout << "\nLa pendiente de la recta 1 es: " << pendiente1;
	cout << "\nLa pendiente de la recta 2 es: " << pendiente2;
	
}
