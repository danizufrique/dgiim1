#include <iostream>
using namespace std;

class Recta{
	private:
		double A,B,C;
		
		bool Validos(int a, int b){
			bool resultado = true;
			
			if(a==0 && b==0){
				resultado = false;
			}
			
			return resultado;
		}
		
	public:
		//Constructor
		Recta(){			
			A=1;
			B=1;
			C=0;
		}	
		
		Recta(int a, int b, int c){
			bool validos = Validos(a, b);
							
			if(validos){
				A=a;
				B=b;
				C=c;				
			}
			
			else{
				cout << "Datos introducidos no validos."
				A=1;
				B=1;
				C=0;			
			}
		}
		
		//Valores
		void SetCoeficientes(double a, double b, double c){
			bool validos;
			
			A=a;
			B=b;
			C=c;
			
			validos = Validos(a, b);
			if(!validos){
				cout << "Argumentos invalidos.";
			}
		}
		
		void MostrarDatos(){
			cout << "Los parametros actuales de la recta son:";
			cout << "\nA = " << A;
			cout << "\nB = " << B;
			cout << "\nC = " << C << "\n";	
		}
		
		void MostrarDatos(string nombre){
			cout << "\nLos parametros actuales de la recta " << nombre << " son:";
			cout << "\nA = " << A;
			cout << "\nB = " << B;
			cout << "\nC = " << C << "\n";	
		}
		
		//C�lculos
		double pendiente(){
			double resultado;			
			resultado = -A/B;
			
			return resultado;
		}
		
		double ObtenerOrdenada(double abscisa){
			double resultado;
			resultado = (-C -abscisa*A)/B;
			
			return resultado;
		}	
			
		double ObtenerAbscisa(double ordenada){
			double resultado;
			resultado = (-C -ordenada*B)/A;
			
			return resultado;
		}				
};

void PedirDatos(Recta& recta, string nombre){
	double A, B, C;
	
	cout << "\nIntroduce 3 valores para la recta " << nombre << ": " ;
	cin >> A >> B >> C;
	
	recta.SetCoeficientes(A,B,C);
}

int main(){
	Recta recta1 = {2, 4, 5}, recta2 = {1, 0, 0};
	double abscisaIntroducida, ordenadaIntroducida;
	double abscisaResultado, ordenadaResultado;
	double pendiente1, pendiente2;
	

	recta1.MostrarDatos("1");	
	recta2.MostrarDatos("2");

	cout << "\nIntroduce un valor de abscisa: ";
	cin >> abscisaIntroducida;
	cout << "\nIntroduce un valor de ordenada: ";
	cin >> ordenadaIntroducida;
	
	ordenadaResultado = recta1.ObtenerOrdenada(abscisaIntroducida);
	abscisaResultado = recta1.ObtenerAbscisa(ordenadaIntroducida);
	pendiente1 = recta1.pendiente();
	pendiente2 = recta2.pendiente();
	
	cout << "\nLa ordenada obtenida con el valor de abscisa " << abscisaIntroducida << " en la recta 1 es: " << ordenadaResultado;
	cout << "\nLa abscisa obtenida con el valor de ordenada " << ordenadaIntroducida << " en la recta 1 es: " << abscisaResultado;
	cout << "\nLa pendiente de la recta 1 es: " << pendiente1;
	cout << "\nLa pendiente de la recta 2 es: " << pendiente2;
}
