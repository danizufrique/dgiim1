#include <iostream>
using namespace std;

class Recta{
	private:
		double A,B,C;
		
	public:
		
		void setA(double valor){
			A = valor;
		}
		void setB(double valor){
			B = valor;
		}
		void setC(double valor){
			C = valor;
		}
		
		double pendiente(){
			double resultado;			
			resultado = -A/B;
			
			return resultado;
		}
		
		double ObtenerOrdenada(double abscisa){
			double resultado;
			resultado = (-C -abscisa*A)/B;
			
			return resultado;
		}		
		double ObtenerAbscisa(double ordenada){
			double resultado;
			resultado = (-C -ordenada*B)/A;
			
			return resultado;
		}
				
};

void Datos(Recta& recta, string nombre){
	double A, B, C;
	
	cout << "Introduce 3 valores para la recta " << nombre << ": " ;
	cin >> A >> B >> C;
	
	recta.setA(A);
	recta.setB(B);
	recta.setC(C);
}

int main(){
	Recta recta1, recta2;
	double abscisaIntroducida, ordenadaIntroducida;
	double abscisaResultado, ordenadaResultado;
	
	Datos(recta1, "1");
		
	cout << "\nIntroduce un valor de abscisa: ";
	cin >> abscisaIntroducida;
	cout << "\nIntroduce un valor de ordenada: ";
	cin >> ordenadaIntroducida;
	
	ordenadaResultado = recta1.ObtenerOrdenada(abscisaIntroducida);
	abscisaResultado = recta1.ObtenerAbscisa(ordenadaIntroducida);
	
	cout << "\nLa ordenada obtenida con el valor de abscisa " << abscisaIntroducida << " es: " << ordenadaResultado;
	cout << "\nLa abscisa obtenida con el valor de ordenada " << ordenadaIntroducida << " es: " << abscisaResultado;
	
}
