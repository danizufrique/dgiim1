#include <iostream>
using namespace std;

class Recta{
	private:
		double A,B,C;
		
	public:
		//Constructor
		Recta(){			
			A=1;
			B=1;
			C=0;
		}	
		
		Recta(int a, int b, int c){
			A=a;
			B=b;
			C=c;				
		}	
		
		//Valores
		/*void SetCoeficientes(double a, double b, double c){
			A=a;
			B=b;
			C=c;
		}*/	
		void MostrarDatos(){
			cout << "Los parametros actuales de la recta son:";
			cout << "\nA = " << A;
			cout << "\nB = " << B;
			cout << "\nC = " << C << "\n";	
		}
		
		//C�lculos
		double pendiente(){
			double resultado;			
			resultado = -A/B;
			
			return resultado;
		}
		
		double ObtenerOrdenada(double abscisa){
			double resultado;
			resultado = (-C -abscisa*A)/B;
			
			return resultado;
		}		
		double ObtenerAbscisa(double ordenada){
			double resultado;
			resultado = (-C -ordenada*B)/A;
			
			return resultado;
		}				
};

/*void PedirDatos(Recta& recta, string nombre){
	double A, B, C;
	
	cout << "\nIntroduce 3 valores para la recta " << nombre << ": " ;
	cin >> A >> B >> C;
	
	recta.SetCoeficientes(A,B,C);
}*/

int main(){
	Recta recta1 = {2, 4, 5}, recta2 = {1, 1, 0};
	double abscisaIntroducida, ordenadaIntroducida;
	double abscisaResultado, ordenadaResultado;
			
	recta1.MostrarDatos();
	
	cout << "\nIntroduce un valor de abscisa: ";
	cin >> abscisaIntroducida;
	cout << "\nIntroduce un valor de ordenada: ";
	cin >> ordenadaIntroducida;
	
	ordenadaResultado = recta1.ObtenerOrdenada(abscisaIntroducida);
	abscisaResultado = recta1.ObtenerAbscisa(ordenadaIntroducida);
	
	cout << "\nLa ordenada obtenida con el valor de abscisa " << abscisaIntroducida << " es: " << ordenadaResultado;
	cout << "\nLa abscisa obtenida con el valor de ordenada " << ordenadaIntroducida << " es: " << abscisaResultado;
}
