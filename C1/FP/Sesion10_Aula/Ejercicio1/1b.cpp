#include <iostream>
using namespace std;

class Recta{
	public:
		double A,B,C;

		double pendiente(){
			double resultado;			
			resultado = -A/B;
			
			return resultado;
		}
		
		double ObtenerOrdenada(double abscisa){
			double resultado;
			resultado = (-C -abscisa*A)/B;
			
			return resultado;
		}
		
		double ObtenerAbscisa(double ordenada){
			double resultado;
			resultado = (-C -ordenada*B)/A;
			
			return resultado;
		}
				
};

void Datos(Recta& recta, string nombre){
	cout << "Introduce 3 valores para la recta " << nombre << ": " ;
	cin >> recta.A >> recta.B >> recta.C;
}

int main(){
	Recta recta1;
	double abscisaIntroducida, ordenadaIntroducida;
	double abscisaResultado, ordenadaResultado;
	double pendiente1, pendiente2;
	
	Datos(recta1, "1");
		
	cout << "\nIntroduce un valor de abscisa: ";
	cin >> abscisaIntroducida;
	cout << "\nIntroduce un valor de ordenada: ";
	cin >> ordenadaIntroducida;
	
	ordenadaResultado = recta1.ObtenerOrdenada(abscisaIntroducida);
	abscisaResultado = recta1.ObtenerAbscisa(ordenadaIntroducida);
	pendiente1 = recta1.pendiente();
	
	cout << "\nLa pendiente de la recta 1 es: " << pendiente1;
	cout << "\nLa ordenada obtenida con el valor de abscisa " << abscisaIntroducida << " es: " << ordenadaResultado;
	cout << "\nLa abscisa obtenida con el valor de ordenada " << ordenadaIntroducida << " es: " << abscisaResultado;
	
}
