#include <iostream>
using namespace std;

class DepositoSimulacion{
	private:
		double interes;
		double capital;
	public:
		DepositoSimulacion(){
			capital = 100;
			interes = 2.5;		
		}
		
		DepositoSimulacion(double a, double b){
			if(a>0 && capital>0){
				capital = a;
				interes = b;				
			}
			else{
				cout << "Datos introducidos para el constructor no validos.";
				capital = 100;
				interes = 2.5;
			}
		}
		
		double LeerCapital(){
			return capital;
		}
		
		void MostrarDatos(){
			cout << "\nSu capital es de " << capital << " euros.";
			cout << "\nEl interes es de un " << interes << "%.\n";
		}
		
		void SetCapital(double a){
			if(a>0){
				capital = a;
			}
			else{
				cout << "El capital no puede ser negativo";
			}	
		}

		void SetInteres(double a){
			if(a>0){
				interes = a;
			}
			else{
				cout << "El interes debe ser positivo";
			}	
		}
			
		// Calcula capital al cabo de n a�os
		double CapitalAnios(int n);
		
		// Calcula numero de a�os hasta alcanzar cantidad.
		// e) Esta funcion es mas generica, pero debo pasarle la cantidad, por lo que para llamarla
		// debere de poder leer el valor de capital, esto es, crear un metodo que la lea.
		int AumentarCapital(double cantidad);
};

double DepositoSimulacion::CapitalAnios(int anios){
	double resultado = capital;
	
	for (int i=0; i<anios; i++){
		resultado += resultado *(interes/100);
	}
	
	return resultado;
}

int DepositoSimulacion::AumentarCapital(double cantidad){
	int resultado = 0;
	double acumulado = capital;
	
	while(acumulado < cantidad){
		acumulado += capital*(interes/100);
		resultado += 1;
	}
	
	return resultado;
}

int main(){
	double capital, capital_en_anios;
	int anios, aniosDoblar;
	
	DepositoSimulacion deposito{245, 8.75};
	
	deposito.MostrarDatos();
	capital = deposito.LeerCapital();
	
	cout << "\nIntroduzca el numero de anios para el que calcular el capital: ";
	cin >> anios;
	
	capital_en_anios = deposito.CapitalAnios(anios);
	aniosDoblar = deposito.AumentarCapital(capital*2);
	
	cout << "\nCuando pasen " << anios << " anios, su capital sera de " << capital_en_anios << " euros.";
	cout << "\nPara doblar su capital deberan pasar " << aniosDoblar << " anios.";
}
