#include <iostream>
#include <vector>
using namespace std;

class Frase{
	private:
		vector<char> caracteres;
	
	public:
		//Constructores	

		Frase(){
		}
		
		Frase(vector<char> frase){
			caracteres = frase;
		}
		
		//Devuelve el lugar en la lista de la palabra a buscar
		int Localizar(int numeroPalabra){
			int contador = 0;
			int posicion;
			bool buscando = true;
			bool acabar = false;
			
			if (numeroPalabra <= NumeroPalabras()){			
				for (int i=0; i<caracteres.size() && acabar == false; i++){
					if(buscando){
						if(caracteres[i] != ' '){
							contador++;
							if(contador == numeroPalabra){
								acabar = true;
								posicion = i;
							}
							buscando = false;
						}
					}
					else{
						if(caracteres[i] == ' '){
							buscando = true;
						}
					}
				}
			}
			else{
				cout << "El numero de palabra no es valido.";
				posicion = -1;
			}
			
			return posicion;
		}
		
		//MostrarFrase
		void Mostrar(){
			cout << "\nLa frase es:";
			for (int i=0; i<caracteres.size(); i++){
				cout << caracteres[i];
			}
		}
		
		//Pedir frase
		void PedirFrase(){
			char respuesta;
			
			cout << "Introduce una frase acabando con #: ";
			cin.get(respuesta);
			
			while(respuesta != '#'){
				caracteres.push_back(respuesta);
				cin.get(respuesta);
			}
		}
		
		//MostrarVector
		void MostrarVector(){
			cout << "\nEl vector es: (";
			cout << caracteres[0];
			
			for (int i=1; i<caracteres.size(); i++){
				cout << ", " << caracteres[i];
			}

			cout << ")";
		}
		
		//Calcula numero de palabras
		int NumeroPalabras(){
			int contador = 0;
			bool buscando = true;
			
			for (int i=0; i<caracteres.size(); i++){
				if(buscando){
					if(caracteres[i] >= 'A' && caracteres[i] <= 'z'){
						contador++;
						buscando = false;
					}
				}
				else{
					if(caracteres[i] == ' '){
						buscando = true;
					}
				}
			}
			
			return contador;
		}
		
		
		//Eliminar blancos iniciales
		void EliminarBlancosIniciales(){
			while(caracteres.front() == ' '){
				for (int i=0; i<caracteres.size(); i++){
					caracteres[i] = caracteres[i+1];					
				}
				caracteres.pop_back();
			}
		}
		
		//Eliminar blancos finales
		void EliminarBlancosFinales(){			
			while(caracteres.back() == ' '){
				caracteres.pop_back();
			}
		}
		
		//Borrar palabra k-esima
		void BorrarPalabra(int numeroPalabra){
			int posicion = Localizar(numeroPalabra);

			while(caracteres[posicion] != ' '){
				for (int i=posicion; i<caracteres.size(); i++){
					caracteres[i] = caracteres[i+1];					
				}
				caracteres.pop_back();
			}	
		}
		
		//Mover palabra k-esima al final
		void MoverPalabraFinal(int numeroPalabra){
			int posicion = Localizar(numeroPalabra);
			int contador = posicion;
			
			while(caracteres[contador] != ' '){				
				caracteres.push_back(caracteres[contador]);
				contador++;
			}
			BorrarPalabra(numeroPalabra);
		}
};

int main(){
	vector<char> vector1 = {' ', ' ', ' ', 'p', 'o', 'n', 'm', 'e', ' ', 'u', 'n', ' ', 'd', 'i', 'e', 'z', ' ', ' '};
	Frase frase1(vector1);
	
	//Datos
	frase1.MostrarVector();
	frase1.Mostrar();
		
	//Salida
	cout << "\n\na) La segunda palabra de la frase se encuentra en la posicion " << frase1.Localizar(2);
	cout << "\n\nb) La frase resultante al quitar los espacios iniciales es: ";
	frase1.EliminarBlancosIniciales();
	frase1.MostrarVector();
	cout << "\n\nc) La frase resultante al quitar los espacios finales es: ";
	frase1.EliminarBlancosFinales();
	frase1.MostrarVector();	
	cout << "\n\nd) La frase tiene " << frase1.NumeroPalabras() << " palabras";
	cout << "\n\ne) La frase resultante al eliminar la segunda palabra es: ";
	frase1.BorrarPalabra(2);
	frase1.MostrarVector();
	cout << "\n\nf) La frase resultante al mover la primera palabra al final es: ";
	frase1.MoverPalabraFinal(1);
	frase1.MostrarVector();
}
