#include <iostream>
using namespace std;


int MCD(int a, int b){
	int menor;
	int divisor;
	
	if (b<a){
		menor = b;
	}
	else{
		menor = a;
	}
	
	divisor = menor;
	
	while (a%divisor != 0 || b%divisor != 0){
		divisor--;
	}
	
	return divisor;
}


int main(){
	int mcd;
	int a, b;
	
	cout << "Introduce a y b: "; 
	cin >> a >> b;
	
	mcd = MCD(a,b);
	cout << "\nMCD(" << a << "," << b << ") = " << mcd;
}


