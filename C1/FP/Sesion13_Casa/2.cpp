#include <iostream>
using namespace std;

int MCD(int n1, int n2){
	int resultado;
	
	if (n2 == 0)
		return n1;
	else
		resultado = MCD(n2, n1%n2);
		
	return resultado;
}

int main(){
	int numero1, numero2;
	int resultado;
	
	cout << "Introduce dos  numeros: ";
	cin >> numero1 >> numero2;
	
	resultado = MCD(numero1, numero2);
	
	cout << "El MCD de " << numero1 << " y " << numero2 << " es " << resultado;
	
}
