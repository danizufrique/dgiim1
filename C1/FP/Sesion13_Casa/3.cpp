#include <iostream>
using namespace std;

const int MAX = 100;

void DecimalToBinary(int decimal, int binario[MAX], int & longitud){
	int digito;
	int division = decimal/2;
	int resto = decimal%2;
	
	if( division == 1 || division == 0){
		longitud += 2;
		
		for(int i=longitud+1; i>1; i--)			
			binario[i] = binario[i-2];
			
		binario[0] = division;
		binario[1] = resto;
	}
	
	else{
		longitud++;
		
		for(int i=longitud; i>0; i--)			
			binario[i] = binario[i-1];
				
		binario[0] = resto;		
		DecimalToBinary(division, binario, longitud);
	}		
}

int main(){
	int decimal;
	int binario[MAX];
	int longitud = 0;
	
	cout << "Introduce un numero: ";
	cin >> decimal;
	
	DecimalToBinary(decimal, binario, longitud);
	
	cout << "El numero en binario es: ";
	
	for (int i=0; i<longitud; i++)
		cout << binario[i] << " ";
	
}
