#include <iostream>
using namespace std;

int sumarDigitos(int n){
	int resultado;
	
	if(n==1)
		return 1;
	else
		resultado = n + sumarDigitos(n-1);
	
	return resultado;
}

int main(){
	int numero, resultado;
	
	cout << "Introduce un  numero: ";
	cin >> numero;
	
	resultado = sumarDigitos(numero);
	
	cout << "La suma de los digitos de " << numero << " es " << resultado;
	
}
