#include <iostream>
using namespace std;


bool Primo(int n){
	bool resultado = true;
	
	for(int i=2; i<n && resultado == true; i++)
		if(n%i == 0)
			resultado = false;
			
	return resultado;	
}

// Calcula cuantos divisores propios positivos tiene n que sean estrictamente menores que k.
int DivisoresMenores(int n, int k){
	
	bool es_primo = Primo(n); 
	int divisores = 0;
	
	k--;
	 
	if(es_primo){
		divisores = 2;
		cout << "\nEl numero es primo y sus unicos divisores son el mismo y el 1. ";
	}
	else if(n%k == 0){
		divisores++;	
		cout << "\nDIVISOR: " << k;	
	}
	
	if(k>1 && !es_primo){
		divisores += DivisoresMenores(n,k);
	}
	
	return divisores;
}

int main(){
	int n, k;
	int divisores;
		
	cout << "Introduce 2 numeros: ";
	cin >> n >> k;
	
	divisores = DivisoresMenores(n, k);
	
	cout << "\nHay " << divisores << " divisores propios de " << n << " menores que " << k;	
}
