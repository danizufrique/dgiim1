#include <iostream>
using namespace std;

double f(int x){
	double resultado;
	resultado = 2*x + 3;
	
	return resultado;
}

bool SignoDiferente(double v1, double v2){
	bool resultado = false;
	
	if(v1<0 && v2>0)
		resultado = true;
	
	else if(v1>0 && v2<0)
		resultado = true;	
		
	return resultado;
}

bool MenorAbsoluto(double v1, double epsilon){	
	bool resultado = false;
	
	if(v1 < 0)
		v1 *= -1;
		
	if(v1 < epsilon)
		resultado = true;
		
	return resultado;	
}

double Raiz(double izquierda, double derecha, double epsilon){
	double m = (izquierda+derecha)/2;
	double suma = izquierda - derecha;
	bool menorEpsilon = MenorAbsoluto(suma, epsilon);
	
	cout << "hola";
	if(menorEpsilon)
		return suma;
		
	else if(!SignoDiferente(f(m), f(izquierda)))
		Raiz(m, derecha, epsilon);
		
	else if(!SignoDiferente(f(m), f(derecha)))
		Raiz(izquierda, m, epsilon);	
}

int main(){
	double izquierda, derecha;
	const double epsilon = 0.00005;
	
	bool valido;
	double raiz;
	
	//Entrada
	cout << "Intrduce los valores del intervalo: ";
	cin >> izquierda >> derecha;
	
	//Calculo
	valido = SignoDiferente(f(izquierda), f(derecha));
	if(valido)
		raiz = Raiz(izquierda, derecha, epsilon);
	else
		cout << "Extremos introducidos no validos.";
		
	//Salida
	cout << "La raiz del polinomio es " << raiz;
		
	
		
	
}
