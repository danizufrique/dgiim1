#include <iostream>
#include <cstring>
using namespace std;

//Busca un caracter en una cadena de caracteres
bool BuscarCaracter(char cadena[1000], char caracter){
	//Recorre la cadena hasta que encuentra el caracter
	bool encontrada = false;
	int longitud;
	
	longitud = strlen(cadena);
	
	for (int i=0; i<longitud && encontrada == false; i++){
		if(cadena[i] == caracter){
			encontrada = true;
		}
	}
	
	return encontrada;
}
	
int main(){
	char cadena[1000];
	char caracter;
	bool encontrada;
	
	//Entrada
	cout << "Introduzca una cadena de texto: ";
	cin.getline(cadena, 1000);
	cout << "Introduzca el caracter a buscar: ";
	cin >> caracter;
	
	//Cálculo
	encontrada = BuscarCaracter(cadena, caracter);
	
	//Salida
	encontrada ? cout << "Se ha encontrado el caracter." : cout << "No se ha encontrado el caracter";

}
