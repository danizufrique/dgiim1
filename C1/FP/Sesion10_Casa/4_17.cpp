#include <iostream>
using namespace std;

//Separa en pares e impares e imprime el resultado por separado
void Separar(int vector[1000], int longitud, int pares[500], int impares[500]){
	// Recorre el vector y si es divisble entre 2 lo añade al vector de pares
	// En caso contrario añade el elemento al de los impares
	int contadorPares = 0;
	int contadorImpares = 0;
	
	for (int i=0; i<longitud; i++){
		if(vector[i]%2 == 0){
			pares[contadorPares] = vector[i];
			contadorPares++;
		}
		else{
			impares[contadorImpares] = vector[i];
			contadorImpares++;
		}
	}
	
	//Salida
	cout << "\n__VECTOR PARES__\n";
	for (int i=0; i<contadorPares; i++){
		cout << pares[i] << " ";
	}
	cout << "\n__VECTOR IMPARES__\n";
	for (int i=0; i<contadorImpares; i++){
		cout << impares[i] << " ";
	}
}
	
int main(){
	int original[1000], longitud;
	int pares[500];
	int impares[500];
	
	//Entrada
	cout << "Introduce la longitud del vector: ";
	cin >> longitud;
	
	cout << "Introduce el vector separado por espacios: ";
	for (int i=0; i<longitud; i++){
		cin >> original[i];
	}
	
	//Cálculo
	Separar(original, longitud, pares, impares);
	
	
}
